# Changelog

Until there is a dedicated guide how to use this log, please stick to this article: https://keepachangelog.com/de/0.3.0/
Please pick from the following sections when categorizing your entry:
`Added`, `Changed`, `Fixed`, `Removed`

Before every entry put one of these to mark the severity of the change:
`Major`, `Minor` or `Patch`

## [Unreleased](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v3.0.0...master)
## [3.0.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.18.0...v3.0.0) - 23.07.2021
### Added
- [Major] First open source release.
- [Minor] Second barometer output channel for the top mounted sensor.
- [Patch] Added plugin documentation.

### Changed
- [Major] Replaced ZCOM with KITECOM
- [Minor] Updated LICENSE headers.
- [Patch] Updated pulicast submodule.
- [Patch] Updated links to GitLab pages in README.md

### Fixed
- [Patch] Fixed pressure conversion bug in the MS5611 plugin.
- [Patch] Fixed launch.json for debugging ARM and X86.

## [2.18.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.17.0...v2.18.0) - 13.07.2021
### Added
- [Minor] Added distributed setting for INE (Q_diag, R_acc_diag, R_gyro_diag, NumCalibrationSamples).
- [Minor] Added distributed setting for ACL (pt2_q_c_diag, pt2_r, q_c_diag, r_ine_diag, max_motor_speed, min_motor_speed).
- [Minor] Added AS5048 driver.
- [Minor] Added tether force sensor plugin.
- [Minor] Added encoder to stepper motor plugin.

### Changed
- [Minor] UWB plugin now publishes measurement data structures instead of ranges.
- [Minor] Telocate UWB driver now internally handles NANs.

### Fixed
- [Patch] Fixed bug causing segfault in INE if command is received without being initialized.
- [Patch] Fixed bug in gnss resulting in wrong vertical velocity..

## [2.17.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.16.0...v2.17.0) - 01.07.2021
### Added
- [Minor] Added temperature measurements to ICM20948 driver.
- [Minor] Added functional driver for the MS5611 barometer.

### Changed
- [Minor] Moved BoardMonitor to core.
- [Minor] Made in application programmer and persistent memory manager non-static.
- [Minor] Switched to the MS5611 as barometer measurement source.
- [Patch] Moved control sources to middleware.

### Removed
- [Minor] Removed fixed size string and replaced with std::string.

## [2.16.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.15.0...v2.16.0) - 14.06.2021
### Added
- [Minor] Added node address puliconf value to UWB plugin.
- [Minor] Made node address of Telocate UWB driver configurable.
- [Minor] Telocate UWB driver is now reporting errors as log messages.
- [Patch] Fixed wrong byte order of range in Telocate UWB driver and fixed float cast.

### Fixed
- [Patch] Rotate calibrated gyro bias from NWU to NED when setting it as initial state
- [Patch] use int64_t instead of int for acl to ine time difference

## [2.15.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.14.1...v2.15.0) - 10.06.2021

### Added
- [Minor] Added new state with gyro bias to INE.
- [Patch] Added distributed setting for enabling/disabling GNSS raw data.

### Changed
- [Minor] PTP sync message generation interval is no longer configurable and fixed to 1Hz now.
- [Minor] Changed tuning reference frame of INE sensor in ACL to drone frame.
- [Minor] Added temperature readout to MPL/Barometer.
- [Minor] Added TMC2160 drver.
- [Minor] Added stepper motor plugin.
- [Minor] PWM now uses prescaler to support duty cycles from 1 microsecond to more than a minute.
- [Minor] Removed plugin definitions. Channel names are now defined in the plugins.
- [Patch] Updated x86 spektrum driver documentation.
- [Patch] Updated pulicast submodule.
- [Patch] Updated zcom submodule.
- [Patch] Added distributed setting for UWB module configuration.
- [Patch] Added PB1 to PWM hal.

### Removed
- [Minor] Changed zcom message type of INE estimate.
- [Patch] Removed .vscode/settings.json file (added to gitignore).
- [Minor] Changed zcom message type of INE estimate.

## [2.14.1](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.14.0...v2.14.1) - 27.05.2021
### Changed
- [Patch] Ine plugin publishes acceleration in "IMU" namespace.

## [2.14.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.13.1...v2.14.0) - 27.05.2021
### Added
- [Minor] Added general documentation of KiteOS main.
- [Minor] Added bounds and clipping to the x86 a4964 driver.
- [Minor] Added Adc plugin and max11040k driver class.
- [Minor] Added Hygrometer plugin and sht25 driver class.

### Changed
- [Minor] Added sensor filter to INE.
- [Minor] Added MS5611 driver.
- [Minor] Uint32 values are now correctly sent as timestamped_vector_uint messages.
- [Patch] Removed static pulicast node object. Provide stack bound pulicast node as argument instead.

## [2.13.1](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.13.0...v2.13.1) - 10.05.2021
### Changed
- [Minor] Moved BoardMonitor to core.
- [Patch] Externalized buzzer logic from PowerMonitor to a driver class.

### Added
- [Patch] Added fake x86 drivers for ina3221, buzzer, battery_voltage_sensor, telocate_uwb.

### Fixed
- [Patch] Removed velocity availability check in x86 gnss driver.

## [2.13.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.12.0...v2.13.0) - 10.05.2021
### Added
- [Minor] Added reset command to in application programmer.
- [Minor] Added TrueVelocity subscription to X86 neo_m8t driver.
- [Patch] Fixed printing of floats/doubles using snprintf.

### Changed
- [Minor] Moved Quaternion to submodule.
- [Minor] StateNodes constructor uses std::function as parameter now.

### Fixed
- [Patch] Removed zero-order-hold in mpl3115A2 driver.
- [Patch] Fixed StateNode origins when no `kite_name` is specified.
- [Patch] Fixed AttitudeEstimate channel label

### Removed
- [Patch] Removed unused PoseReference channel label

## [2.12.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.11.1...v2.12.0) - 29.04.2021
### Added
- [Minor] Added KSZ8567R and KSZ8563R drivers.
- [Minor] Added switch hardware key to persistent memory.
- [Minor] Ethernet services are now instantiating the switch drivers.
- [Minor] Added in application programmer protocol documentation.
- [Minor] Added X86 drivers for a4964.
- [Minor] Added X86 drivers for mpl3115a2.
- [Minor] Added X86 drivers for ublox-m8.
- [Minor] Added callbacks using template context type to software timer.

### Fixed
- [Patch] Fixed order of CI stages and set package upload to only be triggered for tags via web.
- [Patch] Fixed bug in PPS leading to wrong corrections in PPS GNSS system time synchronization modes.
- [Patch] Add Links to doxygen docs and coverage report to the spinx docs.
- [Patch] Upload package for protected tag pipelines only
- [Patch] Fixed quaternion toRotationMatrix function.
- [Patch] Fixed pages job with correct path.

### Changed
- [Minor] Changed Attitude Reference Generator to use own state node
- [Minor] Attitude Reference Generator can be reset by command
- [Minor] Changed kStayInBootloader and kFirmwareBroken to represent internal keys
- [Minor] Changed ACL to INE synchronization mechanism
- [Minor] Software timer template context type callbacks are now used by most drivers and plugins.

## [2.11.1](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.11.0...v2.11.1) - 14.04.2021
### Added
- [Patch] Added sphinx docs.

### Fixed
- [Patch] Fixed q_ps parameter in KS0 ULW 003 model parameter set.

## [2.11.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.10.0...v2.11.0) - 14.04.2021
### Added
- [Minor] Added TIM-TM2 message to NEO-M8T driver.
- [Minor] Added multi access handler to NEO-M8T.
- [Minor] System time can now be synchronized to GNSS time.
- [Minor] Added publish method for uint32 vectors to Plugin class.
- [Minor] System time mode and PTP syncronization interval can now be configured through persistent memory.

### Changed
- [Minor] Renamed Ublox-M8 driver to NEO-M8T.
- [Minor] Moved all NEO-M8T related code into NEO-M8T subfolder.
- [Minor] NEO-M8T messages are no longer enabled through methods but passed in a configuration structure to the constructor.
- [Minor] NEO-M8T is now implemented event based instead of polling.
- [Minor] Increased PPS_OUT pulse length to 1ms.
- [Patch] Reduced number of possible state nodes to 5.
- [Patch] Changed most `std::string` parameters to `const std::string&`.
- [Patch] Clock accuracy is now published as double in seconds.

### Fixed
- [Patch] Increased the number of TX descriptors to prevent message loss.
- [Patch] Fixed quaternion displacement function in INE correction step.

## [2.10.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.9.0...v2.10.0) - 08.04.2020
### Added
- [Minor] Added state node to PowerMonitor.
- [Minor] Added gamepad support for spektrum driver for x86.

### Changed
- [Minor] Now using pulicast namespaces.
- [Patch] Driver for mpl3115a2 is now eventbased and doesn't flood scheduler.
- [Patch] Updated MR template to new version.
- [Patch] KiteOS (x86) first searches memory-definition.json in VIRTUAL_ENV.
- [Patch] Change dependency to ks_models>=7.0.0.

### Fixed
- [Patch] Fixed initial system time clock frequency is to slow.
- [Patch] Fixed MPL3115A2 bug causing it to stop working.
- [Patch] Removed immediate announcement of new created channels to prevent occasional hardfaults.
- [Patch] Fixed ACL to INE sync message.
- [Patch] Added ks-models version dependency.
- [Patch] Adapted INE to new sensor model functions and INE frame conventions
- [Patch] Updated system tests to use newest version of puliping.
- [Patch] Fixed bug causing PRIu8 to print `hu` instead of a number.
- [Patch] INE EKF: Fixed wrapped acc sensor function using invalid gravity reference.
- [Patch] Adapted ACL to new sensor model functions and INE frame conventions
- [Patch] Fixed yaw reference integration in X86
- [Patch] Fixed attitude LQR quaternion error computation

## [2.9.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.8.0...v2.9.0) - 22.03.2021
### Added
- [Minor] Added Mpl3115a2 driver
- [Minor] Added barometer plugin
### Changed
- [Minor] In application programmer now uses FRAM for boot flag.
- [Minor] Boot flag flash page is now part of the firmware memory.
- [Minor] Added address fields to IAP status messages.
- [Minor] Device now stays in bootloader after an unsuccessful ethernet flashing process.
- [Minor] Telocate UWB driver now resets the measurement results after reading them.

## [2.8.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.7.0...v2.8.0) - 18.03.2021
### Added
- [Minor] Added Telocate UWB driver.
- [Minor] Added UWB plugin.
- [Minor] BoardMonitor publishes stack detection information.
- [Minor] Added new model parameter set: KS0 ULW 003.

### Changed
- [Minor] Definitions commonly used by SPI device and slave are nod defined in spi/definitions.hpp.
- [Minor] Definitions commonly used by I2C device and slave are nod defined in i2c/definitions.hpp.
- [Minor] In application programmer now uses FRAM for boot flag.
- [Minor] Boot flag flash page is now part of the firmware memory.
- [Minor] Added address fields to IAP status messages.
- [Minor] Device now stays in bootloader after an unsuccessful ethernet flashing process.
- [Patch] Updated pulicast to v1.1.4
- [Patch] Added 5kHz ping system test.
- [Patch] 10kHz Ping system test now allows up to 1 lost pongs.
- [Patch] Interrupts are now turned of during blocking flash access operations.
- [Patch] Increased IAP read and write block size to 128 to speed up the ethernet flashing process.
- [Minor] Failsafe pilot reference is a middleware component now.
- [Minor] Added multi access handler for Spektrum driver to enable multiple access to a single Spektrum driver instance.
- [Patch] Updated pulicast to v1.1.4
- [Patch] Added 5kHz ping system test.
- [Patch] 10kHz Ping system test now allows up to 1 lost pongs.
- [Patch] Replaced Magic Numbers in the Attitude Reference Generator

### Fixed
- [Patch] Fixed hal::spi bug causing occasionally false callback invokes.
- [Patch] Fixed bug causing persistent memory values not being written to the FRAM.
- [Patch] Fixed a bug in ICM leading to errors on high processor load.
- [Patch] Fixed bug in ICM jamming the scheduler on high processor load.
- [Patch] Fixed bug in flash HAL leading to write failures when using data block sizes other than 8.
- [Patch] Fixed bug in a4964 driver causing artifacts in speed measurements.
- [Patch] Fixed bug in a4964 driver causing integrator to be resetted falsely.

## [2.7.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.6.0...v2.7.0) - 15.03.2021
### Added
- [Minor] Added fixed size string class to utils.

### Changed
- [Minor] Persistent memory is now using fixed size strings instead of fsting.
- [Minor] Name provider is now using fixed size strings instead of std::sting.
- [Patch] Removed Magic Number from attitude LQR and hardcoded the max and min motor speed values in the config receiver
- [Patch] Use state difference function in Attitude LQR to calculate the error.

### Fixed
- [Patch] Fixed bug in INE sync channel name.
- [Patch] Added kite_prefix to simulator subscriptions.
- [Patch] NameProvider: Fixed out-of-range access in pcb_type.

### Removed
- [Minor] Removed fstring class.

## [2.6.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.5.0...v2.6.0) - 11.03.2021
### Added
- [Minor] Added Attitude Control Loop.
- [Minor] Added elliptic altitude and pos/vel accuracy publishing to gnss plugin.
- [Patch] Added log message printed when no magic word was found in the FRAM.
- [Patch] Added assert to igmp_joingroup in PTP.
### Changed
- [Minor] Logger is now delaying messages sent on startup to prevent them from getting lost due to missing Ethernet link.
- [Minor] Added interrupts hal.
- [Minor] Software semaphore is accessing interrupts now through interrupts hal.
- [Patch] Moved INE to own directory.
- [Patch] Generate MAC address from unique ID.
- [Patch] Spektrum driver is event driven instead of polled.
- [Patch] When using DEBUG debug optimization is used (`-Og` instead of `-O0`).
### Fixed
- [Patch] Fixed state machine component name
- [Patch] Reworked local/remote channel creation.
- [Patch] Fix bug in .json enum handling always disabling the first value.
- [Patch] Added persistent-memory configuration section to README.md.
- [Patch] Changed StateNode destination and origin datatype to from std::string_view to const std::string
- [Patch] Augmented number of possible IGMP group joins from 8 to 16 (number of subscriptions).
- [Patch] Fixed bug causing model parameters not to be loaded properly.
- [Patch] X86: spektrum driver supplies dummy data.
- [Patch] X86: correctly initialize logger service.
- [Patch] X86: Fixed bug in fram_emulator .json parser not properly interpreting bool entries.
- [Patch] Exit KiteOSX86 if HOME environment var is not defined.
- [Patch] Fixed bug in publishing Attitude Estimate
- [Patch] Fixed Bug in Attitude Control Loop State Machine. Controls were not forwarded to the estimator
- [Patch] Fixed wrong channel name prefix behavior.
- [Patch] Fixed Bug in Attitude Reference Generator. Store RC channel values for later use.

## [2.5.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.4.0...v2.5.0) - 23.02.2021
### Added
- [Minor] HAL ADC is now able to measure core temperature.
- [Minor] Added INE EKF control component.
- [Minor] Ublox-m8 driver outputs raw messages.
- [Minor] KiteOS-x86 is now able to parse `uint8` and `enum` config types.
- [Minor] Added self test to ICM20948 driver.
- [Minor] Added SPI error handling to MB85RS4T driver.
- [Minor] Added Ina226 shunt voltage driver to motor plugin.
- [Minor] Added power monitor plugin.
- [Minor] Added battery voltage sensor driver.
- [Minor] Added buzzer on low battery sound.
- [Minor] Added Ak09916 driver and make use in imu plugin.
- [Patch] Moved failsafe functionality into a separate driver.
### Changed
- [Minor] NameProvider can now provide hardware info.
- [Minor] Adapted project to use the STM32H753xx chip.
- [Minor] Successful execution of scheduler enqueue is now asserted inside the method instead of returning it.
- [Minor] ICM20948 driver is now using the scheduler.
- [Minor] PPS can now be switched on by define now and is switched off by default.
- [Minor] Switched unit testing framework from cpputest to googletest.
- [Minor] Reworked max+ina to battery_voltage_sensor driver.
- [Minor] Disabled watchdog for Axons.
- [Minor] Updated A4964 StateNode reporting.
- [Minor] Replaced GPS naming occurrences with more proper GNSS naming.
- [Minor] Moved persistent memory manager to core.
- [Minor] Added persistent memory definitions for all data types.
- [Minor] Persistent memory manager now uses unique hardware identifier to address a board.
- [Minor] In application programmer now uses unique hardware identifier to address a board.
- [Patch] Changed CI pipeline to run jobs even if changelog tests fails.
- [Patch] InApplicationProgrammer is now a service instead of a plugin.
- [Patch] Updated CI build images to use Ubuntu 20.04.
- [Patch] FRAM is now activated per default.
- [Patch] Systick event tasks are no longer enqueued in the scheduler if there is already a systick event task enqueued.
- [Patch] hal::i2c write-read transfers now use a repeated start condition.
- [Patch] IP address is requested via autoIP instead of using hardcoded one.
- [Patch] Using puliping instead of pingbench to perform ping system tests.
### Fixed
- [Patch] Fixed boost::program_options optional string.
- [Patch] Fixed potential bug in ringbuffer (ReadElement returned uninitialized element when buffer was empty).
- [Patch] Temporarily using `--http1.1` to upload via curl.
- [Patch] Fixed boost::program_options optional string.
- [Patch] Adjusted path of installed memory-definition.json.
- [Patch] Fixed bug causing software timer callback being executed before timer is started.
- [Patch] Added thread thread safe buffer hotfix to handle more parallel processes.
- [Patch] Fixed bug in hal::Pwm caused by overflows in capture compare register.
- [Patch] Changed pwm period resolution from microseconds to centiseconds.
- [Patch] Fixed wrong interrupt priority group configuration, removed thread safe buffer hotfix.
- [Patch] Fixed initial FRAM configuration not working on release builds.
- [Patch] Fixed UBX package loss by disabling UART port.
- [Patch] Fixed A4964 not reporting speed messages.
- [Patch] Fixed hal::uart causing hardfaults sometimes.
- [Patch] Fixed spektrum driver using wrong UART port.
- [Patch] Fixed wrong hardware timer prescaler causing wrong timing.
- [Patch] Fixed i2c frequency being slightly too high (now 400 kHz).
- [Patch] Added software watchdog to a4964 driver as workaround for missing hardware watchdog.

## [2.4.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.3.0...v2.4.0) - 15.12.2020
### Changed
- [Minor] Using new pulicast frame protocol version 1.0.

## [2.3.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.2.0...v2.3.0) - 14.12.2020
### Added
- [Minor] Added EnqueueTask method to Scheduler with templated callback argument type.
- [Minor] Added mocked persistent memory to KiteOS-x86.
### Changed
- [Minor] Scheduler is now implemented header only and fully inlined to increase performance.
- [Minor] Plugins do not have a run method any more.
- [Minor] Software timer callback and timeout are now set separately.
- [Patch] Resources shared by all state node instances are now only initialized when creating the first instance.
- [Patch] Fixed bug causing persistent memory write errors not being reported.

## [2.2.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.1.3...v2.2.0) - 03.12.2020
### Added
- [Minor] Added focs driver.
- [Minor] Added semaphore HAL.
- [Minor] Added software semaphore.
- [Minor] Added thread safe buffer.
- [Minor] Added scheduler to core.
- [Minor] Added ina3221 driver.
- [Minor] Added ina592 driver.
- [Patch] Added ci/cd job to test successful x86 execution.
- [Patch] Added merge request templates.
- [Patch] BoardMonitor publishes KiteOS version.

### Changed
- [Minor] Logger is now callable from interrupts.
- [Minor] StateNode core service handles state and command messages.
- [Minor] Replaced string in logger API by character array.
- [Patch] Uploaded binary file names now contain version string.
- [Patch] Uploaded binary file names now contain version string.
- [Patch] FRAM magic number is now readout twice to prevent erasing FRAM due to read error.
- [Patch] Building the Documentation and coverage now is a target of the build system.

### Fixed
- [Patch] Fixed mac address configuration in the network interface.
- [Patch] Fixed GPIO interrupts.
- [Patch] Fixed bug leading to missing registry entries in the software timer class.

### Removed
- [Patch] Remove forced copyright header in linter.

## [2.1.3](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.1.2...v2.1.3) - 13.11.2020
### Added
- [Patch] Pipeline automatically adds links to binary to release assets.

## [2.1.2](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.1.1...v2.1.2) - 13.11.2020
### Changed
- [Patch] Updated README.md.
- [Patch] Remove .bin uploader. Using package registry instead.

## [2.1.1](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/v2.1.0...v2.1.1) - 12.11.2020
### Fixed
- [Patch] Fixed broken frequency option for KiteOS (x86).

### Changed
- [Patch] Changed KiteOS (x86) default TTL to 0.

## [2.1.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/2.0.0...v2.1.0) - 11.11.2020
### Added
- [Minor] Added timing service.
- [Minor] Added Error Indicator classes for reporting errors.
- [Minor] Added ublox-m8 driver class.
- [Minor] Added unit test debugging.
- [Minor] Added X86 wrapper files.
- [Minor] Added GPIO write method with boolean argument.
- [Minor] Added PWM functionality to hal::Timer.
- [Minor] Added ICM20948 driver.
- [Minor] Added openocd config file.
- [Minor] Added correction field evaluation to PTP.
- [Minor] Added Spektrum receiver driver and RC receiver plugin.
- [Minor] Added A4964 and Motor plugin.
### Changed
- [Minor] Renamed enums according to google style.
- [Minor] Plugin Instantiation is now done according to FRAM.
- [Minor] Implemented gpio HAL in C++.
- [Minor] Implemented spi HAL in C++.
- [Minor] Implemented i2c HAL in C++.
- [Minor] Implemented systick timer HAL in C++.
- [Minor] Implemented rng HAL in C++.
- [Minor] Implemented flash HAL in C++.
- [Minor] Implemented ADC HAL in C++.
- [Minor] Implemented UART HAL in C++.
- [Minor] Implemented Timer HAL in C++.
- [Minor] Board name is now read from FRAM instead of using hardcoded board name.
- [Minor] Removed build_config.h and moved content to plugins and FRAM.
- [Minor] Ethernet HAL is now implemented in C++ instead of C.
- [Minor] Moved services content into core and removed services.
- [Minor] Replaced LAN8742 C implementation by a C++ version and cleaned up its interface to Ethernet HAL.
- [Minor] MDIO clock range setup is now done inside of the ethernet hal instead of the phy init.
- [Minor] Ethernet code is now PHY independent.
- [Minor] Name provider is now using the board name persistent memory value to assemble the board prefix instead of board type and board index.
- [Minor] Removed hal_mcu, augmented hal::Rcc to enable clock tree configuration.
- [Minor] Switched SPI clock source to PLL3.
- [Minor] Removed GPIO mode definition for chip select pin out of spi slave constructor interface.
- [Minor] Replaced MPU9250 plugin by IMU plugin using the ICM20948 driver.
- [Minor] Moved MB85RS64T driver init code to constructor and deleted init method.
- [Minor] Moved ethernetif code to sys_now, ethernet_services and pbuf_free_custom and removed ethernetif.
- [Minor] Separated software timer from systick timer. Software timer callbacks are now called in synchronous context.
- [Patch] Updated pulicast transport API.
- [Patch] Added asio submodule.
- [Patch] Added checking of max cycles to performance test (now without trio).
- [Patch] Gitlab CI now uses installed kiteos-flashtool.
- [Patch] Replaced git_watcher.cmake by original version with additions.
- [Patch] Pulicast node is now std::optional to enable post boot construction via emplace.
- [Patch] hal::Gpio initialization is now done in constructor instead of Init().
- [Patch] hal::Gpio cleaner non redundant mode configuration.
- [Patch] hal::Gpio has a registry to discover double pin usage.
- [Patch] Removed static Gpio object from hal::Adc
- [Patch] EthernetServices offers an hardware abstracted interface.
- [Patch] Rename source files according to google style.
- [Patch] Gitlab-ci now uses docker to build.

### Fixed
- [Patch] Pulicast is actually using a proper true random session id now.
- [Patch] Fixed wrong enum names in FRAM access causing build errors when using FRAM.
- [Patch] Fixed PPS LED blinking with 2Hz sometimes.
- [Patch] Fixed bug in PPS using old timer causing build error.
- [Patch] Fixed wrong core voltage configuration leading to hard fault when processor clock is too high.
- [Patch] Added zcom subdir to KiteOSTest CMAKE project.
- [Patch] Update flash job to use latest version of kiteos-flashtool.

### Removed
- [Minor] Removed stm32h7xx_hal_driver.
- [Patch] Removed Plugin::Init() (moved to constructor).

## [2.0.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/1.0.0...2.0.0) - 27.05.2020
### Added
- [Minor] Added Services.
- [Minor] Added FramAccess class.
- [Minor] Added persistent memory manager plugin.
- [Minor] Added upload of tagged release binaries to database.
- [Minor] Added enforced documentation coverage.
- [Minor] Added option to mock the FRAM to FramAccess.
- [Minor] Added random number generation support.

### Changed
- [Major] Replace communication framefork ZCM by pulicast.
- [Minor] hal_spi is now handling chip select.
- [Minor] System test framework changed to pytest.
- [Minor] Instantiation of plugins is dynamic instead of static.
- [Minor] Moved PTP from plugin to service.
- [Minor] Moved hal IEEE1588 content into hal Ethernet.
- [Patch] Consistent memory mapping rooted in CMakeLists.txt.
- [Patch] CI/CD: Ethernet flashing is preceeded by JTAG flashing.
- [Patch] Using zcom as interface library.
- [Patch] Top level CMakeLists.txt containing common build definitions.
- [Patch] Updated zcom submodule URL.
- [Patch] Added compiler path distinction for KiteOSMain on Windows.
- [Patch] Moved mocked sources to unit test directory.

### Fixed
- [Patch] Fixed MB85RS64T driver not handling chip select.
- [Patch] Fixed docu coverage calculation.
- [Patch] Fixed FramAccess reading garbage.
- [Patch] Fixed FRAM not being updated when multiple actions happen at once.
- [Patch] Fixed faulty zcom binding generation.
- [Patch] Updated zcom submodule for cmake dependency fix.
- [Patch] Fixed broken links in README.md.
- [Patch] Updated kiteos-flashtool URL.
- [Patch] Fixed faulty zcom binding generation.
- [Patch] Updated zcom submodule for cmake dependency fix.
- [Patch] Fixed broken links in README.md.
- [Patch] BoardMonitor reports errors in communication if present.
- [Patch] Updated kiteos-flashtool URL.

### Removed
- [Major] Removed legacy property handling (PropertyManager).
- [Minor] Removed placeholder targets. Instead using single firmware target.

## [1.0.0](https://code.kiteswarms.com/EmbeddedSoftware/KiteOS/compare/0.0.0...1.0.0) - 27.04.2020
### Added:
- HAL:
 - Software Timer
 - Hardware Timer
 - GPIO
 - I²C
 - SPI
 - UART
 - Ethernet
 - IEEE1588
 - Bootswitch
 - Flash
 - ADC

- In-Application-Programming via Ethernet
- Timing based on IEE1588
- Optional: timing based on PPS
- ZCM as communication framework
- Basic Setup of Unit Testing Framework
