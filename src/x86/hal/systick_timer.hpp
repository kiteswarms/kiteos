// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_X86_HAL_SYSTICK_TIMER_HPP_
#define SRC_X86_HAL_SYSTICK_TIMER_HPP_

#include <cstdint>
#include <functional>
#include <chrono>
#include <asio/basic_waitable_timer.hpp>

//*****************************************************************************
//
//! \addtogroup hal HAL API
//! @{
//!
//! \addtogroup hal_systick_timer_api HAL Sysick Timer API
//! @{
//
//*****************************************************************************

namespace hal {
/*!
*  \brief   Implements the HAL Systick Timer Device class for x86 builds.
 *
*  \details This class is an x86 implementation of the HAL Systick timer class providing the same
 *          API.
*/
class SystickTimer {
 public:
  //! Typedef of function pointer used as callback when timer elapses
  typedef void (*CallbackFunction)(void *cb_arg);

  /*!
   * \brief   Initializes the systick timer class.
   *
   * \return  true if successful, false otherwise.
   */
  static bool Init();

  /*!
   * \brief Gets current tick count.
   *
   * \return Current counter of ticks.
   */
  static uint64_t GetCount();

  /*!
   * \brief Sets a timeout to be called every sys tick.
   *
   * \param callback           Function to be called every sys tick.
   * \param callback_argument  Argument that is provided in callback function.
   */
  static void RegisterSystickCallback(CallbackFunction callback, void *callback_argument);

 private:
  static uint64_t tick_count_;                //!< current count of ticks.
  static CallbackFunction systick_callback_;  //!< callback to be invoked on sys tick events.
  static void* systick_callback_argument_;    //!< argument provided to callback.

  //! Timer used to generate sys ticks.
  static asio::basic_waitable_timer<std::chrono::system_clock> *asio_timer_;
  //! Lambda function called on sys tick events. This function effectively emulates the sys tick
  //! handler.
  static std::function<void(const asio::error_code &)> systick_lambda;
};
}  // namespace hal
/*! @} */
/*! @} */
#endif  // SRC_X86_HAL_SYSTICK_TIMER_HPP_
