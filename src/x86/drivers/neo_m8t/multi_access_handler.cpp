// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "multi_access_handler.hpp"
#include "core/puli_driver_interface.hpp"

namespace neo_m8t {

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
MultiAccessHandler::MultiAccessHandler():
true_geographic_position_channel_(
    (*PuliDriverInterface::GetNamespace() / "GNSS")["TrueGeographicPosition"]),
true_altitude_channel_((*PuliDriverInterface::GetNamespace() / "GNSS")["TrueAltitude"]),
true_velocity_channel_((*PuliDriverInterface::GetNamespace() / "GNSS")["TrueVelocity"]) {
  true_geographic_position_channel_.Subscribe<kitecom::timestamped_vector_double>(
    [this](const kitecom::timestamped_vector_double& message) {
      this->MsgHandlerPosition(message);
    });
  true_altitude_channel_.Subscribe<kitecom::timestamped_vector_double>(
    [this](const kitecom::timestamped_vector_double& message) {
      this->MsgHandlerAltitude(message);
    });
  true_velocity_channel_.Subscribe<kitecom::timestamped_vector_double>(
    [this](const kitecom::timestamped_vector_double& message) {
      this->MsgHandlerVelocity(message);
    });
  update_timer_.SetTimeoutPeriodic(100,
    [](void* ctx){
      auto inst = reinterpret_cast<MultiAccessHandler*>(ctx);
      inst->Dispatch();
    }, this);
}

void MultiAccessHandler::MsgHandlerPosition(const kitecom::timestamped_vector_double& message) {
  geographic_position_available_ = true;
  nav_pvt_.lat = message.values[0] * 1e7;
  nav_pvt_.lon = message.values[1] * 1e7;
}

void MultiAccessHandler::MsgHandlerAltitude(const kitecom::timestamped_vector_double& message) {
  altitude_available_ = true;
  nav_pvt_.h_msl = message.values[0] * 1e3;
}

void MultiAccessHandler::MsgHandlerVelocity(const kitecom::timestamped_vector_double& message) {
  velocity_available_ = true;
  nav_pvt_.vel_n = message.values[0] * 1e3;
  nav_pvt_.vel_e = message.values[0] * 1e3;
  nav_pvt_.vel_d = message.values[0] * 1e3;
}

void MultiAccessHandler::Dispatch() {
  if (geographic_position_available_ && altitude_available_) {
    nav_pvt_.flags = 0x1;
  } else {
    nav_pvt_.flags = 0x0;
  }
  callback_function_(&nav_pvt_);
  geographic_position_available_ = false;
  altitude_available_ = false;
  velocity_available_ = false;
}

}  // namespace neo_m8t
