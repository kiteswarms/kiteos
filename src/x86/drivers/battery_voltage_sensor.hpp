// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_X86_DRIVERS_BATTERY_VOLTAGE_SENSOR_HPP_
#define SRC_X86_DRIVERS_BATTERY_VOLTAGE_SENSOR_HPP_

#include <cstdint>
/*!
 *  \brief  Implements the BatteryVoltageSensor driver.
 *  \details  This class measures the inputs of the "MAX337" Mux differentially
 *            by cycling the through the inputs and measuring the output of the BatteryVoltageSensor
 *            which is connected to the output of the mux.
 *            The Mux uses 3 address bytes to select the input.
 *            The voltages are returned in mV all at once after each measurement in an array.
 *            The amount of channels and the readout frequency are configurable.
 */
template<std::size_t N>
class BatteryVoltageSensor {
 public:
  //!< callback that is called if all channels are finished reading.
  typedef void (*DataReadyCallback)(double voltage_channels_[N], void *instance);

  /*!
   * \brief  Constructor of the BatteryVoltageSensor-Component.
   *
   * \param  max_channel  Number of channels to be read starting from 0.
   * \param  cb  callback that is called if all channels are finished reading.
   * \param  cb_arg  instance pointer.
   *
   * \return Returns constructed BatteryVoltageSensor-instance.
   */
  explicit BatteryVoltageSensor(DataReadyCallback cb, void *cb_arg) {}

   /*!
   * \brief Destructor of the BatteryVoltageSensor-Component.
   */
  ~BatteryVoltageSensor() = default;
};

#endif  // SRC_X86_DRIVERS_BATTERY_VOLTAGE_SENSOR_HPP_
