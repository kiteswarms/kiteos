// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
//
// Created by jan on 28.01.21.
//

#ifndef SRC_X86_DRIVERS_ICM20948_DEVICE_HPP_
#define SRC_X86_DRIVERS_ICM20948_DEVICE_HPP_

#include <cstdint>
#include "pulicast-port/pulicast_embedded.h"
#include "kitecom/timestamped_vector_double.hpp"

namespace icm20948 {
/*!
 *  \brief Implements a driver to easily access the ICM20948 IMU via SPI.
 */
class Device {
 public:
  //! Available gyro-ranges defining the measurement range and resolution.
  enum GyroRange{
    kGyroRange250Dps,   //!< Gyro range +-250 degree per second.
    kGyroRange500Dps,   //!< Gyro range +-500 degree per second.
    kGyroRange1000Dps,  //!< Gyro range +-1000 degree per second.
    kGyroRange2000Dps   //!< Gyro range +-2000 degree per second.
  };

  //! Available gyro-bandwidths defining the filter rate and phase delay.
  enum GyroBandwidth{
    kGyroBandwidth12106Hz,  //!< Bandwidth = 12106 Hz, Samplingrate = 9 kHz.
    kGyroBandwidth361Hz4,   //!< Bandwidth = 361,4 Hz, Samplingrate = 1125 Hz.
    kGyroBandwidth196Hz6,   //!< Bandwidth = 196,6 Hz, Samplingrate = 1125 Hz.
    kGyroBandwidth151Hz8,   //!< Bandwidth = 151,8 Hz, Samplingrate = 1125 Hz.
    kGyroBandwidth119Hz5,   //!< Bandwidth = 119,5 Hz, Samplingrate = 1125 Hz.
    kGyroBandwidth51Hz2,    //!< Bandwidth = 51,2 Hz, Samplingrate = 1125 Hz.
    kGyroBandwidth23Hz9,    //!< Bandwidth = 23,9 Hz, Samplingrate = 1125 Hz.
    kGyroBandwidth11Hz6,    //!< Bandwidth = 11,6 Hz, Samplingrate = 1125 Hz.
    kGyroBandwidth5Hz7,     //!< Bandwidth = 5,7 Hz, Samplingrate = 1125 Hz.
  };

  //! Available accelerometer-ranges defining the measurement range and resolution.
  enum AccelRange{
    kAccelRange2G,  //!< Accelerometer range +-2 g.
    kAccelRange4G,  //!< Accelerometer range +-4 g.
    kAccelRange8G,  //!< Accelerometer range +-8 g.
    kAccelRange16G  //!< Accelerometer range +-16 g.
  };

  //! Available accelerometer-bandwidths defining the filter rate and phase delay.
  enum AccelBandwidth{
    kAccelBandwidth1209Hz,  //!< Bandwidth = 1209 Hz, Samplingrate = 4500 Hz.
    kAccelBandwidth473Hz,   //!< Bandwidth = 473 Hz, Samplingrate = 1125 Hz.
    kAccelBandwidth246Hz,   //!< Bandwidth = 246 Hz, Samplingrate = 1125 Hz.
    kAccelBandwidth111Hz4,  //!< Bandwidth = 111,4 Hz, Samplingrate = 1125 Hz.
    kAccelBandwidth50Hz4,   //!< Bandwidth = 50,4 Hz, Samplingrate = 1125 Hz.
    kAccelBandwidth23Hz9,   //!< Bandwidth = 23,9 Hz, Samplingrate = 1125 Hz.
    kAccelBandwidth11Hz5,   //!< Bandwidth = 11,5 Hz, Samplingrate = 1125 Hz.
    kAccelBandwidth5Hz7,    //!< Bandwidth = 5,7 Hz, Samplingrate = 1125 Hz.
  };

  //! Structure containing the data measured by the ICM20948 including the timestamp of the
  //! measurement.
  struct MeasurementData {
    double acceleration[3] = {0.0};  //!< Acceleration in (x, y, z) direction in m/s^2.
    double gyro[3] = {0.0};  //!< Angular velocity in (x, y, z) direction in rad/s.
    double temperature = 0.0;  //!< Temperature in Celsius.
    uint64_t timestamp = 0;  //!< Timestamp when the measurement data was measured.
  };

  //! Typedef of function pointer used as data callback.
  typedef void (*MeasurementDataCallbackFunction)(MeasurementData measurement_data, void* ctx);

  /*!
   * \brief  Constructor of the ICM20948 component.
   *
   * \param  gyro_range       Measurement range of the gyroscope.
   * \param  gyro_bandwidth   Bandwidth of the gyroscope.
   * \param  accel_range      Measurement range of the accelerometer.
   * \param  accel_bandwidth  Bandwidth of the accelerometer.
   *
   * \return Returns constructed ICM20948 instance.
   */
  Device(GyroRange gyro_range, GyroBandwidth gyro_bandwidth, AccelRange accel_range,
           AccelBandwidth accel_bandwidth, MeasurementDataCallbackFunction data_callback,
         void *callback_argument);

  /*!
   * \brief Destructor of the ICM20948 component.
   */
  ~Device();

 private:
  //! Task executed or enqueued  by the callbacks if something has to be done.
  void Dispatch();

  /*!
   * \brief Callback executed when a new accel message arrives
   *
   * \param msg     KITECOM message
   */
  void MsgHandlerAcc(const kitecom::timestamped_vector_double& message);

  /*!
   * \brief Callback executed when a new gyro message arrives
   *
   * \param msg     KITECOM message
   */
  void MsgHandlerGyro(const kitecom::timestamped_vector_double& message);

  //! Callback that is called when measurement data is available.
  MeasurementDataCallbackFunction data_callback_;
  void *callback_argument_;  //!< Argument that is passed to the data callback.
  pulicast::Channel& true_acceleration_channel_;  //!< Input channel of true acceleration data
  pulicast::Channel& true_gyro_channel_;  //!< Input channel of true gyro data
  MeasurementData measurement_data_;  //!< Last measurement data
  bool accel_available_;  //!< Flag indicating that measurement data is ready to read.
  bool gyro_available_;  //!< Flag indicating that measurement data is ready to read.
};
}  // namespace icm20948

#endif  // SRC_X86_DRIVERS_ICM20948_DEVICE_HPP_
