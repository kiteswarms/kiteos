// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "device.hpp"
#include "core/puli_driver_interface.hpp"
#include "core/system_time/system_time.hpp"

namespace icm20948 {
/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
Device::Device(GyroRange gyro_range, GyroBandwidth gyro_bandwidth, AccelRange accel_range,
               AccelBandwidth accel_bandwidth, MeasurementDataCallbackFunction data_callback,
               void *callback_argument):
true_acceleration_channel_((*PuliDriverInterface::GetNamespace() / "IMU")["TrueAcceleration"]),
true_gyro_channel_((*PuliDriverInterface::GetNamespace() / "IMU")["TrueAngularVelocity"]) {
  data_callback_ = data_callback;
  callback_argument_ = callback_argument;
  true_acceleration_channel_.Subscribe<kitecom::timestamped_vector_double>(
      [this](const kitecom::timestamped_vector_double& message) {
        this->MsgHandlerAcc(message);
      });
  true_gyro_channel_.Subscribe<kitecom::timestamped_vector_double>(
      [this](const kitecom::timestamped_vector_double& message) {
        this->MsgHandlerGyro(message);
      });
  accel_available_ = false;
  gyro_available_ = false;
}

Device::~Device() = default;

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/

void Device::Dispatch() {
  if (accel_available_ && gyro_available_) {
    accel_available_ = false;
    gyro_available_ = false;
    measurement_data_.timestamp = SystemTime::GetTimestamp();
    data_callback_(measurement_data_, callback_argument_);
  }
}

void Device::MsgHandlerAcc(const kitecom::timestamped_vector_double& message) {
  for (int i = 0; i < 3; i++) {
    measurement_data_.acceleration[i] = message.values[i];
  }
  accel_available_ = true;
  Dispatch();
}

void Device::MsgHandlerGyro(const kitecom::timestamped_vector_double& message) {
  for (int i = 0; i < 3; i++) {
    measurement_data_.gyro[i] = message.values[i];
  }
  gyro_available_ = true;
  Dispatch();
}
}  // namespace icm20948
