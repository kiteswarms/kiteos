// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include <cstring>
#include <iostream>
#include "multi_access_handler.hpp"
#include "core/logger.hpp"
#include "core/system_time/system_time.hpp"
#include "core/puli_driver_interface.hpp"

namespace spektrum_receiver {
/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
MultiAccessHandler::MultiAccessHandler(spektrum_receiver::MeasurementDataCallbackFunction callback,
                                       void* ctx):
callback_(callback),
ctx_(ctx),
gamepad_ichan_((*PuliDriverInterface::GetNamespace())["GamepadAxes"]),
channel_data_{{0.0, true},
              {0.0, true},
              {0.0, true},
              {0.0, true},
              {0.0, true},
              {1.0, true},  //! Set AUX1 default to 1.0
              {0.0, true},
              {0.0, true},
              {0.0, true},
              {0.0, true},
              {0.0, true},
              {0.0, true},
              0} {
  gamepad_ichan_.Subscribe<kitecom::timestamped_vector_double>(
      [this](const kitecom::timestamped_vector_double& message) {
        this->MsgHandlerGamepad(message);
      });
  update_timer_.SetTimeoutPeriodic(11,
      [](void* ctx) {
        auto inst = reinterpret_cast<MultiAccessHandler*>(ctx);
        inst->DispatchMeasurementData();
      }, this);
  std::cout << "rc_receiver: sending dummy data until gamepad messages arrive!" << std::endl;
}

MultiAccessHandler::~MultiAccessHandler() = default;

void MultiAccessHandler::DispatchMeasurementData() {
  channel_data_.timestamp = SystemTime::GetTimestamp();
  callback_(channel_data_, ctx_);
}


void MultiAccessHandler::MsgHandlerGamepad(const kitecom::timestamped_vector_double& message) {
  ChannelValue* channel_data_map[] = {
      &channel_data_.rudder, &channel_data_.throttle, &channel_data_.aux2,
      &channel_data_.aileron, &channel_data_.elevator, &channel_data_.aux5, &channel_data_.aux4,
      &channel_data_.aux3, &channel_data_.aux6, &channel_data_.aux7, &channel_data_.gear,
      &channel_data_.aux1};

  for (int i = 0; i < 12; i++) {
    channel_data_map[i]->value = -message.values[i];
  }
}

}  // namespace spektrum_receiver
