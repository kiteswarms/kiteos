// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_X86_DRIVERS_SPEKTRUM_RECEIVER_MULTI_ACCESS_HANDLER_HPP_
#define SRC_X86_DRIVERS_SPEKTRUM_RECEIVER_MULTI_ACCESS_HANDLER_HPP_

#include "core/plugin_manager/plugin.hpp"
#include "core/software_timer.hpp"
#include "drivers/spektrum_receiver/definitions.hpp"

namespace spektrum_receiver {
/**
 * @brief Interface for a Pilot Reference class
 *
 */
class MultiAccessHandler {
 public:
  /*!
   * \brief Constructor of the Spektrum receiver driver component.
   */
  MultiAccessHandler(spektrum_receiver::MeasurementDataCallbackFunction callback, void* ctx);

  /*!
   * \brief Destructor of the Spektrum receiver driver component.
   */
  ~MultiAccessHandler();

  /*!
   * \brief Callback executed when a new gamepad message arrives
   *
   * \param message     KITECOM message
   */
  void MsgHandlerGamepad(const kitecom::timestamped_vector_double& message);

 private:
  //! Callback to be called when channel data was received by the Spektrum device.
  MeasurementDataCallbackFunction callback_;
  void* ctx_;  //!< Callback argument passed to the data callback.
  pulicast::Channel& gamepad_ichan_;  //!< Input channel of simulated spektrum data
  ChannelData channel_data_;  //!< Channel data that is published when a message arrives
  SoftwareTimer update_timer_;  //!< Timer that invokes callback periodically
  void DispatchMeasurementData();  //!< Dispatch measurement data
};
}  // namespace spektrum_receiver

#endif  // SRC_X86_DRIVERS_SPEKTRUM_RECEIVER_MULTI_ACCESS_HANDLER_HPP_
