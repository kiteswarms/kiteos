// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "a4964.hpp"
#include "drivers/a4964_configuration.hpp"
#include "drivers/a4964_registers.hpp"
#include "core/system_time/system_time.hpp"
#include "core/logger.hpp"
#include "core/persistent_memory/persistent_memory.hpp"
#include "core/puli_driver_interface.hpp"

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

A4964::A4964(StateNode* state_node):
state_node_(state_node),
true_setpoint_channel_(
    (*PuliDriverInterface::GetNamespace() / GetMotorNamespace())["TrueSetpoint"]),
true_speed_channel_((*PuliDriverInterface::GetNamespace() / GetMotorNamespace())["TrueSpeed"]) {
  true_speed_channel_.Subscribe<kitecom::timestamped_vector_double>(
    [this](const kitecom::timestamped_vector_double& message) {
      this->MsgHandlerTrueSpeed(message);
    });
  upper_limit_ = MAX_SPEED;
  lower_limit_ = 120.0;
}

A4964::~A4964() = default;

void A4964::Run(MeasurementData* measurement_data) {
  *measurement_data = measurement_data_;
  measurement_data_.speed_valid = false;
  measurement_data_.temperature_valid = false;
}

void A4964::SetSpeed(double setpoint) {
  kitecom::timestamped_vector_double msg;
  msg.ts = SystemTime::GetTimestamp();
  msg.len = 1;
  double control = MIN(MAX(setpoint, lower_limit_), upper_limit_);
  msg.values.push_back(control);
  true_setpoint_channel_ << msg;
}

void A4964::MsgHandlerTrueSpeed(const kitecom::timestamped_vector_double& message) {
  measurement_data_.timestamp = SystemTime::GetTimestamp();
  measurement_data_.speed = message.values[0];
  measurement_data_.speed_valid = true;
}

void A4964::CommandCallback(std::string_view command) {
  if (command.compare("RESET") == 0) {
    state_node_->SetState("UNINITIALIZED");
  } else if (command.compare("DISARM") == 0) {
    state_node_->SetState("DISARMED");
  } else if (command.compare("ARM") == 0) {
    state_node_->SetState("ARMED");
  }
}

std::string A4964::GetMotorNamespace() {
  uint32_t value;
  if (PersistentMemory::GetValue(PersistentMemory::kBoardIndex, &value)) {
    return std::string("Motor") + std::to_string(value);
  }
  return std::string("Motor");
}
