// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_X86_DRIVERS_TELOCATE_UWB_HPP_
#define SRC_X86_DRIVERS_TELOCATE_UWB_HPP_

#include <cstdint>
#include <vector>

/*!
 *  \brief Implements a driver to access the Telocate UWB module receiver via I2C.
 */
class TelocateUwb {
 public:
  //! Enum describing the status of a range measurement result.
  enum Status {
    kStatusValid,
    kStatusTimeout,
    kStatusGarbageData
  };

  //! Data type of the callback that is called when measurement data is ready.
  typedef void (*MeasurementDataCallbackFunction)(uint32_t remote_node_index, float distance,
                                                  float rssi, uint64_t measurement_timestamp,
                                                  Status status, void *ctx);
  //! Data type of the callback that is called when device is fully initialized.
  typedef void (*InitializedCallbackFunction)(uint32_t node_address, void *ctx);

  /*!
   * \brief Constructer of the Telocate UWB driver.
   *
   * \param data_callback      Callback to be called when measurement data is ready.
   * \param callback_argument  Argument to pass to the data callback.
   */
  TelocateUwb(MeasurementDataCallbackFunction data_callback, void* data_callback_argument,
              InitializedCallbackFunction initialized_callback, void* init_callback_argument) {}

  /*!
   * \brief Destructor of the Telocate UWB driver.
   */
  ~TelocateUwb() = default;

  //! Gets the current measurement interval.
  double GetMeasurementInterval() {return 0.1;}
  //! Sets the measurement interval to use.
  void SetMeasurementInterval(double seconds) {}
  //! Sets the node address of the node directly connected to the microcontroller.
  void SetNodeAddress(uint32_t address) {}
  //! Sets the node addresses that should be measured to.
  void SetRemoteNodeAddresses(const uint32_t* addresses, uint8_t num_addresses) {}
};

#endif  // SRC_X86_DRIVERS_TELOCATE_UWB_HPP_
