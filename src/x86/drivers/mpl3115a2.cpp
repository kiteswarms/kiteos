// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "mpl3115a2.hpp"
#include "core/puli_driver_interface.hpp"

Mpl3115a2::Mpl3115a2(Mpl3115a2::DataReadyCallback data_callback, void *callback_argument):
data_callback_(data_callback),
callback_argument_(callback_argument),
true_pressure_channel_((*PuliDriverInterface::GetNamespace() / "Barometer")["TruePressure"]) {
  true_pressure_channel_.Subscribe<kitecom::timestamped_vector_double>(
    [this](const kitecom::timestamped_vector_double& message) {
      this->MsgHandlerPressure(message);
    });
}

Mpl3115a2::~Mpl3115a2() = default;

void Mpl3115a2::MsgHandlerPressure(const kitecom::timestamped_vector_double& message) {
  data_callback_(message.values[0], dummy_temperature_, callback_argument_);
}
