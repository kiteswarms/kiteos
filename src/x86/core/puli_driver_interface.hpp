// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_X86_CORE_PULI_DRIVER_INTERFACE_HPP_
#define SRC_X86_CORE_PULI_DRIVER_INTERFACE_HPP_

#include <memory>
#include "pulicast-port/pulicast_embedded.h"
//! Holds a ptr to a pulicast namespace that drivers can use to communicate with it's backend.
class PuliDriverInterface {
 public:
  /*! \brief   Initializes the PuliDriverInterface with the provided pulicast namespace.
  *  \param   puli_namespace pulicast namespace to use
  */
  static void Init(pulicast::Namespace* puli_namespace);

  /*!
   * \brief  Gets the pulicast namespace hold by the class.
   *
   * \return Returns pulicast namespace.
   */
  static pulicast::Namespace* GetNamespace();

 private:
  static pulicast::Namespace* puli_namespace_;  //!< Reference to puli namespace.
};

#endif  // SRC_X86_CORE_PULI_DRIVER_INTERFACE_HPP_
