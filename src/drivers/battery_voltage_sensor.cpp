// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "battery_voltage_sensor.hpp"
#include <cstdio>
#include "core/logger.hpp"
#include "core/system_time/system_time.hpp"
#include "core/scheduler.hpp"

//! Define update rate in ms.
#define BATTERY_VOLTAGE_MEASUREMENT_PERIOD 1000

// Define the possible cell amounts
template class BatteryVoltageSensor<4>;

template<std::size_t N>
BatteryVoltageSensor<N>::BatteryVoltageSensor(DataReadyCallback cb, void *cb_arg) :
  adc_ina_(hal::Adc::kChannelGpioPB1),
  gpio_max_en_(hal::Gpio::Port::kPortD, hal::Gpio::kPin3,
               hal::Gpio::kModeOutput, nullptr, nullptr),
  gpio_max_a0_(hal::Gpio::Port::kPortE, hal::Gpio::kPin3,
               hal::Gpio::kModeOutput, nullptr, nullptr),
  gpio_max_a1_(hal::Gpio::Port::kPortD, hal::Gpio::kPin5,
               hal::Gpio::kModeOutput, nullptr, nullptr),
  gpio_max_a2_(hal::Gpio::Port::kPortD, hal::Gpio::kPin6,
               hal::Gpio::kModeOutput, nullptr, nullptr),
  data_ready_callback_(cb),
  cb_arg_(cb_arg) {
  gpio_max_en_.Write(true);  // enables the mux.
  readout_timer_.SetTimeoutPeriodic(BATTERY_VOLTAGE_MEASUREMENT_PERIOD, StartChannelRead, this);
}

template<std::size_t N>
BatteryVoltageSensor<N>::~BatteryVoltageSensor() = default;

template<std::size_t N>
void BatteryVoltageSensor<N>::StartChannelRead(BatteryVoltageSensor *inst) {
  inst->current_channel_ = 0;
  ReadNextChannel(inst);
}

template<std::size_t N>
void BatteryVoltageSensor<N>::ReadNextChannel(BatteryVoltageSensor *inst) {
  if (inst->current_channel_ < N) {
    // switch the mux pins
    inst->gpio_max_a0_.Write(inst->IsBitSet(inst->current_channel_, 0));
    inst->gpio_max_a1_.Write(inst->IsBitSet(inst->current_channel_, 1));
    inst->gpio_max_a2_.Write(inst->IsBitSet(inst->current_channel_, 2));
    inst->adc_ina_.StartConversion(AdcCallback, inst);
  } else {
      inst->data_ready_callback_(inst->voltage_channels_, inst->cb_arg_);
  }
}

template<std::size_t N>
bool BatteryVoltageSensor<N>::IsBitSet(uint8_t val, uint8_t pos) {
  return (val >> (pos)) & 0x1u;
}

template<std::size_t N>
void BatteryVoltageSensor<N>::AdcCallback(double current_adc_val, void *cb_arg) {
  auto *inst = reinterpret_cast<BatteryVoltageSensor<N>*>(cb_arg);

  // Ina halves the voltage. returns in mV.
  inst->voltage_channels_[inst->current_channel_] = current_adc_val * 2;
  inst->current_channel_++;
  Scheduler::EnqueueTask(ReadNextChannel, inst);
}
