// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_A4964_HPP_
#define SRC_DRIVERS_A4964_HPP_

#include "hal/spi/slave.hpp"
#include "core/software_timer.hpp"
#include "core/state_node.hpp"

/*!
 *  \brief   Implements the A4964 motor controller driver.
 *
 *  \details This class is used to control the A4964 motor drivers on Axon3 boards.
 */
class A4964 {
 public:
  //! Struct used for communication with the plugin that calls the driver.
  struct MeasurementData {
    double temperature;      //!< Motor temperature.
    bool temperature_valid;  //!< Flag if there is a new temperature value.
    double speed;            //!< Motor speed.
    bool speed_valid;        //!< flag if there is a new speed value.
    uint64_t timestamp;      //!< Timestamp when the measurement data was measured.
  };

  /*!
   * \brief  Constructor of the A4964-Component.
   *
   * \param  plugin_str  String that names the plugin.
   *
   * \return Returns constructed A4964-instance.
   */
  explicit A4964(StateNode* state_node);

  /*!
   * \brief Destructor of the A4964-Component.
   */
  ~A4964();

  /*!
   * \brief  Lets the driver do its work, this should be called periodically.
   *
   * \param  measurement_data  Structure to be filled with the latest measurement if new measurement
   *                           data is available.
   */
  void Run(MeasurementData* measurement_data);

  /*!
   * \brief Sets speed of a4964.
   *
   * \param setpoint  New speed set point.
   */
  void SetSpeed(double setpoint);

  /**
   * @brief Callback for arrived commands
   *
   * @param command which arrives via the state node
   */
  void CommandCallback(std::string_view command);

 private:
  /*!
   * \brief runs the reset procedure.
   */
  void Reset();

  /*!
  * \brief Disarm the A4964 chip.
  */
  void Disarm();

  /*!
   * \brief Arm the A4964 chip.
   */
  void Arm();

  /*!
   * \brief runs the configuration procedure.
   */
  void RunConfig();

  /*!
   * \brief Copy speed to measurement data.
   *
   * \param speed             Value to be stored.
   * \param measurement_data  Struct where speed is stored in.
   */
  void CopySpeedDataToMdata(uint16_t speed, MeasurementData* measurement_data);

  /*!
   * \brief Copy temperature to measurement data.
   *
   * \param temperature       Value to be stored.
   * \param measurement_data  Struct where temp is stored in.
   */
  void CopyTempDataToMdata(uint16_t temp, MeasurementData* measurement_data);

  /*!
   * \brief Interpret the status register content and publish log if necessary.
   *
   * \param status_word  Status register content.
   */
  void CheckStatus(uint16_t status_word);

  /*!
   * \brief Interpret the diagnostic register content and publish log if necessary.
   *
   * \param diagnostic_word  Diagnostic register content.
   */
  void CheckDiagnostic(uint16_t diagnostic_word);

  /*!
   * \brief Callback for spi transmission
   *
   * \param pv_data  User context that is provided (provides the context provided in spi_xfer).
   */
  static void SpiCallback(void *pv_data);

  /*!
   * \brief Transmits a word to A4964 (correctly sets the parity bit).
   */
  bool TransmitWord(uint16_t word);

  /*!
   * \brief Toggles watchdog.
   */
  void ToggleWdog();

  //! TODO(elias): Remove software WD workaround
  static void SoftwareWdogCallback(A4964* inst);

  //! Available A4964-states provided and handled by the run() function.
  typedef enum {
    kA4964StateConfig,
    kA4964StateProtected,
    kA4964StateDisarmed,
    kA4964StateRamping,
    kA4964StateArmed,
    kA4964StateRunning,
  } A4964State;

  //! Available A4964-communication-states provided and handled by the run() function.
  typedef enum {
    kA4964CommStateIdle,
    kA4964CommStateSetpointPending,
    kA4964CommStateSpeedReq,
    kA4964CommStateTemperatureReq,
    kA4964CommStateDiagnosticsReq,
    kA4964CommStateSpeedPending,
    kA4964CommStateTemperaturePending,
    kA4964CommStateDiagnosticsPending
  } A4964CommState;

  StateNode* state_node_;  //!< Callback to report internal state.
  hal::spi::Slave spi_slave_;       //!< SPI slave to communicate with the motor driver.
  hal::Gpio gpio_wdog_;             //!< GPIO that toggles the watchdog.
  A4964CommState comm_state_;       //!< Current communication state of the A4964-instance.
  A4964State state_;                //!< Current state of the A4964-instance.
  uint16_t out_set_point_;          //!< Raw set point value that is written via SPI
  bool out_set_point_available_;    //!< Flag raised when new setPoint is available.
  double set_point_;                 //!< Current set point of the A4964-instance.
  bool set_point_available_;        //!< Flag raised when new setPoint is available.
  double error_integral_;            //!< Current integrator value (of error).
  double k_error_integral_;          //!< Weight of error integral value.
  double max_error_integral_;        //!< Maximum value of error integral (above is clipped).
  double upper_limit_;               //!< Value of upper set point limit (above is clipped).
  double lower_limit_;               //!< Value of lower set point limit (below is clipped)
  double arm_threshold_;             //!< Value of upper threshold where arming is safe.
  uint16_t raw_motor_speed_;        //!< 10-bit value of motor speed in A4964 specific units.
  double motor_speed_;               //!< Value of current motor speed.
  uint64_t motor_speed_timestamp_;  //!< Timestamp of current motor speed value.
  double temp_;                      //!< Value of A4964 temperature.
  uint8_t send_buffer_[3];          //!< Output buffer that is used for spi-communication.
  uint8_t receive_buffer_[3];       //!< Input buffer that is used for spi-communication.
  SoftwareTimer ramp_timer_;         //!< Timer used to keep track of possible spi-timeouts.
  SoftwareTimer watchdog_timer_;     //!< Timer that tracks time of watchdog timeout.
  SoftwareTimer speed_timer_;        //!< Timer used to keep track of the status rate.
  SoftwareTimer temperature_timer_;  //!< Timer used to keep track of the temperature.

  uint64_t spi_timestamp_;  //!< Timestamp of received spi message.
  bool wd_flag_;            //!< Watchdog flag.
  uint8_t config_counter_;  //!< Counter that tracks the progress of the initial configuration.

  SoftwareTimer wd_timer_;  //!< TODO(elias): Remove software WD workaround
};
/*! @} */
#endif  // SRC_DRIVERS_A4964_HPP_
