// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "ksz8563r.hpp"
#include "utils/debug_utils.h"
#include "core/logger.hpp"

#define KSZ8563R_I2C_PORT hal::i2c::kPort3
#define KSZ8563R_SLAVE_ADDRESS 0x5F

#define KSZ8563R_GLOBAL_CHIP_ID_0_REGISTER 0x0000
#define KSZ8563R_GLOBAL_CHIP_ID_4_REGISTER 0x000F
#define KSZ8563R_SWITCH_OPERATION_REGISTER 0x0300
#define KSZ8563R_GLOBAL_PTP_CLOCK_CONTROL_REGISTER_LSB 0x0501
#define KSZ8563R_GLOBAL_PTP_MESSAGE_CONFIG_1_REGISTER_LSB 0x0515

/**************************************************************************************************
 *     Public member functions                                                                    *
 **************************************************************************************************/
Ksz8563R::Ksz8563R(): reset_pin_(hal::Gpio::kPortE, hal::Gpio::kPin12, hal::Gpio::kModeOutput),
                      i2c_slave_(KSZ8563R_I2C_PORT, KSZ8563R_SLAVE_ADDRESS, tx_buffer_, rx_buffer_,
                                 I2CCallback, this) {
  reset_pin_.Write(false);
  state_ = kStateReset;
  reset_timer_.SetCallback(TimerCallback, this);
  reset_timer_.SetTimeout(10);
}

/**************************************************************************************************
 *     Private member functions                                                                   *
 **************************************************************************************************/
void Ksz8563R::Run() {
  switch (state_) {
    case kStateReset:
      reset_pin_.Write(true);
      state_ = kStateWaitAfterReset;
      reset_timer_.SetTimeout(100);
      break;

    case kStateWaitAfterReset:
      state_ = kStateReadingChipId0Register;
      ReadRegister(KSZ8563R_GLOBAL_CHIP_ID_0_REGISTER, 3);
      break;

    case kStateReadingChipId0Register:
      if (rx_buffer_[0] == 0x00 && rx_buffer_[1] == 0x98 && rx_buffer_[2] == 0x93) {
        state_ = kStateReadingChipId4Register;
        ReadRegister(KSZ8563R_GLOBAL_CHIP_ID_4_REGISTER, 1);
      } else {
        Logger::Report("KSZ8563: Configuration failed. Read unexpected chip ID.", Logger::kWarning);
      }
      break;

    case kStateReadingChipId4Register:
      if (rx_buffer_[0] == 0x3C) {
        configuration_attempts_ = 0;
        state_ = kStateReadingGlobalPtpClockControlRegister;
        ReadRegister(KSZ8563R_GLOBAL_PTP_CLOCK_CONTROL_REGISTER_LSB, 1);
      } else {
        Logger::Report("KSZ8563: Configuration failed. Read unexpected chip ID.", Logger::kWarning);
      }
      break;

    case kStateReadingGlobalPtpClockControlRegister:
      if (!(rx_buffer_[0] & 0x02)) {
        if (configuration_attempts_ < 3) {
          configuration_attempts_++;
          state_ = kStateEnablingPtpClock;
          // Set Enable PTP Clock bit
          WriteRegister(KSZ8563R_GLOBAL_PTP_CLOCK_CONTROL_REGISTER_LSB, rx_buffer_[0] | 0x02);
        } else {
          Logger::Report("KSZ8563: Configuration failed. Cannot enable PTP clock.",
                         Logger::kWarning);
        }
      } else {
        configuration_attempts_ = 0;
        state_ = kStateReadingGlobalPtpMessageConfig1Register;
        ReadRegister(KSZ8563R_GLOBAL_PTP_MESSAGE_CONFIG_1_REGISTER_LSB, 1);
      }
      break;

    case kStateEnablingPtpClock:
      state_ = kStateReadingGlobalPtpClockControlRegister;
      ReadRegister(KSZ8563R_GLOBAL_PTP_CLOCK_CONTROL_REGISTER_LSB, 1);
      break;

    case kStateReadingGlobalPtpMessageConfig1Register:
      if (!(rx_buffer_[0] & 0x40)) {
        if (configuration_attempts_ < 3) {
          configuration_attempts_++;
          state_ = kStateEnablingPtpMode;
          // Set Enable IEEE 1588 PTP Mode bit
          WriteRegister(KSZ8563R_GLOBAL_PTP_MESSAGE_CONFIG_1_REGISTER_LSB, rx_buffer_[0] | 0x40);
        } else {
          Logger::Report("KSZ8563: Configuration failed. Cannot enable IEEE 1588 PTP mode.",
                         Logger::kWarning);
        }
      } else {
        state_ = kStateIdle;
      }
      break;

    case kStateEnablingPtpMode:
      state_ = kStateReadingGlobalPtpMessageConfig1Register;
      ReadRegister(KSZ8563R_GLOBAL_PTP_MESSAGE_CONFIG_1_REGISTER_LSB, 1);
      break;

    case kStateIdle:
    default:
      ASSERT(false);
      break;
  }
}

void Ksz8563R::WriteRegister(uint16_t register_address, uint8_t register_value) {
  tx_buffer_[0] = (register_address >> 8) & 0xFF;
  tx_buffer_[1] = register_address & 0xFF;
  tx_buffer_[2] = register_value;
  resends_ = 0;
  current_read_length_ = 0;
  i2c_slave_.Transceive(3, 0);
}

void Ksz8563R::ReadRegister(uint16_t register_address, uint32_t read_length) {
  tx_buffer_[0] = (register_address >> 8) & 0xFF;
  tx_buffer_[1] = register_address & 0xFF;
  resends_ = 0;
  current_read_length_ = read_length;
  i2c_slave_.Transceive(2, read_length);
}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void Ksz8563R::I2CCallback(hal::i2c::Error error, void *instance) {
  auto inst = reinterpret_cast<Ksz8563R*>(instance);

  if (error != hal::i2c::kErrorNone) {
    if (inst->resends_ < 3) {
      inst->resends_++;
      if (inst->current_read_length_ == 0) {
        // Read length of 0 indicates a write command
        inst->i2c_slave_.Transceive(3, 0);
      } else {
        inst->i2c_slave_.Transceive(2, inst->current_read_length_);
      }
    } else {
      Logger::Report("KSZ8563: Configuration failed. Device does not respond.", Logger::kWarning);
    }
    return;
  }

  inst->Run();
}

void Ksz8563R::TimerCallback(Ksz8563R *instance) {
  instance->Run();
}
