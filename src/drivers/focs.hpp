// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_DRIVERS_FOCS_HPP_
#define SRC_DRIVERS_FOCS_HPP_

#include <string>
#include "hal/spi/slave.hpp"
#include "core/software_timer.hpp"

/*!
 *  \brief Implements a driver that uses the Focs motor controller driver on Axon4 boards.
 */
class Focs {
 public:
  //! Struct used for communication with the plugin that calls the driver.
  struct MeasurementData {
    double temperature;      //!< Motor temperature.
    bool temperature_valid;  //!< Flag if there is a new temperature value.
    double speed;            //!< Motor speed.
    bool speed_valid;        //!< flag if there is a new speed value.
    uint8_t status;          //!< Focs status.
    bool status_valid;       //!< flag if there is a new status value.
    uint64_t timestamp;      //!< Timestamp when the spi message was received.
  };

  /*!
   * \brief  Constructor of the Focs-Component.
   *
   * \return Returns constructed Focs-instance.
   */
  Focs();

  /*!
   * \brief Destructor of the Focs-Component.
   */
  ~Focs();

  /*!
   * \brief  Lets the driver do its work, this should be called periodically.
   *
   * \param  measurement_data  Structure to be filled with the latest measurement if new measurement
   *                           data is available.
   */
  void Run(MeasurementData* measurement_data);

  /*!
   * \brief Sets speed of focs.
   *
   * \param setpoint  New speed set point.
   */
  void SetSpeed(float setpoint);

  /*!
   * \brief Disarm the focs chip.
   */
  void Disarm();

  /*!
  * \brief Arm the focs chip.
  */
  void Arm();

 private:
  //! Available SPI requests that can be sent.
  enum SpiRequest {
    kIdle,            //!< No request.
    kSpeedRequested,  //!< request speed.
    kTempRequested,   //!< request temperature.
    kStatusRequested  //!< request status.
  };


  SpiRequest last_spi_request_;      //!< last outstanding spi request.
  hal::spi::Slave spi_slave_;        //!< SPI slave to communicate with the motor driver.
  uint8_t send_buffer_[3];           //!< Output buffer that is used for spi-communication.
  uint8_t receive_buffer_[4];        //!< Input buffer that is used for spi-communication.
  float current_setpoint_;  //!< Holds the last set setpoint.
  SoftwareTimer speed_timer_;        //!< Timer used to keep track of the motor speed.
  SoftwareTimer temperature_timer_;  //!< Timer used to keep track of the heat sink temperature.
  SoftwareTimer status_timer_;       //!< Timer used to keep track of the motor status.
  uint64_t last_rx_timestamp_;       //!< Timestamp of last received transmission.

  //! fills measurement_data if spi transmission was received.
  void CheckSpiRx(MeasurementData* measurement_data);
  //! Starts a SPI requests if a timer elapsed.
  void CheckTimers();
  //! Publishes log messages if the given fault bitfield contains errors.
  void ProcessFaults(uint16_t faults);
  //! Publishes a log messages for a single fault if the masked fault is raised in the current
  //! faults.
  void ReportFault(const char *msg, uint16_t err_mask, uint16_t current_faults);
  //! Callback called when an SPI transmission is finished. Used to capture the last_rx_timestamp_.
  static void SpiCallback(void *const cb_arg);
};

#endif  // SRC_DRIVERS_FOCS_HPP_
