// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "ms5611.hpp"
#include "core/logger.hpp"
#include "core/system_time/system_time.hpp"
#include "core/scheduler.hpp"

#define MS5611_RESET_DELAY 5  // Reset delay in milliseconds
#define MS5611_RECOVER_DELAY 500  // Reset delay in milliseconds
#define MS5611_MEASUREMENT_INTERVAL 100  // in milliseconds

#define MS5611_I2C_PORT hal::i2c::kPort4
#define MS5611_I2C_ADDRESS_1 0x76
#define MS5611_I2C_ADDRESS_2 0x77

// Commands bytes for sensor control
#define MS5611_CMD_ADC_READ 0x00
#define MS5611_CMD_RESET 0x1E
#define MS5611_CMD_CONVERT_D1 0x40  // Bandwith config: OSR256=0x40 ... OSR4096=0x48
#define MS5611_CMD_CONVERT_D2 0x50  // Bandwith config: OSR256=0x50 ... OSR4096=0x58
#define MS5611_CMD_PROM_READ 0xA0


/**************************************************************************************************
 *     Public Methods                                                                             *
 **************************************************************************************************/
Ms5611::Ms5611(bool i2c_address_lsb,
               MeasurementCallback measurement_callback, void *callback_argument)
  : measurement_callback_(measurement_callback), callback_argument_(callback_argument),
    i2c_slave_(MS5611_I2C_PORT, i2c_address_lsb ? MS5611_I2C_ADDRESS_2 : MS5611_I2C_ADDRESS_1,
               tx_buffer_, rx_buffer_, I2CCallback, this),
    prom_{0},
    bandwidth_(kBandwidth1024) {
  delay_timer_.SetCallback(DelayTimerCallback, this);
  interval_timer_.SetCallback(IntervalTimerCallback, this);
  interval_timer_.SetTimeout(MS5611_MEASUREMENT_INTERVAL);
  // Reset the device and initiate configuration readout
  Reset();
}

/**************************************************************************************************
 *     Private Methods                                                                            *
 **************************************************************************************************/
void Ms5611::CommandBandwidth(uint8_t cmd) {
  uint8_t adc_delay = 1;  // max time = 0.60ms
  switch (bandwidth_) {
    case kBandwidth512:
      cmd |= 2;
      adc_delay = 2;  // max time = 1.17ms
      break;
    case kBandwidth1024:
      cmd |= 4;
      adc_delay = 3;  // max time = 2.28ms
      break;
    case kBandwidth2048:
      cmd |= 6;
      adc_delay = 5;  // max time = 4.54ms
      break;
    case kBandwidth4096:
      cmd |= 8;
      adc_delay = 10;  // max time = 9.04ms
      break;
    default:
      break;
  }
  tx_buffer_[0] = cmd;
  i2c_slave_.Transceive(1, 0);
  delay_timer_.SetTimeout(adc_delay);
}

void Ms5611::Reset() {
  tx_buffer_[0] = MS5611_CMD_RESET;
  state_ = kStateReset;
  i2c_slave_.Transceive(1, 0);

  delay_timer_.SetTimeout(MS5611_RESET_DELAY);
}

void Ms5611::PromReadNext() {
  ASSERT(prom_.read_index < 8);
  state_ = kStatePromRead;
  tx_buffer_[0] = MS5611_CMD_PROM_READ | (prom_.read_index << 1);
  i2c_slave_.Transceive(1, 2);
  prom_.read_index++;
}

void Ms5611::ConvertPressure() {
  state_ = kStateConvertPressure;
  CommandBandwidth(MS5611_CMD_CONVERT_D1);
}

void Ms5611::ConvertTemperature() {
  state_ = kStateConvertTemperature;
  CommandBandwidth(MS5611_CMD_CONVERT_D2);
}

void Ms5611::AdcRead() {
  tx_buffer_[0] = MS5611_CMD_ADC_READ;
  i2c_slave_.Transceive(1, 3);
}

bool Ms5611::PromCrcCheck() {
  uint32_t crc_calc = 0;
  uint32_t crc_cmp = prom_.crc;
  prom_.crc &= 0xFF00;
  for (int i = 0; i < 16; i++) {
    if (i & 1) {
      crc_calc ^= (uint16_t) ((prom_.array_[i >> 1]) & 0x00FF);
    } else {
      crc_calc ^= (uint16_t) ((prom_.array_[i >> 1]) >> 8);
    }
    for (int j = 8; j--; ) {
      if (crc_calc & (0x8000)) {
        crc_calc = (crc_calc << 1) ^ 0x3000;
      } else {
        crc_calc = (crc_calc << 1);
      }
    }
  }
  crc_calc = (0x000F & (crc_calc >> 12));  // final 4-bit reminder is CRC code
  prom_.crc = crc_cmp;
  return (crc_calc & 0x000F) == (crc_cmp & 0x000F);
}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void Ms5611::DelayTimerCallback(Ms5611 *inst) {
  switch (inst->state_) {
    case kStateReset:
      inst->state_ = kStatePromRead;
      inst->prom_.read_index = 0;
      inst->PromReadNext();
      break;

    case kStatePromRead:
      if (inst->prom_.read_index < 8) {
        inst->PromReadNext();
      } else if (inst->PromCrcCheck()) {
        inst->state_ = kStateIdle;
      } else {
        Logger::Report("Ms5611 PROM read CRC failed", Logger::kError);
        // Repeat the initialization process
        inst->Reset();
        inst->delay_timer_.SetTimeout(MS5611_RECOVER_DELAY);
      }
      break;

    case kStateConvertPressure:
      inst->state_ = kStateAdcReadPressure;
      inst->AdcRead();
      break;

    case kStateConvertTemperature:
      inst->state_ = kStateAdcReadTemperature;
      inst->AdcRead();
      break;

    default:
      break;
  }
}

void Ms5611::IntervalTimerCallback(Ms5611 *inst) {
  if (inst->state_ == kStateIdle) {
    // Start conversion for measurements
    inst->ConvertPressure();
  }
  inst->interval_timer_.SetTimeout(MS5611_MEASUREMENT_INTERVAL);
}

void Ms5611::InvokeUserCallbackTask(Ms5611* inst) {
  // Pressure and temperature calculation according to datasheet p.8
  inst->raw_temperature_ = (static_cast<uint32_t>(inst->rx_buffer_[0]) << 16) |
                           (static_cast<uint32_t>(inst->rx_buffer_[1]) << 8) |
                            static_cast<uint32_t>(inst->rx_buffer_[2]);

  // Calculate temperature
  int64_t dt = inst->raw_temperature_ - (inst->prom_.C5 * (1 << 8));
  int64_t temp = 2000 + (dt * inst->prom_.C6) / (1 << 23);

  // Calculate temperature compensated pressure
  int64_t off = (static_cast<int64_t>(inst->prom_.C2) * (1 << 16)) +
                (static_cast<int64_t>(inst->prom_.C4) * dt / (1 << 7));
  int64_t sens = (static_cast<int64_t>(inst->prom_.C1) * (1 << 15)) +
                 (static_cast<int64_t>(inst->prom_.C3) * dt / (1 << 8));
  // Apply second order temperature compensation (Page 8 - MS5611-01BA03 datasheet 06/2017)
  if (temp < 2000) {
    int64_t tdiff = static_cast<int64_t>(temp - 2000);
    int64_t t2 = static_cast<int64_t>(dt) * dt / (1 << 31);
    int64_t off2 = 5 * (tdiff * tdiff) / (1 << 1);
    int64_t sens2 = 5 * (tdiff * tdiff) / (1 << 2);
    // Additional compensation for very low temperatures
    if (temp < -1500) {
      tdiff = static_cast<int64_t>(temp + 1500);
      off2 += 7 * (tdiff * tdiff);
      sens2 += 11 * (tdiff * tdiff) / (1 << 1);
    }
    temp -= t2;
    off -= off2;
    sens -= sens2;
  }
  int64_t p = ((inst->raw_pressure_ * sens) / (1 << 21) - off) / (1 << 15);
  // Convert measurements to SI units
  double t_d = static_cast<double>(temp) * 0.01;  // In °C
  double p_d = static_cast<double>(p);  // In Pa
  inst->measurement_callback_(p_d, t_d, inst->callback_argument_);
}

void Ms5611::I2CCallback(hal::i2c::Error error, void *cb_arg) {
  auto inst = reinterpret_cast<Ms5611*>(cb_arg);
  if (error != hal::i2c::kErrorNone) {
    Logger::Report("Ms5611 I2C Error", Logger::kError);
  } else if (inst->state_ == kStatePromRead) {
    inst->prom_.array_[inst->prom_.read_index - 1] =
        (static_cast<uint16_t>(inst->rx_buffer_[0]) << 8) | inst->rx_buffer_[1];
    inst->delay_timer_.SetTimeout(0);
  } else if (inst->state_ == kStateAdcReadPressure) {
      inst->raw_pressure_ = (static_cast<uint32_t>(inst->rx_buffer_[0]) << 16) |
                                (static_cast<uint32_t>(inst->rx_buffer_[1]) << 8) |
                                 static_cast<uint32_t>(inst->rx_buffer_[2]);
      inst->ConvertTemperature();
  } else if (inst->state_ == kStateAdcReadTemperature) {
    Scheduler::EnqueueTask(InvokeUserCallbackTask, inst);
    inst->state_ = kStateIdle;
  }
}
