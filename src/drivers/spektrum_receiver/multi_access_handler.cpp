// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
//
// Created by jan on 08.03.21.
//

#include "utils/debug_utils.h"
#include "multi_access_handler.hpp"

namespace spektrum_receiver {

/**************************************************************************************************
 *     Private static member variables                                                            *
 **************************************************************************************************/
MultiAccessHandler* MultiAccessHandler::registry_[SPEKTRUM_MULTI_ACCESS_HANDLER_REGISTRY_SIZE] =
    {nullptr};
Device* MultiAccessHandler::device_ = nullptr;

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
MultiAccessHandler::MultiAccessHandler(MeasurementDataCallbackFunction data_callback,
                                       void *callback_argument) {
  data_callback_ = data_callback;
  callback_argument_ = callback_argument;

  // Lazy-create device instance
  if (device_ == nullptr) {
    device_ = new Device(MeasurementDataCallback, nullptr);
  }

  for (auto & i : registry_) {
    if (i == nullptr) {
      i = this;
      return;
    }
  }

  ASSERT(false);  // If this code is reached, that means no more space is available in the registry.
}

MultiAccessHandler::~MultiAccessHandler() {
  for (auto & i : registry_) {
    if (i == this) {
      i = nullptr;
    }
  }
}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void MultiAccessHandler::MeasurementDataCallback(ChannelData channel_data, void *ctx) {
  for (auto & i : registry_) {
    if (i != nullptr) {
      auto instance = reinterpret_cast<MultiAccessHandler*>(i);
      instance->data_callback_(channel_data, instance->callback_argument_);
    }
  }
}

}  // namespace spektrum_receiver
