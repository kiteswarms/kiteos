// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
//
// Created by jan on 08.03.21.
//

#ifndef SRC_DRIVERS_SPEKTRUM_RECEIVER_MULTI_ACCESS_HANDLER_HPP_
#define SRC_DRIVERS_SPEKTRUM_RECEIVER_MULTI_ACCESS_HANDLER_HPP_

#include "device.hpp"

#define SPEKTRUM_MULTI_ACCESS_HANDLER_REGISTRY_SIZE 2

namespace spektrum_receiver {

//! Class providing access to a single Spektrum device driver instance for multiple users.
class MultiAccessHandler {
 public:
  /*!
   * \brief Constructor of the MultiAccessHandler class.
   *
   * \param data_callback     Callback to be called when channel data was received by the Spektrum
   *                          device.
   * \param callback_argument Callback argument to be passed to the data callback.
   */
  MultiAccessHandler(MeasurementDataCallbackFunction data_callback, void* callback_argument);
  /*!
   * \brief Destructor of the MultiAccessHandler class.
   */
  ~MultiAccessHandler();

 private:
  //! Callback called when channel data was received by the Spektrum device.
  static void MeasurementDataCallback(ChannelData channel_data, void *ctx);

  //! Instances registered for callbacks.
  static MultiAccessHandler* registry_[SPEKTRUM_MULTI_ACCESS_HANDLER_REGISTRY_SIZE];
  static Device* device_;  //!< Pointer to the static device instance hold by this class.

  //! Callback to be called when channel data was received by the Spektrum device.
  MeasurementDataCallbackFunction data_callback_;
  void* callback_argument_;  //!< Callback argument passed to the data callback.
};

}  // namespace spektrum_receiver

#endif  // SRC_DRIVERS_SPEKTRUM_RECEIVER_MULTI_ACCESS_HANDLER_HPP_
