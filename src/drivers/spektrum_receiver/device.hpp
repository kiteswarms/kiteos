// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_SPEKTRUM_RECEIVER_DEVICE_HPP_
#define SRC_DRIVERS_SPEKTRUM_RECEIVER_DEVICE_HPP_

#include <cstdint>
#include "hal/uart.hpp"
#include "hal/gpio.hpp"
#include "core/software_timer.hpp"
#include "definitions.hpp"

namespace spektrum_receiver {

/*!
 *  \brief Implements a driver to easily access the Spektrum RC receiver via UART.
 */
class Device {
 public:
  /*!
   * \brief Constructor of the Spektrum receiver driver component.
   * \param data_callback      Callback to be called when measurement data is available.
   * \param callback_argument  Argument to be passed to the data callback.
   */
  Device(MeasurementDataCallbackFunction data_callback,
         void* callback_argument);

  /*!
   * \brief Destructor of the Spektrum receiver driver component.
   */
  ~Device();

 private:
  //! Clears all UART Rx buffers and resets the buffer cursor.
  void ResetReception();

  //! Parses the last received message into the given set point data structure.
  void ParseMessage();

  //! Puts the receiver into bind mode.
  void PutInBindMode();

  //! Reads data from the uart buffer, periodically called by the readout timer.
  void ReadUartTask();

  //! Callback that is called when measurement data is available.
  MeasurementDataCallbackFunction data_callback_;
  void *callback_argument_;  //!< Argument that is passed to the data callback.
  hal::Uart uart_;             //!< UART port where the receiver is connected.
  uint8_t receive_buffer_[16];  //!< Buffer containing the received raw bytes of the current packet.
  uint8_t buffer_cursor_;       //!< Index of the currently received byte in the current packet.
  //! Structure holding the measurement data which is passed to the data callback.
  ChannelData channel_data_;
  SoftwareTimer alignment_timer_;  //!< Timer for detecting incomplete packets.
  SoftwareTimer readout_timer_;  //!< Timer that starts reading from the UART.
};

}  // namespace spektrum_receiver

#endif  // SRC_DRIVERS_SPEKTRUM_RECEIVER_DEVICE_HPP_
