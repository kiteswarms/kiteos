// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include <cstring>
#include "buzzer.hpp"

#define BUZZER_SOUND_INTERVAL 30
#define BUZZER_SOUND_PAUSE_TONES 30

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
Buzzer::Buzzer()
  : pwm_(hal::Pwm::Pin::kPinE14, 10, 0.0),
    enabled_(false) {}

Buzzer::~Buzzer() = default;

void Buzzer::Enable() {
  if (!enabled_) {
    enabled_ = true;
    pwm_.SetDutycycle(0.5);
    update_timer_.SetTimeoutPeriodic(BUZZER_SOUND_INTERVAL, TimerElapsedCallback, this);
  }
}

void Buzzer::Disable() {
  if (enabled_) {
    enabled_ = false;
    pwm_.SetDutycycle(0.0);
    update_timer_.Stop();
  }
}
/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
void Buzzer::TimerElapsedCallback(Buzzer *inst) {
  uint8_t tones_size = sizeof(inst->tones_)/sizeof(*inst->tones_);

  if (inst->current_tone < tones_size) {
    // looping tones in array
    inst->pwm_.SetPeriod(inst->tones_[inst->current_tone]);
    inst->pwm_.SetDutycycle(0.5);
    inst->current_tone++;
  } else if (inst->current_tone < tones_size + BUZZER_SOUND_PAUSE_TONES) {
    // wait time between repetitions
    inst->pwm_.SetDutycycle(0.0);
    inst->current_tone++;
  } else {
    // start repetition
    inst->current_tone = 0;
  }
}
