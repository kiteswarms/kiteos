// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "ksz8567r.hpp"
#include "utils/debug_utils.h"
#include "hal/gpio.hpp"
#include "core/scheduler.hpp"
#include "core/logger.hpp"

#define KSZ8567R_SPI_PORT hal::spi::kPort2
#define KSZ8567R_EAST_CS_PORT hal::Gpio::kPortA
#define KSZ8567R_EAST_CS_PIN hal::Gpio::kPin3
#define KSZ8567R_WEST_CS_PORT hal::Gpio::kPortD
#define KSZ8567R_WEST_CS_PIN hal::Gpio::kPin8
#define KSZ8567R_EAST_RESET_PORT hal::Gpio::kPortE
#define KSZ8567R_EAST_RESET_PIN hal::Gpio::kPin12
#define KSZ8567R_WEST_RESET_PORT hal::Gpio::kPortA
#define KSZ8567R_WEST_RESET_PIN hal::Gpio::kPin9

#define KSZ8567R_GLOBAL_CHIP_ID_0_REGISTER 0x0000
#define KSZ8567R_SWITCH_OPERATION_REGISTER 0x0300
#define KSZ8567R_GLOBAL_PTP_CLOCK_CONTROL_REGISTER_LSB 0x0501
#define KSZ8567R_GLOBAL_PTP_MESSAGE_CONFIG_1_REGISTER_LSB 0x0515


/**************************************************************************************************
 *     Public functions                                                                           *
 **************************************************************************************************/
Ksz8567R::Ksz8567R(Position position) {
  spi_slave_ = new hal::spi::Slave(
      KSZ8567R_SPI_PORT,
      {.phase = hal::spi::kPhaseSecondEdge,
       .polarity = hal::spi::kPolarityHigh,
       .baud = hal::spi::kBaud8MHz,  // according to datasheet up to 50MHz is possible
       .chip_select_port =
           (position == kPositionEast) ? KSZ8567R_EAST_CS_PORT : KSZ8567R_WEST_CS_PORT,
       .chip_select_pin =
           (position == kPositionEast) ? KSZ8567R_EAST_CS_PIN : KSZ8567R_WEST_CS_PIN},
      tx_buffer_, rx_buffer_, SpiCallback, this);

  reset_pin_ = new hal::Gpio(
      (position == kPositionEast) ? KSZ8567R_EAST_RESET_PORT : KSZ8567R_WEST_RESET_PORT,
      (position == kPositionEast) ? KSZ8567R_EAST_RESET_PIN : KSZ8567R_WEST_RESET_PIN,
      hal::Gpio::kModeOutput);

  reset_pin_->Write(false);
  state_ = kStateReset;
  reset_timer_.SetCallback(TimerCallback, this);
  reset_timer_.SetTimeout(10);
}

/**************************************************************************************************
 *     Private functions                                                                          *
 **************************************************************************************************/
void Ksz8567R::Run() {
  switch (state_) {
    case kStateReset:
      reset_pin_->Write(true);
      state_ = kStateWaitAfterReset;
      reset_timer_.SetTimeout(100);
      break;

    case kStateWaitAfterReset:
      state_ = kStateReadingChipId0Register;
      ReadRegister(KSZ8567R_GLOBAL_CHIP_ID_0_REGISTER, 3);
      break;

    case kStateReadingChipId0Register:
      if (rx_buffer_[4] == 0x00 && rx_buffer_[5] == 0x85 && rx_buffer_[6] == 0x67) {
        configuration_attempts_ = 0;
        state_ = kStateReadingGlobalPtpClockControlRegister;
        ReadRegister(KSZ8567R_GLOBAL_PTP_CLOCK_CONTROL_REGISTER_LSB, 1);
      } else {
        Logger::Report("KSZ8567: Configuration failed. Read unexpected chip ID.", Logger::kWarning);
      }
      break;

    case kStateReadingGlobalPtpClockControlRegister:
      if (!(rx_buffer_[4] & 0x02)) {
        if (configuration_attempts_ < 3) {
          configuration_attempts_++;
          state_ = kStateEnablingPtpClock;
          // Set Enable PTP Clock bit
          WriteRegister(KSZ8567R_GLOBAL_PTP_CLOCK_CONTROL_REGISTER_LSB, rx_buffer_[4] | 0x02);
        } else {
          Logger::Report("KSZ8567: Configuration failed. Cannot enable PTP clock.",
                         Logger::kWarning);
        }
      } else {
        configuration_attempts_ = 0;
        state_ = kStateReadingGlobalPtpMessageConfig1Register;
        ReadRegister(KSZ8567R_GLOBAL_PTP_MESSAGE_CONFIG_1_REGISTER_LSB, 1);
      }
      break;

    case kStateEnablingPtpClock:
      state_ = kStateReadingGlobalPtpClockControlRegister;
      ReadRegister(KSZ8567R_GLOBAL_PTP_CLOCK_CONTROL_REGISTER_LSB, 1);
      break;

    case kStateReadingGlobalPtpMessageConfig1Register:
      if (!(rx_buffer_[4] & 0x40)) {
        if (configuration_attempts_ < 3) {
          configuration_attempts_++;
          state_ = kStateEnablingPtpMode;
          // Set Enable IEEE 1588 PTP Mode bit
          WriteRegister(KSZ8567R_GLOBAL_PTP_MESSAGE_CONFIG_1_REGISTER_LSB, rx_buffer_[4] | 0x40);
        } else {
          Logger::Report("KSZ8567: Configuration failed. Cannot enable IEEE 1588 PTP mode.",
                         Logger::kWarning);
        }
      } else {
        state_ = kStateIdle;
      }
      break;

    case kStateEnablingPtpMode:
      state_ = kStateReadingGlobalPtpMessageConfig1Register;
      ReadRegister(KSZ8567R_GLOBAL_PTP_MESSAGE_CONFIG_1_REGISTER_LSB, 1);
      break;

    case kStateIdle:
    default:
      ASSERT(false);
      break;
  }
}

void Ksz8567R::WriteRegister(uint16_t register_address, uint8_t register_value) {
  tx_buffer_[0] = 0x40 | (register_address >> 19u);
  tx_buffer_[1] = register_address >> 11u;
  tx_buffer_[2] = register_address >> 3u;
  tx_buffer_[3] = (register_address << 5u);
  tx_buffer_[4] = register_value;
  spi_slave_->Transceive(5);
}

void Ksz8567R::ReadRegister(uint16_t register_address, uint32_t read_length) {
  tx_buffer_[0] = 0x60 | (register_address >> 19u);
  tx_buffer_[1] = register_address >> 11u;
  tx_buffer_[2] = register_address >> 3u;
  tx_buffer_[3] = (register_address << 5u);
  spi_slave_->Transceive(4 + read_length);
}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void Ksz8567R::SpiCallback(void *instance) {
  reinterpret_cast<Ksz8567R*>(instance)->Run();
}

void Ksz8567R::TimerCallback(Ksz8567R *instance) {
  instance->Run();
}
