// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include <string>
#include <cstdio>
#include "ina3221.hpp"
#include "core/logger.hpp"
#include "core/system_time/system_time.hpp"
#include "core/scheduler.hpp"

//! Define update rate in ms.
#define INA3221_MEASUREMENT_PERIOD 10

// ADDRESS: ina3221 address is defined by voltage level on address pin
#define ADDR1_GND 0b1000000
#define ADDR2_VS  0b1000001
#define ADDR3_SDA 0b1000010
#define ADDR4_SCL 0b1000011

// REGISTERS:
#define REG_CONF 0x00
#define REG_ID   0xFF
#define REG_CH1  0x01
#define REG_CH2  0x03
#define REG_CH3  0x05

// RST: false
// reserved[3]
// AVERAGES[3]: 64
// bus conversion time 1.1ms
// shunt conversion time 1.1 ms
// shunt: continuous mode
#define INA_CONFIG 0b0111011100100101

// PINS: SCL: PB8, SDA: PB9, INA_PV: PD10, INA_CRIT: PD11, INA_WARN: PD15, INA_TC: PD9

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
Ina3221::Ina3221(Ina3221::DataReadyCallback cb, void *cb_arg) :
  slave_(hal::i2c::kPort1, ADDR2_VS,
         send_buffer_, receive_buffer_, I2cCallback, this),
         data_ready_callback_(cb),
         cb_arg_(cb_arg) {
  StartContinuousConversion();
  readout_timer_.SetTimeoutPeriodic(INA3221_MEASUREMENT_PERIOD, StartChannelRead, this);
}

/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
void Ina3221::StartContinuousConversion() {
  send_buffer_[0] = REG_CONF;
  send_buffer_[1] = (INA_CONFIG >> 8u) & 0xFFu;
  send_buffer_[2] = INA_CONFIG & 0xFFu;

  slave_.Transceive(3, 0);
}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void Ina3221::StartChannelRead(Ina3221 *inst) {
  inst->current_channel_ = 0;
  ReadNextChannel(inst);
}

void Ina3221::ReadNextChannel(Ina3221 *inst) {
  if (inst->current_channel_ < 3) {
    inst->send_buffer_[0] = inst->channel_registers_[inst->current_channel_];
    inst->slave_.Transceive(1, 2);
  } else {
    inst->data_ready_callback_(inst->voltage_channels_, inst->cb_arg_);
  }
}

void Ina3221::I2cCallback(hal::i2c::Error error, void *cb_arg) {
  auto *inst = reinterpret_cast<Ina3221 *>(cb_arg);
  if (error == hal::i2c::kErrorNone) {
    // Bit 3 is LSB = 40 μV
    double result = static_cast<int16_t>(
                      ((inst->receive_buffer_[0] << 8) |
                       inst->receive_buffer_[1]) >> 3) * 0.00004;
    inst->voltage_channels_[inst->current_channel_] = result;
    inst->current_channel_++;
    Scheduler::EnqueueTask(ReadNextChannel, inst);
  } else {
    Logger::Report("I2C Error", Logger::kError);
  }
}
