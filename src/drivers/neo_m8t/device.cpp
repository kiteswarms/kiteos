// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include <algorithm>
#include <cstdio>

#include "device.hpp"
#include "ubx-utils/composer.hpp"
#include "core/logger.hpp"
#include "core/scheduler.hpp"

namespace neo_m8t {

/**************************************************************************************************
 *     Defines                                                                                    *
 **************************************************************************************************/
// port definitions
#define UBLOX_M8_RESET_GPIO_PORT hal::Gpio::Port::kPortA
#define UBLOX_M8_RESET_GPIO_PIN  hal::Gpio::kPin9

// Define i2c slave address
#define UBLOX_M8_SLAVE_ADDRESS 0x42
// Define uart mode 8-N-1
#define UBLOX_M8_UART_MODE 0x8C0

// Define readable i2c registers
#define UBLOX_M8_REG_BYTES_AVAILABLE 0xFD
#define UBLOX_M8_REG_STREAM 0xFF

/**************************************************************************************************
 *     Configuration struct methods                                                               *
 **************************************************************************************************/
Device::Configuration::Configuration(Device::InitStruct init_struct):
    navigation_rate{init_struct.navigation_rate, false},
    dynamic_model{init_struct.dynamic_model, false},
    gps_enabled{init_struct.gps_enabled, false},
    glonass_enabled{init_struct.glonass_enabled, false},
    galileo_enabled{init_struct.galileo_enabled, false},
    timepulse_0_enabled{init_struct.timepulse_0_enabled, false},
    timepulse_1_enabled{init_struct.timepulse_1_enabled, false},
    tim_tp_enabled{init_struct.tim_tp_messages_enabled, false},
    tim_tm2_enabled{init_struct.tim_tm2_messages_enabled, false},
    nav_pvt_enabled{init_struct.nav_pvt_messages_enabled, false},
    rxm_sfrbx_enabled{init_struct.rxm_sfrbx_messages_enabled, false},
    rxm_rawx_enabled{init_struct.rxm_rawx_messages_enabled, false} {}

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
Device::Device(InitStruct init_struct)
  : config_(init_struct),
    slave_(hal::i2c::kPort1, UBLOX_M8_SLAVE_ADDRESS,
           tx_buffer_, rx_buffer_,
           I2cCallback, this),
    reset_gpio_(UBLOX_M8_RESET_GPIO_PORT,
                UBLOX_M8_RESET_GPIO_PIN,
                hal::Gpio::kModeOutput,
                nullptr, nullptr),
    rx_buffer_{0} {
  dispatcher_.Subscribe(AckNakCallback);
  dispatcher_.Subscribe(ubx::ClassId::kMonVer, MonVerCallback , this);
  Reset();
}

void Device::EnableRaw() {
  config_.rxm_sfrbx_enabled.value = true;
  config_.rxm_sfrbx_enabled.initialized = false;
  config_.rxm_rawx_enabled.value = true;
  config_.rxm_rawx_enabled.initialized = false;
  Reset();
}

void Device::DisableRaw() {
  config_.rxm_sfrbx_enabled.value = true;
  config_.rxm_sfrbx_enabled.initialized = false;
  config_.rxm_rawx_enabled.value = true;
  config_.rxm_rawx_enabled.initialized = false;
  Reset();
}
/**************************************************************************************************
 *     Private methods                                                                             *
 **************************************************************************************************/
void Device::Reset() {
  state_ = kStatePowerOnReset;
  // Reset the ublox device
  reset_gpio_.Write(false);
  reset_timer_.SetCallback(ResetTimerCallback, this);
  reset_timer_.SetTimeout(100);
  update_timer_.SetTimeoutPeriodic(10, UpdateTimerCallback, this);
}

bool Device::ConfigureI2cProtocol() {
  if (slave_.IsBusy()) {
    return false;
  }
  ubx::CfgPrt cfg_prt = {0};
  cfg_prt.port_id = UBX_PORT_ID_DDC;
  cfg_prt.mode = UBLOX_M8_SLAVE_ADDRESS << 1u;
  cfg_prt.in_proto_mask = 0x01;
  cfg_prt.out_proto_mask = 0x01;
  uint8_t wlen = ubx::Composer::Compose(tx_buffer_, &cfg_prt);
  return slave_.Transceive(wlen, 0);
}

bool Device::DisableUartProtocol() {
  if (slave_.IsBusy()) {
    return false;
  }
  ubx::CfgPrt cfg_prt = {0};
  cfg_prt.port_id = UBX_PORT_ID_UART;
  cfg_prt.mode = UBLOX_M8_UART_MODE;
  cfg_prt.baud_rate = 115200;
  cfg_prt.in_proto_mask = 0x00;
  cfg_prt.out_proto_mask = 0x00;
  uint8_t wlen = ubx::Composer::Compose(tx_buffer_, &cfg_prt);
  return slave_.Transceive(wlen, 0);
}

bool Device::ConfigureNav5(uint8_t dyn_model) {
  if (slave_.IsBusy()) {
    return false;
  }
  ubx::CfgNav5 cfg_nav_5 = {0};
  // Mask to set dynModel and minElevation
  cfg_nav_5.mask = 0x03;
  // airborne with <2g acceleration
  cfg_nav_5.dyn_model = dyn_model;
  // 0 degree min elevation
  cfg_nav_5.min_elev = 0;
  uint8_t wlen = ubx::Composer::Compose(tx_buffer_, &cfg_nav_5);
  return slave_.Transceive(wlen, 0);
}

bool Device::ConfigureGnssGps() {
  if (slave_.IsBusy()) {
    return false;
  }
  ubx::CfgGnssBlock cfg_gnss_block = {
    // Configure GPS
    .gnss_id = UBX_GNSS_ID_GPS,
    .res_trk_ch = 8,
    .max_trk_ch = 16,
    .reserved1 = 0,
    .flags = UBX_GNSS_CFG_ENABLE | UBX_GNSS_CFG_GPS_L1CA,
  };

  uint8_t wlen = ubx::Composer::Compose(tx_buffer_, &cfg_gnss_block, 1);
  return slave_.Transceive(wlen, 0);
}

bool Device::ConfigureGnssGlonass() {
  if (slave_.IsBusy()) {
    return false;
  }
  ubx::CfgGnssBlock cfg_gnss_block = {
    // Configure Glonass
    .gnss_id = UBX_GNSS_ID_GLONASS,
    .res_trk_ch = 8,
    .max_trk_ch = 16,
    .reserved1 = 0,
    .flags = UBX_GNSS_CFG_ENABLE | UBX_GNSS_CFG_GLONASS_L1,
  };

  uint8_t wlen = ubx::Composer::Compose(tx_buffer_, &cfg_gnss_block, 1);
  return slave_.Transceive(wlen, 0);
}
bool Device::ConfigureGnssGalileo() {
  if (slave_.IsBusy()) {
    return false;
  }
  ubx::CfgGnssBlock cfg_gnss_block = {
    // Configure Galileo
    .gnss_id = UBX_GNSS_ID_GALILEO,
    .res_trk_ch = 8,
    .max_trk_ch = 8,
    .reserved1 = 0,
    .flags = UBX_GNSS_CFG_ENABLE | UBX_GNSS_CFG_GALILEO_E1,
  };

  uint8_t wlen = ubx::Composer::Compose(tx_buffer_, &cfg_gnss_block, 1);
  return slave_.Transceive(wlen, 0);
}

bool Device::ConfigureRate(uint16_t navigation_rate) {
  if (slave_.IsBusy()) {
    return false;
  }
  ubx::CfgRate cfg_rate = {0};
  cfg_rate.meas_rate = navigation_rate;
  cfg_rate.nav_rate = 1;
  cfg_rate.time_ref = 1;
  uint8_t wlen = ubx::Composer::Compose(tx_buffer_, &cfg_rate);
  return slave_.Transceive(wlen, 0);
}

bool Device::ConfigureTp5(uint8_t tp_idx) {
  if (slave_.IsBusy()) {
    return false;
  }
  ubx::CfgTp5 cfg_tp_5 = {0};
  cfg_tp_5.tp_idx = tp_idx;
  cfg_tp_5.version = 1;
  cfg_tp_5.ant_cable_delay = 50;
  cfg_tp_5.rf_group_delay = 0;
  cfg_tp_5.freq_period = 1000000;
  cfg_tp_5.freq_period_lock = 1000000;
  cfg_tp_5.pulse_len_ratio = 100000;
  cfg_tp_5.pulse_len_ratio_lock = 100000;
  cfg_tp_5.user_config_delay = 0;
  // GPS time grid, positive pulse, align to top of second
  cfg_tp_5.flags = 0x80u | 0x40u | 0x20u | 0x10u | 0x4u | 0x2u | 0x1u;
  uint8_t wlen = ubx::Composer::Compose(tx_buffer_, &cfg_tp_5);
  return slave_.Transceive(wlen, 0);
}

bool Device::EnableMessage(ubx::ClassId class_id) {
  if (slave_.IsBusy()) {
    return false;
  }
  ubx::CfgMsg cfg = {0};
  cfg.msg_class = class_id >> 8u;
  cfg.msg_id = class_id & 0xFFu;
  cfg.rate[0] = 1;
  cfg.rate[1] = 0;
  uint8_t wlen = ubx::Composer::Compose(tx_buffer_, &cfg);
  return slave_.Transceive(wlen, 0);
}

bool Device::ConfigurePollVersion() {
  if (slave_.IsBusy()) {
    return false;
  }
  return slave_.Transceive(ubx::Composer::ComposePoll(tx_buffer_, ubx::ClassId::kMonVer), 0);
}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void Device::ResetTimerCallback(Device *inst) {
  switch (inst->state_) {
    case kStatePowerOnReset:
      inst->state_ = kStateResetWait;
      inst->reset_gpio_.Write(true);
      // Wait until the device is reset
      inst->reset_timer_.SetTimeout(1000);
      break;

    case kStateResetWait:
      inst->state_ = kStateConfiguring;
      inst->ConfigureI2cProtocol();
      break;

    default:
      ASSERT(false);  // This case should not happen
      break;
  }
}

void Device::UpdateTimerCallback(Device *inst) {
  inst->dispatcher_.Dispatch();

  if (inst->state_ == kStateIdle) {
    inst->state_ = kStateBytesAvReadPending;
    inst->tx_buffer_[0] = UBLOX_M8_REG_BYTES_AVAILABLE;
    inst->slave_.Transceive(1, 2);
  }
}

void Device::BytesAvailableCallback() {
  bytes_available_ = (rx_buffer_[0] << 8u) | (rx_buffer_[1] & 0xFFu);
  if (bytes_available_ == 0xFFFF) {
    bytes_available_ = 0;
  } else if (bytes_available_) {
    transfer_len_ = std::min(bytes_available_, kRxBufferLength);
    bytes_available_ -= transfer_len_;
    state_ = kStateDataReadPending;
    tx_buffer_[0] = UBLOX_M8_REG_STREAM;
    slave_.Transceive(1, transfer_len_);
    return;
  }
  state_ = kStateIdle;
}

void Device::DataCallback() {
  dispatcher_.CopyDataToRingbuf(rx_buffer_, transfer_len_);
  if (bytes_available_) {
    transfer_len_ = std::min(bytes_available_, kRxBufferLength);
    bytes_available_ -= transfer_len_;
    state_ = kStateDataReadPending;
    tx_buffer_[0] = UBLOX_M8_REG_STREAM;
    slave_.Transceive(1, transfer_len_);
  } else {
    state_ = kStateIdle;
  }
}

void Device::ConfigureCallback() {
  if (!config_.uart_protocol_disabled) {
    if (DisableUartProtocol()) {
      config_.uart_protocol_disabled = true;
    }
    return;
  }

  if (!config_.navigation_rate.initialized) {
    if (ConfigureRate(config_.navigation_rate.value)) {
      config_.navigation_rate.initialized = true;
    }
    return;
  }

  if (!config_.dynamic_model.initialized) {
    if (ConfigureNav5(config_.dynamic_model.value)) {
      config_.dynamic_model.initialized = true;
    }
    return;
  }

  if (!config_.gps_enabled.initialized && config_.gps_enabled.value) {
    if (ConfigureGnssGps()) {
      config_.gps_enabled.initialized = true;
    }
    return;
  }

  if (!config_.glonass_enabled.initialized && config_.glonass_enabled.value) {
    if (ConfigureGnssGlonass()) {
      config_.glonass_enabled.initialized = true;
    }
    return;
  }

  if (!config_.galileo_enabled.initialized && config_.galileo_enabled.value) {
    if (ConfigureGnssGalileo()) {
      config_.galileo_enabled.initialized = true;
    }
    return;
  }

  if (!config_.timepulse_0_enabled.initialized && config_.timepulse_0_enabled.value) {
    if (ConfigureTp5(0)) {
      config_.timepulse_0_enabled.initialized = true;
    }
    return;
  }

  if (!config_.timepulse_1_enabled.initialized && config_.timepulse_1_enabled.value) {
    if (ConfigureTp5(1)) {
      config_.timepulse_1_enabled.initialized = true;
    }
    return;
  }

  if (!config_.tim_tp_enabled.initialized && config_.tim_tp_enabled.value) {
    if (EnableMessage(ubx::ClassId::kTimTp)) {
      config_.tim_tp_enabled.initialized = true;
    }
    return;
  }

  if (!config_.tim_tm2_enabled.initialized && config_.tim_tm2_enabled.value) {
    if (EnableMessage(ubx::ClassId::kTimTm2)) {
      config_.tim_tm2_enabled.initialized = true;
    }
    return;
  }

  if (!config_.nav_pvt_enabled.initialized && config_.nav_pvt_enabled.value) {
    if (EnableMessage(ubx::ClassId::kNavPvt)) {
      config_.nav_pvt_enabled.initialized = true;
    }
    return;
  }

  if (!config_.rxm_sfrbx_enabled.initialized && config_.rxm_sfrbx_enabled.value) {
    if (EnableMessage(ubx::ClassId::kRxmSfrbX)) {
      config_.rxm_sfrbx_enabled.initialized = true;
    }
    return;
  }

  if (!config_.rxm_rawx_enabled.initialized && config_.rxm_rawx_enabled.value) {
    if (EnableMessage(ubx::ClassId::kRxmRawX)) {
      config_.rxm_rawx_enabled.initialized = true;
    }
    return;
  }

  if (!config_.version_polled) {
    if (ConfigurePollVersion()) {
      config_.version_polled = true;
    }
    return;
  }

  state_ = kStateIdle;
}

void Device::I2cCallback(hal::i2c::Error error, void *cb_arg) {
  if (error != hal::i2c::kErrorNone) {
    return;
  }
  auto* inst = reinterpret_cast<Device*>(cb_arg);
  switch (inst->state_) {
    case kStateConfiguring:
      inst->ConfigureCallback();
      break;
    case kStateIdle:
      break;
    case kStateBytesAvReadPending:
      inst->BytesAvailableCallback();
      break;
    case kStateDataReadPending:
      inst->DataCallback();
      break;
    default:
      break;
  }
}

void Device::AckNakCallback(ubx::AckNak* ack_nak) {
  Logger::Report("Device: Received ACK-NAK", Logger::kWarning);
}

void Device::MonVerCallback(uint8_t* data, uint16_t size, Device* ctx) {
  char output_string[50];

  snprintf(output_string, sizeof(output_string), "swVersion: %s", data);
  Logger::Report(output_string, Logger::kInfo);

  snprintf(output_string, sizeof(output_string), "hwVersion: %s", data + 30);
  Logger::Report(output_string, Logger::kInfo);

  for (uint8_t i = 0; i < (size - 40) / 30; ++i) {
    snprintf(output_string, sizeof(output_string), "extension %u: %s", i, data + 40 + 30 * i);
    Logger::Report(output_string, Logger::kInfo);
  }
}

}  // namespace neo_m8t
