// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "parser.hpp"
namespace ubx {

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
Parser::Parser(uint8_t* payload_buffer, uint16_t payload_buffer_size)
  : payload_buffer_(payload_buffer),
    payload_buffer_size_(payload_buffer_size),
    state_(kUbxParseStateSync1),
    widx_(0), ridx_(0),
    msg_payload_counter_(0) {}

uint16_t Parser::CopyDataToRingbuf(const uint8_t *const data, uint16_t data_length) {
  uint16_t i = 0;
  for (i = 0; i < data_length; ++i) {
    if (!RingbufferFull()) {
      ringbuffer_[widx_] = data[i];
      widx_ = (widx_ + 1) % UBX_RINGBUFFER_SIZE;
    } else {
      break;
    }
  }
  return i;
}

uint16_t Parser::ParseMessage() {
  // Run parser until ringbuffer is empty
  while (ridx_ != widx_) {
    switch (state_) {
      case kUbxParseStateSync1:
        if (ringbuffer_[ridx_] == UBX_SYNC_CHAR_1) {
          // If we find synch char 1 this could be the start
          msg_start_offset_idx_ = ridx_;
          state_ = kUbxParseStateSync2;
        }
        ridx_ = (ridx_ + 1) % UBX_RINGBUFFER_SIZE;
        break;
      case kUbxParseStateSync2:
        if (ringbuffer_[ridx_] == UBX_SYNC_CHAR_2) {
          state_ = kUbxParseStateClass;
          ridx_ = (ridx_ + 1) % UBX_RINGBUFFER_SIZE;
        } else {
          // If this is not synch char 2 then start over
          // searching for synch char 1
          state_ = kUbxParseStateSync1;
        }
        break;
      case kUbxParseStateClass:
        msg_class_id_ = ringbuffer_[ridx_] << 8u;
        state_ = kUbxParseStateId;
        ridx_ = (ridx_ + 1) % UBX_RINGBUFFER_SIZE;
        break;
      case kUbxParseStateId:
        msg_class_id_ |= ringbuffer_[ridx_];
        state_ = kUbxParseStateLength1;
        ridx_ = (ridx_ + 1) % UBX_RINGBUFFER_SIZE;
        break;
      case kUbxParseStateLength1:
        msg_length_ = ringbuffer_[ridx_];
        state_ = kUbxParseStateLength2;
        ridx_ = (ridx_ + 1) % UBX_RINGBUFFER_SIZE;
        break;
      case kUbxParseStateLength2:
        msg_length_ |= ringbuffer_[ridx_] << 8u;
        state_ = kUbxParseStatePayload;
        ridx_ = (ridx_ + 1) % UBX_RINGBUFFER_SIZE;
        // Payload starts after second length byte
        msg_payload_offset_idx_ = ridx_;
        break;
      case kUbxParseStatePayload:
        // Iterate over payload until we reach msg_length_
        if (msg_payload_counter_ < msg_length_) {
          ridx_ = (ridx_ + 1) % UBX_RINGBUFFER_SIZE;
          msg_payload_counter_++;
        } else {
          msg_payload_counter_ = 0;
          state_ = kUbxParseStateCrca;
        }
        break;
      case kUbxParseStateCrca:
        msg_crc_ = ringbuffer_[ridx_] << 8u;
        state_ = kUbxParseStateCrcb;
        ridx_ = (ridx_ + 1) % UBX_RINGBUFFER_SIZE;
        break;
      case kUbxParseStateCrcb:
        msg_crc_ |= ringbuffer_[ridx_];
        state_ = kUbxParseStateSync1;
        // Check if msg_crc_ is equal to locally calculated CRC
        // If it matches we found a valid message
        if (msg_crc_ == RingbufferFrameCrc(msg_start_offset_idx_, msg_length_)) {
          // Continue parsing at the next position
          ridx_ = (ridx_ + 1) % UBX_RINGBUFFER_SIZE;
          // Copy into linear buffer for dispacher
          if (msg_length_ <= payload_buffer_size_) {
            // Increment message counter if successfully parsed,
            // i.e. payload-parser supports that type of message.
            CopyRingbufferToPayloadBuffer(msg_payload_offset_idx_, msg_length_);
            return msg_length_;
          }
        } else {
          // We found an invalid message.
          // Since this could happen by chance
          // (accidental coincidence of random bytes with sync chars)
          // we 'reverse' the read pointer to continue parsing right after
          // current messageStart.
          ridx_ = (msg_start_offset_idx_ + 1) % UBX_RINGBUFFER_SIZE;
        }
        break;
      default:
        return 0;
    }
  }
  return 0;
}

ClassId Parser::GetClassId() {
  return static_cast<ClassId>(msg_class_id_);
}

/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
bool Parser::RingbufferFull() {
  return ((widx_ + 1) % UBX_RINGBUFFER_SIZE == ridx_);
}

uint16_t Parser::RingbufferFrameCrc(uint16_t idx, uint16_t payload_size) {
  idx = (idx + 2) % UBX_RINGBUFFER_SIZE;
  payload_size += 4;
  uint32_t crc_a = 0;
  uint32_t crc_b = 0;
  if (payload_size > 0) {
    do {
      crc_a += ringbuffer_[idx];
      crc_b += crc_a;
      idx = (idx + 1) % UBX_RINGBUFFER_SIZE;
    } while (--payload_size);
    crc_a &= 0xffu;
    crc_b &= 0xffu;
  }
  return (uint16_t)(crc_a << 8u) | crc_b;
}

void Parser::CopyRingbufferToPayloadBuffer(uint16_t start_idx, uint16_t msg_length) {
  for (uint16_t i = 0; i < msg_length; ++i) {
    payload_buffer_[i] = ringbuffer_[start_idx];
    start_idx = (start_idx + 1) % UBX_RINGBUFFER_SIZE;
  }
}
}  // namespace ubx
