// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_NEO_M8T_UBX_UTILS_PARSER_HPP_
#define SRC_DRIVERS_NEO_M8T_UBX_UTILS_PARSER_HPP_

/*! \addtogroup ubx UBX
 * @{
 * \addtogroup  ubxParser UBX Parser
 *
 * @{
 */

#include <stdbool.h>
#include <stdint.h>
#include "messages.hpp"

#define UBX_RINGBUFFER_SIZE 2048

namespace ubx {

//! \brief     Parser of UBX frames message
//
//  \detail     The ubx::Parser is used to parse the frame of UBX messages. After successfully
//              parsing a message, the payload copied to the provided payload_buffer. This
//              parser uses a ringbuffer to enable piecewise adding of read data. An internal state
//              machine stores the current parsing step and continues when new data is copied to the
//              ringbuffer.
//
//  \author    Elias Rosch
//
//  \date      2020
class Parser {
 public:
  //! \brief Constructor of a Parser instance
  //
  //  \param payload_buffer          Pointer to buffer for successfully parsed payload.
  //  \param payload_buffer_size_    Maximum size of payload_buffer to be copied.
  //
  //  \return Constructed Parser instance
  Parser(uint8_t* payload_buffer, uint16_t payload_buffer_size_);

  //! \brief Protocol byte stream into ringbuffer of parser
  //
  //  \param data          Pointer to data that should be copied.
  //  \param length       Size of data to be copied.
  //
  //  \return              Number of bytes copied
  uint16_t CopyDataToRingbuf(const uint8_t *const data, uint16_t length);

  //! \brief Parse messages available in ringbuffer
  //
  //  \return  Size of payload if a frame was successfully parsed
  uint16_t ParseMessage();

  //! \brief Read the ClassId of last successfully parsed message
  //
  //  \return ClassId of last successfully parsed message
  ClassId GetClassId();

 private:
  //! Internal state of the frame parser.
  enum State {
    kUbxParseStateSync1,
    kUbxParseStateSync2,
    kUbxParseStateClass,
    kUbxParseStateId,
    kUbxParseStateLength1,
    kUbxParseStateLength2,
    kUbxParseStatePayload,
    kUbxParseStateCrca,
    kUbxParseStateCrcb,
  };


  //! \brief Checks if ringbuffer is full
  //
  //  \return true if full, false otherwise.
  bool RingbufferFull();

  //! \brief Calculate the CRC value (using ubx specific polynom) iterating within ringbuffer
  //  \param idx ringbuffer idx to the first byte of frame
  //  \param size payload size to calculate CRC
  //  \return calculated CRC value
  uint16_t RingbufferFrameCrc(uint16_t idx, uint16_t size);

  //! \brief Copies data from ringbuf to a provided linear buffer
  //  \param start_idx ringbuffer idx to the first byte to copy
  //  \param size number of bytes to copy
  void CopyRingbufferToPayloadBuffer(uint16_t start_idx, uint16_t size);

  uint8_t* payload_buffer_;  //!< buffer to put payload of successfully parsed frame
  uint16_t payload_buffer_size_;  //!< Max size of payload_buffer
  State state_;  //!< Current state of frame parser
  uint8_t ringbuffer_[UBX_RINGBUFFER_SIZE];  //!< Ringbuffer holding raw ubx data stream
  uint16_t widx_;  //!< ringbuffer write idx
  uint16_t ridx_;  //!< ringbuffer read idx

  //! Start address of currently parsed message (laying in ringbuffer)
  uint16_t msg_start_offset_idx_;
  //! ClassId of currently parsed message
  uint16_t msg_class_id_;
  //! Length of currently parsed message
  uint16_t msg_length_;
  //! Start address of payload of currently parsed message (laying in ringbuffer)
  uint16_t msg_payload_offset_idx_;
  //! Bytecounter of payload of currently parsed message
  uint16_t msg_payload_counter_;
  //! CRC value of currently parsed message
  uint16_t msg_crc_;
};
}  // namespace ubx
//! @}
//! @}
#endif  // SRC_DRIVERS_NEO_M8T_UBX_UTILS_PARSER_HPP_
