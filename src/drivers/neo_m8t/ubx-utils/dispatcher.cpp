// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "dispatcher.hpp"
#include <string.h>

namespace ubx {
/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
Dispatcher::Dispatcher()
  : parser_(payload_buffer_, UBX_PAYLOAD_BUFFER_SIZE) {}

uint16_t Dispatcher::CopyDataToRingbuf(const uint8_t *const data, uint16_t length) {
  return parser_.CopyDataToRingbuf(data, length);
}

bool Dispatcher::Dispatch() {
  uint16_t len = parser_.ParseMessage();
  if (!len) {
    return false;
  }
  ClassId class_id = parser_.GetClassId();
  // Search LUT for entries with that msgClassId
  for (auto & registered_callback : registeredCallbacks_) {
    if (registered_callback) {
      registered_callback(class_id, payload_buffer_, len);
    } else {
      return false;
    }
  }
  return true;
}

/**************************************************************************************************
*     Private methods                                                                            *
**************************************************************************************************/
bool Dispatcher::Register(ClassId class_id, const CbRawUbxData& callback) {
  for (auto & registered_callback : registeredCallbacks_) {
    if (!registered_callback) {
      registered_callback = callback;
      return true;
    }
  }
  return false;
}
}  // namespace ubx
