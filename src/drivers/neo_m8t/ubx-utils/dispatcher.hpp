// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_NEO_M8T_UBX_UTILS_DISPATCHER_HPP_
#define SRC_DRIVERS_NEO_M8T_UBX_UTILS_DISPATCHER_HPP_

/*! \addtogroup ubx UBX
 * @{
 * \addtogroup  ubxDispatcher UBX Dispatcher
 *
 * @{
 */

#include <stdbool.h>
#include <stdint.h>
#include <functional>
#include <optional>
#include "parser.hpp"
#include "messages.hpp"

#define UBX_CB_LUT_SIZE 16
#define UBX_PAYLOAD_BUFFER_SIZE 1024

namespace ubx {

//! \brief     Dispatcher of payload of UBX messages
//
//  \detail     The ubx::Dispatcher is used to parse the payload of UBX messages into a
//              corresponding structure. The user is able to register callbacks for different
//              message classes. If a corresponding callback is registered it is
//              invoked providing the payload interpreted in a structured fashion.
//
//  \author    Elias Rosch
//
//  \date      2020
class Dispatcher {
 public:
     //! Raw Type of callbacks
  using RawCallbackFunction = void (uint8_t* data, uint16_t size);

  //! Raw Type of callbacks with provided context
  template<typename ctx_type>
  using RawCallbackCtxFunction = void (uint8_t* data, uint16_t size, ctx_type* ctx);

  //! Typed callbacks without context
  template<typename message_type>
  using CallbackFunction = void (message_type* msg);
  //! Typed callbacks with provided context
  template<typename message_type, typename ctx_type>
  using CallbackCtxFunction = void (message_type* msg, ctx_type* ctx);

  //! \brief Constructor of a Dispatcher instance
  //
  //  \return Constructed Dispatcher instance
  Dispatcher();

  //! \brief Protocol byte stream into ringbuffer of parser
  //
  //  \param data          Pointer to data that should be copied.
  //  \param length       Size of data to be copied.
  //
  //  \return              Number of bytes copied
  uint16_t CopyDataToRingbuf(const uint8_t *const data, uint16_t size);

     /*!
  * \brief     Generic register function for arbitrary messages.
  *
  * \attention use carefully since it is not typesafe.
  *
  * \param     msg_class_id  classId to be parsed.
  * \param     payload       payload to be parsed
  * \param     len           length of payload to be parsed
  *
  * \return    True if successfully parsed. False otherwise.
  */
  bool Dispatch();

  //! \brief    Function used to register callback for different messages with user provided ctx.
  //            The provided ctx pointer will be provided in the invoked callback.
  //
  //  \param    sub_class_id ClassId to subscribe to
  //  \param    cb_func structs containing parsed data
  //
  //  \return   True if successfully registered. False otherwise.
  bool Subscribe(ClassId sub_class_id, RawCallbackFunction cb_func) {
    for (uint8_t i = 0; i < UBX_CB_LUT_SIZE; ++i) {
      if (!registeredCallbacks_[i]) {
        // Put a lambda into the raw CB LUT which interpretes the data.
        registeredCallbacks_[i] =
          [sub_class_id, cb_func, this](ClassId class_id, uint8_t* data, uint16_t size) {
          if (sub_class_id == class_id) {
            cb_func(data, size);
          }
        };
        return true;
      }
    }
    return false;
  }

  //! \brief    Function used to register callback for different messages with user provided ctx.
  //            The provided ctx pointer will be provided in the invoked callback.
  //
  //  \param    sub_class_id ClassId to subscribe to
  //  \param    cb_func structs containing parsed data
  //  \param    ctx     pointer to user context which will be provided in callback
  //
  //  \return   True if successfully registered. False otherwise.
  template<typename ctx_type>
  bool Subscribe(ClassId sub_class_id, RawCallbackCtxFunction<ctx_type> cb_func, ctx_type* ctx) {
    for (uint8_t i = 0; i < UBX_CB_LUT_SIZE; ++i) {
      if (!registeredCallbacks_[i]) {
        // Put a lambda into the raw CB LUT which interpretes the data.
        registeredCallbacks_[i] =
          [sub_class_id, cb_func, ctx, this](ClassId class_id, uint8_t* data, uint16_t size) {
          if (sub_class_id == class_id) {
            cb_func(data, size, ctx);
          }
        };
        return true;
      }
    }
    return false;
  }

  //! \brief    Function used to register callback for different messages without ctx.
  //
  //  \param    cb_func structs containing parsed data
  //
  //  \return   True if successfully registered. False otherwise.
  template<typename message_type>
  bool Subscribe(CallbackFunction<message_type> cb_func) {
    for (uint8_t i = 0; i < UBX_CB_LUT_SIZE; ++i) {
      if (!registeredCallbacks_[i]) {
        // Put a lambda into the raw CB LUT which interpretes the raw data.
        registeredCallbacks_[i] = [cb_func, this](ClassId class_id, uint8_t* data, uint16_t size) {
          auto msg = ParseMessage<message_type>(class_id, data, size);
          if (msg.has_value()) {
            cb_func(msg.value());
          }
        };
        return true;
      }
    }
    return false;
  }

  //! \brief    Function used to register callback for different messages with user provided ctx.
  //            The provided ctx pointer will be provided in the invoked callback.
  //
  //  \param    cb_func structs containing parsed data
  //  \param    ctx     pointer to user context which will be provided in callback
  //
  //  \return   True if successfully registered. False otherwise.
  template<typename message_type, typename ctx_type>
  bool Subscribe(CallbackCtxFunction<message_type, ctx_type> cb_func, ctx_type* ctx) {
    for (uint8_t i = 0; i < UBX_CB_LUT_SIZE; ++i) {
      if (!registeredCallbacks_[i]) {
        // Put a lambda into the raw CB LUT which interpretes the data.
        registeredCallbacks_[i] =
          [cb_func, ctx, this](ClassId class_id, uint8_t* data, uint16_t size) {
          auto msg = ParseMessage<message_type>(class_id, data, size);
          if (msg.has_value()) {
            cb_func(msg.value(), ctx);
          }
        };
        return true;
      }
    }
    return false;
  }

 private:
  //! Type of generic callback function
  using CbRawUbxData = std::function<void (ClassId, uint8_t*, uint16_t)>;

  //! \brief    Function used to register raw callback.
  //
  //  \param    class_id ClassId which identifies the message type bound to the callback
  //  \param    callback Function that should be invoked for given message type
  //
  //  \return   True if successfully registered. False otherwise.
  bool Register(ClassId class_id, const CbRawUbxData& callback);

  //! \brief  Function that interpretes data according to the specified class_id.
  //
  //  \param  class_id ClassId which determines the interpretation of data
  //  \param  data raw data bytes to be interpreted
  //  \param  size of data buffer
  //
  //  \return  Interpreted data struct if class_id is correct. std::nullopt otherwise.
  template<typename message_type>
  std::optional<message_type*> ParseMessage(ClassId class_id, uint8_t* data, uint16_t size) {
    if (message_type::class_id == class_id) {
      return reinterpret_cast<message_type*>(data);
    } else {
      return std::nullopt;
    }
  }

  CbRawUbxData registeredCallbacks_[UBX_CB_LUT_SIZE];  //!< LUT holding registered callbacks

  Parser parser_;  //!< Instance of UBX frame parser
  //! buffer that holds payload of successfully parsed frame
  uint8_t payload_buffer_[UBX_PAYLOAD_BUFFER_SIZE];
};
}  // namespace ubx
/*! @} */
/*! @} */
#endif  // SRC_DRIVERS_NEO_M8T_UBX_UTILS_DISPATCHER_HPP_
