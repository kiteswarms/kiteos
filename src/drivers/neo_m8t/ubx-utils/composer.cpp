// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "composer.hpp"
#include <string.h>
namespace ubx {

/* ===========================================================================*/
/*                   Public methods                                           */
/* ===========================================================================*/
uint8_t Composer::ComposePoll(uint8_t *const buffer, ClassId class_id) {
  uint16_t payload_size = 0;
  uint8_t msg_size = 0;
  msg_size += UbxHeader(buffer, class_id, payload_size);
  msg_size += UbxFooter(buffer, payload_size);
  return msg_size;
}

uint8_t Composer::ComposePollCfgPrt(uint8_t *const buffer, uint8_t port_id) {
  uint16_t payload_size = 1;
  uint8_t msg_size = 0;
  msg_size += UbxHeader(buffer, kCfgPrt, payload_size);

  memcpy(buffer + msg_size, &port_id, sizeof(port_id));
  msg_size += sizeof(port_id);

  msg_size += UbxFooter(buffer, payload_size);
  return msg_size;
}

uint8_t Composer::ComposePollCfgMsg(uint8_t *const buffer, ClassId class_id) {
  uint16_t payload_size = 2;
  uint8_t msg_size = 0;
  msg_size += UbxHeader(buffer, kCfgMsg, payload_size);

  memcpy(buffer + msg_size, &class_id, sizeof(uint16_t));
  msg_size += sizeof(uint16_t);

  msg_size += UbxFooter(buffer, payload_size);
  return msg_size;
}

uint8_t Composer::Compose(uint8_t *const buffer, CfgGnssBlock *cfg_gnss_blocks, uint8_t num) {
  uint16_t payload_size = sizeof(CfgGnss) + sizeof(CfgGnssBlock) * num;
  uint8_t msg_size = 0;
  uint8_t idx;
  msg_size += UbxHeader(buffer, kCfgGnss, payload_size);

  CfgGnss cfg_gnss = {
    0x00,  // msgVer
    0x00,  // numTrkChHw
    0xFF,  // numTrkChUse
    num  // numConfigBlocks
  };
  memcpy(buffer + msg_size, &cfg_gnss, sizeof(CfgGnss));
  msg_size += sizeof(CfgGnss);

  for (idx = 0; idx < num; idx++) {
    memcpy(buffer + msg_size, cfg_gnss_blocks + idx, sizeof(CfgGnssBlock));
    msg_size += sizeof(CfgGnssBlock);
  }

  msg_size += UbxFooter(buffer, payload_size);
  return msg_size;
}

/* ===========================================================================*/
/*                   Private methods                                          */
/* ===========================================================================*/
uint8_t Composer::UbxHeader(uint8_t *const buffer, ClassId class_id, uint8_t payload_size) {
  buffer[0] = UBX_SYNC_CHAR_1;
  buffer[1] = UBX_SYNC_CHAR_2;
  buffer[2] = class_id >> 8u;    // Class
  buffer[3] = class_id & 0xFFu;  // ID
  buffer[4] = payload_size & 0xFFu;
  buffer[5] = payload_size >> 8u;
  return 6;
}

uint8_t Composer::UbxFooter(uint8_t *const buffer, uint8_t payload_size) {
  uint16_t crc = FrameCrc(buffer, payload_size);
  buffer[payload_size + 6] = crc >> 8u;
  buffer[payload_size + 7] = crc & 0xFFu;
  return 2;
}

uint16_t Composer::FrameCrc(const uint8_t *const data, uint32_t payload_size) {
  uint8_t size = payload_size + 4;
  uint32_t crc_a = 0;
  uint32_t crc_b = 0;
  uint8_t idx = 2;
  if (size > 0) {
    do {
      crc_a += data[idx];
      crc_b += crc_a;
      idx++;
    } while (--size);
    crc_a &= 0xffu;
    crc_b &= 0xffu;
  }
  return (uint16_t)(crc_a << 8u) | crc_b;
}

}  // namespace ubx
