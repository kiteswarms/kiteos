// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_NEO_M8T_MULTI_ACCESS_HANDLER_HPP_
#define SRC_DRIVERS_NEO_M8T_MULTI_ACCESS_HANDLER_HPP_

#include <vector>
#include "device.hpp"

namespace neo_m8t {

//! Class providing access to a single Neo-M8T device driver instance for multiple users.
class MultiAccessHandler {
 public:
  /*!
   * \brief Constructor of the MultiAccessHandler class.
   */
  MultiAccessHandler();
  /*!
   * \brief Destructor of the MultiAccessHandler class.
   */
  ~MultiAccessHandler() = default;

  /*!
   * \brief  Function used to register callback for different messages with user provided ctx.
   *         The provided ctx pointer will be provided in the invoked callback.
   *
   * \param  class_id  ClassId to subscribe to
   * \param  cb_func   callback function with matching signature for desired message type
   *
   * \return True if successfully registered. False otherwise.
   */
  bool Subscribe(ubx::ClassId class_id, ubx::Dispatcher::RawCallbackFunction cb_func);

  /*!
   * \brief  Function used to register callback for different messages with user provided ctx.
   *         The provided ctx pointer will be provided in the invoked callback.
   *
   * \param  class_id  ClassId to subscribe to
   * \param  cb_func   callback function with matching signature for desired message type
   * \param  ctx       pointer to user context which will be provided in callback
   *
   * \return True if successfully registered. False otherwise.
   */
  template<typename ctx_type>
  bool Subscribe(ubx::ClassId class_id, ubx::Dispatcher::RawCallbackCtxFunction<ctx_type> cb_func,
                 ctx_type *ctx) {
    return device_->Subscribe(class_id, cb_func, ctx);
  }

  /*!
   * \brief  Function used to register callback for different messages without ctx.
   *
   * \param  cb_func  callback function with matching signature for desired message type
   *
   * \return True if successfully registered. False otherwise.
   */
  template<typename message_type>
  bool Subscribe(ubx::Dispatcher::CallbackFunction<message_type> cb_func) {
    return device_->Subscribe(cb_func);
  }

  /*!
   * \brief  Function used to register callback for different messages with user provided ctx.
   *         The provided ctx pointer will be provided in the invoked callback.
   *
   * \param  cb_func  callback function with matching signature for desired message type
   * \param  ctx      pointer to user context which will be provided in callback
   *
   * \return True if successfully registered. False otherwise.
   */
  template<typename message_type, typename ctx_type>
  bool Subscribe(ubx::Dispatcher::CallbackCtxFunction<message_type, ctx_type> cb_func,
                 ctx_type *ctx) {
    return device_->Subscribe(cb_func, ctx);
  }

  //! \brief Enables raw messages
  void EnableRaw() {
    device_->EnableRaw();
  }

  //! \brief Disables raw messages
  void DisableRaw() {
    device_->DisableRaw();
  }

 private:
  static Device* device_;  //!< Pointer to the static device instance hold by this class.
};

}  // namespace neo_m8t

#endif  // SRC_DRIVERS_NEO_M8T_MULTI_ACCESS_HANDLER_HPP_
