// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_AS5048_HPP_
#define SRC_DRIVERS_AS5048_HPP_

#include "core/software_timer.hpp"
#include "hal/i2c/slave.hpp"

//! Implements a driver for the AS5048 magnetometer.
class As5048 {
 public:
  //! Function type of the callback which is called when measurement data is ready.
  typedef void (*DataReadyCallback)(float angle, uint64_t timestamp, void *callback_argument);

  /*!
   * \brief Constructor of the AS5048 driver.
   *
   * \param port                 I2C port connected to the AS5048 chip.
   * \param slave_address        I2C slave address of the AS5048 chip. May be 0x40, 0x41, 0x42 or
   *                             0x43.
   * \param data_ready_callback  Callback called when measurement data is ready.
   * \param callback_argument    Argument passed to the data ready callback
   */
  As5048(hal::i2c::Port port, uint8_t slave_address,
         As5048::DataReadyCallback data_ready_callback, void *callback_argument);

   /*!
    * \brief Destructor of the AS5048 driver.
    */
  ~As5048();

 private:
  //!< Possible states of the AS5048 driver.
  enum State {
    kStateIdle,
    kStatePending
  };

  //! Task executing the main driver code, this task is called or enqueued by the callbacks.
  static void RunTask(As5048 *instance);
  //! Start a new channel read sequence.
  static void ReadoutTimerCallback(As5048* instance);
  //! Callback called when an I2C transmission is finished.
  static void I2CCallback(hal::i2c::Error error, void *instance);

  hal::i2c::Slave slave_;        //!< Slave used for I2C communication.
  uint8_t tx_buffer_[1] = {0};   //!< Transmit buffer used for I2C communication.
  uint8_t rx_buffer_[2] = {0};   //!< Receive buffer used for I2C communication.

  SoftwareTimer readout_timer_;  //!< Timer used to periodically readout the measurement data.
  //! Timer used to limit the period for publishing I2C errors.
  SoftwareTimer communication_failure_reported_timer_;

  DataReadyCallback data_ready_callback_;  //!< Callback called when measurement data is ready.
  void* callback_argument_;                //!< Argument passed to the data ready callback.

  State state_ = kStateIdle;  //!< Current state of the AS5048 driver.
};

#endif  // SRC_DRIVERS_AS5048_HPP_
