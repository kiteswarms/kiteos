// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_CORE_TEMPERATURE_HPP_
#define SRC_DRIVERS_CORE_TEMPERATURE_HPP_

#include <cstdint>
#include "core/software_timer.hpp"
#include "hal/adc.hpp"

/*!
 *  \brief Implements a driver to easily access the CoreTemperature.
 */
class CoreTemperature {
 public:
  //! \brief Constructor of the CoreTemperature driver component.
  CoreTemperature();

  //! \brief Destructor of the CoreTemperature driver component.
  ~CoreTemperature();

  //! \brief Gets the current value of the CoreTemperature.
  double GetValue();
 private:
  //! Callback called when the buzzer timer elapsed.
  static void TimerElapsedCallback(CoreTemperature *inst);

  hal::Adc temperature_adc_;  //!< Analog-Digital-Converter instance used for voltage monitoring.
  double temperature_;          //!< Last measured microcontroller junction temperature.

  SoftwareTimer update_timer_;  //!< plays the next tone.
};

#endif  // SRC_DRIVERS_CORE_TEMPERATURE_HPP_
