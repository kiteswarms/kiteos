// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_ICM20948_DEVICE_HPP_
#define SRC_DRIVERS_ICM20948_DEVICE_HPP_

#include <cmath>
#include <vector>
#include "communication.hpp"
#include "core/software_timer.hpp"

namespace icm20948 {
//! Class implementing the driver for a ICM20948 IMU.
class Device {
 public:
  //! Available gyro-ranges defining the measurement range and resolution.
  enum GyroRange {
    kGyroRange250Dps,   //!< Gyro range +-250 degree per second.
    kGyroRange500Dps,   //!< Gyro range +-500 degree per second.
    kGyroRange1000Dps,  //!< Gyro range +-1000 degree per second.
    kGyroRange2000Dps   //!< Gyro range +-2000 degree per second.
  };

  //! Available gyro-bandwidths defining the filter rate and phase delay.
  enum GyroBandwidth {
    kGyroBandwidth12106Hz,  //!< Bandwidth = 12106 Hz, sampling rate = 9 kHz.
    kGyroBandwidth361Hz4,   //!< Bandwidth = 361,4 Hz, sampling rate = 1125 Hz.
    kGyroBandwidth196Hz6,   //!< Bandwidth = 196,6 Hz, sampling rate = 1125 Hz.
    kGyroBandwidth151Hz8,   //!< Bandwidth = 151,8 Hz, sampling rate = 1125 Hz.
    kGyroBandwidth119Hz5,   //!< Bandwidth = 119,5 Hz, sampling rate = 1125 Hz.
    kGyroBandwidth51Hz2,    //!< Bandwidth = 51,2 Hz, sampling rate = 1125 Hz.
    kGyroBandwidth23Hz9,    //!< Bandwidth = 23,9 Hz, sampling rate = 1125 Hz.
    kGyroBandwidth11Hz6,    //!< Bandwidth = 11,6 Hz, sampling rate = 1125 Hz.
    kGyroBandwidth5Hz7,     //!< Bandwidth = 5,7 Hz, sampling rate = 1125 Hz.
  };

  //! Available accelerometer-ranges defining the measurement range and resolution.
  enum AccelRange {
    kAccelRange2G,  //!< Accelerometer range +-2 g.
    kAccelRange4G,  //!< Accelerometer range +-4 g.
    kAccelRange8G,  //!< Accelerometer range +-8 g.
    kAccelRange16G  //!< Accelerometer range +-16 g.
  };

  //! Available accelerometer-bandwidths defining the filter rate and phase delay.
  enum AccelBandwidth {
    kAccelBandwidth1209Hz,  //!< Bandwidth = 1209 Hz, Samplingrate = 4500 Hz.
    kAccelBandwidth473Hz,   //!< Bandwidth = 473 Hz, Samplingrate = 1125 Hz.
    kAccelBandwidth246Hz,   //!< Bandwidth = 246 Hz, Samplingrate = 1125 Hz.
    kAccelBandwidth111Hz4,  //!< Bandwidth = 111,4 Hz, Samplingrate = 1125 Hz.
    kAccelBandwidth50Hz4,   //!< Bandwidth = 50,4 Hz, Samplingrate = 1125 Hz.
    kAccelBandwidth23Hz9,   //!< Bandwidth = 23,9 Hz, Samplingrate = 1125 Hz.
    kAccelBandwidth11Hz5,   //!< Bandwidth = 11,5 Hz, Samplingrate = 1125 Hz.
    kAccelBandwidth5Hz7,    //!< Bandwidth = 5,7 Hz, Samplingrate = 1125 Hz.
  };

  //! Structure containing the data measured by the ICM20948 including the timestamp of the
  //! measurement.
  struct MeasurementData {
    double acceleration[3] = {0.0};  //!< Acceleration in (x, y, z) direction in m/s^2.
    double gyro[3] = {0.0};  //!< Angular velocity in (x, y, z) direction in rad/s.
    double temperature = 0.0;  //!< Temperature in Celsius.
    uint64_t timestamp = 0;  //!< Timestamp when the measurement data was measured.
  };

  //! Typedef of function pointer used as data callback.
  typedef void (*MeasurementDataCallbackFunction)(MeasurementData measurement_data, void *ctx);

  /*!
   * \brief Constructor of the ICM20948 device class.
   *
   * @param gyro_range         Gyroscope measurement range to be configured.
   * @param gyro_bandwidth     Gyroscope bandwidth to be configured.
   * @param accel_range        Accelerometer measurement range to be configured.
   * @param accel_bandwidth    Accelerometer bandwidth to be configured.
   * @param data_callback      Callback to be called when measurement data is available.
   * @param callback_argument  Argument to be passed to the data callback.
   */
  Device(GyroRange gyro_range, GyroBandwidth gyro_bandwidth, AccelRange accel_range,
         AccelBandwidth accel_bandwidth, MeasurementDataCallbackFunction data_callback,
         void *callback_argument);

  //! Destructor of the ICM20948 device class.
  ~Device() = default;

 private:
  //! Available states of the icm20948 device driver.
  enum State {
    kStateReadingWhoAmI,
    kStateWaitingForSteadyState,
    kStateRegisterConfig,
    kStateSelfTest,
    kStateIdle,
    kStatePending,
    kStateError
  };

  //! Structure for a configuration register value.
  struct ConfigurationRegister {
    //! Register bank containing the register to modify.
    Communication::RegisterBank register_bank;
    uint8_t register_address;  //!< Register to modify.
    uint8_t value;             //!< Value to write to the register.
    //! Time to wait after write before verification or next step in milliseconds.
    uint32_t write_delay;
    bool verify;       //!< If true, the register value is verified after writing.
    bool initialized;  //!< True if the value was successfully written to the register.
    //! Constructor of the configuration register structure.
    ConfigurationRegister(Communication::RegisterBank register_bank, uint8_t register_address,
                          uint8_t value, uint32_t write_delay, bool verify);
  };

  //! Structure containing all variables and states relevant for the self test.
  struct SelfTest {
    //! Available states of the self test procedure.
    enum State {
      kStateNotStarted,
      kStateReadingGyroFactorySelfTestValues,
      kStateReadingAccelFactorySelfTestValues,
      kStateEnablingGyroSelfTest,
      kStateEnablingAccelSelfTest,
      kStateWaitingForDataWithSelfTestEnabled,
      kStatePendingWithSelfTestEnabled,
      kStateDisablingGyroSelfTest,
      kStateDisablingAccelSelfTest,
      kStateWaitingForDataWithSelfTestDisabled,
      kStatePendingWithSelfTestDisabled
    };

    State state = kStateNotStarted;  //!< State of the self test procedure.
    //! Factory self test gyro values read from the device.
    double factory_self_test_gyro[3] = {0.0};
    //! Factory self test acceleration values read from the device.
    double factory_self_test_accel[3] = {0.0};
    //! Counter used for counting the number of individual measurements taken.
    int measurement_cnt = 0;
    //! Sum of all gyro measurements taken with self test enabled.
    double gyro_measurements_self_test_enabled[3] = {0};
    //! Sum of all acceleration measurements taken with self test enabled.
    double accel_measurements_self_test_enabled[3] = {0};
    //! Sum of all gyro measurements taken with self test disabled.
    double gyro_measurements_self_test_disabled[3] = {0};
    //! Sum of all acceleration measurements taken with self test disabled.
    double accel_measurements_self_test_disabled[3] = {0};
  };

  //! Array that maps gyroscope range enum values to the values to be written to the gyro config 1
  //! register.
  static constexpr uint8_t gyro_range_register_values_[] = {
    0u,         // kGyroRange250Dps
    1u << 1u,   // kGyroRange500Dps
    2u << 1u,   // kGyroRange1000Dps
    3u << 1u};  // kGyroRange2000Dps
  //! Array that maps gyroscope bandwidth enum values to the values to be written to the gyro config
  //! 1 register.
  static constexpr uint8_t gyro_bandwidth_register_values_[] = {
    0u,  // kGyroBandwidth12106Hz
    (7u << 3u) | 0x01u,   // kGyroBandwidth361Hz4
    0x01u,                // kGyroBandwidth196Hz6
    (1u << 3u) | 0x01u,   // kGyroBandwidth151Hz8
    (2u << 3u) | 0x01u,   // kGyroBandwidth119Hz5
    (3u << 3u) | 0x01u,   // kGyroBandwidth51Hz2
    (4u << 3u) | 0x01u,   // kGyroBandwidth23Hz9
    (5u << 3u) | 0x01u,   // kGyroBandwidth11Hz6
    (6u << 3u) | 0x01u};  // kGyroBandwidth5Hz7
  //! Array that maps accelerometer range enum values to the values to be written to the accel
  //! config register.
  static constexpr uint8_t accel_range_register_values_[] = {
    0u,         // kAccelRange2G
    1u << 1u,   // kAccelRange4G
    2u << 1u,   // kAccelRange8G
    3u << 1u};  // kAccelRange16G
  //! Array that maps accelerometer bandwidth enum values to the values to be written to the accel
  //! config register.
  static constexpr uint8_t accel_bandwidth_register_values_[] = {
    0u,                    // kAccelBandwidth1209Hz
    (7u << 3u) | 0x01u,    // kAccelBandwidth473Hz
    0x01u,                 // kAccelBandwidth246Hz
    (2u << 3u) | 0x01u,    // kAccelBandwidth111Hz4
    (3u << 3u) | 0x01u,    // kAccelBandwidth50Hz4
    (4u << 3u) | 0x01u,    // kAccelBandwidth23Hz9
    (5u << 3u) | 0x01u,    // kAccelBandwidth11Hz5
    (6u << 3u) | 0x01u};  // kAccelBandwidth5Hz7
  //! Array that maps gyroscope range enum values to sensitivity values.
  static constexpr double gyro_sensitivity_values_[] = {
    131.0 * 180.0 / M_PI,  // kGyroRange250Dps
    65.5 * 180.0 / M_PI,   // kGyroRange500Dps
    32.8 * 180.0 / M_PI,   // kGyroRange1000Dps
    16.4 * 180.0 / M_PI};  // kGyroRange2000Dps
  //! Array that maps accelerometer range enum values to sensitivity values.
  static constexpr double accel_sensitivity_values_[] = {
    16384.0 / 9.81,  // kAccelRange2G
    8192.0 / 9.81,   // kAccelRange4G
    4094.0 / 9.81,   // kAccelRange8G
    2048.0 / 9.81};  // kAccelRange16G
  //! Temperature sensitivity value.
  static constexpr double temperature_sensitivity_value_  = 333.87;
  //! Temperature sensitivity value.
  static constexpr double temperature_offset_value_ = 21.0;

  //! Register configuration that contains all register values to be written to the ICM20948 during
  //! initialization.
  std::vector<ConfigurationRegister> register_configuration_;

  //! Structure containing all variables relevant for the self test.
  SelfTest self_test_;

  double gyro_sensitivity_;  //!< Factor that is used to convert raw sensor measurements to rad/s.
  //! Factor that is used to convert raw sensor measurements to m/s^2.
  double accelerometer_sensitivity_;

  //! Callback that is called when measurement data is available.
  MeasurementDataCallbackFunction data_callback_;
  void *callback_argument_;  //!< Argument that is passed to the data callback.

  State state_ = kStateReadingWhoAmI;  //!< State of the ICM20948 driver.

  SoftwareTimer steady_state_timer_;  //!< Timer used to detect the steady state after power up.

  //! Communication object handling the access to the ICM20948 registers.
  Communication communication_;
  hal::Gpio int_gpio_;  //!< Gpio pin used for the data ready interrupt.
  bool run_task_enqueued_ = false;  //!< Flag indicating if the run task is currently enqueued.

  uint64_t timestamp_ = 0;  //!< Timestamp when the last measurement was taken.
  //! Structure holding the measurement data which is passed to the data callback.
  MeasurementData measurement_data_;
  uint8_t read_data_[14] = {0};  //!< Buffer holding the data read from the ICM20948 registers.

  //! Starts reading sensor data from the ICM device.
  void StartSensorRead();

  //! Parses sensor data from the read buffer into gyro and acceleration vectors using si units.
  void ParseSensorData(double *accel, double *gyro, bool increment);

  //! Parses sensor data from the read buffer into gyro and acceleration vectors using si units.
  //! Additionally adds the temperature value.
  void ParseSensorData(double *accel, double *gyro, double *temperature, bool increment);

  //! Runs the self test state machine.
  void RunSelfTest();

  //! Task executed or enqueued  by the callbacks if something has to be done.
  static void RunTask(Device *instance);

  //! Callback which is called when a register write finished.
  static void WriteRegisterCallback(void *cb_arg, Communication::Error error);

  //! Callback which is called when a register read finished.
  static void ReadRegisterCallback(void *cb_arg, const uint8_t *read_data, int read_length,
                                   Communication::Error error);

  //! Callback which is called when there is data ready to read from the ICM20948.
  static void DataRdyCallback(void *inst_ptr);
};
}  // namespace icm20948

#endif  // SRC_DRIVERS_ICM20948_DEVICE_HPP_
