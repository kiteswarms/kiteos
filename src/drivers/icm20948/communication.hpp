// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_ICM20948_COMMUNICATION_HPP_
#define SRC_DRIVERS_ICM20948_COMMUNICATION_HPP_

#include <cstdint>
#include "hal/spi/slave.hpp"
#include "core/software_timer.hpp"

#define ICM20948_COMMUNICATION_BUFFER_LENGTH 15

namespace icm20948 {
//! Class implementing methods to access the registers of a ICM20948 IMU.
class Communication {
 public:
  //! Available register banks.
  enum RegisterBank {
    kRegisterBank0,
    kRegisterBank1,
    kRegisterBank2,
    kRegisterBank3,
    kRegisterBankUnknown
  };

  //! Errors that can occur.
  enum Error {
    kErrorSuccess = 0,
    kErrorVerificationFailed
  };

  //! Typedef of function pointer used as callback by the write register method.
  typedef void (*WriteRegisterCallbackFunction)(void *cb_arg, Error error);

  //! Typedef of function pointer used as callback by the read register method.
  typedef void (*ReadRegisterCallbackFunction)(void *cb_arg, const uint8_t *read_data,
                                               int read_length, Error error);

  //! Constructor of the icm20948 communication class.
  Communication();

  //! Destructor of the icm20948 communication class.
  ~Communication() = default;

  /*!
   * \brief Writes a value to an icm20948 register.
   *
   * \param register_address   Address of the register to write.
   * \param register_bank      Register bank where the register to write is located.
   * \param value              Value to write to the register.
   * \param verify             If true the register is verified after it was written.
   * \param write_delay        Delay in ms to wait after writing and before verifying the register.
   * \param callback           Callback to be called when the write process is finished.
   * \param callback_argument  Argument to be passed to the callback. May be a context pointer.
   */
  void WriteRegister(uint8_t register_address, RegisterBank register_bank, uint8_t value,
                     bool verify, uint32_t write_delay, WriteRegisterCallbackFunction callback,
                     void *callback_argument);

  /*!
   * \brief Reads a value from an icm20948 register.
   *
   * \param register_address   Address of the register to read.
   * \param register_bank      Register bank where the register to read is located.
   * \param callback           Callback to be called when the read process is finished.
   * \param callback_argument  Argument to be passed to the callback. May be a context pointer.
   */
  void ReadRegister(uint8_t register_address, RegisterBank register_bank,
                    ReadRegisterCallbackFunction callback, void *callback_argument);

  /*!
   * \brief Reads a value from an icm20948 starting at a given register address.
   *
   * \param register_address   Address of the register where to start the read operation.
   * \param register_bank      Register bank where the register to read is located.
   * \param read_length        Number of bytes to read. Should not be greater than the communication
   *                           buffer length - 1.
   * \param callback           Callback to be called when the read process is finished.
   * \param callback_argument  Argument to be passed to the callback. May be a context pointer.
   */
  void ReadRegister(uint8_t register_address, RegisterBank register_bank, int read_length,
                    ReadRegisterCallbackFunction callback, void *callback_argument);

  /*!
   * \brief  Gets the busy state of the ICM20948 communication.
   *
   * \return True if communication is busy, false otherwise.
   */
  bool IsBusy();

 private:
  //! Available communication states.
  enum State {
    kStateIdle,
    kStateSelectingRegisterBank,
    kStateVerifyingRegisterBank,
    kStateReadingRegister,
    kStateWritingRegister,
    kStateWaitingAfterRegisterWrite,
    kStateVerifyingRegister,
  };

  //! Array that maps RegisterBank enum values to the values to be written to the ICM bank select
  //! register.
  static uint8_t register_bank_values_[];

  //! Starts a write operation to the given ICM register.
  void StartRegisterWrite(uint8_t register_address, uint8_t value);

  //! Starts a read operation from the ICM starting at the given register.
  void StartRegisterRead(uint8_t register_address, int read_length);

  //! Starts a register selection operation and handles the state accordingly.
  void SelectRegisterBank();
  void ReadRegister();  //!< Starts a register read operation and handles the state accordingly.
  void WriteRegister();  //!< Starts a register read operation and handles the state accordingly.
  //! If a verify operation should be executed, it is started. Otherwise the finish callback is
  //! executed. The state is handled accordingly.
  void VerifyOrFinish();

  //! This task is called or scheduled by the callbacks when something has to be done and implements
  //! the communication logic.
  static void CallbackTask(Communication *instance);

  //! Callback called when the write delay timer elapses.
  static void TimerCallback(Communication *inst);

  //! Callback called when the SPI transmission is finished.
  static void SpiCallback(void *cb_arg);

  //! SPI slave instance that is handling the SPI communication to the ICM hardware.
  hal::spi::Slave spi_slave_;

  //! Timer used to add a delay between the write and the verify step.
  SoftwareTimer write_delay_timer_;

  //! Write buffer used for SPI transmissions.
  uint8_t write_buffer_[ICM20948_COMMUNICATION_BUFFER_LENGTH] = {0};
  //! Read buffer used for SPI transmissions.
  uint8_t read_buffer_[ICM20948_COMMUNICATION_BUFFER_LENGTH] = {0};

  State state_ = kStateIdle;  //!< Current communication state.
  //! Register bank currently selected on the ICM.
  RegisterBank current_register_bank_ = kRegisterBankUnknown;
  int verification_failures_ = 0;  //!< Counter for the register verification failures.

  //! If true, a write command is executed. If false, a read command is executed.
  bool write_not_read_ = false;
  //! Register address used for the current read or write operation.
  uint8_t register_address_ = 0;
  //! Register bank used for the current read or write operation.
  Communication::RegisterBank register_bank_ = kRegisterBankUnknown;
  uint8_t write_value_ = 0;  //!< Value to write in the current write operation.
  //! Time to wait after write before verification or next step in milliseconds.
  uint32_t write_delay_ = 0;
  bool verify_ = true;  //!< The register value is only verified if true.
  int read_length_ = 1;  //!< Number of bytes to read in the current read operation.
  int current_transceive_length_ = 2;  //!< length of the currently executed transceive operation.

  //! Callback to be called after finishing a write operation.
  WriteRegisterCallbackFunction write_callback_ = nullptr;
  //! Callback to be called after finishing a read operation.
  ReadRegisterCallbackFunction read_callback_ = nullptr;
  void *callback_argument_ = nullptr;  //!< Argument to be passed to the callback.
};
}  // namespace icm20948

#endif  // SRC_DRIVERS_ICM20948_COMMUNICATION_HPP_
