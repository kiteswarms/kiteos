// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "communication.hpp"

#include "core/scheduler.hpp"
#include "core/logger.hpp"
#include "utils/debug_utils.h"


/**************************************************************************************************
 *     Defines                                                                                    *
 **************************************************************************************************/

// port definitions
#define ICM20948_SPI_PORT hal::spi::kPort1
#define ICM20948_CS_GPIO_PORT hal::Gpio::Port::kPortA
#define ICM20948_CS_GPIO_PIN  hal::Gpio::kPin4

// If this bit is set in the first transmitted byte, a register read operation is done
#define ICM20948_READ_BIT 0x80u

// Bank select register, exists on all register banks
#define ICM20948_REG_BANK_SEL 0x7Fu

#define ICM20948_MAX_VERIFICATION_FAILURES 2


namespace icm20948 {
/**************************************************************************************************
 *     Private static member variables                                                            *
 **************************************************************************************************/

uint8_t Communication::register_bank_values_[] = {0u,          // kRegisterBank0
                                                  (1u << 4u),  // kRegisterBank1
                                                  (2u << 4u),  // kRegisterBank2
                                                  (3u << 4u),  // kRegisterBank3
                                                  0u};         // kRegisterBankUnknown


/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/

Communication::Communication() : spi_slave_(ICM20948_SPI_PORT,
                                            { hal::spi::kPhaseSecondEdge,
                                              hal::spi::kPolarityHigh,
                                              hal::spi::kBaud4MHz,
                                              ICM20948_CS_GPIO_PORT, ICM20948_CS_GPIO_PIN },
                                            write_buffer_, read_buffer_, SpiCallback, this) {
  write_delay_timer_.SetCallback(TimerCallback, this);
}

void Communication::WriteRegister(uint8_t register_address,
                                  Communication::RegisterBank register_bank, uint8_t value,
                                  bool verify, uint32_t write_delay,
                                  WriteRegisterCallbackFunction callback, void *callback_argument) {
  ASSERT(state_ == kStateIdle);

  write_not_read_ = true;

  register_address_ = register_address;
  register_bank_ = register_bank;
  write_value_ = value;
  verify_ = verify;
  write_delay_ = write_delay;

  write_callback_ = callback;
  callback_argument_ = callback_argument;

  verification_failures_ = 0;
  if (current_register_bank_ != register_bank_) {
    SelectRegisterBank();
  } else {
    WriteRegister();
  }
}

void Communication::ReadRegister(uint8_t register_address,
                                 Communication::RegisterBank register_bank,
                                 ReadRegisterCallbackFunction callback, void *callback_argument) {
  ReadRegister(register_address, register_bank, 1, callback, callback_argument);
}

void Communication::ReadRegister(uint8_t register_address,
                                 Communication::RegisterBank register_bank, int read_length,
                                 ReadRegisterCallbackFunction callback, void *callback_argument) {
  ASSERT(state_ == kStateIdle);
  ASSERT(read_length <= ICM20948_COMMUNICATION_BUFFER_LENGTH - 1 && read_length >= 1);

  write_not_read_ = false;

  register_address_ = register_address;
  register_bank_ = register_bank;
  read_length_ = read_length;
  read_callback_ = callback;
  callback_argument_ = callback_argument;

  if (current_register_bank_ != register_bank_) {
    verification_failures_ = 0;
    SelectRegisterBank();
  } else {
    ReadRegister();
  }
}

bool Communication::IsBusy() {
  return state_ != kStateIdle;
}

/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/

void Communication::StartRegisterWrite(uint8_t register_address, uint8_t value) {
  write_buffer_[0] = register_address;
  write_buffer_[1] = value;
  current_transceive_length_ = 2;
  ASSERT_RESPONSE(spi_slave_.Transceive(current_transceive_length_), true);
}

void Communication::StartRegisterRead(uint8_t register_address, int read_length) {
  write_buffer_[0] = ICM20948_READ_BIT | register_address;
  current_transceive_length_ = read_length + 1;
  ASSERT_RESPONSE(spi_slave_.Transceive(current_transceive_length_), true);
}

void Communication::SelectRegisterBank() {
  state_ = kStateSelectingRegisterBank;
  StartRegisterWrite(ICM20948_REG_BANK_SEL, register_bank_values_[register_bank_]);
}

void Communication::ReadRegister() {
  state_ = kStateReadingRegister;
  StartRegisterRead(register_address_, read_length_);
}

void Communication::WriteRegister() {
  state_ = kStateWritingRegister;
  StartRegisterWrite(register_address_, write_value_);
}

void Communication::VerifyOrFinish() {
  if (verify_) {
    // Register has to be verified, start reading register for verification
    state_ = kStateVerifyingRegister;
    StartRegisterRead(register_address_, 1);
  } else {
    // Register must not be verified, operation finished, call write callback
    state_ = kStateIdle;
    if (write_callback_ != nullptr) {
      write_callback_(callback_argument_, kErrorSuccess);
    }
  }
}


/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/

void Communication::CallbackTask(Communication *instance) {
  switch (instance->state_) {
    case kStateIdle:
      ASSERT(false);
      break;

    case kStateSelectingRegisterBank:
      // Verify bank select register content after writing it
      instance->state_ = kStateVerifyingRegisterBank;
      instance->StartRegisterRead(ICM20948_REG_BANK_SEL, 1);
      break;

    case kStateVerifyingRegisterBank:
      // Check if register bank was written successfully
      if (instance->read_buffer_[1] == register_bank_values_[instance->register_bank_]) {
        // Writing register bank successful, update current register bank
        instance->current_register_bank_ = instance->register_bank_;
        // Continue with write or read, depending on which of both processes are currently executed
        if (instance->write_not_read_) {
          instance->verification_failures_ = 0;
          instance->WriteRegister();
        } else {
          instance->ReadRegister();
        }
      } else {  // Writing register bank failed
        instance->current_register_bank_ = kRegisterBankUnknown;
        Logger::Report("ICM20948: Register verification failed", Logger::kWarning);
        instance->verification_failures_++;
        if (instance->verification_failures_ > ICM20948_MAX_VERIFICATION_FAILURES) {
          // Maximum number of retries reached, go to error state and call error callback
          instance->state_ = kStateIdle;
          if (instance->write_not_read_) {
            if (instance->write_callback_ != nullptr) {
              instance->write_callback_(instance->callback_argument_, kErrorVerificationFailed);
            }
          } else {
            if (instance->read_callback_ != nullptr) {
              instance->read_callback_(instance->callback_argument_, nullptr, 0,
                                       kErrorVerificationFailed);
            }
          }
        } else {
          // Maximum number of retries not reached yet, retry bank select
          instance->SelectRegisterBank();
        }
      }
      break;

    case kStateReadingRegister:
      // Read finished, call read callback
      instance->state_ = kStateIdle;
      if (instance->read_callback_ != nullptr) {
        instance->read_callback_(instance->callback_argument_, &instance->read_buffer_[1],
                                 instance->read_length_, kErrorSuccess);
      }
      break;

    case kStateWritingRegister:
      // Write finished
      if (instance->write_delay_ > 0) {
        // Wait before next step since write delay is configured
        instance->write_delay_timer_.SetTimeout(instance->write_delay_);
        instance->state_ = kStateWaitingAfterRegisterWrite;
      } else {
        // Execute next step
        instance->VerifyOrFinish();
      }
      break;

    case kStateWaitingAfterRegisterWrite:
      // Write delay elapsed, execute next step
      instance->VerifyOrFinish();
      break;

    case kStateVerifyingRegister:
      // Check if register was written successfully
      if (instance->read_buffer_[1] == instance->write_value_) {
        // writing register succeeded, call write callback
        instance->state_ = kStateIdle;
        if (instance->write_callback_ != nullptr) {
          instance->write_callback_(instance->callback_argument_, kErrorSuccess);
        }
      } else {  // register verification failed
        Logger::Report("ICM20948: Register verification failed", Logger::kWarning);
        instance->verification_failures_++;
        if (instance->verification_failures_ > ICM20948_MAX_VERIFICATION_FAILURES) {
          // Maximum number of retries reached, go to error state and call error callback
          instance->state_ = kStateIdle;
          if (instance->write_callback_ != nullptr) {
            instance->write_callback_(instance->callback_argument_, kErrorVerificationFailed);
          }
        } else {
          // Maximum number of retries not reached yet, retry register write
          instance->WriteRegister();
        }
      }
      break;
  }
}

void Communication::TimerCallback(Communication *inst) {
  ASSERT(inst->state_ == kStateWaitingAfterRegisterWrite);
  CallbackTask(inst);
}

void Communication::SpiCallback(void *cb_arg) {
  auto inst = reinterpret_cast<Communication *>(cb_arg);
  ASSERT(inst->state_ != kStateIdle && inst->state_ != kStateWaitingAfterRegisterWrite);
  Scheduler::EnqueueTask(CallbackTask, inst);
}
}  // namespace icm20948
