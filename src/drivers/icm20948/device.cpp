// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include <cmath>
#include <string>
#include "device.hpp"
#include "utils/debug_utils.h"
#include "core/scheduler.hpp"
#include "core/logger.hpp"


/**************************************************************************************************
 *     Defines                                                                                    *
 **************************************************************************************************/
// port definitions
#define ICM20948_INT_GPIO_PORT hal::Gpio::Port::kPortD
#define ICM20948_INT_GPIO_PIN  hal::Gpio::kPin3

// Time to wait for the ICM hardware to complete its reset procedure in milliseconds.
#define ICM20948_RESET_TIMEOUT 100
// Time to wait for the ICM to wake up in milliseconds.
#define ICM20948_WAKE_UP_TIMEOUT 1
// Time to wait for sensor movements before a steady state is detected after power up.
#define ICM20948_STEADY_STATE_TIMEOUT 1000

// Bank 0 register definitions:
#define ICM20948_WHO_AM_I 0x00u
#define ICM20948_PWR_MGMT_1 0x06u
#define ICM20948_INT_ENABLE_1 0x11u
#define ICM20948_ACCEL_XOUT_H 0x2Du

// Bank 1 register definitions:
#define ICM20948_SELF_TEST_X_GYRO 0x02u
#define ICM20948_SELF_TEST_X_ACCEL 0x0Eu

// Bank 2 register definitions:
#define ICM20948_GYRO_CONFIG_1 0x01u
#define ICM20948_GYRO_CONFIG_2 0x02u
#define ICM20948_ACCEL_CONFIG 0x14u
#define ICM20948_ACCEL_CONFIG_2 0x15u

// Content of the who am i register
#define ICM20948_WHO_AM_I_REGISTER_VALUE 0xEAu

// ICM20948_PWR_MGMT_1 register definitions
// Auto selects the best available clock source – PLL if ready, else use the Internal oscillator 6:
// Internal 20 MHz oscillator.
#define ICM20948_PWR_MGMT_1_CLKSEL_AUTO 0x01u
#define ICM20948_PWR_MGMT_1_DEVICE_RESET 0x80u

// ICM20948_INT_ENABLE_1 register definitions
// Enable raw data ready interrupt from any sensor to propagate to interrupt pin 1.
#define ICM20948_ICM20948_INT_ENABLE_1_RAW_DATA_0_RDY_EN 0x01u


namespace icm20948 {
/**************************************************************************************************
 *     RegisterValue methods                                                                      *
 **************************************************************************************************/

Device::ConfigurationRegister::ConfigurationRegister(
    Communication::RegisterBank register_bank, uint8_t register_address, uint8_t value,
    uint32_t write_delay, bool verify) {
  this->register_bank = register_bank;
  this->register_address = register_address;
  this->value = value;
  this->write_delay = write_delay;
  this->verify = verify;
  this->initialized = false;
}

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
Device::Device(GyroRange gyro_range, GyroBandwidth gyro_bandwidth, AccelRange accel_range,
               AccelBandwidth accel_bandwidth, MeasurementDataCallbackFunction data_callback,
               void *callback_argument)
               : int_gpio_(ICM20948_INT_GPIO_PORT, ICM20948_INT_GPIO_PIN,
                           hal::Gpio::kModeInterruptRising, Device::DataRdyCallback, this) {
  // Get sensitivity values matching to selected gyro and accelerometer ranges
  gyro_sensitivity_ = gyro_sensitivity_values_[gyro_range];
  accelerometer_sensitivity_ = accel_sensitivity_values_[accel_range];

  data_callback_ = data_callback;
  callback_argument_ = callback_argument;

  // Reset the ICM hardware
  register_configuration_.emplace_back(
      Communication::kRegisterBank0, ICM20948_PWR_MGMT_1, ICM20948_PWR_MGMT_1_DEVICE_RESET,
      ICM20948_RESET_TIMEOUT, false);
  // Clear the sleep bit to wake up the device from sleep mode. This register should be configured
  // as first register since some of the other registers cannot be written in sleep mode.
  register_configuration_.emplace_back(
      Communication::kRegisterBank0, ICM20948_PWR_MGMT_1, ICM20948_PWR_MGMT_1_CLKSEL_AUTO,
      ICM20948_WAKE_UP_TIMEOUT, true);
  // Enable raw data ready interrupt
  register_configuration_.emplace_back(
      Communication::kRegisterBank0, ICM20948_INT_ENABLE_1,
      ICM20948_ICM20948_INT_ENABLE_1_RAW_DATA_0_RDY_EN, 0, true);
  // Configure gyro range and bandwidth
  register_configuration_.emplace_back(
      Communication::kRegisterBank2, ICM20948_GYRO_CONFIG_1,
      gyro_range_register_values_[gyro_range] | gyro_bandwidth_register_values_[gyro_bandwidth], 0,
      true);
  // Configure accelerometer range and bandwidth
  register_configuration_.emplace_back(
      Communication::kRegisterBank2, ICM20948_ACCEL_CONFIG,
      accel_range_register_values_[accel_range] | accel_bandwidth_register_values_[accel_bandwidth],
      0, true);

  // Verify who am i register content
  communication_.ReadRegister(ICM20948_WHO_AM_I, Communication::kRegisterBank0,
                              ReadRegisterCallback, this);
}


/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/

void Device::StartSensorRead() {
  communication_.ReadRegister(ICM20948_ACCEL_XOUT_H, Communication::kRegisterBank0, 14,
                              ReadRegisterCallback, this);
}

void Device::ParseSensorData(double *accel, double *gyro, bool increment) {
  for (int i = 0; i < 3; i++) {
    if (!increment) {
      accel[i] = 0.0;
      gyro[i] = 0.0;
    }
    // Calculate measurement values from raw sensor data
    accel[i] += static_cast<double>(((int16_t)((
        read_data_[2 * i] << 8u) | read_data_[2 * i + 1]))) / accelerometer_sensitivity_;
    gyro[i] += static_cast<double>(((int16_t)((
        read_data_[2 * i + 6] << 8u) | read_data_[2 * i + 7]))) / gyro_sensitivity_;
  }
}

void Device::ParseSensorData(double *accel, double *gyro, double *temperature, bool increment) {
  ParseSensorData(accel, gyro, increment);
  *temperature =  (static_cast<double>(((int16_t)((read_data_[12] << 8u) | read_data_[13])))
                   / temperature_sensitivity_value_) + temperature_offset_value_;
}

void Device::RunSelfTest() {
  switch (self_test_.state) {
    case SelfTest::kStateNotStarted:
      // Read factory self test gyro results
      communication_.ReadRegister(ICM20948_SELF_TEST_X_GYRO, Communication::kRegisterBank1, 3,
                                  ReadRegisterCallback, this);
      self_test_.state = SelfTest::kStateReadingGyroFactorySelfTestValues;
      break;

    case SelfTest::kStateReadingGyroFactorySelfTestValues:
      // Calculate actual angular velocity values from factory self test values
      for (int i = 0; i < 3; i++) {
        self_test_.factory_self_test_gyro[i] =
            (2620.0 / (pow(2.0, 3.0))) * pow(1.01, read_data_[i] - 1.0) /
            gyro_sensitivity_values_[kGyroRange2000Dps];
      }
      // Read factory self test accelerometer results
      communication_.ReadRegister(ICM20948_SELF_TEST_X_ACCEL, Communication::kRegisterBank1, 3,
                                  ReadRegisterCallback, this);
      self_test_.state = SelfTest::kStateReadingAccelFactorySelfTestValues;
      break;

    case SelfTest::kStateReadingAccelFactorySelfTestValues:
      // Calculate actual acceleration values from factory self test values
      for (int i = 0; i < 3; i++) {
        self_test_.factory_self_test_accel[i] =
            (2620.0 / (pow(2.0, 3.0))) * pow(1.01, read_data_[i] - 1.0) /
            accel_sensitivity_values_[kAccelRange16G];
      }
      // Enable gyro self test (this enables an on chip actuator generating a fake angular
      // velocity)
      communication_.WriteRegister(ICM20948_GYRO_CONFIG_2, Communication::kRegisterBank2, 0x38u,
                                   true, 0, WriteRegisterCallback, this);
      self_test_.state = SelfTest::kStateEnablingGyroSelfTest;
      break;

    case SelfTest::kStateEnablingGyroSelfTest:
      // Enable accel self test (this enables an on chip actuator generating a fake
      // acceleration). Wait after enabling to ensure the values are settled to a steady state.
      communication_.WriteRegister(ICM20948_ACCEL_CONFIG_2, Communication::kRegisterBank2, 0x1Cu,
                                   true, 80, WriteRegisterCallback, this);
      self_test_.state = SelfTest::kStateEnablingAccelSelfTest;
      break;

    case SelfTest::kStateEnablingAccelSelfTest:
      // Initialize self test measurements. The next call of this method will be caused by the
      // data ready interrupt.
      self_test_.measurement_cnt = 0;
      self_test_.state = SelfTest::kStateWaitingForDataWithSelfTestEnabled;
      break;

    case SelfTest::kStateWaitingForDataWithSelfTestEnabled:
      // Read measurement values
      StartSensorRead();
      self_test_.state = SelfTest::kStatePendingWithSelfTestEnabled;
      break;

    case SelfTest::kStatePendingWithSelfTestEnabled:
      // Sum up the values of 100 measurements
      ParseSensorData(self_test_.accel_measurements_self_test_enabled,
                      self_test_.gyro_measurements_self_test_enabled, true);
      self_test_.measurement_cnt++;

      // Check if all 100 measurements were taken
      if (self_test_.measurement_cnt < 100) {
        self_test_.state = SelfTest::kStateWaitingForDataWithSelfTestEnabled;
      } else {
        // All measurements taken, disable gyro self test
        communication_.WriteRegister(ICM20948_GYRO_CONFIG_2, Communication::kRegisterBank2, 0x00u,
                                     true, 0, WriteRegisterCallback, this);
        self_test_.state = SelfTest::kStateDisablingGyroSelfTest;
      }
      break;

    case SelfTest::kStateDisablingGyroSelfTest:
      // Disable accel self test. Wait after disabling to ensure the values are settled to a
      // steady state.
      communication_.WriteRegister(ICM20948_ACCEL_CONFIG_2, Communication::kRegisterBank2, 0x00u,
                                   true, 80, WriteRegisterCallback, this);
      self_test_.state = SelfTest::kStateDisablingAccelSelfTest;
      break;

    case SelfTest::kStateDisablingAccelSelfTest:
      // Initialize self test reference measurements. The next call of this method will be
      // caused by the data ready interrupt.
      self_test_.measurement_cnt = 0;
      self_test_.state = SelfTest::kStateWaitingForDataWithSelfTestDisabled;
      break;

    case SelfTest::kStateWaitingForDataWithSelfTestDisabled:
      // Read measurement values
      StartSensorRead();
      self_test_.state = SelfTest::kStatePendingWithSelfTestDisabled;
      break;

    case SelfTest::kStatePendingWithSelfTestDisabled:
      // Sum up the values of 100 measurements
      ParseSensorData(self_test_.accel_measurements_self_test_disabled,
                      self_test_.gyro_measurements_self_test_disabled, true);
      self_test_.measurement_cnt++;

      // Check if all 100 measurements were taken
      if (self_test_.measurement_cnt < 100) {
        self_test_.state = SelfTest::kStateWaitingForDataWithSelfTestDisabled;
      } else {
        // All measurements taken, calculate self test results
        double self_test_accel[3], self_test_gyro[3],
            self_test_error_accel[3], self_test_error_gyro[3];
        for (int i = 0; i < 3; i++) {
          self_test_accel[i] = (self_test_.accel_measurements_self_test_enabled[i] -
                                self_test_.accel_measurements_self_test_disabled[i]) / 100;
          self_test_gyro[i] = (self_test_.gyro_measurements_self_test_enabled[i] -
                               self_test_.gyro_measurements_self_test_disabled[i]) / 100;
          // Calculate self test errors compared to factory self test in percent
          self_test_error_accel[i] =
              (self_test_accel[i] / self_test_.factory_self_test_accel[i] - 1.0) * 100.0;
          self_test_error_gyro[i] =
              (self_test_gyro[i] / self_test_.factory_self_test_gyro[i] - 1.0) * 100.0;
        }

        // Publish self test results
        char log_message[97];
        snprintf(log_message, sizeof(log_message),
                 "ICM20948: Gyro self test errors gyro: "
                 "(%.1f%%, %.1f%%, %.1f%%), accel: (%.1f%%, %.1f%%, %.1f%%).",
                 self_test_error_gyro[0], self_test_error_gyro[1], self_test_error_gyro[2],
                 self_test_error_accel[0], self_test_error_accel[1], self_test_error_accel[2]);
        Logger::Report(log_message, Logger::kInfo);
        if (fabs(self_test_error_gyro[0]) > 5.0 || fabs(self_test_error_gyro[1]) > 5.0 ||
            fabs(self_test_error_gyro[2]) > 5.0 || fabs(self_test_error_accel[0]) > 5.0 ||
            fabs(self_test_error_accel[1]) > 5.0 || fabs(self_test_error_accel[2]) > 5.0) {
          Logger::Report("ICM20948: Self test failed.", Logger::kWarning);
          state_ = kStateError;
        } else {
          state_ = kStateIdle;
        }
      }
      break;
  }
}


/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/

void Device::RunTask(Device *instance) {
  instance->run_task_enqueued_ = false;

  switch (instance->state_) {
    case kStateReadingWhoAmI:
      // Verify who am i register value
      if (instance->read_data_[0] != ICM20948_WHO_AM_I_REGISTER_VALUE) {
        instance->state_ = kStateError;
        Logger::Report("ICM20948: Read unexpected who am I register value.", Logger::kWarning);
        break;
      }
      instance->state_ = kStateRegisterConfig;

    case kStateRegisterConfig:
      // Configure next uninitialized register
      for (auto & i : instance->register_configuration_) {
        if (!i.initialized) {
          instance->communication_.WriteRegister(i.register_address, i.register_bank, i.value,
                                                 i.verify, i.write_delay, WriteRegisterCallback,
                                                 instance);
          i.initialized = true;
          return;
        }
      }

      // Set timer to detect steady state
      instance->steady_state_timer_.SetTimeout(ICM20948_STEADY_STATE_TIMEOUT);
      instance->state_ = kStateWaitingForSteadyState;

    case kStateWaitingForSteadyState:
    {
      // Calculate measurement values from raw sensor data
      double accel[3], gyro[3];
      instance->ParseSensorData(accel, gyro, false);

      // Reset timer if device was moved
      if (fabs(accel[0]) > 11.0 || fabs(accel[1]) > 11.0 || fabs(accel[2]) > 11.0 ||
          fabs(gyro[0]) > 0.1 || fabs(gyro[1]) > 0.1 || fabs(gyro[2]) > 0.1) {
        instance->steady_state_timer_.SetTimeout(ICM20948_STEADY_STATE_TIMEOUT);
      }

      // Take new measurement or go to self test state if ICM was not moved for a time specified in
      // steady state timeout. Waiting for the imu being not moved is necessary to get meaningful
      // results from the following self test.
      if (instance->steady_state_timer_.Elapsed()) {
        instance->state_ = kStateSelfTest;
      } else {
        instance->StartSensorRead();
        return;
      }
    }

    case kStateSelfTest:
      instance->RunSelfTest();
      break;

    case kStateIdle:
      // Measurement data ready, read sensor values
      instance->measurement_data_.timestamp = instance->timestamp_;
      if (!instance->communication_.IsBusy()) {
        instance->state_ = kStatePending;
        instance->StartSensorRead();
      }
      break;

    case kStatePending:
      // Calculate measurements in output format from sensor values and hand them over to the data
      // callback
      instance->ParseSensorData(instance->measurement_data_.acceleration,
                                instance->measurement_data_.gyro,
                                &instance->measurement_data_.temperature, false);
      instance->data_callback_(instance->measurement_data_, instance->callback_argument_);
      instance->state_ = kStateIdle;
      break;
    case kStateError:
      break;
  }
}

void Device::WriteRegisterCallback(void *cb_arg, Communication::Error error) {
  auto *inst = reinterpret_cast<Device *>(cb_arg);

  ASSERT(inst->state_ == kStateRegisterConfig || inst->state_ == kStateSelfTest);

  if (error != Communication::kErrorSuccess) {
    inst->state_ = kStateError;
    Logger::Report("ICM20948: Device is not responding.", Logger::kWarning);
    return;
  }

  RunTask(inst);
}

void Device::ReadRegisterCallback(void *cb_arg, const uint8_t *read_data, int read_length,
                                  Communication::Error error) {
  auto *inst = reinterpret_cast<Device *>(cb_arg);

  ASSERT(inst->state_ == kStateReadingWhoAmI || inst->state_ == kStateWaitingForSteadyState ||
         inst->state_ == kStateSelfTest || inst->state_ == kStatePending);

  if (error != Communication::kErrorSuccess) {
    inst->state_ = kStateError;
    Logger::Report("ICM20948: Device is not responding.", Logger::kWarning);
    return;
  }

  // Copy measurement data to member variable before calling the run task
  for (int i = 0; i < read_length; i++) {
    inst->read_data_[i] = read_data[i];
  }
  RunTask(inst);
}

void Device::DataRdyCallback(void *inst_ptr) {
  auto *inst = reinterpret_cast<Device *>(inst_ptr);
  if (inst->state_ == kStateIdle ||
      (inst->state_ == kStateSelfTest &&
       (inst->self_test_.state == SelfTest::kStateWaitingForDataWithSelfTestEnabled ||
        inst->self_test_.state == SelfTest::kStateWaitingForDataWithSelfTestDisabled))) {
    // Timestamp is taken in the interrupt to en sure it is as accurate as possible
    inst->timestamp_ = SystemTime::GetTimestamp();
    if (!inst->run_task_enqueued_) {
      inst->run_task_enqueued_ = true;
      Scheduler::EnqueueTask(RunTask, inst);
    }
  }
}
}  // namespace icm20948
