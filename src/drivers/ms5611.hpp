// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_MS5611_HPP_
#define SRC_DRIVERS_MS5611_HPP_

#include "hal/i2c/slave.hpp"
#include "core/software_timer.hpp"

/*!
 *  \brief Implements a driver to access the MS5611 barometer via I2C.
 */
class Ms5611 {
 public:
  //! Bandwidth options for the ADC.
  enum Bandwidth {
    kBandwidth256,
    kBandwidth512,
    kBandwidth1024,
    kBandwidth2048,
    kBandwidth4096
  };

  //! Typedef of function pointer used as callback when a measurement is ready. Pressure is passed
  //! in Pa and temperature is passed in °C.
  typedef void (*MeasurementCallback)(double pressure, double temperature,
                                      void *callback_argument);
  /*!
   * \brief Constructor of the MS5611 component.
   *
   * \param i2c_address_lsb       LSB of the slave I2C address.
   * \param measurement_callback  Callback to be called when a measurement is ready.
   * \param callback_argument     Argument to be passed to the measurement callback.
   */
  Ms5611(bool i2c_address_lsb, MeasurementCallback measurement_callback, void *callback_argument);

  /*!
   * \brief Destructor of the MS5611 component.
   */
  ~Ms5611() = default;

  /*!
   * \brief Getter for the bandwidth configuration.
   */
  Bandwidth getBandwidth() { return bandwidth_; }

  /*!
   * \brief Setter for the bandwidth configuration.
   */
  void setBandwidth(Bandwidth bw) { bandwidth_ = bw; }

 private:
  //! Possible states of the MS5611 driver.
  enum State {
    kStateReset,
    kStatePromRead,
    kStateIdle,
    kStateConvertPressure,
    kStateAdcReadPressure,
    kStateConvertTemperature,
    kStateAdcReadTemperature
  };

  //! Structure containing all PROM values and their current readout state.
  union Prom {
    uint16_t array_[8];  //!< 16 bit array for access through index.
    struct {
      uint16_t C0;  //!< 16 bit reserved for manufacturer.
      uint16_t C1;  //!< Pressure sensitivity.
      uint16_t C2;  //!< Pressure offset.
      uint16_t C3;  //!< Temperature coefficient of pressure sensitivity.
      uint16_t C4;  //!< Temperature coefficient of pressure offset.
      uint16_t C5;  //!< Reference temperature.
      uint16_t C6;  //!< Temperature coefficient of the temperature.
    };  //!< Calibration parameter names from datasheet.
    struct {
      uint16_t manufacturer;  //!< 16 bit reserved for manufacturer.
      uint16_t sens_t1;       //!< Pressure sensitivity.
      uint16_t off_t1;        //!< Pressure offset.
      uint16_t tcs;           //!< Temperature coefficient of pressure sensitivity.
      uint16_t tco;           //!< Temperature coefficient of pressure offset.
      uint16_t t_ref;         //!< Reference temperature.
      uint16_t tempsens;      //!< Temperature coefficient of the temperature.
      uint16_t crc;           //!< CRC used to check data validity in memory.
      uint8_t read_index;     //!< Index of the PROM value which is currently read.
    };  //!< Calibration parameter functional names.
  };

  //! Callback called when measurement data is available.
  MeasurementCallback measurement_callback_;
  void *callback_argument_;       //!< Callback arbument passed to the measurement callback.
  hal::i2c::Slave i2c_slave_;   //!< I2C slave instance handling the I2C communication.
  uint8_t tx_buffer_[1] = {0};  //!< TX buffer used for I2C transmissions.
  uint8_t rx_buffer_[3] = {0};  //!< RX buffer used for I2C receptions.
  State state_ = kStateReset;   //!< Current state of the driver.
  Prom prom_;  //!< Structure containing all PROM values and their current readout state.
  Bandwidth bandwidth_;              //!< ADC bandwidth setting.
  uint32_t raw_pressure_ = 0;        //!< Raw pressure value read from the hardware.
  uint32_t raw_temperature_ = 0;     //!< Raw temperature value read from the hardware.
  SoftwareTimer interval_timer_;     //!< Timer used to periodically trigger measurements.
  SoftwareTimer delay_timer_;        //!< Timer that triggers on a timeout.

  void CommandBandwidth(uint8_t cmd);  //!< Sends the D1/D2 commands with bandwith bits.
  void Reset();               //!< Starts the transmission of a reset command.
  void PromReadNext();        //!< Starts the transmission of a PROM read command.
  void ConvertPressure();     //!< Starts the transmission of a convert pressure command.
  void ConvertTemperature();  //!< Starts the transmission of a convert temperature command.
  void AdcRead();             //!< Starts the transmission of an ADC read command.

  bool PromCrcCheck();        //!< Computed the CRC for the PROM data.

  //! Callback which is called when the timer delay was reached.
  static void DelayTimerCallback(Ms5611 *instance);
  //! Callback called when the interval timer elapsed.
  static void IntervalTimerCallback(Ms5611* instance);
  //! Function that invokes the user callback providing the new measurement.
  static void InvokeUserCallbackTask(Ms5611* instance);

  //! Main task running the driver state machine.
  static void RunTask(Ms5611 *instance);
  //! Callback called when an I2C transceive operation finished.
  static void I2CCallback(hal::i2c::Error error, void* cb_arg);
};

#endif  // SRC_DRIVERS_MS5611_HPP_
