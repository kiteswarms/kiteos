// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_MPL3115A2_HPP_
#define SRC_DRIVERS_MPL3115A2_HPP_

#include "core/software_timer.hpp"
#include "hal/gpio.hpp"
#include "hal/i2c/slave.hpp"

/*!
 *  \brief  Implements the Mpl3115a2 driver.
 */
class Mpl3115a2 {
 public:
  //! callback that is called if all channels are finished reading.
  typedef void (*DataReadyCallback)(double pressure, double temperature, void *ctx);

  /*!
   * \brief  Constructor of the Mpl3115a2-Component.
   *
   * \param  cb   callback that is called if all channels are finished reading.
   * \param  ctx  instance pointer.
   *
   * \return Returns constructed Mpl3115a2-instance.
   */
  Mpl3115a2(Mpl3115a2::DataReadyCallback cb, void *ctx);

   /*!
   * \brief Destructor of the Mpl3115a2-Component.
   */
  ~Mpl3115a2() = default;

 private:
  //! Available MPL-states.
  enum State{
    kMplStateReset,
    kMplStateConfig,
    kMplStateIdle,
    kMplStateDataReadyToPull,
    kMplStateDataPullPending,
  };

  //! Structure defining a configuration value.
  struct ConfigurationRegister{
    uint8_t reg_addr_;  //!< the register address to be configured
    uint8_t value_;     //!< the value of the register
  };
  static ConfigurationRegister config_array_[];  //!< Array holding configuration values.

  //! Runs the configuration.
  void Config();
  //! Starts a new conversion (should be called periodically)
  void StartConversion();

  //! Callback which is called when the reset has been completed.
  static void ResetFinishedCallback(Mpl3115a2 *inst);
  //! Main Task enqueued periodically calling the Run().
  static void PullDataTask(Mpl3115a2* instance);
  //! Function that invokes the user callback providing the new measurement.
  static void InvokeUserCallbackTask(Mpl3115a2* instance);
  //! Callback which is called when there is data ready to read from the ICM20948.
  static void DataRdyCallback(void *inst_ptr);
  //! Callback called when an I2C transmission is finished.
  static void I2cCallback(hal::i2c::Error error, void *cb_arg);

  hal::i2c::Slave slave_;  //!< Slave used for I2C transmissions
  hal::Gpio int_gpio_;     //!< Gpio pin used for the data ready interrupt.
  DataReadyCallback data_ready_callback_;  //!< instance of DataReadyCallback
  void* cb_arg_;                    //!< Argument provided to callback.
  uint8_t config_index_;            //!< Index of the current config value.
  State state_;                     //!< Current state of the Mpl3115A2-instance.
  uint8_t send_buffer_[2];          //!< Output buffer used for i2c-communication.
  uint8_t receive_buffer_[6];       //!< Input buffer used for i2c-communication.
  double pressure_;                 //!< Latest pressure measurement [Pa].
  double temperature_;              //!< Latest temperature measurement [degree Celsius].
  uint64_t timestamp_;              //!< Timestamp of the latest data ready interrupt.
  SoftwareTimer reset_timer_;       //!< Timer that triggers on a timeout.
};

#endif  // SRC_DRIVERS_MPL3115A2_HPP_
