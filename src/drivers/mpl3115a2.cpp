// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "mpl3115a2.hpp"
#include "core/logger.hpp"
#include "core/system_time/system_time.hpp"
#include "core/scheduler.hpp"

#define MPL_POR_DELAY 60        // Time in ms how long to wait for reset.

#define MPL3115A2_SLAVE_ADDR 0x60  // Slave Address on the i2c-bus.


#define MPL_REG_STATUS 0x00  // Address of status registers
#define MPL_REG_DR_STATUS 0x06

#define MPL_REG_OUT_P_MSB 0x01  // Address of the register holding the measurements
#define MPL_REG_DATA_CFG 0x13   // Address of registers required for configuration
#define MPL_REG_CTRL1 0x26
#define MPL_REG_CTRL2 0x27
#define MPL_REG_CTRL3 0x28
#define MPL_REG_CTRL4 0x29
#define MPL_REG_CTRL5 0x2A

// Values to apply to the DATA_CFG register
#define MPL_DATA_CFG_TDEFE 0x01  // Raise event flag on new temperature data
#define MPL_DATA_CFG_PDEFE 0x02  // Raise event flag on new pressure/altitude data
#define MPL_DATA_CFG_DREM 0x04   // Generate data ready event flag on new data

// Values to apply specifically to the CTRL1 register
#define MPL_CTRL1_SBYB 0x01
#define MPL_CTRL1_OST 0x02
#define MPL_CTRL1_RST 0x04
#define MPL_CTRL1_OS_6MS 0x00
#define MPL_CTRL1_OS_10MS 0x08
#define MPL_CTRL1_OS_18MS 0x10
#define MPL_CTRL1_OS_34MS 0x18
#define MPL_CTRL1_OS_66MS 0x20
#define MPL_CTRL1_OS_512MS 0x38

#define MPL_CTRL2_LOAD_OUTPUT 0x20
#define MPL_CTRL2_ALARM_SEL 0x10

// Values to apply specifically to the CTRL3 register
#define MPL_CTRL3_IPOL1 0x20
#define MPL_CTRL3_PP_OD1 0x10
#define MPL_CTRL3_IPOL2 0x02
#define MPL_CTRL3_PP_OD2 0x01

// Interrupt enable masking values
#define MPL_INT_CFG_DRDY 0x80
#define MPL_INT_EN_DRDY 0x80

#define MPL_CTRL1_CONFIG (MPL_CTRL1_OST | MPL_CTRL1_OS_6MS)

Mpl3115a2::ConfigurationRegister Mpl3115a2::config_array_[] = {
      {MPL_REG_DATA_CFG, MPL_DATA_CFG_TDEFE | MPL_DATA_CFG_PDEFE | MPL_DATA_CFG_DREM},
      // {MPL_REG_CTRL3, MPL_CTRL3_PP_OD1},  // Set interrupt polarity to active
      // low (open drain)
      {MPL_REG_CTRL4, MPL_INT_EN_DRDY},   // enable data ready interrupt
      {MPL_REG_CTRL5, MPL_INT_CFG_DRDY},  // route data ready interrupt to "int1"
      {MPL_REG_CTRL1, MPL_CTRL1_CONFIG},  // Start conversion with given timing
};

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
Mpl3115a2::Mpl3115a2(Mpl3115a2::DataReadyCallback cb, void *cb_arg)
  : slave_(hal::i2c::kPort4, MPL3115A2_SLAVE_ADDR,
           send_buffer_, receive_buffer_, I2cCallback, this),
    int_gpio_(hal::Gpio::kPortE, hal::Gpio::kPin4, hal::Gpio::kModeInterruptFalling,
              Mpl3115a2::DataRdyCallback, this),
    data_ready_callback_(cb),
    cb_arg_(cb_arg),
    config_index_(0) {
  state_ = kMplStateReset;
  // Reset the device (also resets i2c interface)
  send_buffer_[0] = MPL_REG_CTRL1;
  send_buffer_[1] = MPL_CTRL1_RST;
  slave_.Transceive(2, 0);

  reset_timer_.SetCallback(ResetFinishedCallback, this);
  reset_timer_.SetTimeout(MPL_POR_DELAY);
}

/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
void Mpl3115a2::Config() {
  if (config_index_ < (sizeof(config_array_) / sizeof(config_array_[0]))) {
    send_buffer_[0] = config_array_[config_index_].reg_addr_;
    send_buffer_[1] = config_array_[config_index_].value_;
    config_index_++;
    slave_.Transceive(2, 0);
  } else {
    state_ = kMplStateIdle;
    StartConversion();
  }
}

void Mpl3115a2::StartConversion() {
  if (state_ == kMplStateIdle) {
    // Start next conversion
    send_buffer_[0] = MPL_REG_CTRL1;
    send_buffer_[1] = MPL_CTRL1_CONFIG;
    slave_.Transceive(2, 0);
  }
}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void Mpl3115a2::ResetFinishedCallback(Mpl3115a2 *inst) {
  inst->state_ = kMplStateConfig;
  inst->Config();
}

void Mpl3115a2::PullDataTask(Mpl3115a2* inst) {
  // Start reading at STATUS register to also clear the interrupt flag
  inst->send_buffer_[0] = MPL_REG_STATUS;
  inst->state_ = kMplStateDataPullPending;
  inst->slave_.Transceive(1, 6);
}

void Mpl3115a2::InvokeUserCallbackTask(Mpl3115a2* inst) {
  inst->state_ = kMplStateIdle;
  uint8_t *p = &(inst->receive_buffer_[1]);
  // Pressure in Pascal: Fixed point Q18.2
  double result = static_cast<double>(((uint32_t)p[0] << 10u) |
                                      ((uint32_t)p[1] << 2u)  |
                                      ((uint32_t)p[2] >> 6u));
  result += static_cast<double>((p[2] >> 4) & 0x3u) / 4.0;
  inst->pressure_ = result;
  result = static_cast<double>((int8_t)p[3]);
  result += static_cast<double>(p[4] >> 4) / 16.0;
  inst->temperature_ = result;
  inst->data_ready_callback_(inst->pressure_, inst->temperature_, inst->cb_arg_);
  inst->StartConversion();
}

void Mpl3115a2::DataRdyCallback(void *inst_ptr) {
  auto *inst = reinterpret_cast<Mpl3115a2 *>(inst_ptr);
  // Get the timestamp of the interrupt occurrence
  inst->timestamp_ = SystemTime::GetTimestamp();
  inst->state_ = kMplStateDataReadyToPull;
  Scheduler::EnqueueTask(PullDataTask, inst);
}

void Mpl3115a2::I2cCallback(hal::i2c::Error error, void *cb_arg) {
  auto inst = reinterpret_cast<Mpl3115a2*>(cb_arg);
  if (error != hal::i2c::kErrorNone) {
    Logger::Report("I2C Error", Logger::kError);
  } else if (inst->state_ == kMplStateConfig) {
    inst->Config();
  } else if (inst->state_ == kMplStateDataPullPending) {
    Scheduler::EnqueueTask(InvokeUserCallbackTask, inst);
  }
}
