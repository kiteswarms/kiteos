// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "sht25.hpp"
#include "core/logger.hpp"
#include "core/system_time/system_time.hpp"
#include "core/scheduler.hpp"


#define SHT25_SLAVE_ADDR 0x40  // Slave Address on the i2c-bus.

// Define sensor reset delay in ms.
#define SHT25_RESET_DELAY 15
// Define temperature measure delay in ms.
#define SHT25_MEASURE_DELAY_TEMP 85
// Define humidity measure delay in ms.
#define SHT25_MEASURE_DELAY_HUMI 29

// Command for soft reset of the sensor. Binary representation from the datasheet: \b 0b11111110
#define SHT25_CMD_SOFT_RESET 0xFE
// Command for triggering a temperature measurement of the sensor in no hold mode. Repeated reads
// during the measuring time (66-85 ms) are disacknowledged. Binary representation from the
// datasheet: \b 0b11110011 */
#define SHT25_CMD_TRIG_T_MEASUREMENT_NOHOLD 0xF3
// Command for triggering a relative humidity measurement of the sensor in no hold mode. Repeated
// reads during the measuring time (22-29 ms ms) are disacknowledged. Binary representation from the
// datasheet: \b 0b11110101 */
#define SHT25_CMD_TRIG_H_MEASUREMENT_NOHOLD 0xF5

const uint8_t Sht25::kCrcTable[256] = { 0, 49,
  98, 83, 196, 245, 166, 151, 185, 136, 219,
  234, 125, 76, 31, 46, 67, 114, 33, 16, 135,
  182, 229, 212, 250, 203, 152, 169, 62, 15,
  92, 109, 134, 183, 228, 213, 66, 115, 32,
  17, 63, 14, 93, 108, 251, 202, 153, 168,
  197, 244, 167, 150, 1, 48, 99, 82, 124, 77,
  30, 47, 184, 137, 218, 235, 61, 12, 95,
  110, 249, 200, 155, 170, 132, 181, 230,
  215, 64, 113, 34, 19, 126, 79, 28, 45, 186,
  139, 216, 233, 199, 246, 165, 148, 3, 50,
  97, 80, 187, 138, 217, 232, 127, 78, 29,
  44, 2, 51, 96, 81, 198, 247, 164, 149, 248,
  201, 154, 171, 60, 13, 94, 111, 65, 112,
  35, 18, 133, 180, 231, 214, 122, 75, 24,
  41, 190, 143, 220, 237, 195, 242, 161, 144,
  7, 54, 101, 84, 57, 8, 91, 106, 253, 204,
  159, 174, 128, 177, 226, 211, 68, 117, 38,
  23, 252, 205, 158, 175, 56, 9, 90, 107, 69,
  116, 39, 22, 129, 176, 227, 210, 191, 142,
  221, 236, 123, 74, 25, 40, 6, 55, 100, 85,
  194, 243, 160, 145, 71, 118, 37, 20, 131,
  178, 225, 208, 254, 207, 156, 173, 58, 11,
  88, 105, 4, 53, 102, 87, 192, 241, 162,
  147, 189, 140, 223, 238, 121, 72, 27, 42,
  193, 240, 163, 146, 5, 52, 103, 86, 120,
  73, 26, 43, 188, 141, 222, 239, 130, 179,
  224, 209, 70, 119, 36, 21, 59, 10, 89, 104,
  255, 206, 157, 172};

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
Sht25::Sht25(Sht25::DataReadyCallback cb, void *cb_arg)
  : slave_(hal::i2c::kPort4, SHT25_SLAVE_ADDR,
           send_buffer_, receive_buffer_, ResetStartedCallback, this),
    data_ready_callback_(cb),
    cb_arg_(cb_arg) {
  // Reset the device (also resets i2c interface)
  send_buffer_[0] = SHT25_CMD_SOFT_RESET;
  slave_.Transceive(1, 0, ResetStartedCallback, this);
}

/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
bool Sht25::CrcCheck() {
  uint8_t crc = 0;
  // Divide the message by the polynomial, a byte at a time.
  crc = kCrcTable[crc ^ receive_buffer_[0]];
  crc = kCrcTable[crc ^ receive_buffer_[1]];
  // The final remainder is the CRC.
  return (crc == receive_buffer_[2]);
}

void Sht25::StartTempConv(Sht25* inst) {
  // Start next conversion
  inst->send_buffer_[0] = SHT25_CMD_TRIG_T_MEASUREMENT_NOHOLD;
  inst->slave_.Transceive(1, 0, TempConvStartedCallback, inst);
}

void Sht25::StartHumiConv(Sht25* inst) {
  // Start next conversion
  inst->send_buffer_[0] = SHT25_CMD_TRIG_H_MEASUREMENT_NOHOLD;
  inst->slave_.Transceive(1, 0, HumiConvStartedCallback, inst);
}

void Sht25::PullTemperatureData(Sht25* inst) {
  inst->slave_.Transceive(0, 3, TempDataPulledCallback, inst);
}

void Sht25::PullHumidityData(Sht25* inst) {
  inst->slave_.Transceive(0, 3, HumiDataPulledCallback, inst);
}

void Sht25::InvokeUserCallbackTask(Sht25* inst) {
  inst->data_ready_callback_(inst->measurement_data_, inst->cb_arg_);
  StartTempConv(inst);
}
/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void Sht25::ResetStartedCallback(hal::i2c::Error error, void *cb_arg) {
  auto inst = reinterpret_cast<Sht25*>(cb_arg);
  if (error != hal::i2c::kErrorNone) {
    Logger::Report("I2C Error", Logger::kError);
  }
  inst->update_timer_.SetCallback(StartTempConv, inst);
  inst->update_timer_.SetTimeout(SHT25_RESET_DELAY);
}

void Sht25::TempConvStartedCallback(hal::i2c::Error error, void *cb_arg) {
  auto inst = reinterpret_cast<Sht25*>(cb_arg);
  if (error != hal::i2c::kErrorNone) {
    Logger::Report("I2C Error", Logger::kError);
  }
  inst->update_timer_.SetCallback(PullTemperatureData, inst);
  inst->update_timer_.SetTimeout(SHT25_MEASURE_DELAY_TEMP);
}

void Sht25::HumiConvStartedCallback(hal::i2c::Error error, void *cb_arg) {
  auto inst = reinterpret_cast<Sht25*>(cb_arg);
  if (error != hal::i2c::kErrorNone) {
    Logger::Report("I2C Error", Logger::kError);
  }
  inst->update_timer_.SetCallback(PullHumidityData, inst);
  inst->update_timer_.SetTimeout(SHT25_MEASURE_DELAY_HUMI);
}

void Sht25::TempDataPulledCallback(hal::i2c::Error error, void *cb_arg) {
  auto inst = reinterpret_cast<Sht25*>(cb_arg);
  if (error != hal::i2c::kErrorNone) {
    Logger::Report("I2C Error", Logger::kError);
  }
  // Datasheet Temperature in Celsius: T = -46.85 + 175.72 * (S_t/2^16)
  if (!inst->CrcCheck()) {
    Logger::Report("CRC check failed!", Logger::kWarning);
  }
  inst->measurement_data_.temperature_timestamp = SystemTime::GetTimestamp();
  inst->measurement_data_.temperature =
    -46.85 + (175.72 / 65536) * ((inst->receive_buffer_[0] << 8u) |
                                 (inst->receive_buffer_[1] & 0xFCu));

  Scheduler::EnqueueTask(StartHumiConv, inst);
}

void Sht25::HumiDataPulledCallback(hal::i2c::Error error, void *cb_arg) {
  auto inst = reinterpret_cast<Sht25*>(cb_arg);
  if (error != hal::i2c::kErrorNone) {
    Logger::Report("I2C Error", Logger::kError);
  }
  // Datasheet relative Humidity in %: RH = -6 + 125 * (S_rh/2^16)
  if (!inst->CrcCheck()) {
    Logger::Report("CRC check failed!", Logger::kWarning);
  }
  inst->measurement_data_.humidity_timestamp = SystemTime::GetTimestamp();
  inst->measurement_data_.humidity =
     -6 + (125.0 / 65536) * ((inst->receive_buffer_[0] << 8u) |
                             (inst->receive_buffer_[1] & 0xFCu));
  Scheduler::EnqueueTask(InvokeUserCallbackTask, inst);
}
