// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "max11040k.hpp"
#include <algorithm>
#include "core/scheduler.hpp"
#include "utils/debug_utils.h"

#define MAX11040K_READ_FLAG 0x80
// Addresses for MPU-registers.
#define MAX11040K_REG_SAMPLING_INSTANT_CONTROL 0x40
#define MAX11040K_REG_DATA_RATE_CONTROL 0x50
#define MAX11040K_REG_CONFIGURATION 0x60
#define MAX11040K_REG_DATA 0x70

// Predefined configuration register values
#define MAX11040K_CONF_SHDN     0x80
#define MAX11040K_CONF_RST      0x40
#define MAX11040K_CONF_EN24BIT  0x20
#define MAX11040K_CONF_XTALEN   0x10
#define MAX11040K_CONF_FAULTDIS 0x08
#define MAX11040K_CONF_PDBUF    0x04
#define MAX11040K_DATA_RATE_FSAMP_1KHZ 0x401A  //!< Configuration for external XTAL (25 MHz)

#define MAX11040K_SPI_PORT hal::spi::kPort1
#define MAX11040K_CS_GPIO_PORT hal::Gpio::Port::kPortA
#define MAX11040K_CS_GPIO_PIN  hal::Gpio::kPin4

#define MAX11040K_V_REFIO 2.5  //!< Using internal reference mode
#define MAX11040K_TRANSFER_FN 0.88 * MAX11040K_V_REFIO * 2.0 / 16777216.0

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/

Max11040k::Max11040k(Max11040kCallback callback, void *callback_argument)
  : spi_slave_(MAX11040K_SPI_PORT,
               { hal::spi::kPhaseFirstEdge, hal::spi::kPolarityHigh,
                 hal::spi::kBaud1MHz, MAX11040K_CS_GPIO_PORT, MAX11040K_CS_GPIO_PIN },
               write_buffer_, read_buffer_, nullptr, nullptr),
    int_gpio_(hal::Gpio::kPortC, hal::Gpio::kPin1, hal::Gpio::kModeInterruptFalling,
              Max11040k::DataRdyCallback, this) {
  // Store callback and callback argument in member variables to call it from the SPI callback
  data_ready_callback_ = callback;
  cb_arg_ = callback_argument;

  write_buffer_[0] = MAX11040K_REG_CONFIGURATION;
  write_buffer_[1] = MAX11040K_CONF_EN24BIT | MAX11040K_CONF_FAULTDIS;
  spi_slave_.Transceive(2, Max11040k::ConfigurationCallback, this);
}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void Max11040k::InvokeUserCallbackTask(Max11040k *inst) {
  inst->measurement_data_.voltage[0] = (((inst->read_buffer_[1] << 24u) |
                               (inst->read_buffer_[2] << 16u) |
                               (inst->read_buffer_[3]) << 8u) >> 8u) * MAX11040K_TRANSFER_FN;
  inst->measurement_data_.voltage[1] = (((inst->read_buffer_[4] << 24u) |
                               (inst->read_buffer_[5] << 16u) |
                               (inst->read_buffer_[6]) << 8u) >> 8u) * MAX11040K_TRANSFER_FN;
  inst->measurement_data_.voltage[2] = (((inst->read_buffer_[7] << 24u) |
                               (inst->read_buffer_[8] << 16u) |
                               (inst->read_buffer_[9]) << 8u) >> 8u) * MAX11040K_TRANSFER_FN;
  inst->measurement_data_.voltage[3] = (((inst->read_buffer_[10] << 24u) |
                               (inst->read_buffer_[11] << 16u) |
                               (inst->read_buffer_[12]) << 8u) >> 8u) * MAX11040K_TRANSFER_FN;

  inst->data_ready_callback_(inst->measurement_data_, inst->cb_arg_);
}

void Max11040k::DataRdyCallback(void *ctx) {
  auto *inst = reinterpret_cast<Max11040k *>(ctx);
  // Get the timestamp of the interrupt occurrence
  inst->measurement_data_.timestamp = SystemTime::GetTimestamp();
  inst->write_buffer_[0] = MAX11040K_READ_FLAG | MAX11040K_REG_DATA;
  std::fill(inst->read_buffer_, inst->read_buffer_ + 13, 0);
  inst->spi_slave_.Transceive(13, Max11040k::DataPullFinishedCallback, inst);
}

void Max11040k::ConfigurationCallback(void *ctx) {
  auto *inst = reinterpret_cast<Max11040k*>(ctx);
  inst->write_buffer_[0] = MAX11040K_REG_DATA_RATE_CONTROL;
  inst->write_buffer_[1] = MAX11040K_DATA_RATE_FSAMP_1KHZ >> 8u;
  inst->write_buffer_[2] = MAX11040K_DATA_RATE_FSAMP_1KHZ & 0xFFu;
  inst->spi_slave_.Transceive(3, nullptr, nullptr);
}

void Max11040k::DataPullFinishedCallback(void *ctx) {
  auto *inst = reinterpret_cast<Max11040k *>(ctx);
  Scheduler::EnqueueTask(InvokeUserCallbackTask, inst);
}
