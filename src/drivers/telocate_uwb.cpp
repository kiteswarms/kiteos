// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "telocate_uwb.hpp"
#include "core/scheduler.hpp"
#include "core/system_time/system_time.hpp"
#include "core/logger.hpp"

#define TELOCATE_UWB_I2C_PORT hal::i2c::kPort4
#define TELOCATE_UWB_I2C_SLAVE_ADDRESS 0x21

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
TelocateUwb::TelocateUwb(
    TelocateUwb::MeasurementDataCallbackFunction data_callback, void *data_callback_argument,
    InitializedCallbackFunction initialized_callback, void *init_callback_argument):
i2c_slave_(TELOCATE_UWB_I2C_PORT, TELOCATE_UWB_I2C_SLAVE_ADDRESS, tx_buffer_, rx_buffer_,
           I2CCallback, this),
data_callback_(data_callback), data_callback_argument_(data_callback_argument),
init_callback_(initialized_callback), init_callback_argument_(init_callback_argument) {
  update_timer_.SetTimeoutPeriodic(measurement_interval_ms_, StartMeasurementCycleTask, this);
  communication_failure_reported_timer_.SetTimeout(0);

  tx_buffer_[0] = kCommandWhoAmI;
  state_ = kStateReadingWhoAmI;
  StartTransceive(1, 6);
}

double TelocateUwb::GetMeasurementInterval() {
  return static_cast<double>(measurement_interval_ms_) / 1000.0;
}

void TelocateUwb::SetMeasurementInterval(double seconds) {
  measurement_interval_ms_ = seconds * 1000;
  update_timer_.SetTimeout(measurement_interval_ms_);
}

void TelocateUwb::SetNodeAddress(uint32_t address) {
  node_address_ = address;
  node_address_changed_ = true;
  Scheduler::EnqueueTask(RunTask, this);
}

void TelocateUwb::SetRemoteNodeAddresses(const uint32_t* addresses, uint8_t num_addresses) {
  num_remote_node_addresses_ = (num_addresses < 4)? num_addresses : 4;
  for (uint8_t i = 0; i < num_remote_node_addresses_; ++i) {
    remote_node_addresses_[i] = addresses[i];
  }
}
/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
void TelocateUwb::SetAddress(uint32_t address) {
  tx_buffer_[0] = kCommandSetAddress;
  tx_buffer_[1] = address >> 24;
  tx_buffer_[2] = address >> 16;
  tx_buffer_[3] = address >> 8;
  tx_buffer_[4] = address;
  state_ = kStateSettingAddress;
  StartTransceive(5, 0);
}

void TelocateUwb::ReadAddress() {
  tx_buffer_[0] = kCommandGetAddress;
  state_ = kStateReadingAddress;
  StartTransceive(1, 4);
}

void TelocateUwb::StartMeasurement() {
  tx_buffer_[0] = kCommandStartMeasurement;
  tx_buffer_[1] = remote_node_addresses_[current_ranging_partner_] >> 24u;
  tx_buffer_[2] = remote_node_addresses_[current_ranging_partner_] >> 16u;
  tx_buffer_[3] = remote_node_addresses_[current_ranging_partner_] >> 8u;
  tx_buffer_[4] = remote_node_addresses_[current_ranging_partner_];
  state_ = kStateStartingMeasurement;
  StartTransceive(5, 0);
}

void TelocateUwb::GetStatus() {
  tx_buffer_[0] = kCommandGetStatus;
  state_ = kStateMeasurementInProgress;
  StartTransceive(1, 1);
}

void TelocateUwb::GetMeasurementData() {
  tx_buffer_[0] = kCommandGetMeasurementData;
  state_ = kStateReadingMeasurementData;
  StartTransceive(1, 4);
}

void TelocateUwb::ResetLocalMeasurementResults() {
  tx_buffer_[0] = kCommandResetLocalMeasurementResults;
  state_ = kStateResettingMeasurementResult;
  StartTransceive(1, 0);
}

void TelocateUwb::IssueRssiMeasurement() {
  tx_buffer_[0] = kCommandIssueRssiMeasurement;
  state_ = kStateMeasuringRssi;
  StartTransceive(1, 0);
}

void TelocateUwb::GetRssi() {
  tx_buffer_[0] = kCommandGetRssi;
  state_ = kStateGettingRssi;
  StartTransceive(1, 4);
}

void TelocateUwb::StartTransceive(uint32_t tx_byte_cnt, uint32_t rx_byte_cnt) {
  tx_byte_cnt_ = tx_byte_cnt;
  rx_byte_cnt_ = rx_byte_cnt;
  i2c_slave_.Transceive(tx_byte_cnt_, rx_byte_cnt_);
}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void TelocateUwb::RunTask(void *instance) {
  auto inst = reinterpret_cast<TelocateUwb*>(instance);
  switch (inst->state_) {
    case kStateReadingWhoAmI:
      if (inst->rx_buffer_[0] == 0x7E && inst->rx_buffer_[1] == 0x10 && inst->rx_buffer_[2] == 0xCA
          && inst->rx_buffer_[3] == 0x7E && inst->rx_buffer_[4] == 0 && inst->rx_buffer_[5] == 0) {
        inst->ReadAddress();
      } else {
        Logger::Report("Telocate UWB: Read unexpected who am I register value.", Logger::kWarning);
      }
      break;

    case kStateReadingAddress:
      inst->node_address_ = static_cast<uint32_t>(inst->rx_buffer_[0]) << 24 |
                            static_cast<uint32_t>(inst->rx_buffer_[1]) << 16 |
                            static_cast<uint32_t>(inst->rx_buffer_[2]) << 8 |
                            static_cast<uint32_t>(inst->rx_buffer_[3]);
      inst->init_callback_(inst->node_address_, inst->init_callback_argument_);
      inst->state_ = kStateIdle;
      break;

    case kStateIdle:
      if (inst->node_address_changed_) {
        inst->node_address_changed_ = false;
        inst->SetAddress(inst->node_address_);
      }
      break;

    case kStateSettingAddress:
      inst->state_ = kStateIdle;
      break;

    case kStateStartingMeasurement:
      inst->GetStatus();
      break;

    case kStateMeasurementInProgress:
      if (inst->rx_buffer_[0] & 0x02) {
        // Measurement rejected
        inst->ResetLocalMeasurementResults();
      } else if (inst->rx_buffer_[0] & 0x01) {
        // Measurement in progress
        inst->GetStatus();
      } else {
        if (!inst->rx_buffer_[0]) {
          // Communication failed, measurement was not started
          Logger::Report("Telocate UWB: Unknown communication failure.", Logger::kWarning);
          inst->ResetLocalMeasurementResults();
        } else if (inst->rx_buffer_[0] & 0x10) {
          // Measurement timed out.
          inst->data_callback_(inst->current_ranging_partner_, 0.0, 0.0, SystemTime::GetTimestamp(),
                               kStatusTimeout, inst->data_callback_argument_);
          inst->ResetLocalMeasurementResults();
        } else if (inst->rx_buffer_[0] & 0x20) {
          // Measurement failed (garbage data)
          inst->data_callback_(inst->current_ranging_partner_, 0.0, 0.0, SystemTime::GetTimestamp(),
                               kStatusGarbageData, inst->data_callback_argument_);
          inst->ResetLocalMeasurementResults();
        } else if (inst->rx_buffer_[0] & 0x40) {
          // Measurement results ready
          inst->measurement_timestamp_ = SystemTime::GetTimestamp();
          inst->GetMeasurementData();
        }
      }
      break;

    case kStateAbortingMeasurement:
      inst->ResetLocalMeasurementResults();
      break;


    case kStateReadingMeasurementData: {
      uint32_t range = static_cast<uint32_t>(inst->rx_buffer_[0]) |
                       (static_cast<uint32_t>(inst->rx_buffer_[1]) << 8u) |
                       (static_cast<uint32_t>(inst->rx_buffer_[2]) << 16u) |
                       (static_cast<uint32_t>(inst->rx_buffer_[3]) << 24u);
      auto range_float_ptr = reinterpret_cast<float*>(&range);

      if ((range & 0x7F800000) == 0x7F800000) {
        // Measure again if range is NAN
        inst->StartMeasurement();
      } else {
        inst->range_ = *range_float_ptr;
        inst->IssueRssiMeasurement();
      }

      break;
    }

    case kStateMeasuringRssi:
      inst->GetRssi();
      break;

    case kStateGettingRssi: {
      uint32_t rssi = static_cast<uint32_t>(inst->rx_buffer_[0]) |
          (static_cast<uint32_t>(inst->rx_buffer_[1]) << 8u) |
          (static_cast<uint32_t>(inst->rx_buffer_[2]) << 16u) |
          (static_cast<uint32_t>(inst->rx_buffer_[3]) << 24u);
      auto rssi_float_ptr = reinterpret_cast<float *>(&rssi);

      if ((rssi & 0x7F800000) == 0x7F800000) {
        // Read again if RSSI is NAN
        inst->GetRssi();
      } else if (*rssi_float_ptr > -2.0) {
        // RSSI == -1 means that rssi value calculation is still in progress
        inst->GetRssi();
      } else {
        Status status = kStatusValid;
        inst->data_callback_(inst->current_ranging_partner_, inst->range_, *rssi_float_ptr,
                             inst->measurement_timestamp_, status, inst->data_callback_argument_);
        inst->ResetLocalMeasurementResults();
      }
      break;
    }

    case kStateResettingMeasurementResult:
      inst->current_ranging_partner_++;
      if (inst->current_ranging_partner_ >= inst->num_remote_node_addresses_) {
        inst->state_ = kStateIdle;
      } else {
        inst->StartMeasurement();
      }
      break;

    default:
      break;
  }
}

void TelocateUwb::StartMeasurementCycleTask(TelocateUwb* inst) {
  if (inst->state_ == kStateIdle) {
    inst->current_ranging_partner_ = 0;
    inst->StartMeasurement();
  }
}

void TelocateUwb::I2CCallback(hal::i2c::Error error, void *cb_arg) {
  if (error != hal::i2c::kErrorNone) {
    auto inst = reinterpret_cast<TelocateUwb*>(cb_arg);

    // Ensure that communication failure is not reported more often than once a second
    if (inst->communication_failure_reported_timer_.Elapsed()) {
      Logger::Report("Telocate UWB: I2C communication failure.", Logger::kWarning);
      inst->communication_failure_reported_timer_.SetTimeout(1000);
    }

    inst->i2c_slave_.Transceive(inst->tx_byte_cnt_, inst->rx_byte_cnt_);
  } else {
    Scheduler::EnqueueTask(RunTask, cb_arg);
  }
}
