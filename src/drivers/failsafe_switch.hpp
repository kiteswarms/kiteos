// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_FAILSAFE_SWITCH_HPP_
#define SRC_DRIVERS_FAILSAFE_SWITCH_HPP_

#include <cstdint>
#include "core/software_timer.hpp"
#include "hal/gpio.hpp"
#include "hal/timer.hpp"

/*!
 *  \brief Implements a driver to easily access the FailsafeSwitch.
 *         The driver uses a hardware timer which triggers a GPIO if the Set()
 *         method is not called for a specific period.
 */
class FailsafeSwitch {
 public:
  //! \brief Constructor of the FailsafeSwitch driver component.
  FailsafeSwitch();

  //! \brief Destructor of the FailsafeSwitch driver component.
  ~FailsafeSwitch();

  //! \brief Sets the value of the failsafe gpio.
  //         Must be called periodically.
  //         If not the driver shuts down the GPIO after a defined timeout.
  void Set(bool value);

 private:
  //! Callback called by a hardware timer when a failsafe occurred.
  static void FailsafeCallback(void *context);

  hal::Timer timeout_timer_;  //!< Timer to detect a failsafe condition.
  hal::Gpio output_pin_;  //!< Output pin that powers the Axon boards.

  bool switch_state_;  //!< Current state of the switch.
  //! Timer used to publish failsafe messages not more than once a second.
  SoftwareTimer publish_timer_;
};

#endif  // SRC_DRIVERS_FAILSAFE_SWITCH_HPP_
