// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_TELOCATE_UWB_HPP_
#define SRC_DRIVERS_TELOCATE_UWB_HPP_

#include <cstdint>
#include <vector>
#include <string>
#include "core/software_timer.hpp"
#include "hal/i2c/slave.hpp"

/*!
 *  \brief Implements a driver to access the Telocate UWB module receiver via I2C.
 */
class TelocateUwb {
 public:
  //! Enum describing the status of a range measurement result.
  enum Status {
    kStatusValid,
    kStatusTimeout,
    kStatusGarbageData
  };

  //! Data type of the callback that is called when measurement data is ready.
  typedef void (*MeasurementDataCallbackFunction)(uint32_t remote_node_index, float distance,
                                                  float rssi, uint64_t measurement_timestamp,
                                                  Status status, void *ctx);
  //! Data type of the callback that is called when device is fully initialized.
  typedef void (*InitializedCallbackFunction)(uint32_t node_address, void *ctx);

  /*!
   * \brief Constructer of the Telocate UWB driver.
   *
   * \param data_callback      Callback to be called when measurement data is ready.
   * \param callback_argument  Argument to pass to the data callback.
   */
  TelocateUwb(MeasurementDataCallbackFunction data_callback, void* data_callback_argument,
              InitializedCallbackFunction initialized_callback, void* init_callback_argument);

  /*!
   * \brief Destructor of the Telocate UWB driver.
   */
  ~TelocateUwb() = default;

  /*!
   * \brief  Gets the current measurement interval.
   *
   * \return Current measurement interval in seconds.
   */
  double GetMeasurementInterval();

  /*!
   * \brief Sets the measurement interval to use.
   *
   * \param seconds  Measurement interval to use in seconds.
   */
  void SetMeasurementInterval(double seconds);

  /*!
   * \brief Sets the node address of the node directly connected to the microcontroller.
   *
   * \param address  Node address to set.
   */
  void SetNodeAddress(uint32_t address);

  /*!
   * \brief Sets the node addresses that should be measured to.
   *
   * \param addresses      Array containing the addresses of the remote nodes.
   * \param num_addresses  Length of the array containing the addresses.
   */
  void SetRemoteNodeAddresses(const uint32_t* addresses, uint8_t num_addresses);

 private:
  //! Commands available in the Telocate UWB driver protocol.
  enum Command {
    kCommandGetStatus = 0x00,
    kCommandStartMeasurement = 0x10,
    kCommandAbortMeasurement = 0x20,
    kCommandGetMeasurementData = 0x30,
    kCommandGetRemoteMeasurementData = 0x31,
    kCommandGetRawMeasurementData = 0x32,
    kCommandGetRawRemoteMeasurementData = 0x33,
    kCommandResetLocalMeasurementResults = 0x40,
    kCommandResetRemoteMeasurementResults = 0x41,
    kCommandSetMeasurementMode = 0x50,
    kCommandGetMeasurementMode = 0x51,
    kCommandSetTxAntennaDelay = 0x60,
    kCommandGetTxAntennaDelay = 0x61,
    kCommandSetRxAntennaDelay = 0x62,
    kCommandGetRxAntennaDelay = 0x63,
    kCommandSetReplyDelay = 0x70,
    kCommandGetReplyDelay = 0x71,
    kCommandSetTimeout = 0x80,
    kCommandGetTimeout = 0x81,
    kCommandIssueChipTemperature = 0x90,
    kCommandGetChipTemperature = 0x91,
    kCommandIssueRssiMeasurement = 0x92,
    kCommandGetRssi = 0x93,
    kCommandSetAddress = 0xA0,
    kCommandGetAddress = 0xA1,
    kCommandWhoAmI = 0xF1,
    kCommandResetReinitialize = 0xFF
  };

  //! Possible states of the Telocate UWB driver.
  enum State {
    kStateReadingWhoAmI,
    kStateReadingAddress,
    kStateIdle,
    kStateSettingAddress,
    kStateStartingMeasurement,
    kStateSendingReset,
    kStateMeasurementInProgress,
    kStateAbortingMeasurement,
    kStateReadingMeasurementData,
    kStateMeasuringRssi,
    kStateGettingRssi,
    kStateResettingMeasurementResult
  };

  void SetAddress(uint32_t address);  //!< Sends a set address command to the UWB module.
  void ReadAddress();  //!< Sends a read address command to the UWB module.
  void StartMeasurement();  //!< Starts a range measurement to the current ranging partner.
  void GetStatus();  //!< Starts getting the current status from the UWB module.
  void GetMeasurementData();  //!< Sends a get measurement data command to the UWB module
  //! Sends a command to the UWB module to reset the local measurement results.
  void ResetLocalMeasurementResults();
  void IssueRssiMeasurement();  //!< Sends an issue RSSI measurement command to the UWB module.
  void GetRssi();  //!< Sends a get RSSI command to the UWB module.
  //! Starts a transceive operation on the I2C bus.
  void StartTransceive(uint32_t tx_byte_cnt, uint32_t rx_byte_cnt);

  //! Starts new measurment cycle with first ranging partner.
  static void StartMeasurementCycleTask(TelocateUwb* inst);
  //! Main Task enqueued, when an I2C transceive operation finished successfully.
  static void RunTask(void *instance);
  //! Callback called by the I2C driver when a transceive operation finished.
  static void I2CCallback(hal::i2c::Error error, void *cb_arg);


  hal::i2c::Slave i2c_slave_;   //!< I2C slave instance handling the I2C communication.
  uint8_t tx_buffer_[5] = {};   //!< TX buffer used for I2C transmissions.
  uint8_t rx_buffer_[20] = {};  //!< RX buffer used for I2C receptions.
  uint32_t tx_byte_cnt_ = 0;    //!< Number of bytes sent in the last I2C transceive operation.
  uint32_t rx_byte_cnt_ = 0;    //!< Number of bytes received in the last I2C transceive operation.

  uint32_t measurement_interval_ms_ = 100;  //!< Interval in ms used between measurement cycles.
  uint32_t node_address_ = 0;  //!< Address of the UWB module connected to the microcontroller.
  //! Indicates if the node address was changed and not updated in the UWB module yet.
  bool node_address_changed_ = false;
  uint32_t remote_node_addresses_[4] = {0};  //!< List of remote node addresses to range to.
  uint8_t num_remote_node_addresses_ = 0;    //!< Number of remote node addresses to range to.
  //! Callback called when measurement data is ready.
  MeasurementDataCallbackFunction data_callback_;
  void* data_callback_argument_;  //!< Argument passed to the data callback.
  //! Callback called when the initialization of the UWB module is finished.
  InitializedCallbackFunction init_callback_;
  void* init_callback_argument_;  //!< Argument passed to the init callback.

  State state_;  //!< State of the Telocate UWB driver.
  //! Index of the node which is the current ranging partner.
  uint32_t current_ranging_partner_ = 0;
  uint64_t measurement_timestamp_ = 0;  //!< Timestamp of the last measurement taken.
  float range_ = 0.0;  //!< Last range measured.
  SoftwareTimer update_timer_;  //!< Timer to keep track of the measurement interval.
  //! Timer used to limit the period for publishing I2C errors.
  SoftwareTimer communication_failure_reported_timer_;
};

#endif  // SRC_DRIVERS_TELOCATE_UWB_HPP_
