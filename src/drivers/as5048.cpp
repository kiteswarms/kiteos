// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "as5048.hpp"
#include <cmath>
#include "core/logger.hpp"
#include "core/system_time/system_time.hpp"
#include "core/scheduler.hpp"

#define AS5048B_MEASUREMENT_INTERVAL 10  // In milliseconds
#define AS5048B_REGISTER_ANGLE 0xFE

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
As5048::As5048(hal::i2c::Port port, uint8_t slave_address,
               As5048::DataReadyCallback data_ready_callback, void *callback_argument):
slave_(port, slave_address, tx_buffer_, rx_buffer_, I2CCallback, this),
data_ready_callback_(data_ready_callback),
callback_argument_(callback_argument) {
  ASSERT(slave_address == 0x40 || slave_address == 0x41 || slave_address == 0x42 ||
         slave_address == 0x43);
  communication_failure_reported_timer_.SetTimeout(0);
  readout_timer_.SetTimeoutPeriodic(AS5048B_MEASUREMENT_INTERVAL, ReadoutTimerCallback, this);
}

As5048::~As5048() = default;

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void As5048::RunTask(As5048 *instance) {
  switch (instance->state_) {
    case kStateIdle:
      instance->tx_buffer_[0] = AS5048B_REGISTER_ANGLE;
      instance->state_ = kStatePending;
      instance->slave_.Transceive(1, 2);
      break;

    case kStatePending: {
      uint16_t raw_angle = (static_cast<uint16_t>(instance->rx_buffer_[0]) << 6u) |
                           (static_cast<uint16_t>(instance->rx_buffer_[1]) & 0x3Fu);
      // Return angle in rad
      instance->data_ready_callback_(static_cast<float>(raw_angle * 2.0 * M_PI / 0x4000),
                                     SystemTime::GetTimestamp(), instance->callback_argument_);
      instance->state_ = kStateIdle;
      break;
    }

    default:
      ASSERT(false);
      break;
  }
}

void As5048::ReadoutTimerCallback(As5048 *instance) {
  instance->state_ = kStateIdle;
  if (instance->state_ == kStateIdle) {
    RunTask(instance);
  }
}

void As5048::I2CCallback(hal::i2c::Error error, void *instance) {
  auto inst = reinterpret_cast<As5048*>(instance);

  if (error == hal::i2c::kErrorNone) {
    Scheduler::EnqueueTask(RunTask, inst);
  } else {
    // Ensure that communication failure is not reported more often than once a second
    if (inst->communication_failure_reported_timer_.Elapsed()) {
      Logger::Report("AS5048: I2C communication failure.", Logger::kWarning);
      inst->communication_failure_reported_timer_.SetTimeout(1000);
    }

    // Resend command
    inst->slave_.Transceive(1, 2);
  }
}
