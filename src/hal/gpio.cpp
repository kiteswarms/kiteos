// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#include "hal/gpio.hpp"
#include "utils/debug_utils.h"

#define GPIO_MODER_INPUT      0x0
#define GPIO_MODER_OUTPUT     0x1
#define GPIO_MODER_AF         0x2
#define GPIO_MODER_ANALOG     0x3

#define GPIO_OTYPER_PP        0x0
#define GPIO_OTYPER_OD        0x1

#define GPIO_OSPEEDR_LOW      0x0
#define GPIO_OSPEEDR_MEDIUM   0x1
#define GPIO_OSPEEDR_HIGH     0x2
#define GPIO_OSPEEDR_VERYHIGH 0x3


namespace hal {

/**************************************************************************************************
 *     Static member variables                                                                    *
 **************************************************************************************************/
const Gpio::PeripheralConfig Gpio::peripheral_config_[] = {
  {GPIOA, Rcc::kGpioA},  // kPortA
  {GPIOB, Rcc::kGpioB},  // kPortB
  {GPIOC, Rcc::kGpioC},  // kPortC
  {GPIOD, Rcc::kGpioD},  // kPortD
  {GPIOE, Rcc::kGpioE},  // kPortE
  {GPIOF, Rcc::kGpioF},  // kPortF
  {GPIOG, Rcc::kGpioG},  // kPortG
  {GPIOH, Rcc::kGpioH},  // kPortH
  {GPIOI, Rcc::kGpioI},  // kPortI
  {GPIOJ, Rcc::kGpioJ},  // kPortJ
  {GPIOK, Rcc::kGpioK},  // kPortK
};

const Gpio::ModeConfig Gpio::mode_config_[] = {
  {GPIO_MODER_INPUT, kPullNopull, 0x0, 0x0},  // kModeInput
  {GPIO_MODER_OUTPUT, kPullNopull, GPIO_OTYPER_PP, 0x0},  // kModeOutput
  {GPIO_MODER_INPUT, kPullNopull, 0x0, 0x0},  // kModeInterruptRising
  {GPIO_MODER_INPUT, kPullNopull, 0x0, 0x0},  // kModeInterruptFalling
  {GPIO_MODER_AF, kPullNopull, GPIO_OTYPER_PP, 0x0},  // kModeAF0Mco2
  {GPIO_MODER_AF, kPullPullup, GPIO_OTYPER_PP, 0x1},  // kModeAF1Tim1Ch4
  {GPIO_MODER_AF, kPullPullup, GPIO_OTYPER_PP, 0x2},  // kModeAF2Tim3Ch4
  {GPIO_MODER_AF, kPullPullup, GPIO_OTYPER_OD, 0x4},  // kModeAF4I2c
  {GPIO_MODER_AF, kPullPullup, GPIO_OTYPER_PP, 0x5},  // kModeAF5Spi
  {GPIO_MODER_AF, kPullPullup, GPIO_OTYPER_PP, 0x7},  // kModeAF7Uart
  {GPIO_MODER_AF, kPullPullup, GPIO_OTYPER_PP, 0x8},  // kModeAF8Uart
  {GPIO_MODER_AF, kPullNopull, GPIO_OTYPER_PP, 0xB},  // kModeAF11Ethernet
  {GPIO_MODER_ANALOG, kPullNopull, 0x0, 0x0},  // kModeAnalog
};

Gpio::Callback* Gpio::callback_registry_[GPIO_PIN_CNT] = {nullptr};

uint16_t Gpio::gpio_registry_[GPIO_PORT_CNT] = {0};

/**************************************************************************************************
 *     Public member functions                                                                    *
 **************************************************************************************************/
Gpio::Gpio(Port port, Pin pins, Mode mode)
  : Gpio(port, pins, mode, nullptr, nullptr) {}

Gpio::Gpio(Config config)
  : Gpio(config.port, config.pins, config.mode, nullptr, nullptr) {}

Gpio::Gpio(Port port, Pin pins, Mode mode, CallbackFunction cb_func)
  : Gpio(port, pins, mode, cb_func, nullptr) {}

Gpio::Gpio(Port port, Pin pins, Mode mode, CallbackFunction cb_func, void* cb_arg)
  : Gpio(port, static_cast<uint16_t>(pins), mode, cb_func, cb_arg) {}

Gpio::Gpio(Port port, uint16_t pins, Mode mode)
  : Gpio(port, pins, mode, nullptr, nullptr) {}

Gpio::Gpio(Port port, uint16_t pins, Mode mode, CallbackFunction cb_func)
  : Gpio(port, pins, mode, cb_func, nullptr) {}

Gpio::Gpio(Port port, uint16_t pins, Mode mode, CallbackFunction cb_func, void* cb_arg)
  : port_(port),
    pins_(pins),
    mode_(mode),
    callback_{cb_func, cb_arg} {
  ASSERT(!(gpio_registry_[port] & pins));
  gpio_registry_[port] |= pins;

  Rcc::EnablePeripheralClock(peripheral_config_[port_].rcc_clock);
  for (uint8_t position = 0; (position < GPIO_PIN_CNT); position++) {
    if (pins_ & (0x1u << position)) {
      /* Configure Alternate function mapped with the current IO */
      Register::BitfieldWrite(REG_ADDR(peripheral_config_[port_].base->AFR[position >> 0x3u]),
                                       mode_config_[mode_].afr, position & 0x7u, 4);
      /* Configure IO Direction mode */
      Register::BitfieldWrite(REG_ADDR(peripheral_config_[port_].base->MODER),
                                       mode_config_[mode_].moder, position, 2);
      /* Configure the IO Speed */
      Register::BitfieldWrite(REG_ADDR(peripheral_config_[port_].base->OSPEEDR),
                                       GPIO_OSPEEDR_VERYHIGH, position, 2);
      /* Configure the IO Output Type */
      Register::BitfieldWrite(REG_ADDR(peripheral_config_[port_].base->OTYPER),
                                       mode_config_[mode_].otyper, position, 1);
      /* Configure the Pull-up resistor for the current IO */
      Register::BitfieldWrite(REG_ADDR(peripheral_config_[port_].base->PUPDR),
                                       mode_config_[mode_].pupdr, position, 2);
      if (mode_ == kModeInterruptRising || mode_ == kModeInterruptFalling) {
        /* Configure the EXTI Port */
        Register::BitfieldWrite(REG_ADDR(SYSCFG->EXTICR[position >> 0x2u]), port_,
                                          position & 0x3u, 4);
        /* Configure EXTI line */
        Register::BitfieldWrite(REG_ADDR(EXTI_D1->IMR1), 0x1, position, 1);
        Register::BitfieldWrite(REG_ADDR(EXTI_D1->EMR1), 0x1, position, 1);
        if (mode_ == kModeInterruptRising) {
          /* Configure rising trigger */
          Register::BitfieldWrite(REG_ADDR(EXTI->RTSR1), 0x1, position, 1);
          /* Clear falling trigger */
          Register::BitfieldWrite(REG_ADDR(EXTI->FTSR1), 0x0, position, 1);
        } else {
          /* Configure rising trigger */
          Register::BitfieldWrite(REG_ADDR(EXTI->RTSR1), 0x0, position, 1);
          /* Clear falling trigger */
          Register::BitfieldWrite(REG_ADDR(EXTI->FTSR1), 0x1, position, 1);
        }

        SetupInterrupt();
      }
    }
  }
}

Gpio::~Gpio() {
  gpio_registry_[port_] &= ~pins_;
}

void Gpio::SetPull(Pull pull) {
  for (uint8_t position = 0; (position < GPIO_PIN_CNT); position++) {
    if (pins_ & (0x1u << position)) {
      Register::BitfieldWrite(REG_ADDR(peripheral_config_[port_].base->PUPDR), pull, position, 2);
    }
  }
}

void Gpio::Write(uint16_t value) {
  // Set negative bits LOW (set upper 16-bits for resetting)
  // Set positive bits HIGH (lower 16-bits for setting)
  peripheral_config_[port_].base->BSRR = ((pins_ & ~value) << 16u) | (pins_ & value);
}

void Gpio::Write(bool value) {
  uint16_t value_ui = value ? 0xFFFF : 0x0000;
  Write(value_ui);
}

uint16_t Gpio::Read() {
  return peripheral_config_[port_].base->IDR & pins_;
}


/**************************************************************************************************
 *     Private member functions                                                                   *
 **************************************************************************************************/
bool Gpio::SetupInterrupt() {
  if (callback_.cb_func == nullptr) {
    return false;
  }
  for (uint8_t i = 0; i < GPIO_PIN_CNT; ++i) {
    if ((pins_ >> i) & 0x1u) {
      callback_registry_[i] =  &callback_;
    }
  }

  if (pins_ & kPin0) {
    NVIC_EnableIRQ(EXTI0_IRQn);
  }
  if (pins_ & kPin1) {
    NVIC_EnableIRQ(EXTI1_IRQn);
  }
  if (pins_ & kPin2) {
    NVIC_EnableIRQ(EXTI2_IRQn);
  }
  if (pins_ & kPin3) {
    NVIC_EnableIRQ(EXTI3_IRQn);
  }
  if (pins_ & kPin4) {
    NVIC_EnableIRQ(EXTI4_IRQn);
  }
  if (pins_ & (kPin9 | kPin8 | kPin7 | kPin6 | kPin5)) {
    NVIC_EnableIRQ(EXTI9_5_IRQn);
  }
  if (pins_ & (kPin15 | kPin14 | kPin13 | kPin12 | kPin11 | kPin10)) {
    NVIC_EnableIRQ(EXTI15_10_IRQn);
  }
  return true;
}
}  // namespace hal

/**************************************************************************************************
 *     Interrupt handler implementations                                                          *
 **************************************************************************************************/
extern "C" {

//! Interrupt handlers for EXTI0
void EXTI0_IRQHandler(void) {
  // Clear interrupt flag
  EXTI->PR1 = hal::Gpio::kPin0;
  // Callback
  if (hal::Gpio::callback_registry_[0] && hal::Gpio::callback_registry_[0]->cb_func) {
    hal::Gpio::callback_registry_[0]->cb_func(hal::Gpio::callback_registry_[0]->cb_arg);
  }
}

//! Interrupt handlers for EXTI1
void EXTI1_IRQHandler(void) {
  // Clear interrupt flag
  EXTI->PR1 = hal::Gpio::kPin1;
  // Callback
  if (hal::Gpio::callback_registry_[1] && hal::Gpio::callback_registry_[1]->cb_func) {
    hal::Gpio::callback_registry_[1]->cb_func(hal::Gpio::callback_registry_[1]->cb_arg);
  }
}

//! Interrupt handlers for EXTI2
void EXTI2_IRQHandler(void) {
  // Clear interrupt flag
  EXTI->PR1 = hal::Gpio::kPin2;
  // Callback
  if (hal::Gpio::callback_registry_[2] && hal::Gpio::callback_registry_[2]->cb_func) {
    hal::Gpio::callback_registry_[2]->cb_func(hal::Gpio::callback_registry_[2]->cb_arg);
  }
}

//! Interrupt handlers for EXTI3
void EXTI3_IRQHandler(void) {
  // Clear interrupt flag
  EXTI->PR1 = hal::Gpio::kPin3;
  // Callback
  if (hal::Gpio::callback_registry_[3] && hal::Gpio::callback_registry_[3]->cb_func) {
    hal::Gpio::callback_registry_[3]->cb_func(hal::Gpio::callback_registry_[3]->cb_arg);
  }
}

//! Interrupt handlers for EXTI4
void EXTI4_IRQHandler(void) {
  // Clear interrupt flag
  EXTI->PR1 = hal::Gpio::kPin4;
  // Callback
  if (hal::Gpio::callback_registry_[4] && hal::Gpio::callback_registry_[4]->cb_func) {
    hal::Gpio::callback_registry_[4]->cb_func(hal::Gpio::callback_registry_[4]->cb_arg);
  }
}

//! Interrupt handlers for EXTI5 to EXTI9
void EXTI9_5_IRQHandler(void) {
  // Make sure that interrupt flag is set
  for (uint8_t i = 5; i < 10; ++i) {
    uint16_t exti_line = 0x1u << i;
    if ((EXTI->PR1 & exti_line) != RESET) {
      // Clear interrupt flag
      EXTI->PR1 = exti_line;
      // Callback
      if (hal::Gpio::callback_registry_[i] && hal::Gpio::callback_registry_[i]->cb_func) {
        hal::Gpio::callback_registry_[i]->cb_func(hal::Gpio::callback_registry_[i]->cb_arg);
      }
    }
  }
}

//! Interrupt handlers for EXTI10 to EXTI15
void EXTI15_10_IRQHandler(void) {
  // Make sure that interrupt flag is set
  for (uint8_t i = 10; i < 16; ++i) {
    uint16_t exti_line = 0x1u << i;
    if ((EXTI->PR1 & exti_line) != RESET) {
      // Clear interrupt flag
      EXTI->PR1 = exti_line;
      // Callback
      if (hal::Gpio::callback_registry_[i] && hal::Gpio::callback_registry_[i]->cb_func) {
        hal::Gpio::callback_registry_[i]->cb_func(hal::Gpio::callback_registry_[i]->cb_arg);
      }
    }
  }
}

}  // extern "C"
