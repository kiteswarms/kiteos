// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "semaphore.hpp"
#include "hal/rcc.hpp"
#include "utils/debug_utils.h"

#define HAL_SEMAPHORE_CNT 32

namespace hal {

/**************************************************************************************************
 *     Private static member variables                                                            *
 **************************************************************************************************/

Semaphore* Semaphore::registry_[HAL_SEMAPHORE_CNT] = { nullptr };


/**************************************************************************************************
 *     Public member functions                                                                    *
 **************************************************************************************************/

Semaphore::Semaphore(ClearCallback clear_callback, void* callback_argument) {
  // Register instance in the device registry and store device index in member variable
  bool semaphore_available = false;
  for (int i = 0; i < HAL_SEMAPHORE_CNT; i++) {
    if (registry_[i] == nullptr) {
      registry_[i] = this;
      device_index_ = i;
      semaphore_available = true;
      break;
    }
  }
  ASSERT(semaphore_available);

  //  Enable hardware semaphore clock
  Rcc::EnablePeripheralClock(Rcc::kHsem);

  // Store callback and callback argument in member variables
  clear_callback_ = clear_callback;
  clear_callback_argument_ = callback_argument;

  // Clear semaphore if it is already locked
  if (IsLocked()) {
    ASSERT_RESPONSE(Semaphore::Clear(HSEM->C1IER & HSEM_R_PROCID), true);
  }

  // Enable interrupt for this semaphore if a callback is registered
  if (clear_callback != nullptr && semaphore_available) {
    HSEM->C1IER |= 1u << device_index_;
  }
}

Semaphore::~Semaphore() {
  // Free device registry entry
  registry_[device_index_] = nullptr;

  // Disable interrupt for this semaphore
  HSEM->C1IER &= ~(1u << device_index_);
}

bool Semaphore::IsLocked() {
  return HSEM->R[device_index_] & HSEM_R_LOCK;
}

bool Semaphore::Lock(uint8_t process_id) {
  // Try to lock the semaphore for the given process
  HSEM->R[device_index_] = HSEM_R_LOCK | HSEM_CR_COREID_CPU1 | process_id;

  // Return if the lock was successful
  return HSEM->R[device_index_] == (HSEM_R_LOCK | HSEM_CR_COREID_CPU1 | process_id);
}

bool Semaphore::Clear(uint8_t process_id) {
  // Try to clear the semaphore
  HSEM->R[device_index_] = HSEM_CR_COREID_CPU1 | process_id;

  // Return if clear was successful
  return !IsLocked();
}

}  // namespace hal


/**************************************************************************************************
 *     Interrupt handler                                                                          *
 **************************************************************************************************/

extern "C" {
//! Hardware semaphore global interrupt handler.
void HSEM1_IRQHandler() {
  // Loop over all hardware semaphore since they all use this interrupt
  for (uint32_t i = 0; i < 32; i++) {
    // Check if the semaphore interrupt flag is set
    if (HSEM->C1MISR & (1u << i)) {
      // Reset interrupt flag
      HSEM->C1ICR &= ~(1u << i);

      // Execute callback if the instance exists and a callback is registered is registered
      hal::Semaphore *instance = hal::Semaphore::registry_[i];
      if (instance != nullptr && instance->clear_callback_) {
        instance->clear_callback_(instance->clear_callback_argument_);
      }
    }
  }
}
}
