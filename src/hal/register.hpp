// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_HAL_REGISTER_HPP_
#define SRC_HAL_REGISTER_HPP_

#include <stm32h7xx.h>

#define REG_ADDR(x) reinterpret_cast<volatile uint32_t*>(&(x))
//*****************************************************************************/
//! \addtogroup hal HAL API
//! @{
//! \addtogroup hal_register_api HAL Register API
//! @{
///
//*****************************************************************************/
namespace hal {
/*!
 *  \brief   Implements the HAL Register class.
 *  \details This class is used to facilitate register access and configuration.
 *
 *  \author   Elias Rosch
 *  \date     2020
 */
class Register {
 public:
  //! \brief Data structure for register configurations
  struct Config {
    volatile uint32_t* address;  //!< register address to be configured
    uint32_t mask;  //!< mask that should be modified (cleared)
    uint32_t value;  //!< value that should be written to the masked bits

    //! Writes a configuration to the register
    void Write() {
      MODIFY_REG(*address, mask, value);
    }

    //! Writes a configuration to the register
    void Clear() {
      MODIFY_REG(*address, mask, 0x0);
    }

    //! Checks if the configuration written is correct
    // \return true if verification succeeds, false otherwise.
    bool Verify() {
      return ((READ_REG(*address) & mask) == (value & mask));
    }
  };

  //! \brief Write to a register in "bitfield" mode.
  //  \note This assumes that the accessed register consists of "bitfields" of same width
  //
  // \param address register adress to write to
  // \param value bitfield value that should be written
  // \param position index of the "bitfield" (i.e idx 1 starts at bit number \width)
  // \param width bitfield width size
  static void BitfieldWrite(volatile uint32_t* address, uint32_t value,
                            uint8_t position, uint8_t width);
};
}  // namespace hal
/*! @} */
/*! @} */
#endif  // SRC_HAL_REGISTER_HPP_
