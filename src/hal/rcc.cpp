// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "hal/rcc.hpp"
#include <stm32h7xx.h>

namespace hal {

Register::Config Rcc::clock_source_config_[] = {
  {REG_ADDR(RCC->PLLCKSELR), RCC_PLLCKSELR_PLLSRC, RCC_PLLCKSELR_PLLSRC_HSI},  // kPllHsi
  {REG_ADDR(RCC->PLLCKSELR), RCC_PLLCKSELR_PLLSRC, RCC_PLLCKSELR_PLLSRC_CSI},  // kPllCsi
  {REG_ADDR(RCC->PLLCKSELR), RCC_PLLCKSELR_PLLSRC, RCC_PLLCKSELR_PLLSRC_HSE},  // kPllHse
  {REG_ADDR(RCC->CFGR), RCC_CFGR_SW, RCC_CFGR_SW_PLL1},  // kSysclkPll
  {REG_ADDR(RCC->CFGR), RCC_CFGR_MCO2, RCC_CFGR_MCO2_0},  // kMco2Pll2P
  {REG_ADDR(RCC->D2CCIP1R), RCC_D2CCIP1R_SPI123SEL, RCC_D2CCIP1R_SPI123SEL_0},  // kSpi123Pll2P
  {REG_ADDR(RCC->D2CCIP1R), RCC_D2CCIP1R_SPI123SEL, RCC_D2CCIP1R_SPI123SEL_1},  // kSpi123Pll3P
  {REG_ADDR(RCC->D2CCIP2R), RCC_D2CCIP2R_I2C123SEL, 0x00000000},  // kI2c123Pclk1
  {REG_ADDR(RCC->D3CCIPR), RCC_D3CCIPR_I2C4SEL, 0x00000000},  // kI2c4Pclk4
  {REG_ADDR(RCC->D2CCIP2R), RCC_D2CCIP2R_USART16SEL, 0x00000000},  // kUsart16Pclk2
  {REG_ADDR(RCC->D2CCIP2R), RCC_D2CCIP2R_USART28SEL, 0x00000000},  // kUsart234578Pclk1
  {REG_ADDR(RCC->D3CCIPR), RCC_D3CCIPR_ADCSEL, 0x00000000},  // kAdcPll2P
  {REG_ADDR(RCC->D2CCIP2R), RCC_D2CCIP2R_RNGSEL, RCC_D2CCIP2R_RNGSEL_0}  // kRngPll1Q
};

Register::Config Rcc::peripheral_clock_config_[] = {
  {REG_ADDR(RCC->APB4ENR), RCC_APB4ENR_SYSCFGEN, RCC_APB4ENR_SYSCFGEN},  // kSysCfg
  {REG_ADDR(RCC->AHB4ENR), RCC_AHB4ENR_GPIOAEN, RCC_AHB4ENR_GPIOAEN},  // kGpioA
  {REG_ADDR(RCC->AHB4ENR), RCC_AHB4ENR_GPIOBEN, RCC_AHB4ENR_GPIOBEN},  // kGpioB
  {REG_ADDR(RCC->AHB4ENR), RCC_AHB4ENR_GPIOCEN, RCC_AHB4ENR_GPIOCEN},  // kGpioC
  {REG_ADDR(RCC->AHB4ENR), RCC_AHB4ENR_GPIODEN, RCC_AHB4ENR_GPIODEN},  // kGpioD
  {REG_ADDR(RCC->AHB4ENR), RCC_AHB4ENR_GPIOEEN, RCC_AHB4ENR_GPIOEEN},  // kGpioE
  {REG_ADDR(RCC->AHB4ENR), RCC_AHB4ENR_GPIOFEN, RCC_AHB4ENR_GPIOFEN},  // kGpioF
  {REG_ADDR(RCC->AHB4ENR), RCC_AHB4ENR_GPIOGEN, RCC_AHB4ENR_GPIOGEN},  // kGpioG
  {REG_ADDR(RCC->AHB4ENR), RCC_AHB4ENR_GPIOHEN, RCC_AHB4ENR_GPIOHEN},  // kGpioH
  {REG_ADDR(RCC->AHB4ENR), RCC_AHB4ENR_GPIOIEN, RCC_AHB4ENR_GPIOIEN},  // kGpioI
  {REG_ADDR(RCC->AHB4ENR), RCC_AHB4ENR_GPIOJEN, RCC_AHB4ENR_GPIOJEN},  // kGpioJ
  {REG_ADDR(RCC->AHB4ENR), RCC_AHB4ENR_GPIOKEN, RCC_AHB4ENR_GPIOKEN},  // kGpioK
  // kEthernet
  {REG_ADDR(RCC->AHB1ENR), RCC_AHB1ENR_ETH1MACEN | RCC_AHB1ENR_ETH1TXEN | RCC_AHB1ENR_ETH1RXEN,
                           RCC_AHB1ENR_ETH1MACEN | RCC_AHB1ENR_ETH1TXEN | RCC_AHB1ENR_ETH1RXEN},
  {REG_ADDR(RCC->APB2ENR), RCC_APB2ENR_SPI1EN, RCC_APB2ENR_SPI1EN},  // kSpi1
  {REG_ADDR(RCC->APB1LENR), RCC_APB1LENR_SPI2EN, RCC_APB1LENR_SPI2EN},  // kSpi2
  {REG_ADDR(RCC->APB1LENR), RCC_APB1LENR_I2C1EN, RCC_APB1LENR_I2C1EN},  // kI2c1
  {REG_ADDR(RCC->APB1LENR), RCC_APB1LENR_I2C3EN, RCC_APB1LENR_I2C3EN},  // kI2c3
  {REG_ADDR(RCC->APB4ENR), RCC_APB4ENR_I2C4EN, RCC_APB4ENR_I2C4EN},  // kI2c4
  {REG_ADDR(RCC->APB2ENR), RCC_APB2ENR_USART1EN, RCC_APB2ENR_USART1EN},  // kUsart1
  {REG_ADDR(RCC->APB1LENR), RCC_APB1LENR_USART2EN, RCC_APB1LENR_USART2EN},  // kUsart2
  {REG_ADDR(RCC->APB1LENR), RCC_APB1LENR_USART3EN, RCC_APB1LENR_USART3EN},  // kUsart3
  {REG_ADDR(RCC->APB1LENR), RCC_APB1LENR_UART4EN, RCC_APB1LENR_UART4EN},  // kUart4
  {REG_ADDR(RCC->APB1LENR), RCC_APB1LENR_UART5EN, RCC_APB1LENR_UART5EN},  // kUart5
  {REG_ADDR(RCC->APB2ENR), RCC_APB2ENR_USART6EN, RCC_APB2ENR_USART6EN},  // kUsart6
  {REG_ADDR(RCC->APB2ENR), RCC_APB2ENR_TIM1EN, RCC_APB2ENR_TIM1EN},  // kTim1
  {REG_ADDR(RCC->APB1LENR), RCC_APB1LENR_TIM2EN, RCC_APB1LENR_TIM2EN},  // kTim2
  {REG_ADDR(RCC->APB1LENR), RCC_APB1LENR_TIM3EN, RCC_APB1LENR_TIM3EN},  // kTim3
  {REG_ADDR(RCC->APB1LENR), RCC_APB1LENR_TIM4EN, RCC_APB1LENR_TIM4EN},  // kTim4
  {REG_ADDR(RCC->APB1LENR), RCC_APB1LENR_TIM5EN, RCC_APB1LENR_TIM5EN},  // kTim5
  {REG_ADDR(RCC->AHB1ENR), RCC_AHB1ENR_ADC12EN, RCC_AHB1ENR_ADC12EN},  // kAdc12
  {REG_ADDR(RCC->AHB4ENR), RCC_AHB4ENR_ADC3EN, RCC_AHB4ENR_ADC3EN},  // kAdc3
  {REG_ADDR(RCC->AHB2ENR), RCC_AHB2ENR_RNGEN, RCC_AHB2ENR_RNGEN},  // kRng
  {REG_ADDR(RCC->AHB4ENR), RCC_AHB4ENR_HSEMEN, RCC_AHB4ENR_HSEMEN}  // kHsem
};

void Rcc::EnableOscillator(Oscillator oscillator) {
  switch (oscillator) {
    case kOscillatorHsi:
      SET_BIT(RCC->CR, RCC_CR_HSION);
      while (!READ_BIT(RCC->CR, RCC_CR_HSIRDY)) {}
      break;
    case kOscillatorCsi:
      SET_BIT(RCC->CR, RCC_CR_CSION);
      while (!READ_BIT(RCC->CR, RCC_CR_CSIRDY)) {}
      break;
    case kOscillatorHse:
      SET_BIT(RCC->CR, RCC_CR_HSEBYP);
      SET_BIT(RCC->CR, RCC_CR_HSEON);
      while (!READ_BIT(RCC->CR, RCC_CR_HSERDY)) {}
      break;
  }
}

void Rcc::EnablePll(Pll pll, PllConfig* pll_config) {
  switch (pll) {
    case kPll1:
      //! Disable PLL1
      CLEAR_BIT(RCC->CR, RCC_CR_PLL1ON);

      MODIFY_REG(RCC->PLLCKSELR, RCC_PLLCKSELR_DIVM1, pll_config->divm << RCC_PLLCKSELR_DIVM1_Pos);
      MODIFY_REG(RCC->PLL1FRACR, RCC_PLL1FRACR_FRACN1,
                 pll_config->fracn << RCC_PLL1FRACR_FRACN1_Pos);

      //! Enable FRACN & PLL1P
      SET_BIT(RCC->PLLCFGR, RCC_PLLCFGR_PLL1FRACEN);
      SET_BIT(RCC->PLLCFGR, RCC_PLLCFGR_DIVP1EN);
      SET_BIT(RCC->PLLCFGR, RCC_PLLCFGR_DIVQ1EN);
      // SET_BIT(RCC->PLLCFGR, RCC_PLLCFGR_DIVR1EN);
      MODIFY_REG(RCC->PLLCFGR, RCC_PLLCFGR_PLL1RGE, RCC_PLLCFGR_PLL1RGE_3);
      CLEAR_BIT(RCC->PLLCFGR, RCC_PLLCFGR_PLL1VCOSEL);

      MODIFY_REG(RCC->PLL1DIVR, RCC_PLL1DIVR_N1, (pll_config->divn-1UL) << RCC_PLL1DIVR_N1_Pos);
      MODIFY_REG(RCC->PLL1DIVR, RCC_PLL1DIVR_P1, (pll_config->divp-1UL) << RCC_PLL1DIVR_P1_Pos);
      MODIFY_REG(RCC->PLL1DIVR, RCC_PLL1DIVR_Q1, (pll_config->divq-1UL) << RCC_PLL1DIVR_Q1_Pos);
      MODIFY_REG(RCC->PLL1DIVR, RCC_PLL1DIVR_R1, (pll_config->divr-1UL) << RCC_PLL1DIVR_R1_Pos);

      //! Enable PLL1
      SET_BIT(RCC->CR, RCC_CR_PLL1ON);

      // Wait till PLL1 is ready
      while (!READ_BIT(RCC->CR, RCC_CR_PLL1RDY)) {}
      break;
    case kPll2:
      //! Disable PLL2
      CLEAR_BIT(RCC->CR, RCC_CR_PLL2ON);

      MODIFY_REG(RCC->PLLCKSELR, RCC_PLLCKSELR_DIVM2, pll_config->divm << RCC_PLLCKSELR_DIVM2_Pos);
      MODIFY_REG(RCC->PLL2FRACR, RCC_PLL2FRACR_FRACN2,
                 pll_config->fracn << RCC_PLL2FRACR_FRACN2_Pos);

      //! Enable PLL2P
      SET_BIT(RCC->PLLCFGR, RCC_PLLCFGR_PLL2FRACEN);
      SET_BIT(RCC->PLLCFGR, RCC_PLLCFGR_DIVP2EN);
      // SET_BIT(RCC->PLLCFGR, RCC_PLLCFGR_DIVQ2EN);
      // SET_BIT(RCC->PLLCFGR, RCC_PLLCFGR_DIVR2EN);
      MODIFY_REG(RCC->PLLCFGR, RCC_PLLCFGR_PLL2RGE, RCC_PLLCFGR_PLL2RGE_3);
      CLEAR_BIT(RCC->PLLCFGR, RCC_PLLCFGR_PLL2VCOSEL);

      MODIFY_REG(RCC->PLL2DIVR, RCC_PLL2DIVR_N2, (pll_config->divn-1UL) << RCC_PLL2DIVR_N2_Pos);
      MODIFY_REG(RCC->PLL2DIVR, RCC_PLL2DIVR_P2, (pll_config->divp-1UL) << RCC_PLL2DIVR_P2_Pos);
      MODIFY_REG(RCC->PLL2DIVR, RCC_PLL2DIVR_Q2, (pll_config->divq-1UL) << RCC_PLL2DIVR_Q2_Pos);
      MODIFY_REG(RCC->PLL2DIVR, RCC_PLL2DIVR_R2, (pll_config->divr-1UL) << RCC_PLL2DIVR_R2_Pos);

      //! Enable FRACN & PLL2
      SET_BIT(RCC->CR, RCC_CR_PLL2ON);

      // Wait till PLL2 is ready
      while (!READ_BIT(RCC->CR, RCC_CR_PLL2RDY)) {}
      break;
    case kPll3:
      //! Disable PLL3
      CLEAR_BIT(RCC->CR, RCC_CR_PLL3ON);

      MODIFY_REG(RCC->PLLCKSELR, RCC_PLLCKSELR_DIVM3, pll_config->divm << RCC_PLLCKSELR_DIVM3_Pos);
      MODIFY_REG(RCC->PLL3FRACR, RCC_PLL3FRACR_FRACN3,
                 pll_config->fracn << RCC_PLL3FRACR_FRACN3_Pos);

      //! Enable FRACN & PLL3P
      SET_BIT(RCC->PLLCFGR, RCC_PLLCFGR_PLL3FRACEN);
      SET_BIT(RCC->PLLCFGR, RCC_PLLCFGR_DIVP3EN);
      // SET_BIT(RCC->PLLCFGR, RCC_PLLCFGR_DIVQ3EN);
      // SET_BIT(RCC->PLLCFGR, RCC_PLLCFGR_DIVR3EN);
      MODIFY_REG(RCC->PLLCFGR, RCC_PLLCFGR_PLL3RGE, RCC_PLLCFGR_PLL3RGE_3);
      CLEAR_BIT(RCC->PLLCFGR, RCC_PLLCFGR_PLL3VCOSEL);

      MODIFY_REG(RCC->PLL3DIVR, RCC_PLL3DIVR_N3, (pll_config->divn-1UL) << RCC_PLL3DIVR_N3_Pos);
      MODIFY_REG(RCC->PLL3DIVR, RCC_PLL3DIVR_P3, (pll_config->divp-1UL) << RCC_PLL3DIVR_P3_Pos);
      MODIFY_REG(RCC->PLL3DIVR, RCC_PLL3DIVR_Q3, (pll_config->divq-1UL) << RCC_PLL3DIVR_Q3_Pos);
      MODIFY_REG(RCC->PLL3DIVR, RCC_PLL3DIVR_R3, (pll_config->divr-1UL) << RCC_PLL3DIVR_R3_Pos);

      //! Enable PLL3
      SET_BIT(RCC->CR, RCC_CR_PLL3ON);

      // Wait till PLL3 is ready
      while (!READ_BIT(RCC->CR, RCC_CR_PLL3RDY)) {}
      break;
  }
}

void Rcc::SetClockSource(ClockSource clock_source) {
  clock_source_config_[clock_source].Write();
}

void Rcc::ConfigureSysclkDomain() {
  // Set D1 domain Core prescaler
  MODIFY_REG(RCC->D1CFGR, RCC_D1CFGR_D1CPRE, RCC_D1CFGR_D1CPRE_DIV1);
  // Set D1 domain AHB prescaler
  MODIFY_REG(RCC->D1CFGR, RCC_D1CFGR_HPRE, RCC_D1CFGR_HPRE_DIV2);
  // Set D1 domain APB3 prescaler
  MODIFY_REG(RCC->D1CFGR, RCC_D1CFGR_D1PPRE, RCC_D1CFGR_D1PPRE_DIV2);
  // Set D2 domain APB1 prescaler
  MODIFY_REG(RCC->D2CFGR, RCC_D2CFGR_D2PPRE1, RCC_D2CFGR_D2PPRE1_DIV2);
  // Set D2 domain APB2 prescaler
  MODIFY_REG(RCC->D2CFGR, RCC_D2CFGR_D2PPRE2, RCC_D2CFGR_D2PPRE2_DIV2);
  // Set D3 domain APB4 prescaler
  MODIFY_REG(RCC->D3CFGR, RCC_D3CFGR_D3PPRE, RCC_D3CFGR_D3PPRE_DIV2);
  // Set MCO2 prescaler
  MODIFY_REG(RCC->CFGR, RCC_CFGR_MCO2PRE, RCC_CFGR_MCO2PRE_2);
}

void Rcc::EnablePeripheralClock(Peripheral peripheral) {
  peripheral_clock_config_[peripheral].Write();
  // Delay after an RCC peripheral clock enabling.
  peripheral_clock_config_[peripheral].Verify();
}

void Rcc::DisablePeripheralClock(Peripheral peripheral) {
  peripheral_clock_config_[peripheral].Clear();
}

uint32_t Rcc::GetFrequency(Clock clock) {
  switch (clock) {
    case kClockSysclk:
      return SystemCoreClock;
      break;
    case kClockD1Sysclk:
      return GetFrequency(kClockSysclk) >>
          (D1CorePrescTable[(RCC->D1CFGR & RCC_D1CFGR_D1CPRE)>> RCC_D1CFGR_D1CPRE_Pos] & 0x1FU);
      break;
    case kClockHclk:
      return GetFrequency(kClockD1Sysclk) >>
          ((D1CorePrescTable[(RCC->D1CFGR & RCC_D1CFGR_HPRE)>> RCC_D1CFGR_HPRE_Pos]) & 0x1FU);
      break;
    case kClockPclk1:
      return GetFrequency(kClockHclk) >>
          ((D1CorePrescTable[(RCC->D2CFGR & RCC_D2CFGR_D2PPRE1)>> RCC_D2CFGR_D2PPRE1_Pos]) & 0x1FU);
      break;
  }
  return 0;
}

}  // namespace hal
