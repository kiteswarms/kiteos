// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_HAL_I2C_SLAVE_HPP_
#define SRC_HAL_I2C_SLAVE_HPP_

//*****************************************************************************/
//! \addtogroup hal HAL API
//! @{
//! \addtogroup hal_i2c_api HAL I2C API
//! @{
///
//*****************************************************************************/
#include "hal/i2c/device.hpp"
#include "hal/i2c/definitions.hpp"

namespace hal {
namespace i2c {
/*!
 *  \brief   Implements the HAL I2C Slave class.
 *  \details This class is used to abstract the Device class.
 *
 *  \author   Elias Rosch
 *  \date     2020
 */
class Slave {
 public:
  /*!
   * \brief Constructor for Slave instance.
   *
   * \param port Port number of Device to initialize
   * \param slave_address Slave address bound to this instance
   * \param tx_buffer Buffer for transmitted bytes
   * \param rx_buffer Buffer for received bytes
   * \param cb_func Callback function to be called after transfers
   * \param cb_arg Argument provided in i2c callbacks
   */
  Slave(Port port, uint8_t slave_address, uint8_t* tx_buffer, uint8_t* rx_buffer,
        CallbackFunction cb_func, void* cb_arg);

  /*!
   * \brief Initiates a transfer of provided lengths.
   *
   * \param write_len number of bytes to write
   * \param read_len number of bytes to read
   *
   * \return true if successful, false otherwise
   */
  bool Transceive(uint8_t write_len, uint8_t read_len);

  /*!
   * \brief Initiates a transfer of provided lengths.
   *
   * \param write_len number of bytes to write
   * \param read_len number of bytes to read
   * \param cb_func Callback function to be called after transfers
   * \param cb_arg Argument provided in i2c callbacks
   *
   * \return true if successful, false otherwise
   */
  bool Transceive(uint8_t write_len, uint8_t read_len,
                  CallbackFunction cb_func, void* cb_arg);

  /*!
   * \brief Returns the current state of the slave.
   *
   * \return true if slave is busy, false otherwise.
   */
  bool IsBusy();

  /*!
   * \brief Returns the last error status.
   *
   * \return Error of last transmission.
   */
  Error GetErrorStatus();

 private:
  Device* device_;  //!< Device instance the slave is bound to
  Device::Job job_;  //!< Job used to submit to the Device
};
}  // namespace i2c
}  // namespace hal
/*! @} */
/*! @} */
#endif  // SRC_HAL_I2C_SLAVE_HPP_
