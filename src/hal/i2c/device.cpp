// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include <stm32h7xx.h>
#include "hal/i2c/device.hpp"
#include "utils/debug_utils.h"

//  Prescaler of I2C kernel clock (i2c_ker_ck)
#define TIMINGR_PRESC  (0x08U << I2C_TIMINGR_PRESC_Pos)
//  Delay cycles between SDA edge and SCL rising edge
#define TIMINGR_SCLDEL (0x03U << I2C_TIMINGR_SCLDEL_Pos)
//  Delay cycles between SCL falling edge and SDA edge
#define TIMINGR_SDADEL (0x03U << I2C_TIMINGR_SDADEL_Pos)
// Cycles defining the SCL high period in master mode
#define TIMINGR_SCLH   (0x0CU << I2C_TIMINGR_SCLH_Pos)
// Cycles defining the SCL low period in master mode
#define TIMINGR_SCLL   (0x0EU << I2C_TIMINGR_SCLL_Pos)

namespace hal {
namespace i2c {
/**************************************************************************************************
 *     Static member variables                                                                    *
 **************************************************************************************************/
Device* Device::device_registry_[3] = { nullptr };

const Device::PeripheralConfig Device::peripheral_config_[] = {
  // kPort1
  { I2C1,  //  base
    {I2C1_EV_IRQn, I2C1_ER_IRQn},  // interrupt_nos
    hal::Rcc::kI2c1,  // rcc_clock
    {hal::Gpio::kPortB, hal::Gpio::kPortB},  // gpio_port (SCL[0], SDA[1])
    {hal::Gpio::kPin8, hal::Gpio::kPin9},  // gpio_pins (SCL[0], SDA[1])
    hal::Gpio::Mode::kModeAF4I2c },  // gpio_mode
  // kPort3
  { I2C3,  //  base
    {I2C3_EV_IRQn, I2C3_ER_IRQn},  // interrupt_nos
    hal::Rcc::kI2c3,  // rcc_clock
    {hal::Gpio::kPortA, hal::Gpio::kPortC},  // gpio_port (SCL[0], SDA[1])
    {hal::Gpio::kPin8, hal::Gpio::kPin9},  // gpio_pins (SCL[0], SDA[1])
    hal::Gpio::Mode::kModeAF4I2c },  // gpio_mode
  // kPort4
  { I2C4,  //  base
    {I2C4_EV_IRQn, I2C4_ER_IRQn},  // interrupt_nos
    hal::Rcc::kI2c4,  // rcc_clock
    {hal::Gpio::kPortD, hal::Gpio::kPortD},  // gpio_port (SCL[0], SDA[1])
    {hal::Gpio::kPin12, hal::Gpio::kPin13},  // gpio_pins (SCL[0], SDA[1])
    hal::Gpio::Mode::kModeAF4I2c }  // gpio_mode
};

/**************************************************************************************************
 *     Public member functions                                                                    *
 **************************************************************************************************/
Device::Device(Port port): gpio_scl_ {peripheral_config_[port].gpio_port[0],
                                  peripheral_config_[port].gpio_pins[0],
                                  peripheral_config_[port].gpio_mode },
                           gpio_sda_ {peripheral_config_[port].gpio_port[1],
                                  peripheral_config_[port].gpio_pins[1],
                                  peripheral_config_[port].gpio_mode } {
      ASSERT(!device_registry_[port]);
      device_registry_[port] = this;

      port_ = port;
      peripheral_ = peripheral_config_[port_].base;
      state_ = kStateIdle;

      Rcc::EnablePeripheralClock(peripheral_config_[port_].rcc_clock);
      for (auto i : peripheral_config_[port_].interrupt_nos) {
          NVIC_EnableIRQ(i);
      }
      // Reset and Disable the selected I2c peripheral
      WRITE_REG(peripheral_->CR1, 0x00000000);

      // Configure I2C peripheral timing parameters.
      // Prescaler = 0
      // Delay between SDA edge and SCL rising edge = 145 cycles
      // SCL high period = 65 cycles
      // SCL low period = 145 cycles
      WRITE_REG(peripheral_->TIMINGR, TIMINGR_PRESC |
                                      TIMINGR_SCLDEL |
                                      TIMINGR_SDADEL |
                                      TIMINGR_SCLH |
                                      TIMINGR_SCLL);

      // Enable the selected I2C peripheral
      SET_BIT(peripheral_->CR1, I2C_CR1_PE);
}

Device::~Device() {
  device_registry_[port_] = nullptr;
}

bool Device::Transceive(Job* job) {
  if (job_queue_.IsFull()) {
    return false;
  }
  job->busy = true;
  job->error = kErrorNone;
  // Add element to the queue
  bool ret = job_queue_.WriteElement(job);
  // Start the job immediately if port is idle
  StartNextJob();
  return ret;
}

Device* Device::GetDevice(Port port) {
  if (!device_registry_[port]) {
    device_registry_[port] = new Device(port);
  }
  return device_registry_[port];
}

void Device::FreeRegistry() {
  for (auto i : device_registry_) {
    delete i;
  }
}
/**************************************************************************************************
 *     Private member functions                                                                   *
 **************************************************************************************************/
void Device::StartNextJob() {
  if ((state_ == kStateIdle) &&
      (!job_queue_.IsEmpty())) {
    if (job_queue_.ReadElement(&current_job_)) {
      if (current_job_->wlen) {
        StartWrite();
      } else if (current_job_->rlen) {
        StartRead();
      }
    }
  }
}

void Device::StartWrite() {
  state_ = kStateWriting;
  tx_count_ = 0;
  uint32_t autoend = current_job_->rlen? 0x00000000 : I2C_CR2_AUTOEND;
  // Start write operation:
  // SADD: Set slave address
  // NBYTES: Set transmission length
  // ~RD_WRN: dont set read request
  // START: Start transmission
  // ~STOP: dont generate STOP condition
  // AUTOEND: Automatic generate stop condition after NBYTES
  MODIFY_REG(peripheral_->CR2,
             I2C_CR2_SADD | I2C_CR2_NBYTES | I2C_CR2_RD_WRN |
             I2C_CR2_START | I2C_CR2_STOP | I2C_CR2_AUTOEND,
             (uint32_t)(((uint32_t)(current_job_->slave_address << 1) & I2C_CR2_SADD) |
                        (((uint32_t)current_job_->wlen << I2C_CR2_NBYTES_Pos) & I2C_CR2_NBYTES) |
                        I2C_CR2_START |
                        autoend));

  // Enable interrupts: Error, Stop, Not-acknowledge, Transmit
  SET_BIT(peripheral_->CR1,
          I2C_CR1_ERRIE | I2C_CR1_STOPIE | I2C_CR1_NACKIE | I2C_CR1_TXIE | I2C_CR1_TCIE);
}

void Device::StartRead() {
  state_ = kStateReading;
  rx_count_ = 0;
  // Start read operation:
  // SADD: Set slave address
  // NBYTES: Set transmission length
  // RD_WRN: set read request
  // START: Start transmission
  // ~STOP: dont generate STOP condition
  // AUTOEND: Automatic generate stop condition after NBYTES
  MODIFY_REG(peripheral_->CR2,
             I2C_CR2_SADD | I2C_CR2_NBYTES | I2C_CR2_RD_WRN |
             I2C_CR2_START | I2C_CR2_STOP | I2C_CR2_AUTOEND,
             (uint32_t)(((uint32_t)(current_job_->slave_address << 1) & I2C_CR2_SADD) |
                        (((uint32_t)current_job_->rlen << I2C_CR2_NBYTES_Pos) & I2C_CR2_NBYTES) |
                        I2C_CR2_RD_WRN |
                        I2C_CR2_START |
                        I2C_CR2_AUTOEND));
  // Enable interrupts: Error, Stop condition, Not-acknowledge, Receive
  SET_BIT(peripheral_->CR1,
          I2C_CR1_ERRIE | I2C_CR1_STOPIE | I2C_CR1_NACKIE | I2C_CR1_RXIE);
}

void Device::IRQHandler() {
  uint32_t itflag = peripheral_->ISR;

  if (READ_BIT(itflag, I2C_ISR_NACKF)) {
    // Clear flag
    SET_BIT(peripheral_->ICR, I2C_ICR_NACKCF);
    // Flush I2C_TXDR (send register)
    SET_BIT(peripheral_->ISR, I2C_ISR_TXE);

    current_job_->error = kErrorNack;
  } else if (READ_BIT(itflag, I2C_ISR_BERR)) {
    // Clear flag
    SET_BIT(peripheral_->ICR, I2C_ICR_BERRCF);
    current_job_->error = kErrorBus;
  } else if (READ_BIT(itflag, I2C_ISR_ARLO)) {
    // Clear flag
    SET_BIT(peripheral_->ICR, I2C_ICR_ARLOCF);
    current_job_->error = kErrorArbitration;
  } else if (READ_BIT(itflag, I2C_ISR_TXIS)) {
    // Write data to TXDR (send register)
    peripheral_->TXDR = current_job_->tx_buffer[tx_count_];
    tx_count_++;
  } else if (READ_BIT(itflag, I2C_ISR_RXNE)) {
    // Read data from RXDR (receive register)
    current_job_->rx_buffer[rx_count_] = peripheral_->RXDR;
    rx_count_++;
  }

  // Stop condition is detected
  if (READ_BIT(itflag, I2C_ISR_STOPF)) {
    // Clear flag
    SET_BIT(peripheral_->ICR, I2C_ICR_STOPCF);

    // Disable interrupts: Error, Stop condition, Not-acknowledge, Transmit, Receive
    CLEAR_BIT(peripheral_->CR1,
              I2C_CR1_ERRIE | I2C_CR1_STOPIE | I2C_CR1_NACKIE |
              I2C_CR1_TXIE | I2C_CR1_RXIE | I2C_CR1_TCIE);

    // If port was writing and job needs read,
    // start the read operation
    if ((state_ == kStateWriting) &&
        (current_job_->rlen > 0) &&
        (current_job_->error == kErrorNone)) {
      StartRead();
    } else {
      // Else call the user callback
      state_ = kStateIdle;
      current_job_->busy = false;
      if (current_job_->cb_func) {
        current_job_->cb_func(current_job_->error, current_job_->cb_arg);
      }
    }
    // Start next job if there is one.
    StartNextJob();
  } else if (READ_BIT(itflag, I2C_ISR_TC)) {
    // Repeated start condition is detected
    if (state_ == kStateWriting) {
      Device::StartRead();
    }
  }
}
}  // namespace i2c
}  // namespace hal

/**************************************************************************************************
 *     Global Interrupt handler implementations                                                   *
 **************************************************************************************************/
extern "C" {
//! Event Interrupt handler of I2C1
void I2C1_EV_IRQHandler() {
  hal::i2c::Device::device_registry_[hal::i2c::kPort1]->IRQHandler();
}
//! Error Interrupt handler of I2C1
void I2C1_ER_IRQHandler() {
  hal::i2c::Device::device_registry_[hal::i2c::kPort1]->IRQHandler();
}
//! Interrupt handler of I2C3
void I2C3_EV_IRQHandler() {
  hal::i2c::Device::device_registry_[hal::i2c::kPort3]->IRQHandler();
}
//! Error Interrupt handler of I2C3
void I2C3_ER_IRQHandler() {
  hal::i2c::Device::device_registry_[hal::i2c::kPort3]->IRQHandler();
}
//! Interrupt handler of I2C4
void I2C4_EV_IRQHandler() {
  hal::i2c::Device::device_registry_[hal::i2c::kPort4]->IRQHandler();
}
//! Error Interrupt handler of I2C4
void I2C4_ER_IRQHandler() {
  hal::i2c::Device::device_registry_[hal::i2c::kPort4]->IRQHandler();
}
}  // extern "C"
