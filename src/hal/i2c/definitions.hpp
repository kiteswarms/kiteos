// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_HAL_I2C_DEFINITIONS_HPP_
#define SRC_HAL_I2C_DEFINITIONS_HPP_

namespace hal {
namespace i2c {

//! I2C ports available on the microcontroller.
enum Port {
  kPort1,
  kPort3,
  kPort4
};

//! I2C errors that can arise.
enum Error {
  kErrorNone,  //!< No error occured
  kErrorNack,  //!< Nack received
  kErrorBus,  //!< misplaced Start or STOP condition is detected
  kErrorArbitration  //!< Arbitration loss
};

//! Typedef of function pointer used as callback after \ref I2c::Transceive finishes.
typedef void (*CallbackFunction)(Error error, void *cb_arg);

}  // namespace i2c
}  // namespace hal

#endif  // SRC_HAL_I2C_DEFINITIONS_HPP_
