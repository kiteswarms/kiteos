// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#include "hal/i2c/slave.hpp"

namespace hal {
namespace i2c {
/**************************************************************************************************
 *     Public member functions                                                                    *
 **************************************************************************************************/
Slave::Slave(Port port, uint8_t slave_address, uint8_t* tx_buffer, uint8_t* rx_buffer,
             CallbackFunction cb_func, void* cb_arg)
: device_(Device::GetDevice(port)), job_{ slave_address, tx_buffer, rx_buffer, 0, 0,
                                          cb_func, cb_arg, false } {}

bool Slave::Transceive(uint8_t write_len, uint8_t read_len) {
  if (job_.busy) {
    return false;
  }

  job_.wlen = write_len;
  job_.rlen = read_len;

  device_->Transceive(&job_);

  return true;
}

bool Slave::Transceive(uint8_t write_len, uint8_t read_len,
                       CallbackFunction cb_func, void* cb_arg) {
  job_.cb_func = cb_func;
  job_.cb_arg = cb_arg;
  return Transceive(write_len, read_len);
}

bool Slave::IsBusy() {
  return job_.busy;
}

Error Slave::GetErrorStatus() {
  return job_.error;
}
}  // namespace i2c
}  // namespace hal
