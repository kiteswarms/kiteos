// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_HAL_I2C_DEVICE_HPP_
#define SRC_HAL_I2C_DEVICE_HPP_

//*****************************************************************************/
//! \addtogroup hal HAL API
//! @{
//! \addtogroup hal_i2c_api HAL I2C API
//! @{
///
//*****************************************************************************/
#include "hal/i2c/definitions.hpp"
#include "hal/gpio.hpp"
#include "hal/rcc.hpp"
#include "utils/ring_buffer.hpp"

#define I2C_JOB_QUEUE_LEN 10

extern "C" {
  void I2C1_EV_IRQHandler();
  void I2C1_ER_IRQHandler();
  void I2C3_EV_IRQHandler();
  void I2C3_ER_IRQHandler();
  void I2C4_EV_IRQHandler();
  void I2C4_ER_IRQHandler();
}

namespace hal {
namespace i2c {
/*!
 *  \brief   Implements the HAL I2C Device class.
 *  \details This class is used to setup the I2C peripherals, configure and process the
 *           interrupts and callbacks.
 *
 *  \author   Elias Rosch
 *  \date     2020
 */
class Device {
 public:
  //! Data structure defining parameters of a transfer operation
  struct Job {
    uint8_t slave_address;     //!< I2c address to be used for the transfer
    uint8_t *tx_buffer;        //!< Buffer used for sending
    uint8_t *rx_buffer;        //!< Buffer used for receiving
    uint8_t wlen;              //!< Length of write operation
    uint8_t rlen;              //!< Length of read operation
    CallbackFunction cb_func;  //!< User callback after a transmission
    void *cb_arg;              //!< User callback argument
    bool busy;                 //!< Flag indicating if job is busy
    Error error;               //!< Variable indicating the error status of the transfer
  };

  /*!
   * \brief Constructor for I2c Device instance.
   *
   * \param port Port number that should be initialized
   */
  explicit Device(Port port);

  /*!
   * \brief Destructor for Spi Device instance.
   */
  ~Device();

  /*!
   * \brief Initiates a transfer of provided job.
   *
   * \param job pointer to job which defines the transfer parameters.
   *
   * \return true if successful, false otherwise.
   */
  bool Transceive(Job* job);

  /*!
   * \brief Return one of the static device objects.
   *
   * \return pointer to a static device object (of provided port).
   */
  static Device* GetDevice(Port port);

  /*!
   * \brief Destroy and  free all instances in registry
   */
  static void FreeRegistry();

 private:
  /*!
   * \brief Deques the next job from the job queue if Device is IDLE.
   */
  void StartNextJob();
  /*!
   * \brief Starts a write operation on the I2cDevice.
   */
  void StartWrite();
  /*!
   * \brief Starts a read operation on the I2cDevice.
   */
  void StartRead();

  /*!
   * \brief Generic I2C interrupt handler
   */
  void IRQHandler();

  //! States that the I2cDevice operates.
  enum State {
    kStateIdle,
    kStateWriting,
    kStateReading,
  };

  //! Data structure defining configuration parameters of an I2cDevice
  struct PeripheralConfig {
    I2C_TypeDef* base;              //!< Peripheral base address
    IRQn_Type interrupt_nos[2];     //!< Interrupts numbers to enable
    Rcc::Peripheral rcc_clock;      //!< Peripheral clock to enable
    Gpio::Port gpio_port[2];        //!< Gpio ports to enable (SCL[0], SDA[1])
    uint16_t gpio_pins[2];          //!< Gpio pins to enable  (SCL[0], SDA[1])
    Gpio::Mode gpio_mode;           //!< Gpio mode to enable
  };

  //! LUT containing the configurations for different ports
  static const PeripheralConfig peripheral_config_[];

  static Device* device_registry_[];  //!< References to instantiated devices

  Port port_;  //!< Enum defining the port of the device
  I2C_TypeDef* peripheral_;  //!< base address of the I2cDevice
  Gpio gpio_scl_;  //!< Gpio to enable
  Gpio gpio_sda_;  //!< Gpio to enable
  State state_;  //!< Current state of the I2cDevice
  RingBuffer<Job*, I2C_JOB_QUEUE_LEN> job_queue_;  //!< Ringbuffer of I2c jobs
  Job* current_job_;  //!< Job which is currently processed
  uint8_t tx_count_;  //!< Byte counter for transmitted bytes
  uint8_t rx_count_;  //!< Byte counter for received bytes

  friend void ::I2C1_EV_IRQHandler();  //!< Friended I2C1 event ISR to get access to class members
  friend void ::I2C1_ER_IRQHandler();  //!< Friended I2C1 error ISR to get access to class members
  friend void ::I2C3_EV_IRQHandler();  //!< Friended I2C3 event ISR to get access to class members
  friend void ::I2C3_ER_IRQHandler();  //!< Friended I2C3 error ISR to get access to class members
  friend void ::I2C4_EV_IRQHandler();  //!< Friended I2C4 event ISR to get access to class members
  friend void ::I2C4_ER_IRQHandler();  //!< Friended I2C4 error ISR to get access to class members
};
}  // namespace i2c
}  // namespace hal
/*! @} */
/*! @} */
#endif  // SRC_HAL_I2C_DEVICE_HPP_
