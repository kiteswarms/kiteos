// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_HAL_FLASH_HPP_
#define SRC_HAL_FLASH_HPP_

#include <cstdint>

namespace hal {
/*!
 *  \brief   Implements the HAL Flash class.
 *  \details This class is used to setup the Flash, write and read form it.
 *
 *  \author   Elias Rosch
 *  \date     2020
 */
class Flash {
 public:
  //! Enumerator for available flash banks
  enum Bank {
    kBank1 = 0x01U,  // Address Range: 0x08000000 - 0x080FFFFF
    kBank2 = 0x02U,  // Address Range: 0x08100000 - 0x081FFFFF
  };

  //! Enumerator for available flash sectors
  enum Sector {
    kSector0 = 0U,  // 0x08x00000 - 0x08x1FFFF
    kSector1 = 1U,  // 0x08x20000 - 0x08x3FFFF
    kSector2 = 2U,  // 0x08x40000 - 0x08x5FFFF
    kSector3 = 3U,  // 0x08x60000 - 0x08x7FFFF
    kSector4 = 4U,  // 0x08x80000 - 0x08x9FFFF
    kSector5 = 5U,  // 0x08xA0000 - 0x08xBFFFF
    kSector6 = 6U,  // 0x08xC0000 - 0x08xDFFFF
    kSector7 = 7U,  // 0x08xE0000 - 0x08xFFFFF
  };

  /*!
  * \brief     Reads data from flash.
  *
  * \details   Reads data from the flash memory. The data is read in blocks of 32 bits and stored in
  *            the data array. The lowest index is stored on the lowest memory address. The 32 bit
  *            values are stored in little-endian.
  *
  * \attention This function is blocking.
  *
  * \param     flash_address  Address to read from.
  * \param     data           Array to store the data.
  * \param     data_length    Number of values to read.
  */
  static void Read(uint32_t flash_address, uint32_t *data, uint16_t data_length);

  /*!
  * \brief     Erases flash sectors.
  *
  * \details   Erases a number of sectors in the flash memory. The sectors to erase are given by a
  *            bank number, a start sector number and the number of sectors to erase, starting from
  *            the start sector.
  *
  * \attention This function is blocking.
  *
  * \param     bank          Bank containing the sectors to erase.
  * \param     start_sector  First sector to erase.
  * \param     sector_cnt    Number of sectors to erase.
  *
  * \return    TRUE when erasing was successful. FALSE otherwise.
  */
  static bool EraseSectors(Bank bank, Sector start_sector, uint32_t sector_cnt);

  /*!
  * \brief     Erases a memory area.
  *
  * \details   Erases a memory area of a given size starting at a given address. All sectors which
  *            contain memory in the given address range will be erased. This may also include memory
  *            before or after the given area.
  *
  * \attention Always erases full flash sectors. This may also include memory before or after the
  *            given area. This function is blocking.
  *
  * \param     start_address  Start address of the memory area to erase.
  * \param     memory_size    Amount of memory to erase, starting from the start address.
  *
  * \return    TRUE when erasing was successful. FALSE otherwise.
  */
  static bool Erase(uint32_t start_address, uint32_t memory_size);

  /*!
  * \brief     Writes data to flash.
  *
  * \details   Writes data to the flash memory. The data is written in blocks of 32 bits. The lowest
  *            index is stored on the lowest memory address. The 32 bit values are stored in
  *            little-endian.
  *
  * \attention This function is blocking.
  *
  * \param     flash_address  Address to write to.
  * \param     data           Array to write to flash.
  * \param     data_length    Number of values to write.
  *
  * \return    TRUE when writing was successful. FALSE otherwise.
  */
  static bool Write(uint32_t flash_address, uint32_t *data, uint16_t data_length);

 private:
  /*!
   * \brief  Unlock the FLASH control registers access
   *
   * \return True if unlock was successful, false otherwise.
   */
  static bool Unlock();

  /*!
   * \brief  Lock the FLASH control registers access
   *
   * \return True if lock was successful, false otherwise.
   */
  static bool Lock();
};
}  // namespace hal
#endif  // SRC_HAL_FLASH_HPP_
