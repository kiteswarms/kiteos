// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_HAL_UART_HPP_
#define SRC_HAL_UART_HPP_

//*****************************************************************************
//
//! \addtogroup hal HAL API
//! @{
//!
//! \addtogroup hal_uart_api HAL UART API
//! @{
//
//*****************************************************************************
#include <stdbool.h>
#include <stdint.h>
#include "hal/rcc.hpp"
#include "hal/gpio.hpp"
#include "utils/ring_buffer.hpp"

#define HAL_UART_BUF_SIZE 512

extern "C" {
  void USART1_IRQHandler();
  void UART4_IRQHandler();
}

namespace hal {

/*!
 *  \brief   Implements the HAL Uart class.
 *  \details This class is used to setup the Uart, write and read form it.
 *
 *  \author   Elias Rosch
 *  \date     2020
 */
class Uart {
 public:
  //! UART ports available on the microcontroller.
  enum Port {
    kPort1,
    kPort4,
  };

  //! UART errors that can arise.
  enum Error {
    kErrorNone,  //!< No error occured
    kErrorFrame,  //!< Framing error or break character is detected
    kErrorNoise,  //!< Noise is detected
    kErrorOverrun,  //!< Overrun error is detected
    kErrorRxBufferOverflow,  //!< Overflow of rx buffer is detected
  };

  //! Typedef of function pointer used as callback by \ref hal::Uart::Write.
  typedef void (*CallbackFunction)(Error error, void *cb_arg);

  /*!
   * \brief Constructor for Uart instance.
   *
   * \param port Port number of Device to initialize
  *  \param baud_rate  UART baud rate to be used.
   */
  Uart(Port port, uint32_t baud_rate);

  /*!
  * \brief   Starts a UART transfer in a non-blocking manner.
  *
  * \details Prior to call to this function it is required initialize the UART port first! This
  *          function starts a non-blocking UART transfer, that is, the function will return to the
  *          caller right after initial processing. No callback is executed afterwards.
  *
  * \param   tx_data    Pointer to data buffer that should be transferred.
  * \param   data_len   Length of provided data to be transferred.
  *
  * \return  number of actually transferred bytes (may be less than \ref dataLen if ringbuffer is
  *          full).
  */
  bool Write(uint8_t *tx_data, uint16_t data_len);

  /*!
  * \brief   Starts a UART transfer in a non-blocking manner with callback.
  *
  * \details Prior to call to this function it is required initialize the UART port first! This
  *          function starts a non-blocking UART transfer, that is, the function will return to the
  *          caller right after initial processing. Provided callback is executed afterwards.
  *
  * \param   tx_data    Pointer to data buffer that should be transferred.
  * \param   data_len   Length of provided data to be transferred.
  * \param   cb         Function pointer to callback that should be executed upon finishing the
  *                     transfer.
  * \param   cb_arg     Pointer to callback argument that will be provided to \ref cb.
  *
  * \return  number of actually transferred bytes (may be 0 if uart is currently busy).
  */
  bool Write(uint8_t *tx_data, uint16_t data_len, CallbackFunction cb, void *cb_arg);

  /*!
  * \brief   Reads data from the rx ringbuf.
  *
  * \details Prior to call to this function it is required initialize the UART port first to ensure
  *          the rx transfer is enabled (otherwise rx ringbuf is always empty)!
  *
  * \param   rx_data    Pointer to data buffer that should be read to.
  * \param   data_len  Length of provided data to be transferred.
  *
  * \return  Number of actually read bytes (may be less than \ref data_len if rx * ringbuffer is
  *          empty).
  */
  uint16_t Read(uint8_t *rx_data, uint16_t data_len);

  /*!
  * \brief Resets the state of the corresponding rx ringbuf (all containing data is deleted).
  */
  void ClearRxBuffer();

  /*!
   * \brief Returns the current state of the Uart.
   *
   * \return true if Uart TX is busy, false otherwise.
   */
  bool IsBusy();

  /*!
   * \brief Returns the last error status.
   *
   * \return Uart::Error of last transmission.
   */
  Uart::Error GetErrorStatus();

 private:
  //! \brief Tx interrupt handler
  void TxIsr8Bit();
  //! \brief Rx interrupt handler
  void RxIsr8Bit();
  //! \brief Generic Uart interrupt handler
  void IRQHandler();

  //! Structure that holds callback information.
  struct Callback {
    CallbackFunction cb_func;  //!< function to be called
    void* cb_arg;  //!< argument to be provided in callback function
  };

  //! Typedef of structure defining a UART ringbuf.
  typedef struct {
    uint8_t ringbuf[HAL_UART_BUF_SIZE];  //!< Actual data buffer
    uint16_t write_index;                //!< Write index of ringbuf
    uint16_t read_index;                 //!< Read index of ringbuf
  } Ringbuf;

  //! Data structure defining configuration parameters of an Device
  struct PeripheralConfig {
    USART_TypeDef* base;  //!< Peripheral base address
    IRQn_Type interrupt_no;  //!< Interrupts numbers to enable
    Rcc::Peripheral rcc_clock;  //!< Peripheral clock to enable
    Gpio::Port gpio_port;  //!< Gpio port to enable for RX and TX
    uint16_t gpio_pins;  //!< Gpio in to enable for RX and TX
    Gpio::Mode gpio_mode;  //!< Gpio alternate function to enable for RX and TX
  };
  //! LUT containing the configurations for different ports
  static const PeripheralConfig peripheral_config_[];

  //! Holds the global registry of uart device instances for every UART port.
  static Uart* device_registry_[];

  Port port_;  //!< Enum defining the port of the device
  Gpio gpio_;  //!< Gpios used for RX and TX
  USART_TypeDef* peripheral_;  //!< base address of the Uart device
  bool busy_;  //!< Current state of the Uart device
  Error error_;  //!< Variable indicating the error status of the transfer
  uint8_t* tx_buffer_;  //!< Buffer for data to be sent
  uint16_t tx_length_;  //!< Number of bytes still to send
  RingBuffer<uint8_t, HAL_UART_BUF_SIZE> rx_buffer_;  //!< Ringbuffer of received bytes

  Callback callback_;  //!< User callback after a transmission

  friend void ::USART1_IRQHandler();  //!< Friended USART1 ISR to get access to class members
  friend void ::UART4_IRQHandler();  //!< Friended UART4 ISR to get access to class members
};
}  // namespace hal
/*! @} */
/*! @} */
#endif  // SRC_HAL_UART_HPP_
