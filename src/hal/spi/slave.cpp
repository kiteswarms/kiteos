// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#include "hal/spi/slave.hpp"

namespace hal {
namespace spi {
/**************************************************************************************************
 *     Public member functions                                                                    *
 **************************************************************************************************/
Slave::Slave(Port port, Config config, uint8_t* tx_buffer, uint8_t* rx_buffer,
             CallbackFunction cb_func, void* cb_arg)
: device_(Device::GetDevice(port)), job_{config,
                                         Gpio(config.chip_select_port, config.chip_select_pin,
                                              hal::Gpio::kModeOutput),
                                         tx_buffer, rx_buffer, 0, cb_func, cb_arg, false} {
  job_.chip_select.Write(true);
}

bool Slave::Transceive(uint8_t len) {
  if (job_.busy) {
    return false;
  }

  job_.len = len;

  device_->Transceive(&job_);

  return true;
}

bool Slave::Transceive(uint8_t len, CallbackFunction cb_func, void* cb_arg) {
  job_.cb_func = cb_func;
  job_.cb_arg = cb_arg;
  return Transceive(len);
}

bool Slave::IsBusy() {
  return job_.busy;
}

}  // namespace spi
}  // namespace hal
