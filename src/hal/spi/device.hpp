// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_HAL_SPI_DEVICE_HPP_
#define SRC_HAL_SPI_DEVICE_HPP_

//*****************************************************************************/
//! \addtogroup hal HAL API
//! @{
//! \addtogroup hal_spi_api HAL SPI API
//! @{
///
//*****************************************************************************/
#include "hal/spi/definitions.hpp"
#include "hal/gpio.hpp"
#include "hal/rcc.hpp"
#include "utils/ring_buffer.hpp"

#define SPI_JOB_QUEUE_LEN 10

extern "C" {
  void SPI1_IRQHandler();
  void SPI2_IRQHandler();
}
namespace hal {
namespace spi {
/*!
 *  \brief   Implements the HAL SPI Device class.
 *  \details This class is used to setup the SPI peripherals, configure and process the
 *           interrupts and callbacks.
 *
 *  \author   Elias Rosch
 *  \date     2020
 */
class Device {
 public:
  //! Data structure defining parameters of a transfer operation
  struct Job {
    Config config;              //!< Configuration of job parameters
    Gpio chip_select;           //!< GPIO used as chip select
    uint8_t *tx_buffer;         //!< Buffer used for sending
    uint8_t *rx_buffer;         //!< Buffer used for receiving
    uint8_t len;                //!< Length of transmission
    CallbackFunction cb_func;   //!< User callback after a transmission
    void *cb_arg;               //!< User callback argument
    bool busy;                  //!< Flag denoting the status of the transmission
  };

  /*!
   * \brief Constructor for Spi Device instance.
   *
   * \param port Port number that should be initialized
   */
  explicit Device(Port port);

  /*!
   * \brief Destructor for Spi Device instance.
   */
  ~Device();

  /*!
   * \brief Initiates a transfer of provided job.
   *
   * \param job pointer to job which defines the transfer parameters.
   *
   * \return true if successful, false otherwise.
   */
  bool Transceive(Job *job);

  /*!
   * \brief Return one of the static device objects.
   *
   * \return pointer to a static device object (of provided port).
   */
  static Device* GetDevice(Port port);

  /*!
   * \brief Destroy and  free all instances in registry
   */
  static void FreeRegistry();

 private:
  /*!
   * \brief Deques the next job from the job queue if Device is IDLE.
   */
  void StartNextJob();

  /*!
   * \brief Starts a transceive operation on the Device.
   */
  void StartTransceive();

  /*!
  * \brief Finishes a write operation on the I2cDevice.
  */
  void FinishTransceive();

  /*!
  * \brief Tx interrupt handler
  */
  void TxIsr8Bit();

  /*!
  * \brief Rx interrupt handler
  */
  void RxIsr8Bit();

  /*!
   * \brief Generic I2C interrupt handler
   */
  void IRQHandler();

  //! Data structure defining configuration parameters of an Device
  struct PeripheralConfig {
    SPI_TypeDef* base;  //!< Peripheral base address
    IRQn_Type interrupt_no;  //!< Interrupts numbers to enable
    Rcc::Peripheral rcc_clock;  //!< Peripheral clock to enable
    Gpio::Port gpio_port[2];  //!< Gpio port to enable for RX and TX
    uint16_t gpio_pins[2];  //!< Gpio in to enable for RX and TX
    Gpio::Mode gpio_mode;  //!< Gpio alternate function to enable for RX and TX
  };

  //! LUT containing the configurations for different ports
  static const PeripheralConfig peripheral_config_[];

  static Device* device_registry_[];  //!< References to instantiated devices

  Port port_;  //!< Port of this instance
  Gpio gpios_[2];  //!< Gpios to enable
  SPI_TypeDef* peripheral_;  //!< Peripheral context
  bool busy_;  //!< Current state of the Spi instance
  RingBuffer<Job*, SPI_JOB_QUEUE_LEN> job_queue_;  //!< Ringbuffer of I2c jobs
  Job* current_job_;  //!< Pointer to current spi job
  uint16_t tx_count_;  //!< Current TX transmission byte count
  uint16_t rx_count_;  //!< Current RX transmission byte count

  friend void ::SPI1_IRQHandler();  //!< Friended SPI1 ISR to get access to class members
  friend void ::SPI2_IRQHandler();  //!< Friended SPI2 ISR to get access to class members
};
}  // namespace spi
}  // namespace hal
/*! @} */
/*! @} */
#endif  // SRC_HAL_SPI_DEVICE_HPP_
