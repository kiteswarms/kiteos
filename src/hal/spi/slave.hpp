// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_HAL_SPI_SLAVE_HPP_
#define SRC_HAL_SPI_SLAVE_HPP_

//*****************************************************************************/
//! \addtogroup hal HAL API
//! @{
//! \addtogroup hal_spi_api HAL SPI API
//! @{
///
//*****************************************************************************/
#include "hal/spi/device.hpp"
#include "hal/spi/definitions.hpp"

namespace hal {
namespace spi {
/*!
 *  \brief   Implements the HAL SPI Slave class.
 *  \details This class is used to abstract the Device class.
 *
 *  \author   Elias Rosch
 *  \date     2020
 */
class Slave {
 public:
  /*!
   * \brief Constructor for Spi instance.
   *
   * \param port      Port number to initialize
   * \param config    Slave parameters
   * \param tx_buffer Buffer for transmitted bytes
   * \param rx_buffer Buffer for received bytes
   * \param cb_func Callback function to be called after transfers
   * \param cb_arg Argument provided in spi callbacks
   */
  Slave(Port port, Config config, uint8_t* tx_buffer, uint8_t* rx_buffer,
        CallbackFunction cb_func, void* cb_arg);

  /*!
   * \brief Initiates a transfer of provided lengths.
   *
   * \param len number of bytes to tranceive
   *
   * \return true if successful, false otherwise
   */
  bool Transceive(uint8_t len);

  /*!
   * \brief Initiates a transfer of provided lengths.
   *
   * \param len number of bytes to transceive
   * \param cb_func Callback function to be called after transfers
   * \param cb_arg Argument provided in spi callbacks
   *
   * \return true if successful, false otherwise
   */
  bool Transceive(uint8_t len, CallbackFunction cb_func, void* cb_arg);

  /*!
   * \brief Returns the current state of the slave.
   *
   * \return true if slave is busy, false otherwise.
   */
  bool IsBusy();

 private:
  Device* device_;  //!< Device instance the slave is bound to
  Device::Job job_;  //!< Job used to submit to the I2cDevice
};
}  // namespace spi
}  // namespace hal
/*! @} */
/*! @} */
#endif  // SRC_HAL_SPI_SLAVE_HPP_
