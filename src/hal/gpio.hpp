// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_HAL_GPIO_HPP_
#define SRC_HAL_GPIO_HPP_

#include <hal/rcc.hpp>

//*****************************************************************************/
//! \addtogroup hal HAL API
//! @{
//! \addtogroup hal_gpio_api HAL GPIO API
//! @{
///
//*****************************************************************************/
#define GPIO_PORT_CNT 11
#define GPIO_PIN_CNT 16

extern "C" {
void EXTI0_IRQHandler(void);
void EXTI1_IRQHandler(void);
void EXTI2_IRQHandler(void);
void EXTI3_IRQHandler(void);
void EXTI4_IRQHandler(void);
void EXTI9_5_IRQHandler(void);
void EXTI15_10_IRQHandler(void);
}

namespace hal {
/*!
 *  \brief   Implements the HAL GPIO class.
 *  \details This class is used to setup the GPIO peripherals, configure and process the
 *           interrupts and callbacks.
 *
 *  \author   Elias Rosch
 *  \date     2020
 */
class Gpio {
 public:
  //! GPIO ports available on the microcontroller.
  enum Port {
    kPortA,
    kPortB,
    kPortC,
    kPortD,
    kPortE,
    kPortF,
    kPortG,
    kPortH,
    kPortI,
    kPortJ,
    kPortK
  };

  //! GPIO pins available on the microcontroller.
  enum Pin {
    kPin0 = 0x0001,
    kPin1 = 0x0002,
    kPin2 = 0x0004,
    kPin3 = 0x0008,
    kPin4 = 0x0010,
    kPin5 = 0x0020,
    kPin6 = 0x0040,
    kPin7 = 0x0080,
    kPin8 = 0x0100,
    kPin9 = 0x0200,
    kPin10 = 0x0400,
    kPin11 = 0x0800,
    kPin12 = 0x1000,
    kPin13 = 0x2000,
    kPin14 = 0x4000,
    kPin15 = 0x8000,
  };


  //! Pin modes that can be configured.
  enum Mode {
    kModeInput,
    kModeOutput,
    kModeInterruptRising,
    kModeInterruptFalling,
    kModeAF0Mco2,
    kModeAF1Tim1Ch4,
    kModeAF2Tim3Ch4,
    kModeAF4I2c,
    kModeAF5Spi,
    kModeAF7Uart,
    kModeAF8Uart,
    kModeAF11Ethernet,
    kModeAnalog,
  };

  //! Pull modes that can be configured.
  enum Pull {
    kPullNopull = 0x0,
    kPullPullup = 0x1,
    kPullPulldown = 0x2
  };

  //! Configuration that defines Gpio operation.
  struct Config {
    Port port;  //!<  Gpio::Port that should be configured
    uint16_t pins;  //!<  Gpio::Pins that should be configured
    Mode mode;  //!<  Gpio::Mode that should be configured
  };

  //! Typedef of function pointer used as callback by \ref SetInterrupt.
  typedef void (*CallbackFunction)(void *cb_arg);

  //! Constructor without callback.
  Gpio(Port port, Pin pins, Mode mode);

  //! Constructor without callback.
  explicit Gpio(Config config);

  //! Constructor with callback, without callback argument.
  Gpio(Port port, Pin pins, Mode mode, CallbackFunction cb_func);

  //! Constructor with callback and callback argument.
  Gpio(Port port, Pin pins, Mode mode, CallbackFunction cb_func, void* cb_arg);

  //! Constructor without callback.
  Gpio(Port port, uint16_t pins, Mode mode);

  //! Constructor with callback, without callback argument.
  Gpio(Port port, uint16_t pins, Mode mode, CallbackFunction cb_func);

  //! Constructor with callback and callback argument.
  Gpio(Port port, uint16_t pins, Mode mode, CallbackFunction cb_func, void* cb_arg);

  //! Destructor for a Gpio instance.
  ~Gpio();

  //! Sets the pull of the GPIO
  void SetPull(Pull pull);

  //! Writes a value to the GPIO instance.
  void Write(uint16_t value);

  //! Writes a value to the GPIO instance. If the GPIO instance contains multiple pins, all Pins are
  //! written with the same value.
  void Write(bool value);

  //! Reads from the GPIO instance.
  uint16_t Read();

 private:
  //! Data structure defining configuration parameters of a Gpio
  struct PeripheralConfig {
    GPIO_TypeDef* base;              //!< Peripheral base address
    Rcc::Peripheral rcc_clock;      //!< Peripheral clock to enable
  };

  //! LUT containing the configurations for different ports
  static const PeripheralConfig peripheral_config_[];

  //! Data structure defining configuration parameters of an I
  struct ModeConfig {
    uint32_t moder;   //!< Configuration of MODER register
    uint32_t pupdr;   //!< Configuration of PUPDR register
    uint32_t otyper;  //!< Configuration of OTYPER register
    uint32_t afr;     //!< Configuration of AFR register
  };
  //! LUT containing the configurations for different ports
  static const ModeConfig mode_config_[];

  //! Structure that holds callback information.
  struct Callback {
    CallbackFunction cb_func;  //!< function to be called
    void* cb_arg;  //!< argument to be provided in callback function
  };

  //! Holds the global registry of callbacks for every pin number.
  static Callback* callback_registry_[];

  //! Registry containing information about pins that are already in use.
  static uint16_t gpio_registry_[];

  //! Configures the interrupt for this GPIO instance.
  bool SetupInterrupt();

  Port port_;           //!< The port of a GPIO instance
  uint16_t pins_;       //!< The pin mask that is configured by the instance
  Mode mode_;           //!< The mode in which the instance is running
  Callback callback_;   //!< Callback informations used in interrupt mode

  friend void ::EXTI0_IRQHandler(void);  //!< Friend EXTI0 ISR for access to class members
  friend void ::EXTI1_IRQHandler(void);  //!< Friend EXTI1 ISR for access to class members
  friend void ::EXTI2_IRQHandler(void);  //!< Friend EXTI2 ISR for access to class members
  friend void ::EXTI3_IRQHandler(void);  //!< Friend EXTI3 ISR for access to class members
  friend void ::EXTI4_IRQHandler(void);  //!< Friend EXTI4 ISR for access to class members
  friend void ::EXTI9_5_IRQHandler(void);  //!< Friend EXTI9_5 ISR for access to class members
  friend void :: EXTI15_10_IRQHandler(void);  //!< Friend EXTI15_10 ISR for access to class members
};
}  // namespace hal
/*! @} */
/*! @} */
#endif  // SRC_HAL_GPIO_HPP_
