// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_HAL_TIMER_HPP_
#define SRC_HAL_TIMER_HPP_

#include <stdbool.h>
#include <stdint.h>
#include "hal/rcc.hpp"

//*****************************************************************************
//
//! \addtogroup hal HAL API
//! @{
//!
//! \addtogroup hal_timer_api HAL Timer API
//! @{
//
//*****************************************************************************
#define TIMER_CNT 5

extern "C" {
  void TIM1_IRQHandler();
  void TIM2_IRQHandler();
  void TIM3_IRQHandler();
  void TIM4_IRQHandler();
  void TIM5_IRQHandler();
}

namespace hal {
/*!
 *  \brief   Implements the HAL Timer class.
 *  \details This class is used to setup the timer peripherals.
 *
 *  \author   Elias Rosch
 *  \date     2020
 */
class Timer {
 public:
  //! Enumerator of supported timers.
  enum Device{
    kDevice1,  //!< 16-bit
    kDevice2,  //!< 32-bit
    kDevice3,  //!< 16-bit
    kDevice4,  //!< 16-bit
    kDevice5   //!< 32-bit
  };

  //! Typedef of user callback function signature.
  typedef void (*CallbackFunction)(void *cb_arg);

  /*!
   * \brief Constructor for Timer Device instance.
   *
   * \param device Device number that should be initialized.
   */
  explicit Timer(Device device);

  /*!
   * \brief Deconstructor for Timer Device instance.
   */
  ~Timer();
  /*!
  * \brief   Initializes and starts the hardware timer that periodically executes callbacks.
  *
  * \details The user gives the timeout value that is the relative future time when the timer
  *          expires.
  *
  * \param   period_us  Execution intervall in microseconds.
  * \param   cb_func    Callback that should be executed periodically.
  * \param   cb_arg     Argument that should be provided to the callback.
  */
  void PeriodicCallback(uint16_t period_us, CallbackFunction cb_func, void *cb_arg);

  /*!
  * \brief   Configures the hardware timer to generate a PWM signal.
  * \note    Currently PWM generation is only supported on channel 4.
  *
  * \param   period_us  Period length of a single PWM cycle in microseconds.
  * \param   dutycycle  Dutycycle to be modulated.
  */
  void PwmEnable(uint32_t period_us, double dutycycle);

  /*!
  * \brief   Configures the period length of an already enabled PWM timer
  *
  * \param   period_us  Period length of a single PWM cycle in microseconds.
  * \param   dutycycle  Dutycycle to be modulated.
  */
  void PwmSet(uint32_t period_us, double dutycycle = 0.5);

  /*!
  * \brief disables the hardware timer.
  */
  void Disable();

  /*!
  * \brief  Read the value of the hardware timer.
  *
  * \return Current timer value.
  */
  uint32_t GetValue();

  /*!
  * \brief Set the value of the hardware timer.
  *
  * \param value   Value that should be written to hardware timer count.
  */
  void SetValue(uint32_t value);

 private:
  //! Data structure defining configuration parameters of an Timer device.
  struct PeripheralConfig {
    TIM_TypeDef* peripheral;    //!< Peripheral base address.
    IRQn_Type interrupt_no;     //!< Interrupts numbers to enable.
    Rcc::Peripheral rcc_clock;  //!< Peripheral clock to enable.
  };
  //! LUT containing the configurations for different Timer devices.
  static const PeripheralConfig peripheral_config_[];

  //! Struct definition for a timer context.
  struct Callback {
    CallbackFunction cb_func;  //!< User callback function pointer.
    void *cb_arg;              //!< User callback argument.
  };

  //! Registry containing information about timers that are already in use.
  static Timer* timer_registry_[];

  //! Holds the timer input clock frequency in MHz.
  static uint32_t in_clk_freq_;

  Device device_;            //!< The identifier of the Timer to use.
  TIM_TypeDef* peripheral_;  //!< Peripheral context.
  Callback callback_;        //!< Callback informations used in interrupt.

  //! Function called from the interrupt service routines.
  static void IRQHandler(Device device);

  friend void ::TIM1_IRQHandler();  //!< Friended TIM1 ISR to get access to class members.
  friend void ::TIM2_IRQHandler();  //!< Friended TIM2 ISR to get access to class members.
  friend void ::TIM3_IRQHandler();  //!< Friended TIM3 ISR to get access to class members.
  friend void ::TIM4_IRQHandler();  //!< Friended TIM4 ISR to get access to class members.
  friend void ::TIM5_IRQHandler();  //!< Friended TIM5 ISR to get access to class members.
};
}  // namespace hal
//*****************************************************************************
//
// Close the Doxygen groups.
//! @}
//! @}
//
//*****************************************************************************
#endif  // SRC_HAL_TIMER_HPP_
