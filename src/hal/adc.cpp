// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "hal/adc.hpp"
#include "hal/rcc.hpp"
#include "utils/debug_utils.h"

/**************************************************************************************************
 *                  Defines/Macros                                                                *
 **************************************************************************************************/
#define OVERSAMPLING_RATIO 1024  // 1024-oversampling
#define RIGHTBITSHIFT 10  // 10-bit right shift of the oversampled summation

namespace hal {

/**************************************************************************************************
 *     Static member variables                                                                    *
 **************************************************************************************************/
const Adc::PeripheralConfig Adc::peripheral_config_[] = {
  { ADC1,
    ADC12_COMMON,
    ADC_IRQn,
    hal::Rcc::kAdc12,
    5  // GpioPB1 channel number
  },
  { ADC3,
    ADC3_COMMON,
    ADC3_IRQn,
    hal::Rcc::kAdc3,
    18  // Internal temperature sensor channel number
  }
};

Adc* Adc::adc_registry_[2] = {nullptr};

/**************************************************************************************************
 *     API function implementations                                                               *
 **************************************************************************************************/
Adc::Adc(Channel channel)
  : channel_(channel),
    peripheral_(peripheral_config_[channel].base) {
  ASSERT(adc_registry_[channel_] == nullptr);
  adc_registry_[channel_] = this;
  Rcc::EnablePeripheralClock(peripheral_config_[channel].rcc_clock);

  NVIC_EnableIRQ(peripheral_config_[channel].interrupt_no);

  // Configure ADC clock:
  // Mode ASYNC (using pll2p ~ 100 MHz)
  // Prescaler 16 -> 6.25 MHz (0.16 us cycle)
  // Asynchronous clock mode [CKMODE = 00],  prescaler 16 [PRESC = 111]
  MODIFY_REG(peripheral_config_[channel].common->CCR,
             ADC_CCR_CKMODE | ADC_CCR_PRESC, ADC_CCR_PRESC_2 | ADC_CCR_PRESC_1 | ADC_CCR_PRESC_0);

  // configure Gpio if the gpio channel is used
  if (channel_ == kChannelGpioPB1) {
    gpio_.emplace(Gpio::kPortB, Gpio::kPin1, Gpio::kModeAnalog);
  }
  // wake up the temperature sensor from power-down mode
  // if the temperature sensor channel is used
  if (channel_ == kChannelTemperatureSensor) {
    SET_BIT(peripheral_config_[channel].common->CCR, ADC_CCR_TSEN);
  }

  // Disable ADC for configuration
  SET_BIT(peripheral_->CR, ADC_CR_ADDIS);
  // Wait until ADC is disabled
  while (READ_BIT(peripheral_->CR, ADC_CR_ADDIS)) {}

  // Exit from deep-power-down mode
  CLEAR_BIT(peripheral_->CR, ADC_CR_DEEPPWD);
  // Enable voltage regulator
  SET_BIT(peripheral_->CR, ADC_CR_ADVREGEN);
  // Wait until voltage regulator started
  while (!READ_BIT(peripheral_->CR, ADC_CR_ADVREGEN)) {}

  // Overwrite DR when overrun is detected, resolution 16 bit
  MODIFY_REG(peripheral_->CFGR, ADC_CFGR_OVRMOD | ADC_CFGR_RES, ADC_CFGR_OVRMOD);

  // Regular Oversampling Enable
  MODIFY_REG(peripheral_->CFGR2,
             ADC_CFGR2_LSHIFT | ADC_CFGR2_OVSR | ADC_CFGR2_OVSS | ADC_CFGR2_ROVSE,
             (OVERSAMPLING_RATIO - 1UL) << ADC_CFGR2_OVSR_Pos |
             RIGHTBITSHIFT << ADC_CFGR2_OVSS_Pos |
             ADC_CFGR2_ROVSE);

  // Disable BOOST (make sure to adapt this in newer revision uCs [STM32H7 RevV])
  CLEAR_BIT(peripheral_->CR, ADC_CR_BOOST);

  // Sequencer disabled (ADC conversion on only 1 channel: channel set on rank 1)
  CLEAR_BIT(peripheral_->SQR1, ADC_SQR1_L);

  // Start ADC calibration in mode single-ended and without linearity calibration
  MODIFY_REG(peripheral_->CR, ADC_CR_ADCAL | ADC_CR_ADCALDIF | ADC_CR_ADCALLIN, ADC_CR_ADCAL);

  // Wait for calibration completion
  while (READ_BIT(peripheral_->CR, ADC_CR_ADCAL)) {}

  // Channel configuration
  // Configure channel to be used
  SET_BIT(peripheral_->PCSEL, 1UL << peripheral_config_[channel].channel);
  // Configure sequencer rank of channel
  MODIFY_REG(peripheral_->SQR1, ADC_SQR1_SQ1,
                                peripheral_config_[channel].channel << ADC_SQR1_SQ1_Pos);

  // Configure sampling time of channel to 64.5 clock cycles => 0.16 * 64.5 = 10.32 us > 9 us
  if (peripheral_config_[channel].channel < 10) {
    Register::BitfieldWrite(REG_ADDR(peripheral_->SMPR1),
                            ADC_SMPR1_SMP0_2 | ADC_SMPR1_SMP0_0,
                            peripheral_config_[channel].channel, 3);
  } else {
    Register::BitfieldWrite(REG_ADDR(peripheral_->SMPR2),
                            ADC_SMPR2_SMP10_2 | ADC_SMPR2_SMP10_0,
                            peripheral_config_[channel].channel - 10, 3);
  }

  // Set channel mode single-ended
  CLEAR_BIT(peripheral_->DIFSEL, 1UL << peripheral_config_[channel].channel);

  // Enable ADC
  SET_BIT(peripheral_->CR, ADC_CR_ADEN);

  // Disable all interruptions before enabling the desired ones
  SET_BIT(peripheral_->IER, ADC_IER_EOSIE);
}

void Adc::StartConversion(CallbackFunction cb_func, void* cb_arg) {
  callback_ = { cb_func, cb_arg };
  // Start ADC regular conversion
  SET_BIT(peripheral_->CR, ADC_CR_ADSTART);
  busy_ = true;
}

bool Adc::IsBusy() {
  return busy_;
}

double Adc::GetProcessedValue() {
  if (channel_ == kChannelTemperatureSensor) {
    // Memory locations of the temperature sensor calibration values according to data sheet p. 179
    auto* calibration_value_30 = reinterpret_cast<uint16_t*>(0x1FF1E820);
    auto* calibration_value_110 = reinterpret_cast<uint16_t*>(0x1FF1E840);
    // Temperature calculation in °C according to reference manual p. 999
    return ((110 - 30) / static_cast<double>(*calibration_value_110 - *calibration_value_30))
           * static_cast<double>(peripheral_->DR - *calibration_value_30) + 30;
  }
  return peripheral_->DR * 3.3 / 0xFFFF;
}
}  // namespace hal

extern "C" {
//! ADC12 interrupt handler
void ADC_IRQHandler() {
  if (READ_BIT(ADC1->ISR, ADC_ISR_EOS)) {
    // Clear interrupt flag
    SET_BIT(ADC1->ISR, ADC_ISR_EOS);
    auto channel = hal::Adc::adc_registry_[hal::Adc::kChannelGpioPB1];
    if (channel) {
      channel->callback_.cb_func(channel->GetProcessedValue(), channel->callback_.cb_arg);
    }
    channel->busy_ = false;
  }
}

//! ADC3 interrupt handler
void ADC3_IRQHandler() {
  if (READ_BIT(ADC3->ISR, ADC_ISR_EOS)) {
    // Clear interrupt flag
    SET_BIT(ADC3->ISR, ADC_ISR_EOS);
    auto channel = hal::Adc::adc_registry_[hal::Adc::kChannelTemperatureSensor];
    if (channel) {
      channel->callback_.cb_func(channel->GetProcessedValue(), channel->callback_.cb_arg);
    }
    channel->busy_ = false;
  }
}
}  // extern "C"
