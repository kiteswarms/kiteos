// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_HAL_INTERRUPTS_HPP_
#define SRC_HAL_INTERRUPTS_HPP_

namespace hal {
//! Implements the HAL interrupts class which is used for enabling and disabling interrupts
//! globally.
class Interrupts {
 public:
  /*!
   * \brief   Enables all interrupts globally.
   *
   * \details This enables only interrupts that have been enabled also individually.
   */
  static void Enable();

  /*!
   * \brief Disables all interrupts globally.
   */
  static void Disable();
};
}  // namespace hal

#endif  // SRC_HAL_INTERRUPTS_HPP_
