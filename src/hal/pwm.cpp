// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#include "hal/pwm.hpp"

namespace hal {
/**************************************************************************************************
 *     Global variables                                                                           *
 **************************************************************************************************/
const Pwm::Config Pwm::pwm_config_[] = {
  {Gpio::kPortE, Gpio::kPin14, Gpio::kModeAF1Tim1Ch4, Timer::kDevice1},  // kPinE14
  {Gpio::kPortB, Gpio::kPin1, Gpio::kModeAF2Tim3Ch4, Timer::kDevice3}  // kPinB1
};

/**************************************************************************************************
 *     API function implementations                                                               *
 **************************************************************************************************/
Pwm::Pwm(Pin pin, uint32_t period, double dutycycle)
  : gpio_(Gpio(pwm_config_[pin].gpio_port, pwm_config_[pin].gpio_pin, pwm_config_[pin].gpio_mode)),
    timer_(Timer(pwm_config_[pin].timer_device)),
    period_(period),
    dutycycle_(dutycycle) {
  timer_.PwmEnable(period_, dutycycle_);
}

void Pwm::SetPeriod(uint32_t period) {
  period_ = period;
  timer_.PwmSet(period_, dutycycle_);
}

void Pwm::SetDutycycle(double dutycycle) {
  dutycycle_ = dutycycle;
  timer_.PwmSet(period_ , dutycycle_);
}

}  // namespace hal
