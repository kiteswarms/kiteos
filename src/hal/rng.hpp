// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_HAL_RNG_HPP_
#define SRC_HAL_RNG_HPP_

#include <stm32h7xx.h>

//*****************************************************************************
//
//! \addtogroup hal HAL API
//! @{
//!
//! \addtogroup hal_rng_api HAL RNG API
//! @{
//
//*****************************************************************************
namespace hal {
/*!
 *  \brief   Implements the HAL Rng class.
 *  \details This class is used to setup the random number generator peripheral.
 *
 *  \author   Elias Rosch
 *  \date     2020
 */
class Rng {
 public:
  /*!
  * \brief   Initialize the HAL rng module.
  *
  * \details A call to this function is required prior to use any rng function.
  *          Following operations are performed by this function: *
  *          - Enable the RNG controller clock.
  *          - Activate the RNG peripheral.
  *
  * \return  TRUE if initialized successfully. FALSE otherwise.
  */
  static bool Init(void);


  /*!
  * \brief  Generates a 32-bit random number. Blocks until the next random number is ready.
  *
  * \return 32-bit random number. Returns 0 if not successful.
  */
  static uint32_t GetNumberBlocking(void);

  /*!
  * \brief  Generates a 32-bit pseudo random number.
  *
  * \details The seed of the pseudo random number generator is a true random number.
  *
  * \return 32-bit pseudo random number.
  */
  static uint32_t GetPseudoRandomNumber(void);

 private:
  static uint32_t latest_random_;  //!< latest returned random value.
  static uint32_t lfsr_value_;  //!< Value of the lfsr used for pseudo random number.
};
};  // namespace hal

//*****************************************************************************
//
// Close the Doxygen groups.
//! @}
//! @}
//
//*****************************************************************************
#endif  // SRC_HAL_RNG_HPP_
