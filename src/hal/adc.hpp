// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_HAL_ADC_HPP_
#define SRC_HAL_ADC_HPP_

  //*****************************************************************************
  //
  //! \addtogroup hal HAL API
  //! @{
  //!
  //! \addtogroup hal_adc_api HAL ADC API
  //! @{
  //
  //*****************************************************************************
#include <optional>
#include "hal/gpio.hpp"

extern "C" {
  void ADC_IRQHandler();
  void ADC3_IRQHandler();
}

namespace hal {
/*!
 *  \brief    Implements the HAL ADC class.
 *  \details  This class is used to setup the ADC peripheral.
 *
 *  \author   Elias Rosch
 *  \date     2020
 */
class Adc {
 public:
  //! Adc analog channels that can be used.
  enum Channel {
    kChannelGpioPB1,
    // Internal microcontroller core junction temperature sensor channel
    kChannelTemperatureSensor
  };

  /*! Typedef of function pointer used as callback by \ref HalAdcInit. */
  typedef void (*CallbackFunction)(double current_adc_val, void *cb_arg);

  /*! \brief Constructor for Spi Device instance.
   *
   *  \param port Port number that should be initialized
   */
  explicit Adc(Channel gpio);

  /*! \brief  Starts an ADC conversion and calls the callback specified during init as soon as the
   *          result is valid.
   */
  void StartConversion(CallbackFunction cb_func, void* cb_arg);

  /*!
   * \brief Returns the current state of the adc.
   *
   * \return true if adc is busy, false otherwise.
   */
  bool IsBusy();

 private:
  /*!
   * \brief Returns the processed measurement value of the ADC according to the channel type.
   *
   * \return Measured voltage for GPIO channel, temperature for temperature channel.
   */
  double GetProcessedValue();
  //! Structure that holds callback information.
  struct Callback {
    CallbackFunction cb_func;  //!< function to be called
    void* cb_arg;  //!< argument to be provided in callback function
  };

  //! Data structure defining configuration parameters of an Adc device
  struct PeripheralConfig {
    ADC_TypeDef* base;  //!< Peripheral base address
    ADC_Common_TypeDef* common;  //!< Peripheral shared base address
    IRQn_Type interrupt_no;  //!< Interrupts numbers to enable
    Rcc::Peripheral rcc_clock;  //!< Peripheral clock to enable
    uint8_t channel;  //!< Channel that should be converted in ADC
  };

  //! LUT containing the configurations for different read channels devices
  static const PeripheralConfig peripheral_config_[];
  static Adc* adc_registry_[];  //!< Holds the global registry of every used adc.

  Channel channel_;  //!< The identifier of the ADC GPIO to use
  ADC_TypeDef* peripheral_;  //!< Peripheral context
  bool busy_;  //!< Flag that shows if the adc is currently busy converting.
  Callback callback_;  //!< Callback informations used in interrupt
  std::optional<Gpio> gpio_;  //!< optional Gpio to configure

  friend void ::ADC_IRQHandler();  //!< Friended ADC12 ISR to get access to class members
  friend void ::ADC3_IRQHandler();  //!< Friended ADC3 ISR to get access to class members
};
}  // namespace hal
//*****************************************************************************
//
// Close the Doxygen groups.
//! @}
//! @}
//
//*****************************************************************************
#endif  // SRC_HAL_ADC_HPP_
