// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_HAL_PWM_HPP_
#define SRC_HAL_PWM_HPP_

#include <stdbool.h>
#include <stdint.h>
#include "hal/gpio.hpp"
#include "hal/timer.hpp"

//*****************************************************************************
//
//! \addtogroup hal HAL API
//! @{
//!
//! \addtogroup hal_pwm_api HAL Hardware PWM API
//! @{
//
//*****************************************************************************

namespace hal {
/*!
 *  \brief   Implements the HAL Pwm class.
 *  \details This class is used to setup the timer and gpio peripherals to generate a PWM signal.
 *
 *  \author   Elias Rosch
 *  \date     2020
 */
class Pwm {
 public:
  //! Enumerator of supported PWM pins
  enum Pin{
    kPinE14,
    kPinB1
  };

  /*!
   * \brief Constructor for Pwm instance.
   *
   * \param pin        Pin number that should be initialized.
   * \param period     Period length of a single PWM cycle in microseconds.
   * \param dutycycle  Dutycycle to be modulated.
   */
  explicit Pwm(Pin pin, uint32_t period, double dutycycle);

  /*!
  * \brief   Configures the period length of an already enabled PWM timer.
  *
  * \param   period  Period length of a single PWM cycle in microseconds.
  */
  void SetPeriod(uint32_t period);

  /*!
  * \brief   Configures the duty cycle of an already enabled PWM timer.
  *
  * \param   dutycycle  Dutycycle to be modulated.
  */
  void SetDutycycle(double dutycycle);

 private:
  //! Data structure defining configuration parameters of an Timer device.
  struct Config {
    Gpio::Port gpio_port;        //!< Gpio port to enable for PWM.
    Gpio::Pin gpio_pin;          //!< Gpio pin to enable for PWM.
    Gpio::Mode gpio_mode;        //!< Gpio alternate function to enable for PWM.
    Timer::Device timer_device;  //!< Timer device to use for PWM generation.
  };

  //! LUT containing the configurations for different Timer devices.
  static const Config pwm_config_[];

  Gpio gpio_;         //!< The GPIO to use for PWM output.
  Timer timer_;       //!< The timer which generates the PWM signal.
  uint32_t period_;   //!< Current period of a PWM cycle in microseconds.
  double dutycycle_;  //!< Current dutycycle of the PWM.
};
}  // namespace hal
//*****************************************************************************
//
// Close the Doxygen groups.
//! @}
//! @}
//
//*****************************************************************************
#endif  // SRC_HAL_PWM_HPP_
