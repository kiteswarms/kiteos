// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_HAL_SYSTICK_TIMER_HPP_
#define SRC_HAL_SYSTICK_TIMER_HPP_

#include <cstdint>

//*****************************************************************************
//
//! \addtogroup hal HAL API
//! @{
//!
//! \addtogroup hal_systick_timer_api HAL Sysick Timer API
//! @{
//
//*****************************************************************************

extern "C" {
void SysTick_Handler(void);
}

namespace hal {
/*!
*  \brief   Implements the HAL Systick Timer Device class.
*  \details This class is used to setup the System Tick, configure and process the
*           interrupts and callbacks.
*
*  \author   Elias Rosch
*  \date     2020
*/
class SystickTimer {
 public:
  //! Typedef of function pointer used as callback when timer elapses
  typedef void (*CallbackFunction)(void *cb_arg);

  /*!
   * \brief   Initializes the systick timer class.
   *
   * \details This function has to be executed once before using the systick timer class.
   *
   * \return  true if successful, false otherwise.
   */
  static bool Init();

  /*!
   * \brief Gets current tick count.
   *
   * \return Current counter of ticks.
   */
  static uint64_t GetCount();

  /*!
   * \brief Sets a timeout to be called every sys tick.
   *
   * \param callback           Function to be called every sys tick.
   * \param callback_argument  Argument that is provided in callback function.
   */
  static void RegisterSystickCallback(CallbackFunction callback, void *callback_argument);

 private:
  static uint64_t tick_count_;                //!< current count of ticks.
  static CallbackFunction systick_callback_;  //!< callback to be invoked on sys tick events.
  static void* systick_callback_argument_;    //!< argument provided to callback.

  friend void ::SysTick_Handler(void);  //!< Friend Systick ISR for access to class members.
};
}  // namespace hal
/*! @} */
/*! @} */
#endif  // SRC_HAL_SYSTICK_TIMER_HPP_
