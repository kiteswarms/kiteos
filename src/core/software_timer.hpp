// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_CORE_SOFTWARE_TIMER_HPP_
#define SRC_CORE_SOFTWARE_TIMER_HPP_

#include <cstdint>
#include <chrono>
#include "hal/systick_timer.hpp"

#define SOFTWARE_TIMER_NUM_CALLBACKS 20

/*!
 *  \brief   Implements software timers based on the systick timer.
 *
 *  \details This class implements software timers with millisecond resolution. These timers are all
 *           driven by the sys tick hardware timer. They offer a polling and an interrupt-API.
 */
class SoftwareTimer {
 public:
  //! Typedef of function pointer used as callback when timer elapses.
  template<typename CONTEXT_TYPE>
  using CallbackFunction = void(*)(CONTEXT_TYPE*);

  /*!
   * \brief Constructor for SoftwareTimer instance.
   */
  SoftwareTimer() = default;

  /*!
   * \brief Deconstructor for SoftwareTimer instance.
   */
  ~SoftwareTimer();

  /*!
   * \brief   Initializes the software timer class.
   *
   * \details This function has to be executed once before using the software timer class.
   */
  static void Init();

  /*!
   * \brief  Sets a timeout when the timer should elapse.
   *
   * \param  timeout  Timeout to be set in milliseconds.
   *
   * \return If a callback needs to be registered for this timer, this method returns if the
   *         registration was successful. Otherwise it returns true.
   */
  bool SetTimeout(uint32_t timeout);

  /*!
   * \brief Sets a timeout when the timer should elapse.
   *
   * \param timeout  Timeout duration to be set.
   */
  bool SetTimeout(std::chrono::milliseconds timeout);

  /*!
   * \name  SetTimeoutPeriodicFunctionGroup
   */
  ///@{
  /*!
   * \brief Sets an interval when the timer should elapse,
   *        and a callback to be executed after each period.
   *
   * \param interval  Interval to be set in milliseconds.
   * \param cb_func   Function to be called when timer elapses. This callback is called in a
   *                  synchronous context.
   * \param cb_arg    Argument that is provided in callback function.
   *
   * \return True on success, false if the callback could not be registered.
   */
  bool SetTimeoutPeriodic(uint32_t interval, CallbackFunction<void> cb_func, void* cb_arg);

  template<typename CONTEXT_TYPE>
  bool SetTimeoutPeriodic(uint32_t interval,
                          CallbackFunction<CONTEXT_TYPE> cb_func, CONTEXT_TYPE* cb_arg) {
    return SetTimeoutPeriodic(interval, reinterpret_cast<CallbackFunction<void>>(cb_func),
                              reinterpret_cast<void*>(cb_arg));
  }
  ///@}

  /*!
   * \name  SetTimeoutPeriodicWithChronoFunctionGroup
   */
  ///@{
  /*!
   * \brief Sets an interval when the timer should elapse,
   *        and a callback to be executed after each period.
   *
   * \param timeout   Interval duration to be set.
   * \param cb_func   Function to be called when timer elapses. This callback is called in a
   *                  synchronous context.
   * \param cb_arg    Argument that is provided in callback function.
   *
   * \return True on success, false if the callback could not be registered.
   */
  bool SetTimeoutPeriodic(std::chrono::milliseconds timeout,
                          CallbackFunction<void> cb_func, void* cb_arg);

  template<typename CONTEXT_TYPE>
  bool SetTimeoutPeriodic(std::chrono::milliseconds timeout,
                          CallbackFunction<CONTEXT_TYPE> cb_func, CONTEXT_TYPE* cb_arg) {
    return SetTimeoutPeriodic(timeout, reinterpret_cast<CallbackFunction<void>>(cb_func),
                              reinterpret_cast<void*>(cb_arg));
  }
  ///@}

  /*!
   * \name  SetCallbackFunctionGroup
   */
  ///@{
  /*!
   * \brief  Sets a callback to be executed when the timer elapses.
   *
   * \param  cb_func  Function to be called when timer elapses. This callback is called in a
   *                  synchronous context.
   * \param  cb_arg   Argument that is provided in callback function.
   *
   * \return True on success, false if the callback could not be registered.
   */
  bool SetCallback(CallbackFunction<void> cb_func, void* cb_arg);

  template<typename CONTEXT_TYPE>
  bool SetCallback(CallbackFunction<CONTEXT_TYPE> cb_func, CONTEXT_TYPE* cb_arg) {
    return SetCallback(reinterpret_cast<CallbackFunction<void>>(cb_func),
                       reinterpret_cast<void*>(cb_arg));
  }
  ///@}

  /*!
   * \brief  Clears the callback registered with SetCallback.
   *
   * \return True on success, false if no callback was registered.
   */
  bool ClearCallback();

  /*!
   * \brief  Gets status of SoftwareTimer.
   *
   * \return True if timer elapsed, false otherwise.
   */
  inline bool Elapsed() {
    return  hal::SystickTimer::GetCount() >= elapse_time_;
  }

  /*!
  * \brief  Stops a SoftwareTimer from being executed.
  */
  void Stop();

 private:
  bool RegisterTimer();  //!< Adds the timer to the timer callback registry
  bool UnregisterTimer();  //!< Removes the timer from the timer callback registry
  static void UpdateNextElapsing();  //!< Updates the reference to the next elapsing timer.

  //! Callback called by the scheduler in synchronous context after it was enqueued by the
  //! systick handler. Processes elapsed timers.
  static void ProcessSystickEventTask(void *arg);

  //! Callback called every sys tick by the sys tick timer.
  static void SysTickHandler(void* context);

  //! Timers registered for callbacks.
  static SoftwareTimer* timer_registry_[SOFTWARE_TIMER_NUM_CALLBACKS];
  static SoftwareTimer* next_elapsing_;  //!< Reference to the next timer to elapse.

  //! True if a process systick event task is enqueued in the scheduler.
  static bool sys_tick_enqueued_;

  uint64_t elapse_time_ = 0;  //!< Count of ticks when timer is considered elapsed.
  CallbackFunction<void> cb_func_ = nullptr;  //!< Callback to be invoked when timer elapses.
  void* cb_arg_ = nullptr;  //!< Argument provided to callback.
  uint32_t interval_ = 0;   //!< Interval value for periodic callbacks.
  //! Flag indicating if the timer is currently registered in the timer registry.
  bool registered_ = false;
};

#endif  // SRC_CORE_SOFTWARE_TIMER_HPP_
