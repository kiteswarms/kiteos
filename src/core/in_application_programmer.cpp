// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "in_application_programmer.hpp"
#include <string>
#include "hal/flash.hpp"
#include "core/logger.hpp"
#include "core/name_provider.hpp"
#include "core/system_time/system_time.hpp"
#include "core/persistent_memory/persistent_memory.hpp"

// Message structure:
//
// Bytes:          0 - 22        23 - 45            46           47 - n
//              ------------------------------------------------------------
// Description: | Source ID | Destination ID | Message Type | Message Data |
//              ------------------------------------------------------------

// Message definitions
constexpr const uint8_t kIapMsgState = 0;         //!< Message type denoting a state message.
constexpr const uint8_t kIapMsgEraseCommand = 1;  //!< Message type denoting an erase command.
//! Message type denoting an erase command response.
constexpr const uint8_t kIapMsgEraseResponse = 2;
constexpr const uint8_t kIapMsgWriteCommand = 3;  //!< Message type denoting a write command.
//! Message type denoting a write command response.
constexpr const uint8_t kIapMsgWriteResponse = 4;
constexpr const uint8_t kIapMsgReadCommand = 5;  //!< Message type denoting a read command.
//! Message type denoting a read command response.
constexpr const uint8_t kIapMsgReadResponse = 6;
constexpr const uint8_t kIapMsgJumpCommand = 7;  //!< Message type denoting a jump command.
//! Message type denoting a jump command response.
constexpr const uint8_t kIapMsgJumpResponse = 8;
//! Message type denoting a set firmware broken command.
constexpr const uint8_t kIapMsgSetFirmwareBrokenCommand = 9;
//! Message type denoting a set firmware broken response.
constexpr const uint8_t kIapMsgSetFirmwareBrokenResponse = 10;
//! Message type denoting a reset command.
constexpr const uint8_t kIapMsgResetCommand = 100;
//! Message type denoting a reset response.
constexpr const uint8_t kIapMsgResetResponse = 101;
constexpr const uint8_t kIapMsgError = 255;  //!< Message type denoting an error message.

// Memory area definitions
constexpr const uint8_t kIapBootloaderMemory = 0;  //!< Flag denoting flashing of bootloader.
constexpr const uint8_t kIapFirmwareMemory = 1;    //!< Flag denoting flashing of firmware.

//! Number of 32 bit values contained in 1 data block
static constexpr const int kIapDataBlockSize = 128;

// Error types
//! Error type indicating that an unknown command code was received
constexpr const uint8_t kIapErrorUnknownCommand = 0;
//! Error type indicating that the received command has an unexpected length
constexpr const uint8_t kIapErrorInvalidCommandLength = 1;
/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
InApplicationProgrammer::InApplicationProgrammer(pulicast::Namespace puli_namespace) :
  communication_channel_(puli_namespace["/iap"]) {
#ifdef BOARD_BOOTLOADER
  // Check boot flags
  bool stay_in_bootloader = false;
  PersistentMemory::GetValue(PersistentMemory::kStayInBootloader, &stay_in_bootloader);
  bool firmware_broken = false;
  PersistentMemory::GetValue(PersistentMemory::kFirmwareBroken, &firmware_broken);

  if (!(stay_in_bootloader || firmware_broken)) {
    // Boot flag set -> jump to application
    JumpToAddress(FIRMWARE_ADDR);

    // Print error if jump was unsuccessful
    Logger::Report("No firmware found.", Logger::LogLevel::kFatal);
  } else {
    // Reset boot flag for the next power up
    ASSERT_RESPONSE(PersistentMemory::SetValue(PersistentMemory::kStayInBootloader, false), true);
  }
#endif

  // Subscribe to command channel
  communication_channel_.Subscribe<kitecom::timestamped_vector_byte>(
      [this](const kitecom::timestamped_vector_byte& message) {
        CommandHandler(message);
  });

  state_timer_.SetTimeoutPeriodic(1000, StateTimerCallback, this);
}
/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
bool InApplicationProgrammer::JumpToAddress(uint32_t address) {
  // Ensure that the memory to jump to is not empty
  if ((*(__IO uint32_t *)address) != 0xffffffff) {
    // Disable interrupts. Interrupts are enabled again after jump.
    __disable_irq();

    // Set new vector table offset
    SCB->VTOR = address;

    // Initialize application stack pointer
    __set_MSP(*(__IO uint32_t *)address);

    // Jump to application
    typedef void (*PFunction)();
    uint32_t jump_address = *(__IO uint32_t *)(address + 4);
    auto jump_to_application = (PFunction)jump_address;
    jump_to_application();
  }

  return false;
}

void InApplicationProgrammer::SendResponse(std::vector<uint8_t> &values) {
  kitecom::timestamped_vector_byte msg;
  msg.ts = SystemTime::GetTimestamp();
  msg.values = values;
  msg.len = msg.values.size();
  communication_channel_ << msg;
}

void InApplicationProgrammer::HandleCommand(const std::vector<uint8_t > &command) {
  // Check if the command size is invalid
  if (command.size() < 47) {
    return;
  }

  // Check if the target ID of the message matches the ID of this board
  for (uint32_t i = 0; i < 23; i++) {
    if (command[i + 23] != NameProvider::GetBoardIdentifierBinary()[i]) {
      return;
    }
  }

  uint8_t message_type = command[46];  // First byte defines message type
  switch (message_type) {
    case kIapMsgEraseCommand:
      HandleEraseCommand(command);
      break;

    case kIapMsgWriteCommand:
      HandleWriteCommand(command);
      break;

    case kIapMsgReadCommand:
      HandleReadCommand(command);
      break;

    case kIapMsgJumpCommand:
      HandleJumpCommand(command);
      break;

    case kIapMsgSetFirmwareBrokenCommand:
      HandleSetFirmwareBrokenCommand(command);
      break;

    case kIapMsgResetCommand:
      HandleResetCommand(command);
      break;

    default:
      PublishErrorResponse(command, kIapErrorUnknownCommand);
      break;
  }
}

void InApplicationProgrammer::HandleEraseCommand(const std::vector<uint8_t > &command) {
  // Check if the command size valid
  if (command.size() < 48) {
    PublishErrorResponse(command, kIapErrorInvalidCommandLength);
    return;
  }

  //  Parse command
  uint8_t memory_area = command[47];

  // Ensure that a program does not erase itself
#if defined(BOARD_BOOTLOADER)
  uint8_t error = memory_area == kIapBootloaderMemory ? 0x01 : 0x00;
#else
  uint8_t error = memory_area == kIapFirmwareMemory ? 0x01 : 0x00;
#endif

  // Erase memory area
  if (error == 0x00) {
    switch (memory_area) {
      case kIapBootloaderMemory:
        hal::Flash::Erase(BOOTLOADER_ADDR, BOOTLOADER_SIZE);
        break;
      case kIapFirmwareMemory:
        hal::Flash::Erase(FIRMWARE_ADDR, FIRMWARE_SIZE);
        break;
      default:
        error = 0x01;
    }
  }

  // Send response
  std::vector<uint8_t> response;
  response.reserve(23 + 23 + 3);
  SetResponseSourceAndDestination(command, response);
  response.push_back(kIapMsgEraseResponse);
  response.push_back(memory_area);
  response.push_back(error);
  SendResponse(response);
}

void InApplicationProgrammer::HandleWriteCommand(const std::vector<uint8_t > &command) {
  // Check if the command size valid
  if (command.size() < (23 + 23 + 4 + 4 * kIapDataBlockSize)) {
    PublishErrorResponse(command, kIapErrorInvalidCommandLength);
    return;
  }

  //  Parse command
  uint8_t memory_area = command[47];
  uint16_t data_block_index = (((uint16_t)command[48]) << 8u) | ((uint16_t)command[49]);
  uint32_t data_block[kIapDataBlockSize];
  for (int i = 0; i < kIapDataBlockSize; i++) {
    data_block[i] = (uint32_t)(command[50 + 4 * i]) | (((uint32_t)(command[51 + 4 * i])) << 8u) |
                    (((uint32_t)(command[52 + 4 * i])) << 16u) |
                    (((uint32_t)(command[53 + 4 * i])) << 24u);
  }

  // Ensure that a program does not overwrite itself
#if defined(BOARD_BOOTLOADER)
  uint8_t error = memory_area == kIapBootloaderMemory ? 0x01 : 0x00;
#else
  uint8_t error = memory_area == kIapFirmwareMemory ? 0x01 : 0x00;
#endif

  // Write to area
  if (error == 0x00) {
    switch (memory_area) {
      case kIapBootloaderMemory:
        if (!hal::Flash::Write(BOOTLOADER_ADDR + (4 * kIapDataBlockSize * data_block_index),
                               data_block, kIapDataBlockSize)) {
          error |=0x02;
        }
        break;
      case kIapFirmwareMemory:
        if (!hal::Flash::Write(FIRMWARE_ADDR + (4 * kIapDataBlockSize * data_block_index),
                               data_block, kIapDataBlockSize)) {
          error |=0x02;
        }
        break;
      default:
        error |= 0x04;
    }
  }

  // Send response
  std::vector<uint8_t> response;
  response.reserve(23 + 23 + 5);
  SetResponseSourceAndDestination(command, response);
  response.push_back(kIapMsgWriteResponse);
  response.push_back(memory_area);
  response.push_back((uint8_t)((data_block_index & 0xff00u) >> 8u));
  response.push_back((uint8_t)(data_block_index & 0x00ffu));
  response.push_back(error);
  SendResponse(response);
}

void InApplicationProgrammer::HandleReadCommand(const std::vector<uint8_t > &command) {
  // Check if the command size valid
  if (command.size() < 50) {
    PublishErrorResponse(command, kIapErrorInvalidCommandLength);
    return;
  }

  //  Parse command
  uint8_t memory_area = command[47];
  uint16_t data_block_index = (((uint16_t)command[48]) << 8u) | ((uint16_t)command[49]);

  // Read data block
  uint32_t data_block[kIapDataBlockSize];
  switch (memory_area) {
    case kIapBootloaderMemory:
      hal::Flash::Read(BOOTLOADER_ADDR + (4 * kIapDataBlockSize * data_block_index), data_block,
                   kIapDataBlockSize);
      break;

    case kIapFirmwareMemory:
      hal::Flash::Read(FIRMWARE_ADDR + (4 * kIapDataBlockSize * data_block_index), data_block,
                   kIapDataBlockSize);
      break;

    default:
      return;  // Invalid command
  }

  // Send response
  std::vector<uint8_t> response;
  response.reserve(23 + 23 + 4 + 4 * kIapDataBlockSize);
  SetResponseSourceAndDestination(command, response);
  response.push_back(kIapMsgReadResponse);
  response.push_back(memory_area);
  response.push_back((uint8_t)((data_block_index & 0xff00u) >> 8u));
  response.push_back((uint8_t)(data_block_index & 0x00ffu));
  for (uint32_t i : data_block) {
    response.push_back((uint8_t)(i & 0x000000ffu));
    response.push_back((uint8_t)((i & 0x0000ff00u) >> 8u));
    response.push_back((uint8_t)((i & 0x00ff0000u) >> 16u));
    response.push_back((uint8_t)((i & 0xff000000u) >> 24u));
  }
  SendResponse(response);
}

void InApplicationProgrammer::HandleJumpCommand(const std::vector<uint8_t > &command) {
  // Check if the command size valid
  if (command.size() < 48) {
    PublishErrorResponse(command, kIapErrorInvalidCommandLength);
    return;
  }

  //  Parse command
  uint8_t memory_area = command[47];

  // Ensure that a program does not overwrite itself
#if defined(BOARD_BOOTLOADER)
  uint8_t error = memory_area == kIapBootloaderMemory ? 0x01 : 0x00;
#else
  uint8_t error = memory_area == kIapFirmwareMemory ? 0x01 : 0x00;
#endif

  switch (memory_area) {
    case kIapBootloaderMemory: {
      // Set boot flag
      ASSERT_RESPONSE(PersistentMemory::SetValue(PersistentMemory::kStayInBootloader, true), true);

      // Send response TODO: send response after jump
      std::vector<uint8_t> response;
      response.reserve(23 + 23 + 3);
      SetResponseSourceAndDestination(command, response);
      response.push_back(kIapMsgJumpResponse);
      response.push_back(memory_area);
      response.push_back(error);
      SendResponse(response);

      if (error == 0) {
        // Wait 100ms to ensure that the response was sent before the jump
        SoftwareTimer delay_timer;
        delay_timer.SetTimeout(100);
        while (!delay_timer.Elapsed()) {}

        // Stay in bootloader flag must be synchronized with the persistent memory hardware in the
        // background before the jump is executed, therefore wait 100ms before executing the jump.
        jump_to_bootloader_timer_.SetTimeout(100);
        jump_to_bootloader_timer_.SetCallback(JumpToBootloaderTimerCallback, this);
      }
      break;
    }

    case kIapFirmwareMemory: {
      bool firmware_broken = false;
      PersistentMemory::GetValue(PersistentMemory::kFirmwareBroken, &firmware_broken);
      if (firmware_broken) {
        error |= 0x02;
      }

      // Send response TODO: send response after jump
      std::vector<uint8_t> response;
      response.reserve(23 + 23 + 3);
      SetResponseSourceAndDestination(command, response);
      response.push_back(kIapMsgJumpResponse);
      response.push_back(memory_area);
      response.push_back(error);
      SendResponse(response);

      if (error == 0) {
        // Wait 100ms to ensure that the response was sent before the jump
        SoftwareTimer delay_timer;
        delay_timer.SetTimeout(100);
        while (!delay_timer.Elapsed()) {}

        // Jump to bootloader
        JumpToAddress(FIRMWARE_ADDR);

        // Print error if jump was unsuccessful
        Logger::Report("Cannot jump to firmware. No firmware found.", Logger::LogLevel::kError);
      }
      break;
    }

    default:
      return;  // Invalid command
  }
}

void InApplicationProgrammer::HandleSetFirmwareBrokenCommand(
    const std::vector<uint8_t> &command) {
  // Check if the command size valid
  if (command.size() < 48) {
    PublishErrorResponse(command, kIapErrorInvalidCommandLength);
    return;
  }

  //  Parse command
  uint8_t firmware_broken = command[47];

  // Set firmware broken flag in persistent memory according to parsed value
  ASSERT_RESPONSE(PersistentMemory::SetValue(PersistentMemory::kFirmwareBroken,
                                             firmware_broken != 0), true);

  // Send response
  std::vector<uint8_t> response;
  response.reserve(23 + 23 + 2);
  SetResponseSourceAndDestination(command, response);
  response.push_back(kIapMsgSetFirmwareBrokenResponse);
  response.push_back(firmware_broken);
  SendResponse(response);
}

void InApplicationProgrammer::HandleResetCommand(const std::vector<uint8_t> &command) {
  // Send response
  std::vector<uint8_t> response;
  response.reserve(23 + 23 + 1);
  SetResponseSourceAndDestination(command, response);
  response.push_back(kIapMsgResetResponse);
  SendResponse(response);

  // Wait 100ms to ensure that the response was sent before reset
  SoftwareTimer delay_timer;
  delay_timer.SetTimeout(100);
  while (!delay_timer.Elapsed()) {}

  NVIC_SystemReset();
}

void InApplicationProgrammer::PublishErrorResponse(const std::vector<uint8_t> &command,
                                                   uint8_t error_type) {
  // Assemble an error message
  std::vector<uint8_t> response;
  // Allocate enough memory to hold the full message
  response.reserve(23 + 23 + 2);
  SetResponseSourceAndDestination(command, response);
  response.push_back(kIapMsgError);
  response.push_back(error_type);

  // Publish error message
  SendResponse(response);
}

void InApplicationProgrammer::SetResponseSourceAndDestination(const std::vector<uint8_t> &command,
                                                              std::vector<uint8_t> &response) {
  // Source ID is board identifier
  for (uint32_t i = 0; i < 23; i++) {
    response.push_back(NameProvider::GetBoardIdentifierBinary()[i]);
  }
  // Destination ID is command source ID
  for (uint32_t i = 0; i < 23; i++) {
    response.push_back(command[i]);
  }
}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void InApplicationProgrammer::CommandHandler(const kitecom::timestamped_vector_byte &msg) {
  HandleCommand(msg.values);
}

void InApplicationProgrammer::StateTimerCallback(InApplicationProgrammer *inst) {
  // Send state message periodically
  std::vector<uint8_t> state_message;
  // Allocate enough memory to hold the full message
  state_message.reserve(23 + 23 + 2);

  // Source ID is board identifier
  for (uint32_t i = 0; i < 23; i++) {
    state_message.push_back(NameProvider::GetBoardIdentifierBinary()[i]);
  }

  // Destination ID is broadcast address (all bytes 1)
  for (uint32_t i = 0; i < 23; i++) {
    state_message.push_back(0xFF);
  }

  state_message.push_back(kIapMsgState);

#ifdef BOARD_BOOTLOADER
  state_message.push_back(kIapBootloaderMemory);
#endif
#ifndef BOARD_BOOTLOADER
  state_message.push_back(kIapFirmwareMemory);
#endif

  inst->SendResponse(state_message);
}

void InApplicationProgrammer::JumpToBootloaderTimerCallback(InApplicationProgrammer *inst) {
  // Jump to bootloader
  inst->JumpToAddress(BOOTLOADER_ADDR);

  // Print error if jump was unsuccessful
  Logger::Report("Cannot jump to bootloader. No bootloader found.", Logger::LogLevel::kError);
}
