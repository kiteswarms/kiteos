// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
// Define macro for poisoned FramAccess class.
#define FRAM_ACCESS FramAccess

#include "core.hpp"
#include <memory>
#include "utils/debug_utils.h"
#include "core/board_signals.hpp"
#include "core/persistent_memory/fram_access.hpp"
#include "core/name_provider.hpp"
#include "core/ethernet_services/ethernet_services.hpp"
#include "core/system_time/system_time.hpp"
#include "core/plugin_manager/plugin_manager.hpp"
#include "core/plugin_manager/external_plugin_manager.hpp"
#include "core/software_timer.hpp"
#include "core/logger.hpp"
#include "core/scheduler.hpp"
#include "core/in_application_programmer.hpp"
#include "core/persistent_memory/persistent_memory_manager.hpp"
#include "core/state_node.hpp"
#include "core/board_monitor.hpp"

void Core::Start() {
  BoardSignals::Init();
  SoftwareTimer::Init();
  ASSERT_RESPONSE(FRAM_ACCESS::Init(), true);
  NameProvider::Init();  // Requires FramAccess to be initialized
  // Requires FramAccess and NameProvider to be initialized
  ASSERT_RESPONSE(EthernetServices::Init(), true);
  SystemTime::Init();  // Requires EthernetServices to be initialized

  PulicastEmbeddedNode node(
    // Node name is unique identifier
    NameProvider::GetBoardIdentifier(),
    // Namespace is kite name only
    NameProvider::GetKiteName(),
    pulicast::DEFAULT_PORT, 1);

  Logger::Init(node);
  StateNode::Init(node);

  auto board_monitor = std::make_unique<BoardMonitor>(node / NameProvider::GetBoardName());
  auto in_application_programmer = std::make_unique<InApplicationProgrammer>(node);
  auto persistent_memory_manager = std::make_unique<PersistentMemoryManager>(node);

#ifdef BOARD_FIRMWARE
PluginManager::Init(node);  // Requires FramAccess to be initialized
#ifdef EXTERNAL_PLUGINS
ExternalPluginManager::Init(node);  // Requires FramAccess to be initialized
#endif
#endif

  while (true) {
    Scheduler::Run();
    FRAM_ACCESS::Run();
    EthernetServices::Run();
    SystemTime::Run();
  }
}
