// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "core/plugin_manager/plugin_manager.hpp"
#include "core/persistent_memory/persistent_memory.hpp"
#include "core/name_provider.hpp"
#include "actuators/motor.hpp"
#include "actuators/stepper_motor.hpp"
#include "sensors/power_monitor.hpp"
#include "sensors/imu.hpp"
#include "sensors/barometer.hpp"
#include "sensors/gnss.hpp"
#include "sensors/rc_receiver.hpp"
#include "sensors/uwb.hpp"
#include "sensors/adc.hpp"
#include "sensors/hygrometer.hpp"
/**************************************************************************************************
 *     Private static member variables                                                            *
 **************************************************************************************************/
std::vector<Plugin*> PluginManager::plugins_;

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
void PluginManager::Init(pulicast::Namespace puli_namespace) {
  if (PersistentMemory::ValueIsPresentAndTrue(PersistentMemory::kUseMotorPlugin)) {
    uint32_t index = 0;
    if (PersistentMemory::GetValue(PersistentMemory::kBoardIndex, &index)) {
      PluginManager::plugins_.push_back(
        new Motor(puli_namespace / ("Motor" + std::to_string(index))));
    } else {
      PluginManager::plugins_.push_back(new Motor(puli_namespace / "Motor"));
    }
  }

  if (PersistentMemory::ValueIsPresentAndTrue(PersistentMemory::kUseStepperMotorPlugin)) {
    PluginManager::plugins_.push_back(new StepperMotor(puli_namespace / "StepperMotor"));
  }

  if (PersistentMemory::ValueIsPresentAndTrue(PersistentMemory::kUseImuPlugin)) {
    PluginManager::plugins_.push_back(new Imu(puli_namespace / "IMU"));
  }

  if (PersistentMemory::ValueIsPresentAndTrue(PersistentMemory::kUseBaroPlugin)) {
    // Bottom mounted sensor
    PluginManager::plugins_.push_back(new Barometer(puli_namespace / "Barometer", false));
    // Top mounted sensor
    PluginManager::plugins_.push_back(new Barometer(puli_namespace / "Barometer2", true));
  }

  if (PersistentMemory::ValueIsPresentAndTrue(PersistentMemory::kUseHygrometerPlugin)) {
    PluginManager::plugins_.push_back(new Hygrometer(puli_namespace / "Hygrometer"));
  }

  if (PersistentMemory::ValueIsPresentAndTrue(PersistentMemory::kUseGnssPlugin)) {
    PluginManager::plugins_.push_back(new Gnss(puli_namespace / "GNSS"));
  }

  if (PersistentMemory::ValueIsPresentAndTrue(PersistentMemory::kUseRcReceiverPlugin)) {
    PluginManager::plugins_.push_back(new RcReceiver(puli_namespace / "RC"));
  }

  if (PersistentMemory::ValueIsPresentAndTrue(PersistentMemory::kUseUwbPlugin)) {
    PluginManager::plugins_.push_back(new Uwb(puli_namespace / "UWB"));
  }

  if (PersistentMemory::ValueIsPresentAndTrue(PersistentMemory::kUsePowerMonitorPlugin)) {
    PluginManager::plugins_.push_back(new PowerMonitor(puli_namespace));
  }

  if (PersistentMemory::ValueIsPresentAndTrue(PersistentMemory::kUseAdcPlugin)) {
    PluginManager::plugins_.push_back(new Adc(puli_namespace / "Adc"));
  }
}
