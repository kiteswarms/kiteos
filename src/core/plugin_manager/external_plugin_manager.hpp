// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_CORE_PLUGIN_MANAGER_EXTERNAL_PLUGIN_MANAGER_HPP_
#define SRC_CORE_PLUGIN_MANAGER_EXTERNAL_PLUGIN_MANAGER_HPP_

#include <vector>
#include "core/plugin_manager/plugin.hpp"

//*****************************************************************************/
//! \addtogroup core
//! @{
///
//*****************************************************************************/
/*!
 * \brief   Implements the ExternalPluginManager class.
 *
 * \details This class is used to initialize and run all custom external plugins at once.
 *
 * \author  Elias Rosch
 *
 * \date    2020
 */
class ExternalPluginManager {
 public:
  /*!
   * \brief Creates instances of the plugins on HEAP based on configuration.
   *
   * \param puli_namespace  The pulicast namespace the plugin manager operates in.
   */
  static void Init(pulicast::Namespace puli_namespace);

 private:
  static std::vector<Plugin*> plugins_;  //!< List containing all plugins
};
/*! @} */
#endif  // SRC_CORE_PLUGIN_MANAGER_EXTERNAL_PLUGIN_MANAGER_HPP_
