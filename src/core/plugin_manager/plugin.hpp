// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_CORE_PLUGIN_MANAGER_PLUGIN_HPP_
#define SRC_CORE_PLUGIN_MANAGER_PLUGIN_HPP_

//*****************************************************************************/
//
//! \addtogroup plugins
//! @{
//
//*****************************************************************************/
#include "pulicast-port/pulicast_embedded.h"

/*!
 *  \brief     This describes the base class for any plug-in (sensor, actuator, misc) that should be
 *             implemented.
 *
 *  \author    Elias Rosch
 *
 *  \date      2018
 *
 */
class Plugin {
 public:
  /*!
   * \brief  Constructor of a plugin base object.
   */
  Plugin() {}

  /*!
   * \brief Destructor of a plugin base object.
   */
  virtual ~Plugin() = default;
};

/*! @} */
#endif  // SRC_CORE_PLUGIN_MANAGER_PLUGIN_HPP_
