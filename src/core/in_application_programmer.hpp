// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_CORE_IN_APPLICATION_PROGRAMMER_HPP_
#define SRC_CORE_IN_APPLICATION_PROGRAMMER_HPP_

#include <vector>
#include "pulicast-port/pulicast_embedded.h"
#include "kitecom/timestamped_vector_byte.hpp"

//*****************************************************************************/
//! \addtogroup core
//! @{
///
//*****************************************************************************/

/*!
 *  \brief  Implements the InApplicationProgrammer.
 *
 *  \author Jan Lehmann
 *
 *  \date   2019
 */
class InApplicationProgrammer {
 public:
  /*!
   * \brief Initializer of the InApplicationProgrammer-Component.
   *
   * \param puli_namespace  The pulicast namespace the in application programmer operates in.
   */
  explicit InApplicationProgrammer(pulicast::Namespace puli_namespace);

 private:
  /*!
   * \brief     Jumps to a specified memory address.
   *
   * \details   Jumps to a specified address in the flash memory. This function is used to switch
   *            between multiple programs stored in the flash memory.
   *
   * \attention This function is blocking. This function switches off the interrupts. Interrupts can
   *            be enabled again after jump by calling __enable_irq();
   *
   * \param     address  Address to jump to.
   *
   * \return    Function does not return if successful. FALSE if the specified memory section is
   *            empty.
   */
  bool JumpToAddress(uint32_t address);

  /*!
   * \brief Sends the provided bytes array via the response channel.
   *
   * \param values to send via response channel.
   */
  void SendResponse(std::vector<uint8_t> &values);

  //! Handles the received command.
  void HandleCommand(const std::vector<uint8_t > &command);

  //! Handles an incoming erase command.
  void HandleEraseCommand(const std::vector<uint8_t > &command);

  //! Handles an incoming write command.
  void HandleWriteCommand(const std::vector<uint8_t > &command);

  //! Handles an incoming read command.
  void HandleReadCommand(const std::vector<uint8_t > &command);

  //! Handles an incoming jump command.
  void HandleJumpCommand(const std::vector<uint8_t > &command);

  //! Handles an incoming set firmware broken command.
  void HandleSetFirmwareBrokenCommand(const std::vector<uint8_t > &command);

  //! Handles an incoming reset command.
  void HandleResetCommand(const std::vector<uint8_t > &command);

  //! Publishes an error response.
  void PublishErrorResponse(const std::vector<uint8_t> &command, uint8_t error_type);

  //! Sets the source and destination fields of a response message.
  void SetResponseSourceAndDestination(const std::vector<uint8_t > &command,
                                       std::vector<uint8_t > &response);

  //! Handles incoming pulicast iap messages.
  void CommandHandler(const kitecom::timestamped_vector_byte &msg);

  //! Callback called when the state timer elapsed.
  static void StateTimerCallback(InApplicationProgrammer *instance);

  //! Callback called when the jump timer elapsed.
  static void JumpToBootloaderTimerCallback(InApplicationProgrammer *instance);

  //! Channel used for all in application programmer communication.
  pulicast::Channel& communication_channel_;

  SoftwareTimer state_timer_;  //!< Timer that times the IAP heartbeat messages.
  //! Timer used to delay jump to ensure persistent memory is written before.
  SoftwareTimer jump_to_bootloader_timer_;
};
/*! @} */
#endif  // SRC_CORE_IN_APPLICATION_PROGRAMMER_HPP_
