// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "software_timer.hpp"
#include "hal/systick_timer.hpp"
#include "utils/debug_utils.h"
#include "scheduler.hpp"


/**************************************************************************************************
 *     Private static member variables                                                            *
 **************************************************************************************************/

SoftwareTimer* SoftwareTimer::timer_registry_[SOFTWARE_TIMER_NUM_CALLBACKS] = {nullptr};
SoftwareTimer* SoftwareTimer::next_elapsing_ = nullptr;
bool SoftwareTimer::sys_tick_enqueued_ = false;


/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/

SoftwareTimer::~SoftwareTimer() {
  ClearCallback();
}

void SoftwareTimer::Init() {
  hal::SystickTimer::RegisterSystickCallback(SysTickHandler, nullptr);
}

bool SoftwareTimer::SetTimeout(uint32_t timeout) {
  interval_ = 0;
  elapse_time_ = hal::SystickTimer::GetCount() + timeout;

  // If the timer has a callback, ensure that the timer is registered and next elapsing is updated
  if (cb_func_ != nullptr) {
    if (!registered_) {
      return RegisterTimer();
    }
    UpdateNextElapsing();
  }
  return true;
}

bool SoftwareTimer::SetTimeout(std::chrono::milliseconds timeout) {
  return SetTimeout(timeout.count());
}

bool SoftwareTimer::SetTimeoutPeriodic(uint32_t interval,
                                       CallbackFunction<void> cb_func, void* cb_arg) {
  interval_ = interval;
  elapse_time_ = hal::SystickTimer::GetCount() + interval;
  return SetCallback(cb_func, cb_arg);
}

bool SoftwareTimer::SetTimeoutPeriodic(std::chrono::milliseconds interval,
                                       CallbackFunction<void> cb_func, void* cb_arg) {
  return SetTimeoutPeriodic(interval.count(), cb_func, cb_arg);
}

bool SoftwareTimer::SetCallback(CallbackFunction<void> cb_func, void *cb_arg) {
  cb_func_ = cb_func;
  cb_arg_ = cb_arg;
  if (!Elapsed()) {
    return RegisterTimer();
  } else {
    return true;
  }
}

bool SoftwareTimer::ClearCallback() {
  if (cb_func_ == nullptr) {
    return false;
  }

  UnregisterTimer();
  cb_func_ = nullptr;
  cb_arg_ = nullptr;
  return true;
}

void SoftwareTimer::Stop() {
  interval_ = 0;
  elapse_time_ = 0;
  UnregisterTimer();
}

/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/

bool SoftwareTimer::RegisterTimer() {
  if (registered_) {
    return true;
  }

  for (auto & i : timer_registry_) {
    if (i == nullptr) {
      i = this;
      registered_ = true;
      break;
    }
  }

  UpdateNextElapsing();
  ASSERT(registered_);
  return registered_;
}

bool SoftwareTimer::UnregisterTimer() {
  if (!registered_) {
    return true;
  }

  for (auto & i : timer_registry_) {
    if (i == this) {
      i = nullptr;
      registered_ = false;
    }
  }

  UpdateNextElapsing();
  ASSERT(!registered_);
  return !registered_;
}

void SoftwareTimer::UpdateNextElapsing() {
  next_elapsing_ = nullptr;
  for (auto i : timer_registry_) {
    if (i) {
      if (!next_elapsing_ || (i->elapse_time_ < next_elapsing_->elapse_time_)) {
        next_elapsing_ = i;
      }
    }
  }
}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/

void SoftwareTimer::ProcessSystickEventTask(void *arg) {
  sys_tick_enqueued_ = false;
  if ((next_elapsing_) && (next_elapsing_->Elapsed())) {
    // Invoke callbacks of registered timer that elapsed.
    for (auto &i : timer_registry_) {
      if ((!i) ||
          (!i->Elapsed()) ||
          (!i->cb_func_)) {
        continue;
      }
      if (i->interval_) {
        i->cb_func_(i->cb_arg_);
        i->elapse_time_ += i->interval_;
      } else {
        i->registered_ = false;
        i->cb_func_(i->cb_arg_);
        i = nullptr;
      }
    }
    UpdateNextElapsing();
  }
}

void SoftwareTimer::SysTickHandler(void* context) {
  if (!sys_tick_enqueued_) {
    sys_tick_enqueued_ = true;
    Scheduler::EnqueueTask(SoftwareTimer::ProcessSystickEventTask, nullptr);
  }
}
