// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#define FRAM_ACCESS_SET_PROTECTED_VALUE SetProtectedValue
#define FRAM_ACCESS_ERASE_PROTECTED_KEY_VALUE_PAIR EraseProtectedKeyValuePair

// Define macro for poisoned FramAccess class.
#define FRAM_ACCESS FramAccess

#include <cinttypes>
#include <string>
#include "pulicast-port/pulicast_embedded.h"
#include "persistent_memory_manager.hpp"
#include "fram_access.hpp"
#include "core/system_time/system_time.hpp"
#include "core/name_provider.hpp"
#include "core/logger.hpp"

/**************************************************************************************************
 *     Definitions                                                                                *
 **************************************************************************************************/
// Message structure:
//
// Bytes:          0 - 22        23 - 45            46           47 - n
//              ------------------------------------------------------------
// Description: | Source ID | Destination ID | Message Type | Message Data |
//              ------------------------------------------------------------

// Message definitions
//! Message header denoting a get size command
constexpr const uint8_t kPeristentMemoryMsgGetSize = 0;
//! Message header denoting a get size response
constexpr const uint8_t kPeristentMemoryMsgGetSizeResponse = 1;
//! Message header denoting a read command
constexpr const uint8_t kPeristentMemoryMsgRead = 2;
//! Message header denoting a read response
constexpr const uint8_t kPeristentMemoryMsgReadResponse = 3;
//! Message header denoting a read all command
constexpr const uint8_t kPeristentMemoryMsgReadAll = 4;
//! Message header denoting a read all response
constexpr const uint8_t kPeristentMemoryMsgReadAllResponse = 5;
//! Message header denoting a write command
constexpr const uint8_t kPeristentMemoryMsgWrite = 6;
//! Message header denoting a write response
constexpr const uint8_t kPeristentMemoryMsgWriteResponse = 7;
//! Message header denoting a erase command
constexpr const uint8_t kPeristentMemoryMsgErase = 8;
//! Message header denoting a erase response
constexpr const uint8_t kPeristentMemoryMsgEraseResponse = 9;
//! Message header denoting an error message
constexpr const uint8_t kPeristentMemoryMsgError = 255;

// Error types
//! Error type indicating that an unknown command code was received
constexpr const uint8_t kPeristentMemoryErrorUnknownCommand = 0;
//! Error type indicating that the received command has an unexpected length
constexpr const uint8_t kPeristentMemoryErrorInvalidCommandLength = 1;

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
PersistentMemoryManager::PersistentMemoryManager(pulicast::Namespace puli_namespace) :
  communication_channel_(puli_namespace["/mem"]) {
  // Subscribe to command channel
  communication_channel_.Subscribe<kitecom::timestamped_vector_byte>(
      [this](const kitecom::timestamped_vector_byte& message) {
        PersistentMemoryManager::CommandHandler(message);
      });

  // Publish FRAM usage statistics
  char log_message[94];
  snprintf(log_message, sizeof(log_message),
           "Read %" PRIu32
           " values from the persistent memory. The total persistent memory size is %" PRIu16 ".",
           FRAM_ACCESS::GetOccupiedSpace(), kFramSize);
  Logger::Report(log_message, Logger::LogLevel::kInfo);

  // Publish warning if less than 20% of persistent memory space is empty
  if (FRAM_ACCESS::GetFreeSpace() < kFramSize * 0.2) {
    Logger::Report("Less than 20% persistent memory space left.", Logger::LogLevel::kWarning);
  }

  update_timer_.SetTimeoutPeriodic(1000, UpdateTimerCallback, this);
}

/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
void PersistentMemoryManager::HandleCommand(const std::vector<uint8_t > &command) {
  // Check if the command size is invalid
  if (command.size() < 47) {
    return;
  }

  // Check if the target ID of the message matches the ID of this board
  for (uint32_t i = 0; i < 23; i++) {
    if (command[i + 23] != NameProvider::GetBoardIdentifierBinary()[i]) {
      return;
    }
  }

  uint8_t message_type = command[46];  // First byte defines the message type
  switch (message_type) {
    case kPeristentMemoryMsgGetSize:
      HandleGetSizeCommand(command);
      break;

    case kPeristentMemoryMsgRead:
      HandleReadCommand(command);
      break;

    case kPeristentMemoryMsgReadAll:
      HandleReadAllCommand(command);
      break;

    case kPeristentMemoryMsgWrite:
      HandleWriteCommand(command);
      break;

    case kPeristentMemoryMsgErase:
      HandleEraseCommand(command);
      break;

    default:  // Unknown command
      PublishErrorResponse(command, kPeristentMemoryErrorUnknownCommand);
  }
}

void PersistentMemoryManager::HandleGetSizeCommand(const std::vector<uint8_t > &command) {
  // Check command length
  if (command.size() != 23 + 23 + 1) {
    PublishErrorResponse(command, kPeristentMemoryErrorInvalidCommandLength);
    return;
  }

  // Get FRAM size and occupied space
  auto fram_size = (uint32_t)kFramSize;
  uint32_t occupied_space = FRAM_ACCESS::GetOccupiedSpace();

  // Assemble get size response message
  std::vector<uint8_t> response_message;
  // Allocate enough memory to hold the full message
  response_message.reserve(23 + 23 + 9);
  SetResponseSourceAndDestination(command, response_message);
  response_message.push_back(kPeristentMemoryMsgGetSizeResponse);
  response_message.push_back(static_cast<uint8_t>((fram_size & 0xff000000u) >> 24u));
  response_message.push_back(static_cast<uint8_t>((fram_size & 0x00ff0000u) >> 16u));
  response_message.push_back(static_cast<uint8_t>((fram_size & 0x0000ff00u) >> 8u));
  response_message.push_back(static_cast<uint8_t>(fram_size & 0x000000ffu));
  response_message.push_back(static_cast<uint8_t>((occupied_space & 0xff000000u) >> 24u));
  response_message.push_back(static_cast<uint8_t>((occupied_space & 0x00ff0000u) >> 16u));
  response_message.push_back(static_cast<uint8_t>((occupied_space & 0x0000ff00u) >> 8u));
  response_message.push_back(static_cast<uint8_t>(occupied_space & 0x000000ffu));

  // Publish get size response
  PublishResponse(response_message);
}

void PersistentMemoryManager::HandleReadCommand(const std::vector<uint8_t > &command) {
  // Check command length
  if (command.size() != 23 + 23 + 1 + 4) {
    PublishErrorResponse(command, kPeristentMemoryErrorInvalidCommandLength);
    return;
  }

  // Parse read command
  uint32_t  key = (((uint32_t)command[47]) << 24u) | (((uint32_t)command[48]) << 16u)
      | (((uint32_t)command[49]) << 8u) | ((uint32_t)command[50]);

  // Read value from FRAM
  uint32_t value = 0;
  bool key_exists = FRAM_ACCESS::GetValue(key, &value);

  // Assemble read response message
  std::vector<uint8_t> response_message;
  // Allocate enough memory to hold the full message
  response_message.reserve(23 + 23 + 10);
  SetResponseSourceAndDestination(command, response_message);
  response_message.push_back(kPeristentMemoryMsgReadResponse);
  response_message.push_back(static_cast<uint8_t>((key & 0xff000000u) >> 24u));
  response_message.push_back(static_cast<uint8_t>((key & 0x00ff0000u) >> 16u));
  response_message.push_back(static_cast<uint8_t>((key & 0x0000ff00u) >> 8u));
  response_message.push_back(static_cast<uint8_t>(key & 0x000000ffu));
  response_message.push_back(static_cast<uint8_t>((value & 0xff000000u) >> 24u));
  response_message.push_back(static_cast<uint8_t>((value & 0x00ff0000u) >> 16u));
  response_message.push_back(static_cast<uint8_t>((value & 0x0000ff00u) >> 8u));
  response_message.push_back(static_cast<uint8_t>(value & 0x000000ffu));
  response_message.push_back(static_cast<uint8_t>(key_exists ? 0x00 : 0x01));

  // Publish read response
  PublishResponse(response_message);
}

void PersistentMemoryManager::HandleReadAllCommand(const std::vector<uint8_t > &command) {
  // Check command length
  if (command.size() != 23 + 23 + 1) {
    PublishErrorResponse(command, kPeristentMemoryErrorInvalidCommandLength);
    return;
  }

  // Get the number of values stored in the FRAM
  uint32_t number_of_fram_values = FRAM_ACCESS::GetOccupiedSpace();

  // Read and send key value pairs in packets of 10
  const int kValuesPerMessage = 10;
  uint32_t  values_sent = 0;
  do {
    // Assemble response message
    std::vector<uint8_t> response_message;
    // Allocate enough memory to hold the full message
    response_message.reserve(23 + 23 + 9 + (8 * kValuesPerMessage));
    SetResponseSourceAndDestination(command, response_message);
    response_message.push_back(kPeristentMemoryMsgReadAllResponse);
    response_message.push_back(static_cast<uint8_t>((number_of_fram_values & 0xff000000u) >> 24u));
    response_message.push_back(static_cast<uint8_t>((number_of_fram_values & 0x00ff0000u) >> 16u));
    response_message.push_back(static_cast<uint8_t>((number_of_fram_values & 0x0000ff00u) >> 8u));
    response_message.push_back(static_cast<uint8_t>(number_of_fram_values & 0x000000ffu));
    response_message.push_back(static_cast<uint8_t>((values_sent & 0xff000000u) >> 24u));
    response_message.push_back(static_cast<uint8_t>((values_sent & 0x00ff0000u) >> 16u));
    response_message.push_back(static_cast<uint8_t>((values_sent & 0x0000ff00u) >> 8u));
    response_message.push_back(static_cast<uint8_t>(values_sent & 0x000000ffu));

    // Read key value pairs and write them to the response message
    uint32_t i;
    for (i = 0; (i < kValuesPerMessage) && (i < (number_of_fram_values - values_sent)); i++) {
      // Read key value pair
      uint32_t key, value;
      FRAM_ACCESS::GetNextKeyValuePair(&key, &value);

      // Write key value pair to response message
      response_message.push_back((key & 0xff000000u) >> 24u);
      response_message.push_back((key & 0x00ff0000u) >> 16u);
      response_message.push_back((key & 0x0000ff00u) >> 8u);
      response_message.push_back(key & 0x000000ffu);
      response_message.push_back((value & 0xff000000u) >> 24u);
      response_message.push_back((value & 0x00ff0000u) >> 16u);
      response_message.push_back((value & 0x0000ff00u) >> 8u);
      response_message.push_back(value & 0x000000ffu);
    }

    // Publish response message
    PublishResponse(response_message);
    values_sent += i;
  } while (values_sent < number_of_fram_values);
}

void PersistentMemoryManager::HandleWriteCommand(const std::vector<uint8_t > &command) {
  // Check command length
  if (command.size() != 23 + 23 + 1 + 9) {
    PublishErrorResponse(command, kPeristentMemoryErrorInvalidCommandLength);
    return;
  }

  // Parse write command
  uint32_t  key = (((uint32_t)command[47]) << 24u) | (((uint32_t)command[48]) << 16u)
      | (((uint32_t)command[49]) << 8u) | ((uint32_t)command[50]);
  uint32_t value = (((uint32_t)command[51]) << 24u) | (((uint32_t)command[52]) << 16u)
      | (((uint32_t)command[53]) << 8u) | ((uint32_t)command[54]);
  bool ignore_fuse_bit = command[55] != 0;

  // Write value to FRAM
  bool write_successful;
  if (ignore_fuse_bit) {
    write_successful = FRAM_ACCESS::FRAM_ACCESS_SET_PROTECTED_VALUE(key, value);
  } else {
    write_successful = FRAM_ACCESS::SetValue(key, value);
  }

  // Assemble write response message
  std::vector<uint8_t> response_message;
  // Allocate enough memory to hold the full message
  response_message.reserve(23 + 23 + 10);
  SetResponseSourceAndDestination(command, response_message);
  response_message.push_back(kPeristentMemoryMsgWriteResponse);
  response_message.push_back(static_cast<uint8_t>((key & 0xff000000u) >> 24u));
  response_message.push_back(static_cast<uint8_t>((key & 0x00ff0000u) >> 16u));
  response_message.push_back(static_cast<uint8_t>((key & 0x0000ff00u) >> 8u));
  response_message.push_back(static_cast<uint8_t>(key & 0x000000ffu));
  response_message.push_back(static_cast<uint8_t>((value & 0xff000000u) >> 24u));
  response_message.push_back(static_cast<uint8_t>((value & 0x00ff0000u) >> 16u));
  response_message.push_back(static_cast<uint8_t>((value & 0x0000ff00u) >> 8u));
  response_message.push_back(static_cast<uint8_t>(value & 0x000000ffu));
  response_message.push_back(static_cast<uint8_t>(write_successful ? 0x00 : 0x01));

  // Publish write response
  PublishResponse(response_message);
}

void PersistentMemoryManager::HandleEraseCommand(const std::vector<uint8_t > &command) {
  // Check command length
  if (command.size() != 23 + 23 + 1 + 5) {
    PublishErrorResponse(command, kPeristentMemoryErrorInvalidCommandLength);
    return;
  }

  // Parse erase command
  uint32_t  key = (((uint32_t)command[47]) << 24u) | (((uint32_t)command[48]) << 16u)
      | (((uint32_t)command[49]) << 8u) | ((uint32_t)command[50]);
  bool ignore_fuse_bit = command[51] != 0;

  // Erase value from FRAM
  bool erase_successful;
  if (ignore_fuse_bit) {
    erase_successful = FRAM_ACCESS::FRAM_ACCESS_ERASE_PROTECTED_KEY_VALUE_PAIR(key);
  } else {
    erase_successful = FRAM_ACCESS::EraseKeyValuePair(key);
  }

  // Assemble erase response message
  std::vector<uint8_t> response_message;
  // Allocate enough memory to hold the full message
  response_message.reserve(23 + 23 + 5);
  SetResponseSourceAndDestination(command, response_message);
  response_message.push_back(kPeristentMemoryMsgEraseResponse);
  response_message.push_back(static_cast<uint8_t>((key & 0xff000000u) >> 24u));
  response_message.push_back(static_cast<uint8_t>((key & 0x00ff0000u) >> 16u));
  response_message.push_back(static_cast<uint8_t>((key & 0x0000ff00u) >> 8u));
  response_message.push_back(static_cast<uint8_t>(key & 0x000000ffu));
  response_message.push_back(static_cast<uint8_t>(erase_successful ? 0x00 : 0x01));

  // Publish erase response
  PublishResponse(response_message);
}

void PersistentMemoryManager::PublishErrorResponse(const std::vector<uint8_t> &command,
                                                   uint8_t error_type) {
  // Assemble an error message
  std::vector<uint8_t> response_message;
  // Allocate enough memory to hold the full message
  response_message.reserve(23 + 23 + 2);
  SetResponseSourceAndDestination(command, response_message);
  response_message.push_back(kPeristentMemoryMsgError);
  response_message.push_back(error_type);

  // Publish error message
  PublishResponse(response_message);
}

void PersistentMemoryManager::SetResponseSourceAndDestination(const std::vector<uint8_t> &command,
                                                              std::vector<uint8_t> &response) {
  // Source ID is board identifier
  for (uint32_t i = 0; i < 23; i++) {
    response.push_back(NameProvider::GetBoardIdentifierBinary()[i]);
  }
  // Destination ID is command source ID
  for (uint32_t i = 0; i < 23; i++) {
    response.push_back(command[i]);
  }
}

void PersistentMemoryManager::PublishResponse(const std::vector<uint8_t > &response) {
  kitecom::timestamped_vector_byte msg;
  msg.ts = SystemTime::GetTimestamp();
  msg.len = response.size();
  msg.values = response;
  communication_channel_ << msg;
}

/**************************************************************************************************
 *     Callback functions                                                                         *
 **************************************************************************************************/
void PersistentMemoryManager::CommandHandler(const kitecom::timestamped_vector_byte &msg) {
  // Process command
  HandleCommand(msg.values);
}

void PersistentMemoryManager::UpdateTimerCallback(PersistentMemoryManager *instance) {
  // Publish warning if write errors occurred
  uint32_t write_error_cnt = FRAM_ACCESS::ClearWriteErrorCounter();
  if (write_error_cnt > 0) {
    char log_message[46];
    snprintf(log_message, sizeof(log_message),
             "%" PRIu32 " write error(s) occurred.", write_error_cnt);
    Logger::Report(log_message, Logger::LogLevel::kWarning);
  }
}
