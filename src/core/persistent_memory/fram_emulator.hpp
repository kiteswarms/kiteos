// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_CORE_PERSISTENT_MEMORY_FRAM_EMULATOR_HPP_
#define SRC_CORE_PERSISTENT_MEMORY_FRAM_EMULATOR_HPP_

#include <vector>
#include <utility>
#include <memory>
#include <cstdint>

/*! \brief     FramEmulator class that creates a key value map from a provoded json file.
 *             Interpretation of content is done according to the system provided
 *             persistent-memory-definition file.
 *
 *  \author    Elias Rosch
 *
 *  \date      2020
 */
class FramEmulator {
 public:
 /*! \brief   Gets the content of the emulated fram as a vector of key-value pairs.
  *  \returns std::vector containing key-value pairs
  */
  static std::unique_ptr<std::vector<std::pair<uint32_t, uint32_t>>> GetContent();
};

static constexpr const uint16_t kMb85Rs64TSize = 0xFFFF;  //!< Size of the FRAM in bytes
class Mb85Rs64T;  //!< Dummy class providing an empty class definition of the Mb85Rs64T driver

#endif  // SRC_CORE_PERSISTENT_MEMORY_FRAM_EMULATOR_HPP_
