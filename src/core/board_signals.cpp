// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#include "board_signals.hpp"

hal::Gpio::Config BoardSignals::board_signal_config_[5] = {
  {hal::Gpio::kPortB, hal::Gpio::kPin2, hal::Gpio::kModeInput},  // kTopStackDetection
  {hal::Gpio::kPortC, hal::Gpio::kPin0, hal::Gpio::kModeInput},  // kBottomStackDetection
  {hal::Gpio::kPortC, hal::Gpio::kPin7, hal::Gpio::kModeOutput},  // kEthMuxSelect
  {hal::Gpio::kPortA, hal::Gpio::kPin10, hal::Gpio::kModeOutput},  // kPpsNotRerouting
  {hal::Gpio::kPortC, hal::Gpio::kPin10, hal::Gpio::kModeOutput}  // kPpsTermination
};

hal::Gpio* BoardSignals::board_signal_pin_[5] = {nullptr};

void BoardSignals::Init() {
  // Construct the gpio objects
  for (uint8_t i = 0; i < 5; ++i) {
    board_signal_pin_[i] = new hal::Gpio(board_signal_config_[i]);
  }
  // Configure the ethernet multiplexer accordingly
  if (GetValue(kBottomStackDetection)) {
    board_signal_pin_[kEthMuxSelect]->Write(true);
  } else {
    board_signal_pin_[kEthMuxSelect]->Write(false);
  }

  // Enable PPS termination if no board is detected on top
  if (GetValue(kTopStackDetection)) {
    board_signal_pin_[kPpsTermination]->Write(false);
  } else {
    board_signal_pin_[kPpsTermination]->Write(true);
  }

  // Always disable PPS rerouting
  board_signal_pin_[kPpsNotRerouting]->Write(true);
}

bool BoardSignals::GetValue(Type type) {
  return (board_signal_pin_[type]->Read() != 0x0);
}
