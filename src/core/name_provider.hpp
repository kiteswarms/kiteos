// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_CORE_NAME_PROVIDER_HPP_
#define SRC_CORE_NAME_PROVIDER_HPP_

#include <string>
/*!
 *  \brief   Implements the name provider class.
 *
 *  \details This class provides the name string of the board.
 *
 *  \author  Jan Lehmann
 *
 *  \date    2020
 */
class NameProvider {
 public:
  /*!
  * \brief Initializes the name provider class.
  */
  static void Init();

  /*!
   * \brief  Gets the board identifier.
   *
   * \return Returns the board identifier consisting of the pcb type, the pcb version and pcb id.
   */
  static const std::string& GetBoardIdentifier();

  /*!
   * \brief  Gets the the binary board identifier.
   *
   * \return Returns the board identifier in a binary representation as an array of 23 8 bit
   *         unsigned integer values.
   */
  static const uint8_t* GetBoardIdentifierBinary();

  /*!
   * \brief   Gets the kite prefix.
   *
   * \return  Returns the kite prefix.
   */
  static const std::string& GetKiteName();

  /*!
   * \brief  Gets the kite prefix.
   *
   * \return Returns the kite prefix.
   */
  static const std::string& GetBoardName();

  /*!
   * \brief  Gets the the full board prefix.
   *
   * \return Returns the full board prefix consisting of the kite name and the full board name
   *         including the board index.
   */
  static const std::string& GetFullBoardNamespace();

 private:
  //! Board identifier consisting of the pcb type, the pcb version and pcb id.
  static std::string* board_identifier_;
  static uint8_t board_identifier_binary_[23];  //!< Binary representation of the board identifier.
  //! Kite name.
  static std::string* kite_name_;
  //! Board name.
  static std::string* board_name_;
  //! Full board namespace.
  static std::string* full_board_namespace_;
};

#endif  // SRC_CORE_NAME_PROVIDER_HPP_
