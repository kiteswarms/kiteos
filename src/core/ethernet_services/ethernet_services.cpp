// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "ethernet_services.hpp"
#include "lwip-port/ethernet_interface.hpp"
#include "core/persistent_memory/persistent_memory.hpp"

/**************************************************************************************************
 *     Private static member variables                                                            *
 **************************************************************************************************/

Ksz8563R* EthernetServices::ksz8563r_;
Ksz8567R* EthernetServices::ksz8567r_east_;
Ksz8567R* EthernetServices::ksz8567r_west_;

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/

bool EthernetServices::Init() {
  // Get device ip, netmask and gateway from FRAM
  uint32_t device_ip = 0;
  if (PersistentMemory::GetValue(PersistentMemory::kIpAddress, &device_ip)) {
    device_ip = htonl(device_ip);
  }
  uint32_t netmask = 0;
  if (PersistentMemory::GetValue(PersistentMemory::kNetmask, &netmask)) {
    netmask = htonl(netmask);
  }
  uint32_t gateway = 0;
  if (PersistentMemory::GetValue(PersistentMemory::kGatewayAddress, &gateway)) {
    gateway = htonl(gateway);
  }

  // Add network interface to LwIP network interface list
  EthernetInterface::Init(device_ip, netmask, gateway);


#ifndef BOARD_BOOTLOADER
  PersistentMemory::SwitchHardware switch_hardware = PersistentMemory::kSwitchHardwareNone;
  if (PersistentMemory::GetValue(PersistentMemory::kSwitchHardware,
                                 reinterpret_cast<uint32_t*>(&switch_hardware))) {
    switch (switch_hardware) {
      case PersistentMemory::kSwitchHardwareNone:
        break;

      case PersistentMemory::kSwitchHardwareKsz8563R:
        ksz8563r_ = new Ksz8563R();
        break;

      case PersistentMemory::kSwitchHardwareDualKsz8567R:
        ksz8567r_east_ = new Ksz8567R(Ksz8567R::kPositionEast);
        ksz8567r_west_ = new Ksz8567R(Ksz8567R::kPositionWest);
        break;

      default:
        ASSERT(false);
        break;
    }
  }
#endif

  return true;
}

void EthernetServices::Run() {
  EthernetInterface::Run();
}
