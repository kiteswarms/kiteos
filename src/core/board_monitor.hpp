// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_CORE_BOARD_MONITOR_HPP_
#define SRC_CORE_BOARD_MONITOR_HPP_

//*****************************************************************************/
//! \addtogroup plugins
//! @{
//! \addtogroup misc
//! @{
///
//*****************************************************************************/

#include "core/plugin_manager/plugin.hpp"
#include "core/system_time/system_time.hpp"
#include "core/software_timer.hpp"
#include "drivers/core_temperature.hpp"

/*!
 * \brief   Implements the BoardMonitor plug-in.
 * \details BoardMonitor is used to monitor the board status. This includes voltage, pps status,
 *          system load, software version. It also features a PING mechanism on pulicast level. It
 *          is responsible for setting the correct timestamp when received.
 * \author  Elias Rosch
 * \date    2019
 */
class BoardMonitor {
 public:
  /*!
   * \brief Constructor of the BoardMonitor-Component.
   *
   * \param puli_namespace  The pulicast namespace the board monitor operates in.
   */
  explicit BoardMonitor(pulicast::Namespace puli_namespace);

 private:
  /*!
   * \brief Publishes the software version.
   */
  void SendVersion();

  /*!
   * \brief Publishes the current PPS mode.
   */
  void SendPpsMode();

  /*!
   * \brief Publishes the stack detection information.
   */
  void SendStackDetection();

  /*!
   * \brief Publishes the core temperature information.
   */
  void SendCoreTemperature();

  //! Callback called when the update timer elapsed.
  static void UpdateTimerElapsedCallback(BoardMonitor *inst);

  /*! @} */

  /*!
   * \brief Handles incoming ZCM-ping messages.
   *
   * \param msg  Message that is delivered.
   */
  void MsgHandlerPing(const kitecom::timestamped_vector_uint &msg);

  CoreTemperature core_temperature_;             //!< Driver that reads core temperature
  pulicast::Channel& version_channel_;           //!< Output channel for software version.
  pulicast::Channel& stack_detection_channel_;   //!< Output channel for stack detection.
  pulicast::Channel& mainloops_channel_;         //!< Output channel for main loops.
  pulicast::Channel& maxcycle_channel_;          //!< Output channel for max cycle.
  pulicast::Channel& temperature_channel_;       //!< Output channel for voltage.
  pulicast::Channel& clock_accuracy_channel_;  //!< Output channel for system time clock accuracy.
  pulicast::Channel& ping_channel_;              //!< Input channel for ping.
  pulicast::Channel& pong_channel_;              //!< Output channel for pong.
  //! Flag indicating if the system time is currently synchronized to its configured synchronization
  //! source.
  bool system_time_is_synchronized_;
  SoftwareTimer update_timer_;  //!< Timer used for mainLoopCounter.
};
/*! @} */
/*! @} */
#endif  // SRC_CORE_BOARD_MONITOR_HPP_
