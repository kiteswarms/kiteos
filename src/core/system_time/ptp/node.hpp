// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_CORE_SYSTEM_TIME_PTP_NODE_HPP_
#define SRC_CORE_SYSTEM_TIME_PTP_NODE_HPP_

//*****************************************************************************/
//! \addtogroup plugins
//! @{
//! \addtogroup misc
//! @{
///
//*****************************************************************************/

#include "core/plugin_manager/plugin.hpp"
#include "lwip/ip_addr.h"
#include "message_data_structures.hpp"
#include "core/software_timer.hpp"
#include "utils/ring_buffer.hpp"


/**************************************************************************************************
 *     Defines                                                                                    *
 **************************************************************************************************/

#define PTP_CLOCK_ID 0x000000000002  // ID of the PTP port (should be unique in the network)
// Size of the delay resp queue, should be the number of slaves
#define PTP_DELAY_RESP_QUEUE_SIZE 10

namespace ptp {
//! Implemets the PTP protocol for KiteOS
class Node {
 public:
  /*!
   * \brief  Initializes the PTP class.
   *
   * \param  master  If true, the PTP node acts as a master. If false, it acts as a slave.
   *
   * \return True on success, false otherwise
   */
  static bool Init(bool master);

  /*!
   * \brief Lets the PTP class do its work. This method should be called as often as possible.
   */
  static void Run();

  /*!
   * \brief     Checks if the PTP slave is synchronized with a PTP master.
   *
   * \attention This function only returns a meaningful result when the PTP node is in slave mode.
   *
   * \return    True, if the PTP slave is currently synchronized with a PTP master, false otherwise.
   */
  static bool IsSynchronized();

  /*!
   * \brief     Gets the time offset calculated in the last synchronization cycle.
   *
   * \attention This function only returns a meaningful result when the PTP node is in slave mode.
   *
   * \return    Returns the time offset calculated in the last synchronization cycle.
   */
  static int64_t GetLastTimeOffset();

 private:
/**************************************************************************************************
 *     Private Enums                                                                              *
 **************************************************************************************************/

  //! Enum defining the states of a PTP plugin
  enum State {
    kStateIdle,
    kStateSendingSync,
    kStateSendingSyncFinished,
    kStateSyncTimestampReceived,
    kStateSyncReceived,
    kStateFollowUpReceived,
    kStateSendingDelayReq,
    kStateAwaitingDelayResp,
    kStateDelayRespReceived
  };


/**************************************************************************************************
 *     Private Structs                                                                            *
 **************************************************************************************************/

  //! Structure containing the data used by the slave for a synchronization cycle.
  struct SynchronizationDataSet {
    uint16_t sequence_id;  //!< Sequence ID of the current synchronization cycle.
    message::PortIdentity master_port_identity;  //!< Port identity of the master to synchronize to.
    //! Timestamp when the sync message was sent by the master.
    message::Timestamp sync_transmit_timestamp;
    //! Timestamp when the sync message was received by the slave.
    message::Timestamp sync_receive_timestamp;
    //! Timestamp when the delay req message was sent by the slave.
    message::Timestamp delay_req_transmit_timestamp;
    //! Timestamp when the delay req message was received by the master.
    message::Timestamp delay_req_receive_timestamp;
    int64_t sync_correction;  //!< Total correction of the sync transmission time in nanoseconds.
    //! Total correction of the delay req transmission time in nanoseconds.
    int64_t delay_req_correction;
    //! Master time corresponding to previousSlaveClockTime in nanoseconds.
    int64_t previous_master_clock_time = 0;
    //! Sync received timestamp of the previous synchronization cycle in nanoseconds.
    int64_t previous_slave_clock_time = 0;
  };

  //! Structure defining an element of the delay response queue.
  struct DelayRespQueueElement {
    uint16_t sequence_id;  //!< Sequence ID of the delay resp to send.
    //! Requesting port identity of the delay resp to send.
    message::PortIdentity requesting_port_identity;
    message::Timestamp delay_req_receive_timestamp;  //!< Timestamp of the delay resp to send.
    //! Total correction of the delay req transmission time.
    int64_t delay_req_correction;
  };


/**************************************************************************************************
 *     Private functions                                                                          *
 **************************************************************************************************/

  /*!
   * \brief Sends a sync message, automatically increments the sequence ID.
   */
  static void SendSyncMessage();

  /*!
   * \brief Sends a follow up message corresponding to the lastly sent sync message.
   */
  static void SendFollowUpMessage();

  /*!
   * \brief Sends a delay req message.
   */
  static void SendDelayReqMessage();

  /*!
   * \brief Sends the first delay resp message in the delay resp message queue.
   */
  static void SendDelayRespMessage();

  /*!
   * \brief Sends an announce message, automatically increments the sequence ID.
   */
  static void SendAnnounceMessage();

  /*!
   * \brief Checks if a timeout occurred and cancels the synchronization cycle if this is the case.
   */
  static void CheckTimeout();

  /*!
   * \brief  Update the system time using the information in the synchronization data set.
   *
   * \return Returns true on success, false otherwise.
   */
  static bool UpdateSystemTime();


/**************************************************************************************************
 *     Callback functions                                                                         *
 **************************************************************************************************/

  /*!
   * \brief     Callback to be called by lwip if a PTP event message was received.
   *
   * \details   According to the function prototype for udp pcb receive callback functions. addr and
   *            port are in same byte order as in the pcb. The callback is responsible for freeing
   *            the pbuf if it's not used any more.
   *
   * \attention Be aware that 'addr' might point into the pbuf 'p' so freeing this pbuf can make
   *            'addr' invalid, too.
   *
   * \param     arg   User supplied argument (udp_pcb.recv_arg).
   * \param     pcb   The udp_pcb which received data.
   * \param     p     The packet buffer that was received.
   * \param     addr  The remote IP address from which the packet was received.
   * \param     port  The remote port from which the packet was received.
   */
  static void EventMessageReceivedCallback(void *arg, struct udp_pcb *pcb, struct pbuf *p,
                                           const ip_addr_t *addr, u16_t port);

  /*!
   * \brief     Callback to be called by lwip if a general PTP message was received.
   *
   * \details   According to the function prototype for udp pcb receive callback functions. addr and
   *            port are in same byte order as in the pcb. The callback is responsible for freeing
   *            the pbuf if it's not used any more.
   *
   * \attention Be aware that 'addr' might point into the pbuf 'p' so freeing this pbuf can make
   *            'addr' invalid, too.
   *
   * \param     arg   User supplied argument (udp_pcb.recv_arg).
   * \param     pcb   The udp_pcb which received data.
   * \param     p     The packet buffer that was received.
   * \param     addr  The remote IP address from which the packet was received.
   * \param     port  The remote port from which the packet was received.
   */
  static void GeneralMessageReceivedCallback(void *arg, struct udp_pcb *pcb, struct pbuf *p,
                                             const ip_addr_t *addr, u16_t port);

  /*!
   * \brief Callback to be called if a timestamp for an outgoing PTP message was captured.
   *
   * \param timestamp_seconds      Seconds field of the captured timestamp.
   * \param timestamp_nanoseconds  Nanoseconds field of the captured timestamp.
   * \param message_type           PTP message type of the outgoing message.
   * \param sequence_id            PTP sequence ID of the outgoing message.
   */
  static void TxTimestampCallback(uint32_t timestamp_seconds, uint32_t timestamp_nanoseconds,
                                  uint8_t message_type, uint16_t sequence_id);

  /*!
   * \brief Callback to be called if a timestamp for an incoming PTP message was captured.
   *
   * \param timestamp_seconds      Seconds field of the captured timestamp.
   * \param timestamp_nanoseconds  Nanoseconds field of the captured timestamp.
   * \param message_type           PTP message type of the incoming message.
   * \param sequence_id            PTP sequence ID of the incoming message.
   * \param clock_identity         PTP clock identity of the incoming messages source port identity.
   * \param port_number            PTP port number of the incoming messages source port identity.
   */
  static void RxTimestampCallback(uint32_t timestamp_seconds, uint32_t timestamp_nanoseconds,
                                  uint8_t message_type, uint16_t sequence_id,
                                  uint64_t clock_identity, uint16_t port_number);

/**************************************************************************************************
 *     Private variables                                                                          *
 **************************************************************************************************/

  static struct udp_pcb *event_pcb_;    //!< lwIP socket for event messages.
  static struct udp_pcb *general_pcb_;  //!< lwIP socket for general messages.
  static ip4_addr_t group_ip_addr_;     //!< IP address of the destination ethernet side.

  static bool master_;                    //!< Flag denoting that the node acts as a PTP master.
  static State state_;                    //!< Current state of the PTP plugin.
  static uint16_t sync_sequence_number_;  //!< Sequence ID of the next sync message to be sent.
  //! Sequence ID of the next announce message to be sent.
  static uint16_t announce_sequence_number_;
  static SoftwareTimer sync_timer_;      //!< Timer for periodic sync message generation.
  static SoftwareTimer announce_timer_;  //!< Timer for periodic announce message generation.
  static SoftwareTimer timeout_timer_;   //!< Timer to check for timeouts during a sync cycle.
  //! Structure containing the data of the current sync cycle.
  static SynchronizationDataSet synchronization_data_set_;
  //! Queue element which is not fully assembled yet and needs to be enqueued next.
  static DelayRespQueueElement current_delay_req_;
  //! Queue containing delay resp messages which need to be sent.
  static RingBuffer<DelayRespQueueElement, PTP_DELAY_RESP_QUEUE_SIZE> delay_resp_queue_;
  //! Timer for checking if a PTP slave is synchronized to a PTP master.
  static SoftwareTimer synchronized_timeout_timer_;
  static int64_t last_time_offset_;  //!< Time offset calculated in the last synchronization cycle.
};
}  // namespace ptp
/*! @} */
/*! @} */
#endif  // SRC_CORE_SYSTEM_TIME_PTP_NODE_HPP_
