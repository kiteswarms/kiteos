// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#include "message.hpp"


#define PTP_MESSAGE_PTP_VERSION (2)  // Used PTP version (2 for IEEE1588)
#define PTP_MESSAGE_DEFAULT_DOMAIN_NUMBER (0)  // Default domain number used to create messages
// Default message interval used to create messages
#define PTP_MESSAGE_DEFAULT_MESSAGE_INTERVAL (0x7F)


/**************************************************************************************************
 *     Public functions                                                                           *
 **************************************************************************************************/

namespace ptp {
Message::Message(message::Type message_type, uint16_t sequence_id, uint64_t clock_id) {
  // Initialize message header
  message_header.transport_specific = 0x0;
  message_header.message_type = message_type;
  message_header.ptp_version = PTP_MESSAGE_PTP_VERSION;
  SetMessageLength();
  message_header.domain_number = PTP_MESSAGE_DEFAULT_DOMAIN_NUMBER;
  message_header.flag_field.alternate_master_flag = false;
  message_header.flag_field.two_step_flag = true;
  message_header.flag_field.unicast_flag = false;
  message_header.flag_field.ptp_profile_specific_1 = false;
  message_header.flag_field.ptp_profile_specific_2 = false;
  message_header.flag_field.leap_61 = false;
  message_header.flag_field.leap_59 = false;
  message_header.flag_field.current_utc_offset_valid = false;
  message_header.flag_field.ptp_timescale = false;
  message_header.flag_field.time_traceable = false;
  message_header.flag_field.frequency_traceable = false;
  message_header.correction_field = 0;
  message_header.source_port_identity.clock_identity =
      0xFF00000000000000u |  // No IEEE EUI-64- or EUI-48-based clock_identity_ value
      0x00FF000000000000u |  // Closed system outside the scope of the IEE 1588 standard
      (0x0000FFFFFFFFFFFFu & clock_id);
  message_header.source_port_identity.port_number = 1;
  message_header.sequence_id = sequence_id;
  SetControlField();
  message_header.log_message_interval = PTP_MESSAGE_DEFAULT_MESSAGE_INTERVAL;

  // Allocate message buffer memory
  message_buffer_ = pbuf_alloc(PBUF_TRANSPORT, message_header.message_length, PBUF_RAM);
}

Message::Message(struct pbuf *p) {
  // Set message buffer pointer
  message_buffer_ = p;

  // Set up message buffer payload pointer
  auto *payload_pointer = reinterpret_cast<uint8_t *>(message_buffer_->payload);

  // Parse message header
  message_header.FromBuffer(payload_pointer);

  // Parse message payload
  message_body.FromBuffer(message_header.message_type, &(payload_pointer[34]));
}

Message::~Message() {
  // Free message buffer
  if (message_buffer_ != nullptr) {
    pbuf_free(message_buffer_);
  }
}

struct pbuf *Message::GetMessageBuffer() {
  // Set up message buffer payload pointer
  auto *payload_pointer = reinterpret_cast<uint8_t *>(message_buffer_->payload);

  // Assemble message header
  message_header.ToBuffer(payload_pointer);

  // Assemble message payload
  message_body.ToBuffer(message_header.message_type, &(payload_pointer[34]));

  // Return message buffer
  return message_buffer_;
}

/**************************************************************************************************
 *     Private functions                                                                          *
 **************************************************************************************************/

void Message::SetMessageLength() {
  // Set message length according to message type
  switch (message_header.message_type) {
    case message::kTypeSync:
    case message::kTypeDelayReq:
    case message::kTypeFollowUp:
      message_header.message_length = 44;
      break;

    case message::kTypePdelayReq:
    case message::kTypePdelayResp:
    case message::kTypeDelayResp:
    case message::kTypePdelayRespFollowUp:
      message_header.message_length = 54;
      break;

    case message::kTypeAnnounce:
      message_header.message_length = 64;
      break;

    case message::kTypeSignaling:
      // TODO(jan): set length dependent of TLVs somewhere else
      message_header.message_length = 44;
      break;

    case message::kTypeManagement:
      // TODO(jan): set length dependent of TLVs somewhere else
      message_header.message_length = 44;
      break;
  }
}

void Message::SetControlField() {
  // Set control field according to message type
  switch (message_header.message_type) {
    case message::kTypeSync:
      message_header.control_field = message::kControlFieldSync;
      break;

    case message::kTypeDelayReq:
      message_header.control_field = message::kControlFieldDelayReq;
      break;

    case message::kTypeFollowUp:
      message_header.control_field = message::kControlFieldFollowUp;
      break;

    case message::kTypeDelayResp:
      message_header.control_field = message::kControlFieldDelayResp;
      break;

    case message::kTypeManagement:
      message_header.control_field = message::kControlFieldManagement;
      break;

    default:
      message_header.control_field = message::kControlFieldOther;
      break;
  }
}
}  // namespace ptp
