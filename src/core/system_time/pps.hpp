// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_CORE_SYSTEM_TIME_PPS_HPP_
#define SRC_CORE_SYSTEM_TIME_PPS_HPP_

#include <cstdint>
#include "hal/gpio.hpp"
#include "hal/timer.hpp"
#include "drivers/neo_m8t/multi_access_handler.hpp"

//! Class implementing a mechanism to synchronize the PTP system time register to a PPS time source.
class Pps {
 public:
  /*!
   * \brief   Initialize the PPS class.
   *
   * \details This function shall be called prior to any PPS use.
   *
   * \param   subscribe_to_gnss_module  If true, the system time is directly synchronized to the
   *                                    GPS time by evaluating TIM-TP messages of the GNSS module.
   *
   * \return  TRUE if initialized successfully. FALSE otherwise.
   */
  static void Init(bool subscribe_to_gnss_module);

  /*!
   * \brief  Checks if synchronized to a master clock.
   *
   * \return True, if currently synchronized with a master clock, false otherwise.
   */
  static bool IsSynchronized();

  /*!
   * \brief   Gets the accuracy calculated when processing the last PPS pulse.
   *
   * \details When this function is called when not synchronized, it returns 0xFFFFFFFFFFFFFFFF.
   *
   * \return  Returns the accuracy in nanoseconds.
   */
  static uint64_t GetAccuracy();

  /*!
   * \brief     Sets the master time of the last received PPS pulse.
   *
   * \attention If the PPS class is set inactive (which is the default behavior), the last pps time
   *            is automatically overwritten by the current system time with every PPS pulse.
   *
   * \param     last_pps_time  Time to set the internal value of the last PPS master time to.
   *
   * \return    offset of the old master time to the time just set.
   */
  static int64_t SetLastPpsTime(uint64_t last_pps_time);

 private:
  //! Driver handler used to access the NEO-M8T device.
  static neo_m8t::MultiAccessHandler* neo_m8t_;
  static uint32_t last_quantization_error_;  //!< Quantization error of the last TIM-TP message.
  static bool synchronized_;  //!< Flag denoting if synchronized to a master clock.
  //! Time of the master clock when the last pps pulse was received.
  static int64_t last_pps_master_time_;
  static int64_t last_pps_time_;  //!< Own time when the last pps pulse was received.
  static uint32_t last_pps_systick_;  //!< Systick value when the last pps pulse was received.
  static int64_t last_time_offset_;  //!< Time offset calculated when processing the last PPS pulse.
  static hal::Gpio::Config pps_gpio_config_;  //!< Gpio config used to track the PPS pulses.
  static hal::Gpio* pps_gpio_;  //!< Gpio used to track the PPS pulses.
  static hal::Timer timer_;  //!< Timer used to count cycles between the PPS pulses

  /*!
   * \brief This callback is executed every second by the hwtimer when in fallback mode.
   *
   * \param cbArg optional user context.
   */
  static void FallbackCallback(void *cb_arg);

  /*!
   * \brief This callback is executed when a pulse is sensed on the PPS GPIO.
   *
   * \param cbArg optional user context.
   */
  static void PpsGpioCallback(void *cb_arg);

  //! Callback called by the NEO M8T driver when a TIM-TP message was received.
  static void TimTpCallback(ubx::TimTp* message);
};

#endif  // SRC_CORE_SYSTEM_TIME_PPS_HPP_
