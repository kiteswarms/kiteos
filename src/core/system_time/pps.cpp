// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "pps.hpp"
#include <cstdlib>
#include "hal/systick_timer.hpp"
#include "hal/ethernet.hpp"

/**************************************************************************************************
 *     Defines                                                                                    *
 **************************************************************************************************/

// port definitions
#define PPS_INPUT_PORT hal::Gpio::kPortD
#define PPS_INPUT_PIN hal::Gpio::kPin2

/**************************************************************************************************
 *     Static member variables                                                                    *
 **************************************************************************************************/

neo_m8t::MultiAccessHandler* Pps::neo_m8t_ = nullptr;
uint32_t Pps::last_quantization_error_ = 0;
bool Pps::synchronized_ = false;
int64_t Pps::last_pps_master_time_ = 0;
int64_t Pps::last_pps_time_ = 0;
uint32_t Pps::last_pps_systick_ = 0;
int64_t Pps::last_time_offset_ = 0xFFFFFFFFFFFFFFFF;
hal::Gpio::Config Pps::pps_gpio_config_ = {PPS_INPUT_PORT, PPS_INPUT_PIN,
                                           hal::Gpio::kModeInterruptRising};
hal::Gpio* Pps::pps_gpio_ = nullptr;

hal::Timer Pps::timer_(hal::Timer::kDevice2);
/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/

void Pps::Init(bool subscribe_to_gnss_module) {
  pps_gpio_ = new hal::Gpio(pps_gpio_config_.port, pps_gpio_config_.pins, pps_gpio_config_.mode,
                            Pps::PpsGpioCallback);

  pps_gpio_->SetPull(hal::Gpio::Pull::kPullPulldown);

  // Setup the fallback timer to 1.5 seconds so that PPS can jump in before it elapses, if available
  timer_.PeriodicCallback(1500, &FallbackCallback, nullptr);

  if (subscribe_to_gnss_module) {
    neo_m8t_ = new neo_m8t::MultiAccessHandler();
    neo_m8t_->Subscribe(TimTpCallback);
  }
}

bool Pps::IsSynchronized() {
  return synchronized_;
}

uint64_t Pps::GetAccuracy() {
  if (IsSynchronized()) {
    return abs(last_time_offset_) + last_quantization_error_;
  } else {
    return 0xFFFFFFFFFFFFFFFF;
  }
}

int64_t Pps::SetLastPpsTime(uint64_t last_pps_time) {
  int64_t time_offset = last_pps_time - last_pps_master_time_;
  last_pps_master_time_ = last_pps_time;
  return time_offset;
}


/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/

void Pps::FallbackCallback(void *cb_arg) {
  if (synchronized_) {
    // System time was synchronized to PPS before this interrupt was called. Increment last pps time
    // and last pps master times by two seconds, which is the fallback timeout interval.
    last_pps_master_time_ += 2000000000;
    last_pps_time_ += 2000000000;

    synchronized_ = false;
  } else {
    // PPS was not synchronized before this interrupt was called. Increment last pps time and last
    // pps master times by one second, which is the fallback timer interval.
    last_pps_master_time_ += 1000000000;
    last_pps_time_ += 1000000000;
  }

  timer_.PeriodicCallback(1000, &FallbackCallback, nullptr);
}

void Pps::PpsGpioCallback(void *cb_arg) {
  // Get current system time right after this callback is called
  uint32_t current_seconds, current_nanoseconds;
  hal::Ethernet::Ieee1588GetSystemTime(&current_seconds, &current_nanoseconds);

  // Check if the PPS pulse was received within a reasonable range around the expected time
  uint32_t current_systick = hal::SystickTimer::GetCount();
  if (synchronized_) {
    if (((current_systick - last_pps_systick_) < 900) ||
        ((current_systick - last_pps_systick_) > 1100)) {
      // Received unexpected PPS pulse, set PPS to not synchronized
      synchronized_ = false;
      return;
    }
  }
  last_pps_systick_ = current_systick;

  // Calculate current time and time of the PPS master time source as well as the offset to the
  // master time source
  int64_t current_time = static_cast<uint64_t >(current_seconds) * 1000000000 + current_nanoseconds;
  int64_t current_master_time = last_pps_master_time_ + 1000000000;
  int64_t time_offset = current_master_time - current_time;
  last_time_offset_ = time_offset;

  // Correct system time depending on current and previous system time offset
  if (std::abs(time_offset) > 1000000000 ||
      std::abs(last_pps_master_time_ - last_pps_time_) > 1000000000) {
    // Correct system time using coarse correction method
    bool subtract = false;
    if (time_offset < 0) {
      subtract = true;
      time_offset *= -1;
    }
    uint32_t seconds = time_offset / 1000000000;
    uint32_t nanoseconds = time_offset % 1000000000;
    if (!hal::Ethernet::Ieee1588CoarseSystemTimeCorrection(seconds, nanoseconds, subtract)) {
      // Timestamp correction failed
      return;
    }
  } else {
    // Correct system time using the fine timestamp correction method
    int64_t master_clock_count_n = 1000000000;
    int64_t slave_clock_count_n = current_time - last_pps_time_;
    int64_t clock_diff_count = current_master_time - current_time;
    double freq_scale_factor_n = static_cast<double>(master_clock_count_n + clock_diff_count) /
        static_cast<double>(slave_clock_count_n);
    if (freq_scale_factor_n <= 0.0) {
      return;
    }
    if (!hal::Ethernet::Ieee1588RescaleAddend(freq_scale_factor_n)) {
      // Timestamp correction failed
      return;
    }
  }

  // Store current clock times for next update cycle
  last_pps_master_time_ = current_master_time;
  last_pps_time_ = current_time;

  // Set PPS to synchronized
  synchronized_ = true;

  // Set up fallback timer to two seconds as a timeout
  timer_.PeriodicCallback(2000, &FallbackCallback, nullptr);
}

void Pps::TimTpCallback(ubx::TimTp *message) {
  uint64_t gps_time = (static_cast<uint64_t >(message->week) * 7 * 24 * 60 * 60 * 1000000000) +
      (static_cast<uint64_t >(message->tow_ms) * 1000000) + message->tow_sub_ms;
  // Add offset to unix time (gps time started at 6.1.1980, unix time at 1.1.1970)
  gps_time += 315964800000000000;

  uint64_t current_accuracy = abs(last_pps_master_time_ - gps_time) + message->q_err / 1000;
  if (current_accuracy > 500000000 && GetAccuracy() < 1000000) {
    // It is very likely that this is an error due to a very delayed TIM-TP message, therefore
    // ignore the message.
    return;
  }

  last_quantization_error_ = message->q_err / 1000;
  SetLastPpsTime(gps_time);
}
