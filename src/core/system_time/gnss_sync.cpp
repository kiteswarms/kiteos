// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "gnss_sync.hpp"
#include <cmath>
#include "hal/ethernet.hpp"

/**************************************************************************************************
 *     Static private member variables                                                            *
 **************************************************************************************************/
neo_m8t::MultiAccessHandler* GnssSync::neo_m8t_ = nullptr;
SoftwareTimer GnssSync::timeout_timer_;

int64_t GnssSync::previous_gps_time_ = 0;
int64_t GnssSync::previous_system_time_ = 0;
uint64_t GnssSync::accuracy_ = 0xFFFFFFFFFFFFFFFF;
bool GnssSync::is_synchronized_ = false;
uint64_t GnssSync::current_accuracy_ = 0xFFFFFFFFFFFFFFFF;
uint64_t GnssSync::previous_accuracy_ = 0xFFFFFFFFFFFFFFFF;
uint32_t GnssSync::timeout_cnt_ = 0;
uint64_t GnssSync::timeout_accuracy_increment_ = 0xFFFFFFFFFFFFFFFF;

/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
void GnssSync::Init() {
  neo_m8t_ = new neo_m8t::MultiAccessHandler();
  neo_m8t_->Subscribe(TimTm2Callback);
  timeout_timer_.SetCallback(TimeoutTimerCallback, nullptr);
}

bool GnssSync::IsSynchronized() {
  return is_synchronized_;
}

uint64_t GnssSync::GetAccuracy() {
  return accuracy_;
}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void GnssSync::TimTm2Callback(ubx::TimTm2 *message) {
  // Checks if time is valid, a new rising edge was detected and the time base is GNSS.
  if (!((message->flags & 0x40) && (message->flags & 0x80) && ((message->flags & 0x18) == 0x08))) {
    return;
  }

  uint32_t system_time_seconds, system_time_nanoseconds;
  hal::Ethernet::Ieee1588GetSystemTime(&system_time_seconds, &system_time_nanoseconds);
  // Only use seconds part, since we need the system time when the PPS pulse was generated
  uint64_t system_time = static_cast<uint64_t >(system_time_seconds) * 1000000000;

  uint64_t gps_time = (static_cast<uint64_t >(message->wn_r) * 7 * 24 * 60 * 60 * 1000000000) +
      (static_cast<uint64_t >(message->tow_ms_r) * 1000000) + message->tow_sub_ms_r;
  // Add offset to unix time (gps time started at 6.1.1980, unix time at 1.1.1970)
  gps_time += 315964800000000000;

  int64_t time_offset = gps_time - system_time;

  previous_accuracy_ = current_accuracy_;
  current_accuracy_ = abs(time_offset) + message->acc_est;

  if (current_accuracy_ > 500000000 && previous_accuracy_ < 1000000) {
    // It is very likely that this is an error due to a very delayed TIM-TM2 message, therefore
    // ignore the message.
    return;
  }

  accuracy_ = current_accuracy_;

  int64_t master_cycle_time = gps_time - previous_gps_time_;
  int64_t slave_cycle_time = system_time - previous_system_time_;

  // Correct system time depending on current and previous system time offset
  if (previous_gps_time_ == 0 || ((std::abs(time_offset) > 1000000000) &&
                                  (std::abs(master_cycle_time - slave_cycle_time) <= 500000000))) {
    // Correct system time using coarse correction method
    bool subtract = false;
    if (time_offset < 0) {
      subtract = true;
      time_offset *= -1;
    }
    uint32_t seconds = time_offset / 1000000000;
    uint32_t nanoseconds = time_offset % 1000000000;
    if (!hal::Ethernet::Ieee1588CoarseSystemTimeCorrection(seconds, nanoseconds, subtract)) {
      // Timestamp correction failed
      return;
    }
  } else {
    // Correct system time using the fine timestamp correction method
    double freq_scale_factor_n = static_cast<double>(master_cycle_time + time_offset) /
        static_cast<double>(slave_cycle_time);
    if (freq_scale_factor_n <= 0.9) {
      freq_scale_factor_n = 0.9;
    }
    if (freq_scale_factor_n > 1.1) {
      freq_scale_factor_n = 1.1;
    }
    if (!hal::Ethernet::Ieee1588RescaleAddend(freq_scale_factor_n)) {
      // Timestamp correction failed
      return;
    }
  }

  if (accuracy_ < 0.001) {
    is_synchronized_ = true;
  }
  timeout_cnt_ = 0;
  timeout_timer_.SetTimeout(1500);

  // Store current clock times for next update cycle
  previous_gps_time_ = gps_time;
  previous_system_time_ = system_time;
}

void GnssSync::TimeoutTimerCallback(void *context) {
  if (timeout_cnt_ == 0) {
    timeout_accuracy_increment_ = previous_accuracy_ + current_accuracy_;
  }
  if (timeout_cnt_ < 60) {
    accuracy_ += timeout_accuracy_increment_;
    timeout_cnt_++;
    timeout_timer_.SetTimeout(1000);
  } else {
    accuracy_ = 0xFFFFFFFFFFFFFFFF;
    is_synchronized_ = false;
  }
}
