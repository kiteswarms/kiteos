// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "system_time.hpp"
#include "ptp/node.hpp"
#include "pps.hpp"
#include "gnss_sync.hpp"
#include "hal/ethernet.hpp"


/**************************************************************************************************
 *     Defines                                                                                    *
 **************************************************************************************************/
#define PPS_LED_PORT hal::Gpio::kPortB
#define PPS_LED_PIN (hal::Gpio::kPin7)

/**************************************************************************************************
 *     Static member variables                                                                    *
 **************************************************************************************************/
PersistentMemory::SystemTimeMode SystemTime::system_time_mode_ =
    PersistentMemory::kSystemTimeModePtpSlave;
bool SystemTime::led_state_;
hal::Gpio::Config SystemTime::led_gpio_config_ = {PPS_LED_PORT,
                                                  PPS_LED_PIN,
                                                  hal::Gpio::kModeOutput};
hal::Gpio* SystemTime::led_gpio_ = {nullptr};

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
void SystemTime::Init() {
  led_gpio_ = new hal::Gpio(led_gpio_config_);

  uint32_t system_time_mode;
  if (PersistentMemory::GetValue(PersistentMemory::kSystemTimeMode, &system_time_mode)) {
    system_time_mode_ = static_cast<PersistentMemory::SystemTimeMode>(system_time_mode);
  }

  switch (system_time_mode_) {
    case PersistentMemory::kSystemTimeModePtpSlave:
      ASSERT_RESPONSE(ptp::Node::Init(false), true);
      break;
    case PersistentMemory::kSystemTimeModePtpMasterLocal:
      ASSERT_RESPONSE(ptp::Node::Init(true), true);
      break;
    case PersistentMemory::kSystemTimeModePtpMasterGnss:
      ASSERT_RESPONSE(ptp::Node::Init(true), true);
      GnssSync::Init();
      break;
    case PersistentMemory::kSystemTimeModePpsSlave:
      ASSERT_RESPONSE(ptp::Node::Init(false), true);
      Pps::Init(false);
      break;
    case PersistentMemory::kSystemTimeModePpsSlaveGnss:
      ASSERT_RESPONSE(ptp::Node::Init(false), true);
      Pps::Init(true);
      break;
    case PersistentMemory::kSystemTimeModePtpMasterPpsSlave:
      ASSERT_RESPONSE(ptp::Node::Init(true), true);
      Pps::Init(false);
      break;
    case PersistentMemory::kSystemTimeModePtpMasterPpsSlaveGnss:
      ASSERT_RESPONSE(ptp::Node::Init(true), true);
      Pps::Init(true);
      break;
    default:
      ASSERT(false);
  }

  led_state_ = false;
}

void SystemTime::Run() {
  switch (system_time_mode_) {
    case PersistentMemory::kSystemTimeModePtpSlave:
    case PersistentMemory::kSystemTimeModePtpMasterLocal:
    case PersistentMemory::kSystemTimeModePtpMasterGnss:
    case PersistentMemory::kSystemTimeModePtpMasterPpsSlave:
    case PersistentMemory::kSystemTimeModePtpMasterPpsSlaveGnss:
      ptp::Node::Run();
      break;
    case PersistentMemory::kSystemTimeModePpsSlave:
    case PersistentMemory::kSystemTimeModePpsSlaveGnss:
      break;
    default:
      ASSERT(false);
  }

  // Handle PPS LED
  uint32_t seconds, nanoseconds;
  hal::Ethernet::Ieee1588GetSystemTime(&seconds, &nanoseconds);
  if ((nanoseconds < 10000000) ||  // Blink LED always at least once
      (IsSynchronized() &&  // Second blink only for PPS and PTP
          ((nanoseconds > 150000000) && (nanoseconds < 160000000)))) {
    if (!led_state_) {
      led_gpio_->Write(false);
      led_state_ = true;
    }
  } else {
    if (led_state_) {
      led_gpio_->Write(true);
      led_state_ = false;
    }
  }
}

uint64_t SystemTime::GetTimestamp() {
  // Get timestamp from the system time register
  uint32_t seconds, nanoseconds;
  hal::Ethernet::Ieee1588GetSystemTime(&seconds, &nanoseconds);
  return static_cast<uint64_t >(seconds) * 1000000000 + nanoseconds;
}

bool SystemTime::IsSynchronized() {
  switch (system_time_mode_) {
    case PersistentMemory::kSystemTimeModePtpSlave:
      return ptp::Node::IsSynchronized();
    case PersistentMemory::kSystemTimeModePtpMasterLocal:
      return true;
    case PersistentMemory::kSystemTimeModePtpMasterGnss:
      return GnssSync::IsSynchronized();
    case PersistentMemory::kSystemTimeModePpsSlave:
    case PersistentMemory::kSystemTimeModePpsSlaveGnss:
    case PersistentMemory::kSystemTimeModePtpMasterPpsSlave:
    case PersistentMemory::kSystemTimeModePtpMasterPpsSlaveGnss:
      return Pps::IsSynchronized();
    default:
      ASSERT(false);
      return false;
  }
}

uint64_t SystemTime::GetCurrentClockAccuracy() {
  switch (system_time_mode_) {
    case PersistentMemory::kSystemTimeModePtpSlave:
      if (ptp::Node::IsSynchronized()) {
        return abs(ptp::Node::GetLastTimeOffset());
      } else {
        return 0xFFFFFFFFFFFFFFFF;
      }
    case PersistentMemory::kSystemTimeModePtpMasterLocal:
      return 0;
    case PersistentMemory::kSystemTimeModePtpMasterGnss:
      return GnssSync::GetAccuracy();
    case PersistentMemory::kSystemTimeModePpsSlave:
    case PersistentMemory::kSystemTimeModePpsSlaveGnss:
    case PersistentMemory::kSystemTimeModePtpMasterPpsSlave:
    case PersistentMemory::kSystemTimeModePtpMasterPpsSlaveGnss:
      return Pps::GetAccuracy();
    default:
      ASSERT(false);
      return 0xFFFFFFFFFFFFFFFF;
  }
}

bool SystemTime::SetLastPpsTime(uint64_t last_pps_time) {
  switch (system_time_mode_) {
    case PersistentMemory::kSystemTimeModePtpSlave:
    case PersistentMemory::kSystemTimeModePtpMasterLocal:
    case PersistentMemory::kSystemTimeModePtpMasterGnss:
    case PersistentMemory::kSystemTimeModePpsSlaveGnss:
    case PersistentMemory::kSystemTimeModePtpMasterPpsSlaveGnss:
      return false;
    case PersistentMemory::kSystemTimeModePpsSlave:
    case PersistentMemory::kSystemTimeModePtpMasterPpsSlave:
      if (Pps::IsSynchronized()) {
        Pps::SetLastPpsTime(last_pps_time);
        return true;
      }
      return false;
    default:
      ASSERT(false);
      return false;
  }
}
