// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_LWIP_PORT_PBUF_FREE_CUSTOM_HPP_
#define SRC_LWIP_PORT_PBUF_FREE_CUSTOM_HPP_

#include <cstdint>


/*!
 * \brief   Implements a class which handles the LwIP packet buffer and ethernet Rx data buffer.
 *
 * \details This class handles the LwIP memory pool and implements custom free functions. The free
 *          functions map LwIP pbuf structs to Ethernet RX data buffers and release them tho the
 *          Ethernet DMA when processed.
 */
class PbufCustom {
 public:
  /*!
   * \brief Initializes the LwIP packet buffer memory pool handled by this class.
   */
  static void Init();

  /*!
   * \brief  Allocates an LwIP packet buffer for an Ethernet Rx data buffer from the LwIP memory
   *         pool.
   *
   * \param  length Length of the Ethernet Rx packet data.
   * \param  data   Ethernet Rx packet data.
   * \param  index  Index of the Ethernet Rx buffer.
   *
   * \return Allocated packet buffer.
   */
  static struct pbuf * AllocatePbuf(uint32_t length, uint8_t *data, int index);

 private:
  //! Array mapping the data buffer index to a packet buffer free function.
  static void (*pbuf_free_function_map_[32])(struct pbuf*);

  /*!
   * \name  PbufFreeFunctionGroup
   */
  ///@{
  /*!
   * \brief Custom Rx packet buffer free callback
   *
   * \param packet_buffer  Packet buffer to be freed.
   */
  static void PbufFreeCustom0(struct pbuf *packet_buffer);
  static void PbufFreeCustom1(struct pbuf *packet_buffer);
  static void PbufFreeCustom2(struct pbuf *packet_buffer);
  static void PbufFreeCustom3(struct pbuf *packet_buffer);
  static void PbufFreeCustom4(struct pbuf *packet_buffer);
  static void PbufFreeCustom5(struct pbuf *packet_buffer);
  static void PbufFreeCustom6(struct pbuf *packet_buffer);
  static void PbufFreeCustom7(struct pbuf *packet_buffer);
  static void PbufFreeCustom8(struct pbuf *packet_buffer);
  static void PbufFreeCustom9(struct pbuf *packet_buffer);
  static void PbufFreeCustom10(struct pbuf *packet_buffer);
  static void PbufFreeCustom11(struct pbuf *packet_buffer);
  static void PbufFreeCustom12(struct pbuf *packet_buffer);
  static void PbufFreeCustom13(struct pbuf *packet_buffer);
  static void PbufFreeCustom14(struct pbuf *packet_buffer);
  static void PbufFreeCustom15(struct pbuf *packet_buffer);
  static void PbufFreeCustom16(struct pbuf *packet_buffer);
  static void PbufFreeCustom17(struct pbuf *packet_buffer);
  static void PbufFreeCustom18(struct pbuf *packet_buffer);
  static void PbufFreeCustom19(struct pbuf *packet_buffer);
  static void PbufFreeCustom20(struct pbuf *packet_buffer);
  static void PbufFreeCustom21(struct pbuf *packet_buffer);
  static void PbufFreeCustom22(struct pbuf *packet_buffer);
  static void PbufFreeCustom23(struct pbuf *packet_buffer);
  static void PbufFreeCustom24(struct pbuf *packet_buffer);
  static void PbufFreeCustom25(struct pbuf *packet_buffer);
  static void PbufFreeCustom26(struct pbuf *packet_buffer);
  static void PbufFreeCustom27(struct pbuf *packet_buffer);
  static void PbufFreeCustom28(struct pbuf *packet_buffer);
  static void PbufFreeCustom29(struct pbuf *packet_buffer);
  static void PbufFreeCustom30(struct pbuf *packet_buffer);
  static void PbufFreeCustom31(struct pbuf *packet_buffer);
};
///@}

#endif  // SRC_LWIP_PORT_PBUF_FREE_CUSTOM_HPP_
