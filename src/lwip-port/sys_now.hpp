// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_LWIP_PORT_SYS_NOW_HPP_
#define SRC_LWIP_PORT_SYS_NOW_HPP_

#ifdef __cplusplus
extern "C" {
#endif

#include "lwip/arch.h"

/*!
  * \brief  Returns the current time in milliseconds. This function is used by LwIP and needs to be
  *         implemented somewhere.
  *
  * \return Current Time value.
  */
u32_t sys_now();

#ifdef __cplusplus
}
#endif

#endif  // SRC_LWIP_PORT_SYS_NOW_HPP_
