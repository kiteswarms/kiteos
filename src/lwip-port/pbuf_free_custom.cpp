// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#include "pbuf_free_custom.hpp"
#include "lwip/opt.h"
#include "netif/etharp.h"
#include "lwip/stats.h"
#include "hal/ethernet.hpp"


#define PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(index) \
          void PbufCustom::PbufFreeCustom##index(struct pbuf *packet_buffer) { \
            hal::Ethernet::ReleaseReceiveBuffer(index);                        \
            LWIP_MEMPOOL_FREE(RX_POOL, packet_buffer);                         \
          }


LWIP_MEMPOOL_DECLARE(RX_POOL, HAL_ETHERNET_RX_DESCRIPTOR_COUNT, sizeof(struct pbuf_custom),
                     "Zero-copy RX PBUF pool");  //!< Our private RX memory pool


/**************************************************************************************************
 *      Static member variables                                                                   *
 **************************************************************************************************/

void (*PbufCustom::pbuf_free_function_map_[32])(struct pbuf*) = {
    PbufFreeCustom0, PbufFreeCustom1, PbufFreeCustom2, PbufFreeCustom3, PbufFreeCustom4,
    PbufFreeCustom5, PbufFreeCustom6, PbufFreeCustom7, PbufFreeCustom8, PbufFreeCustom9,
    PbufFreeCustom10, PbufFreeCustom11, PbufFreeCustom12, PbufFreeCustom13, PbufFreeCustom14,
    PbufFreeCustom15, PbufFreeCustom16, PbufFreeCustom17, PbufFreeCustom18, PbufFreeCustom19,
    PbufFreeCustom20, PbufFreeCustom21, PbufFreeCustom22, PbufFreeCustom23, PbufFreeCustom24,
    PbufFreeCustom25, PbufFreeCustom26, PbufFreeCustom27, PbufFreeCustom28, PbufFreeCustom29,
    PbufFreeCustom30, PbufFreeCustom31
};


/**************************************************************************************************
 *      Public methods                                                                            *
 **************************************************************************************************/

void PbufCustom::Init() {
  // Initialize the RX POOL
  LWIP_MEMPOOL_INIT(RX_POOL);
}

struct pbuf *PbufCustom::AllocatePbuf(uint32_t length, uint8_t *data, int index) {
  struct pbuf *p = nullptr;
  auto* custom_pbuf = (struct pbuf_custom*)LWIP_MEMPOOL_ALLOC(RX_POOL);
  custom_pbuf->custom_free_function = pbuf_free_function_map_[index];
  p = pbuf_alloced_custom(PBUF_RAW, length, PBUF_POOL, custom_pbuf, data, HAL_ETHERNET_BUFFER_SIZE);
  return p;
}


/**************************************************************************************************
 *      Private methods                                                                           *
 **************************************************************************************************/

PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(0)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(1)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(2)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(3)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(4)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(5)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(6)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(7)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(8)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(9)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(10)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(11)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(12)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(13)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(14)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(15)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(16)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(17)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(18)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(19)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(20)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(21)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(22)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(23)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(24)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(25)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(26)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(27)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(28)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(29)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(30)
PBUF_FREE_CUSTOM_BUFFER_IMPLEMENTATION(31)
