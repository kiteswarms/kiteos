// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#include "lwip-port/ethernet_interface.hpp"
#include "lwip-port/pbuf_free_custom.hpp"
#include "hal/ethernet.hpp"
#include "lwip/timeouts.h"
#include "lwip/autoip.h"
#include "netif/ethernet.h"
#include "netif/etharp.h"


/**************************************************************************************************
 *     Private member variables                                                                   *
 **************************************************************************************************/

struct netif EthernetInterface::network_interface_;


/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/

bool EthernetInterface::Init(const char *ip, const char *mask, const char *gate) {
  return EthernetInterface::Init(ipaddr_addr(ip),
                                 ipaddr_addr(mask),
                                 ipaddr_addr(gate));
}

bool EthernetInterface::Init(uint32_t ip, uint32_t mask, uint32_t gate) {
  return EthernetInterface::Init(reinterpret_cast<ip_addr_t *>(&ip),
                                 reinterpret_cast<ip_addr_t *>(&mask),
                                 reinterpret_cast<ip_addr_t *>(&gate));
}

bool EthernetInterface::Init(ip_addr_t* ip, ip_addr_t* mask, ip_addr_t* gate) {
  netif_add(&network_interface_,
            ip, mask, gate,
            nullptr, &LwipInitCallback, &ethernet_input);

  if (!(ip->addr)) {
    autoip_start(&network_interface_);
  }

  // Register network interface callback for setting up MAC filter
  netif_set_igmp_mac_filter(&network_interface_, &LwipSetMacFilterCallback);

  // Set network interface structure as default network interface
  netif_set_default(&network_interface_);

  return true;
}

void EthernetInterface::Run() {
  sys_check_timeouts();

  hal::Ethernet::RxBuffer rx_buffer{};

  if (hal::Ethernet::Receive(&rx_buffer)) {
    struct pbuf *p = PbufCustom::AllocatePbuf(rx_buffer.length, rx_buffer.data,
                                              rx_buffer.data_buffer_index);

    if (p != nullptr) {
      // Entry point to the LwIP stack
      if (network_interface_.input(p, &network_interface_) != ERR_OK) {
        pbuf_free(p);
      }
    }
  }
}


/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/

err_t EthernetInterface::LwipSendCallback(struct netif *network_interface,
                                          struct pbuf *packet_buffer) {
  return hal::Ethernet::Send(reinterpret_cast<uint8_t*>(packet_buffer->payload),
                             packet_buffer->len) ? ERR_OK : ERR_WOULDBLOCK;
}

err_t EthernetInterface::LwipInitCallback(struct netif *network_interface) {
  LWIP_ASSERT("netif != NULL", (network_interface != nullptr));

  // Initialize LwIP network interface
#if LWIP_NETIF_HOSTNAME
  network_interface->hostname = "lwip";
#endif /* LWIP_NETIF_HOSTNAME */
  network_interface->name[0] = 's';
  network_interface->name[1] = 't';
  network_interface->output = etharp_output;
  network_interface->linkoutput = LwipSendCallback;
  network_interface->hwaddr_len = ETHARP_HWADDR_LEN;
  network_interface->hwaddr[0] = (hal::Ethernet::mac_address_ & 0x00000000000000fful);
  network_interface->hwaddr[1] = (hal::Ethernet::mac_address_ & 0x000000000000ff00ul) >> 8;
  network_interface->hwaddr[2] = (hal::Ethernet::mac_address_ & 0x0000000000ff0000ul) >> 16;
  network_interface->hwaddr[3] = (hal::Ethernet::mac_address_ & 0x00000000ff000000ul) >> 24;
  network_interface->hwaddr[4] = (hal::Ethernet::mac_address_ & 0x000000ff00000000ul) >> 32;
  network_interface->hwaddr[5] = (hal::Ethernet::mac_address_ & 0x0000ff0000000000ul) >> 40;
  network_interface->mtu = hal::Ethernet::maximum_payload_size_;
  network_interface->flags |= NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP;
#ifdef LWIP_IGMP
  network_interface->flags |= NETIF_FLAG_IGMP;
#endif

  // Initialize LWIP mempool
  PbufCustom::Init();

  // Ethernet peripheral starts operating
  hal::Ethernet::Start();

  // Set LwIP interface and interface link up
  netif_set_up(&network_interface_);
  netif_set_link_up(&network_interface_);

  return ERR_OK;
}

err_t EthernetInterface::LwipSetMacFilterCallback(struct netif *network_interface,
                                                  const ip4_addr_t *group_address,
                                                  enum netif_mac_filter_action mac_filter_action) {
  if (mac_filter_action == NETIF_ADD_MAC_FILTER) {
    hal::Ethernet::SetMulticastEnabled(true);
  }
  return ERR_OK;
}
