// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_PULICAST_PORT_ARM_PULICAST_PORT_PULICAST_EMBEDDED_H_
#define SRC_PULICAST_PORT_ARM_PULICAST_PORT_PULICAST_EMBEDDED_H_

#define CUSTOM_HTON "lwip/def.h"
#define CUSTOM_RAND "pulicast-port/custom_rand.h"

#include <string_view>
#include <string>
#include <chrono>
#include <memory>

#include "core/software_timer.hpp"

#include "pulicast/node.h"
#include "pulicast/channel.h"
#include "pulicast/serialization.h"
#include "pulicast/address.h"
#include "pulicast/distributed_setting.h"
#include "pulicast/core/transport.h"
#include "pulicast-port/lwip_transport.h"
#include "pulicast-port/publish.h"

// TODO(max): should I write destructors? If yes where? Talk with Tobi about it once you know more.
namespace pulicast {

constexpr uint16_t DEFAULT_PORT = 8765;  //!< Default pulicast port

/*!
 *  \brief   LwipNode node implementation using lwIP transport.
 *
 *  \author   Elias Rosch
 *  \date     2020
 */
class LwipNode : public pulicast::Node {
 public:
  /*!
   * \brief  Constructor of the LwipNode.
   *
   * \param name string to identify the node.
   * \param port Multicast UDP port to use.
   * \param ttl Multicast UDP ttl to use.
   * \param announcement_period_ns time which determines the node metadata publishing period.
   *
   * \return Returns constructed LwipNode-instance.
   */
  LwipNode(const std::string &name,
           const std::string &default_namespace,
           uint16_t port = DEFAULT_PORT, uint8_t ttl = 0,
           PublishingPeriod announcement_period_ns = std::chrono::milliseconds(100)) :
      pulicast::Node(name,
                     default_namespace,
                     announcement_period_ns,
                     std::make_unique<transport::LwipTransport>(port, ttl)) {
    update_timer_.SetTimeoutPeriodic(
      std::chrono::duration_cast<std::chrono::milliseconds>(GetAnnouncementPeriod()),
      [](void* ctx) {
        LwipNode* inst = reinterpret_cast<LwipNode*>(ctx);
        inst->AnnounceNode();
      },
      this);
  }

 private:
  SoftwareTimer update_timer_;  //!< Timer used to track the metadata publishing period
};
}  // namespace pulicast

//! Create abstraction alias
using PulicastEmbeddedNode = pulicast::LwipNode;

#endif  // SRC_PULICAST_PORT_ARM_PULICAST_PORT_PULICAST_EMBEDDED_H_
