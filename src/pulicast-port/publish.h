// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_PULICAST_PORT_PUBLISH_H_
#define SRC_PULICAST_PORT_PUBLISH_H_

#include <vector>
#include <utility>
#include "pulicast/serialization.h"

#include "kitecom/timestamped_vector_double.hpp"
#include "kitecom/timestamped_vector_sint.hpp"
#include "kitecom/timestamped_vector_uint.hpp"
#include "kitecom/timestamped_vector_byte.hpp"
#include "kitecom/timestamped_vector_string.hpp"
/*!
  * \brief  Helper function to publish double value messages.
  *
  * \param  ochan      Channel that should be published on.
  * \param  timestamp  Timing information to the content of the data.
  * \param  values     Double data that should be published.
  */
inline void Publish(pulicast::Channel& ochan, uint64_t timestamp,
                    const std::vector<double> &values) {
  kitecom::timestamped_vector_double msg;
  msg.ts = timestamp;
  msg.values = std::move(values);
  msg.len = msg.values.size();
  ochan << msg;
}

/*!
  * \brief  Helper function to publish int32_t value messages.
  *
  * \param  ochan      Channel that should be published on.
  * \param  timestamp  Timing information to the content of the data.
  * \param  values     int32_t data that should be published.
  */
inline void Publish(pulicast::Channel& ochan, uint64_t timestamp,
                    const std::vector<int32_t> &values) {
  kitecom::timestamped_vector_sint msg;
  msg.ts = timestamp;
  msg.values = std::move(values);
  msg.len = msg.values.size();
  ochan << msg;
}

/*!
  * \brief  Helper function to publish uint32_t value messages.
  *
  * \param  ochan      Channel that should be published on.
  * \param  timestamp  Timing information to the content of the data.
  * \param  values     uint32_t data that should be published.
  */
inline void Publish(pulicast::Channel& ochan, uint64_t timestamp,
                    const std::vector<uint32_t> &values) {
  kitecom::timestamped_vector_uint msg;
  msg.ts = timestamp;
  msg.values = std::move(reinterpret_cast<const std::vector<int32_t> &>(values));
  msg.len = msg.values.size();
  ochan << msg;
}

/*!
  * \brief  Helper function to publish byte value messages.
  *
  * \param  ochan      Channel that should be published on.
  * \param  timestamp  Timing information to the content of the data.
  * \param  values     Byte data that should be published.
  */
inline void Publish(pulicast::Channel& ochan, uint64_t timestamp,
                    const std::vector<uint8_t> &values) {
  kitecom::timestamped_vector_byte msg;
  msg.ts = timestamp;
  msg.values = std::move(values);
  msg.len = msg.values.size();
  ochan << msg;
}


// template<class Target>
// inline void operator<<(Target&& target, const std::tuple<int64_t,
//                        std::vector<int32_t> >& message) {
//   auto& [ts, values] = message;
//   Publish(target, ts, values);
// }

// template<class Target>
// inline void operator<<(Target&& target, const std::tuple<int64_t,
//                        std::vector<uint32_t> >& message) {
//   auto& [ts, values] = message;
//   Publish(target, ts, values);
// }

// template<class Target>
// inline void operator<<(Target&& target, const std::tuple<int64_t,
//                        std::vector<double> >& message) {
//   auto& [ts, values] = message;
//   Publish(target, ts, values);
// }

// template<class Target>
// inline void operator<<(Target&& target, const std::tuple<int64_t,
//                        std::vector<uint8_t> >& message) {
//   auto& [ts, values] = message;
//   Publish(target, ts, values);
// }

/*!
  * \brief  Helper function to publish single double value messages.
  *
  * \param  ochan      Channel that should be published on.
  * \param  timestamp  Timing information to the content of the data.
  * \param  value     Double data that should be published.
  */
inline void Publish(pulicast::Channel& ochan, uint64_t timestamp, double value) {
  Publish(ochan, timestamp, std::vector<double>{value});
}

/*!
  * \brief  Helper function to publish single int32_t value messages.
  *
  * \param  ochan      Channel that should be published on.
  * \param  timestamp  Timing information to the content of the data.
  * \param  value     int32_t data that should be published.
  */
inline void Publish(pulicast::Channel& ochan, uint64_t timestamp, int32_t value) {
  Publish(ochan, timestamp, std::vector<int32_t>{value});
}

/*!
  * \brief  Helper function to publish single uint32_t value messages.
  *
  * \param  ochan      Channel that should be published on.
  * \param  timestamp  Timing information to the content of the data.
  * \param  value     uint32_t data that should be published.
  */
inline void Publish(pulicast::Channel& ochan, uint64_t timestamp, uint32_t value) {
  Publish(ochan, timestamp, std::vector<uint32_t>{value});
}

#endif  // SRC_PULICAST_PORT_PUBLISH_H_
