// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "failsafe_pilot_reference.hpp"

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
FailsafePilotReference::FailsafePilotReference(): spektrum_receiver_handler_(ChannelDataCallback,
                                                                             this) {}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void FailsafePilotReference::ChannelDataCallback(spektrum_receiver::ChannelData channel_data,
                                                 void* ctx) {
  auto inst = reinterpret_cast<FailsafePilotReference*>(ctx);
  // Set power output pin according to channel value (this is the arm switch)
  if (channel_data.gear.valid) {
    inst->failsafe_switch_.Set(channel_data.gear.value > 0);
  }
}
