// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include <vector>
#include "sensors/gnss.hpp"
#include "core/system_time/system_time.hpp"
#include "core/scheduler.hpp"
#include "utils/debug_utils.h"

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
Gnss::Gnss(pulicast::Namespace puli_namespace):
fixtype_ochan_(puli_namespace["FixType"]),
gpstime_ochan_(puli_namespace["GpsTime"]),
numsat_ochan_(puli_namespace["NumSatellites"]),
pos_ochan_(puli_namespace["GeographicPosition"]),
altmsl_ochan_(puli_namespace["Altitude"]),
altelip_ochan_(puli_namespace["EllipsoidalAltitude"]),
vel_ochan_(puli_namespace["Velocity"]),
pvacc_ochan_(puli_namespace["PVAccuracy"]),
rawx_ochan_(puli_namespace["RawGnss"]),
sfrbx_ochan_(puli_namespace["NavFrame"]),
raw_enabled_(false, "GNSS/RawEnabled", puli_namespace,
             [this](){
               raw_enabled_.value() ? neo_m8t_.EnableRaw() : neo_m8t_.DisableRaw();
             }) {
  neo_m8t_.Subscribe(TimTpCallback, this);
  neo_m8t_.Subscribe(NavPvtCallback, this);
  // Subscribe to raw data messages
  neo_m8t_.Subscribe(ubx::kRxmRawX, RxmRawxCallback, this);
  neo_m8t_.Subscribe(ubx::kRxmSfrbX, RxmSfrbxCallback, this);
}

Gnss::~Gnss() = default;

/**************************************************************************************************
 *     Callback functions                                                                         *
 **************************************************************************************************/
void Gnss::TimTpCallback(ubx::TimTp* tim_tp, Gnss* ctx) {
  uint64_t gps_ts = tim_tp->week * 604800 + (tim_tp->tow_ms / 1000.0f + 0.5);
  std::vector<int32_t> value = {static_cast<int32_t>(gps_ts >> 32u),
                                static_cast<int32_t>(gps_ts & 0xFFFFFFFF)};

  Publish(ctx->gpstime_ochan_, SystemTime::GetTimestamp(), value);
}

void Gnss::NavPvtCallback(ubx::NavPvt* nav_pvt, Gnss* ctx) {
  Publish(ctx->fixtype_ochan_, SystemTime::GetTimestamp(), (uint32_t) nav_pvt->fix_type);
  Publish(ctx->numsat_ochan_, SystemTime::GetTimestamp(), (uint32_t) nav_pvt->num_sv);

  if (nav_pvt->flags & 0x1) {
    std::vector<double> pos = {static_cast<double>(nav_pvt->lat) * 1e-7,
                               static_cast<double>(nav_pvt->lon) * 1e-7};
    Publish(ctx->pos_ochan_, SystemTime::GetTimestamp(), pos);
    Publish(ctx->altmsl_ochan_, SystemTime::GetTimestamp(), nav_pvt->h_msl / 1000.0);
    Publish(ctx->altelip_ochan_, SystemTime::GetTimestamp(), nav_pvt->height / 1000.0);

    std::vector<double> vel = {static_cast<double>(nav_pvt->vel_n) / 1000.0,
                               static_cast<double>(nav_pvt->vel_e) / 1000.0,
                               static_cast<double>(nav_pvt->vel_d) / 1000.0};
    Publish(ctx->vel_ochan_, SystemTime::GetTimestamp(), vel);
    std::vector<double> acc = {static_cast<double>(nav_pvt->h_acc) / 1000.0,
                               static_cast<double>(nav_pvt->v_acc) / 1000.0,
                               static_cast<double>(nav_pvt->s_acc) / 1000.0};
    Publish(ctx->pvacc_ochan_, SystemTime::GetTimestamp(), acc);
  }
}
void Gnss::RxmRawxCallback(uint8_t* data, uint16_t size, Gnss* ctx) {
  std::vector<uint8_t> value;
  value.assign(data, data + size);
  Publish(ctx->rawx_ochan_, SystemTime::GetTimestamp(), value);
}

void Gnss::RxmSfrbxCallback(uint8_t* data, uint16_t size, Gnss* ctx) {
  std::vector<uint8_t> value;
  value.assign(data, data + size);
  Publish(ctx->sfrbx_ochan_, SystemTime::GetTimestamp(), value);
}
