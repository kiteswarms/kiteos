// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_PLUGINS_SENSORS_POWER_MONITOR_HPP_
#define SRC_PLUGINS_SENSORS_POWER_MONITOR_HPP_

//*****************************************************************************/
//! \addtogroup plugins
//! @{
//! \addtogroup misc
//! @{
///
//*****************************************************************************/

#include "core/plugin_manager/plugin.hpp"
#include "core/system_time/system_time.hpp"
#include "core/software_timer.hpp"
#include "core/state_node.hpp"
#include "drivers/battery_voltage_sensor.hpp"
#include "drivers/ina3221.hpp"
#include "drivers/buzzer.hpp"
#include "drivers/motor_power_sense.hpp"

#define BATTERY_CELLS 4

/*!
 * \brief   Implements the PowerMonitor plug-in.
 *
 * \details PowerMonitor is used to monitor different voltages on Cordis.
 *
 * \author  Julian Reimer
 *
 * \date    2021
 */

class PowerMonitor : public Plugin {
 public:
  /*!
   * \brief Constructor of the PowerMonitor-Component.
   *
   * \param puli_namespace  The pulicast namespace the power monitor operates in.
   */
  explicit PowerMonitor(pulicast::Namespace puli_namespace);

 private:
  /*!
  * @brief Callback for arrived commands
  *
  * @param command which arrives via the state node
  */
  void CommandCallback(std::string_view command) {}

  //! Publishes the battery cell voltages.
  static void SendBatteryVoltages(double voltage_channels_[BATTERY_CELLS], void *instance);
  //! Publishes the ina voltages (shunt voltages for stacked boards and xt60 connectors).
  static void SendInaVoltages(double voltage_channels_[3], void *instance);
  //! Publishes the ina voltages (shunt voltages for stacked boards and xt60 connectors).
  static void SendGpioSensing(PowerMonitor *inst);


  StateNode state_node_;  //!< StateNode interface to report MTRON states

  //! BatteryVoltageSensor instance (Battery voltage monitoring).
  BatteryVoltageSensor<BATTERY_CELLS> battery_;
  Ina3221 ina_;  //!< Ina3221 instance (voltage sensor).
  Buzzer buzzer_;  //!< Driver that handles the buzzer sound.
  MotorPowerSense motor_power_sense_;  //!< Driver that reads the motor power sense signals
  bool mtron_state_;  //!< holds the current state of the MTRON signal
  bool button_state_;  //!< holds the current state of the BUTTON signal
  SoftwareTimer gpio_update_timer_;  //!< reads gpio and publishes changed values.

  pulicast::Channel& battery_voltages_ochan_;  //!< pulicast channel for battery voltages.
  pulicast::Channel& ina_voltages_ochan_;  //!< pulicast channel for ina voltages.
};
/*! @} */
/*! @} */
#endif  // SRC_PLUGINS_SENSORS_POWER_MONITOR_HPP_
