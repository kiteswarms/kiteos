// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include <vector>
#include "barometer.hpp"
#include "core/scheduler.hpp"
#include "core/logger.hpp"

/**************************************************************************************************
 *     Public Methods                                                                             *
 **************************************************************************************************/
Barometer::Barometer(pulicast::Namespace puli_namespace, bool secondary_sensor)
  : ms5611_(secondary_sensor, MeasurementCallback, this),
    pressure_channel_(puli_namespace["Pressure"]),
    temperature_channel_(puli_namespace["Temperature"]) {
  timeout_timer_.SetCallback(NoDataTimeoutCallback, this);
  timeout_timer_.SetTimeout(no_data_timeout_);
}

Barometer::~Barometer() = default;


/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void Barometer::MeasurementCallback(double pressure, double temperature, void* ctx) {
  auto inst = reinterpret_cast<Barometer*>(ctx);

  // Measurement available, restart timeout timer
  inst->timeout_timer_.SetTimeout(no_data_timeout_);
  Publish(inst->pressure_channel_, SystemTime::GetTimestamp(), pressure);
  Publish(inst->temperature_channel_, SystemTime::GetTimestamp(), temperature);
}

void Barometer::NoDataTimeoutCallback(Barometer *inst) {
  Logger::Report("Ms5611 timed out.", Logger::kWarning);
  inst->timeout_timer_.SetTimeout(no_data_timeout_);
}
