// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_PLUGINS_SENSORS_HYGROMETER_HPP_
#define SRC_PLUGINS_SENSORS_HYGROMETER_HPP_

#include "core/plugin_manager/plugin.hpp"
#include "core/software_timer.hpp"
#include "drivers/sht25.hpp"

/*!
 *  \brief Implements a plug-in containing and handling an barometer object.
 */
class Hygrometer : public Plugin {
 public:
  /*!
   * \brief Constructor of the Hygrometer component.
   *
   * \param puli_namespace  The pulicast namespace the Hygrometer operates in.
   */
  explicit Hygrometer(pulicast::Namespace puli_namespace);

  /*!
   * \brief Destructor of the Hygrometer component.
   */
  ~Hygrometer() override;

 private:
  //! Timeout to report that the Hygrometer is not working after the last measurement in ms.
  static constexpr const uint64_t no_data_timeout_ = 1000;

  //! Callback called when Hygrometer measurement data is available.
  static void MeasurementCallback(Sht25::MeasurementData measurement_data, void* ctx);
  //! Publishes a warning. This method is called by the timeout_timer_ when a timeout occurred.
  static void NoDataTimeoutCallback(Hygrometer *inst);

  Sht25 sht25_;  //!< Sht25 driver instance which handles the Sht25 communication.
  SoftwareTimer timeout_timer_;  //!< Timer to detect that the driver is not working.
  //! Output channel for temperature measurement values.
  pulicast::Channel& temperature_channel_;
  //! Output channel for humidity measurement values.
  pulicast::Channel& humidity_channel_;
};

#endif  // SRC_PLUGINS_SENSORS_HYGROMETER_HPP_
