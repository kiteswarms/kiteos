// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_PLUGINS_SENSORS_ADC_HPP_
#define SRC_PLUGINS_SENSORS_ADC_HPP_

#include "core/plugin_manager/plugin.hpp"
#include "core/software_timer.hpp"
#include "drivers/max11040k.hpp"

/*!
 *  \brief Implements a plug-in containing and handling an barometer object.
 */
class Adc : public Plugin {
 public:
  /*!
   * \brief Constructor of the Adc component.
   *
   * \param puli_namespace  The pulicast namespace the Adc operates in.
   */
  explicit Adc(pulicast::Namespace puli_namespace);

  /*!
   * \brief Destructor of the Adc component.
   */
  ~Adc() override;

 private:
  //! Timeout to report that the Adc is not working after the last measurement in ms.
  static constexpr const uint64_t no_data_timeout_ = 1000;

  //! Callback called when Adc measurement data is available.
  static void MeasurementCallback(Max11040k::MeasurementData measurement_data, void* ctx);
  //! Publishes a warning. This method is called by the timeout_timer_ when a timeout occurred.
  static void NoDataTimeoutCallback(Adc *inst);

  Max11040k max11040k_;  //!< Max11040k driver instance which handles the Adc communication.
  SoftwareTimer timeout_timer_;  //!< Timer to detect that the Max11040k is not working.
  //! Output channel for converted analog voltage measurement values.
  pulicast::Channel& voltage_channel_;
};

#endif  // SRC_PLUGINS_SENSORS_ADC_HPP_
