// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include <vector>
#include "uwb.hpp"
#include "core/logger.hpp"
#include "kitecom/uwb_measurement.hpp"

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
Uwb::Uwb(pulicast::Namespace puli_namespace):
puli_namespace_(puli_namespace),
telocate_uwb_(MeasurementDataCallback, this, UwbInitCallback, this),
range_1_channel_(puli_namespace["Range1"]),
range_2_channel_(puli_namespace["Range2"]),
range_3_channel_(puli_namespace["Range3"]),
range_4_channel_(puli_namespace["Range4"]),
measurement_interval_(telocate_uwb_.GetMeasurementInterval(),
                      "Uwb/MeasurementInterval", puli_namespace,
                      [this](){
                        telocate_uwb_.SetMeasurementInterval(measurement_interval_.value());
                      }),
remote_node_addresses_(
    "Uwb/RemoteNodeAddresses", puli_namespace,
    [this](){
      telocate_uwb_.SetRemoteNodeAddresses(reinterpret_cast<const uint32_t *>(
          remote_node_addresses_->values.data()), remote_node_addresses_->len);
    }) {}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void Uwb::UwbInitCallback(uint32_t node_address, void *instance) {
  auto inst = reinterpret_cast<Uwb*>(instance);

  inst->node_address_.emplace(*reinterpret_cast<int32_t*>(&node_address), "Uwb/NodeAddress",
                              inst->puli_namespace_,
                              [inst](){
                                inst->telocate_uwb_.SetNodeAddress(
                                    *reinterpret_cast<const uint32_t*>(
                                        &(inst->node_address_.value().value())));
                              });
}

void Uwb::MeasurementDataCallback(uint32_t remote_node_index, float distance, float rssi,
                                  uint64_t measurement_timestamp, TelocateUwb::Status status,
                                  void *ctx) {
  auto inst = reinterpret_cast<Uwb*>(ctx);


  kitecom::uwb_measurement message;
  message.distance = distance;
  message.rssi = rssi;
  switch (status) {
    case TelocateUwb::kStatusValid:
      message.status = "valid";
      break;
    case TelocateUwb::kStatusTimeout:
      message.status = "timeout";
      break;
    case TelocateUwb::kStatusGarbageData:
      message.status = "garbage";
      break;
  }
  message.ts = measurement_timestamp;

  switch (remote_node_index) {
    case 0:
      inst->range_1_channel_ << message;
      break;

    case 1:
      inst->range_2_channel_ << message;
      break;

    case 2:
      inst->range_3_channel_ << message;
      break;

    case 3:
      inst->range_4_channel_ << message;
      break;

    default:
      Logger::Report("UWB: Received unexpected measurement.", Logger::kWarning);
      break;
  }
}
