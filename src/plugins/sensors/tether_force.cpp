// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include <vector>
#include "tether_force.hpp"
#include "core/logger.hpp"

#define STEPPER_MOTOR_ENCODER_PORT hal::i2c::kPort4
#define STEPPER_MOTOR_ENCODER_X_ADDRESS 0x40
#define STEPPER_MOTOR_ENCODER_Y_ADDRESS 0x41
#define STEPPER_MOTOR_FORCE_ADC_CHANNEL 1  // Channel 1 and 3 can be external sensors

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
TetherForce::TetherForce(pulicast::Namespace puli_namespace):
raw_x_angle_channel_(puli_namespace["RawTetherXAngle"]),
raw_y_angle_channel_(puli_namespace["RawTetherYAngle"]),
raw_force_channel_(puli_namespace["RawTetherForce"]),
force_vector_channel_(puli_namespace["TetherForce"]),
max11040k_(AdcCallback, this),
as5048_x_(STEPPER_MOTOR_ENCODER_PORT, STEPPER_MOTOR_ENCODER_X_ADDRESS, XAngleCallback, this),
as5048_y_(STEPPER_MOTOR_ENCODER_PORT, STEPPER_MOTOR_ENCODER_Y_ADDRESS, YAngleCallback, this) {}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void TetherForce::AdcCallback(Max11040k::MeasurementData measurement_data, void *instance) {
  auto inst = reinterpret_cast<TetherForce*>(instance);
  inst->adc_voltage_ = measurement_data.voltage[STEPPER_MOTOR_FORCE_ADC_CHANNEL];
  inst->adc_voltage_timestamp_ = measurement_data.timestamp;
  Publish(inst->raw_force_channel_, inst->adc_voltage_timestamp_, inst->adc_voltage_);
}

void TetherForce::XAngleCallback(float angle, uint64_t timestamp, void *instance) {
  auto inst = reinterpret_cast<TetherForce*>(instance);
  inst->x_angle_ = angle;
  inst->x_angle_timestamp_ = timestamp;
  Publish(inst->raw_x_angle_channel_, inst->x_angle_timestamp_, inst->x_angle_);
}

void TetherForce::YAngleCallback(float angle, uint64_t timestamp, void *instance) {
  auto inst = reinterpret_cast<TetherForce*>(instance);
  inst->y_angle_ = angle;
  inst->y_angle_timestamp_ = timestamp;
  Publish(inst->raw_y_angle_channel_, inst->y_angle_timestamp_, inst->y_angle_);
}
