// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "rc_receiver.hpp"
#include "core/scheduler.hpp"
#include "utils/debug_utils.h"

/**************************************************************************************************
 *     Public Methods                                                                             *
 **************************************************************************************************/
RcReceiver::RcReceiver(pulicast::Namespace puli_namespace):
spektrum_receiver_handler_(ChannelDataCallback, this),
roll_channel_(puli_namespace["Roll"]),
pitch_channel_(puli_namespace["Pitch"]),
yaw_channel_(puli_namespace["Yaw"]),
throttle_channel_(puli_namespace["Throttle"]),
gear_channel_(puli_namespace["Gear"]),
aux1_channel_(puli_namespace["Aux1"]),
aux2_channel_(puli_namespace["Aux2"]),
aux3_channel_(puli_namespace["Aux3"]) {}

RcReceiver::~RcReceiver() = default;

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void RcReceiver::ChannelDataCallback(spektrum_receiver::ChannelData channel_data, void* ctx) {
  auto inst = reinterpret_cast<RcReceiver*>(ctx);

  // Publish new set points
  if (channel_data.aileron.valid) {
    Publish(inst->roll_channel_, channel_data.timestamp, channel_data.aileron.value);
  }
  if (channel_data.elevator.valid) {
    Publish(inst->pitch_channel_, channel_data.timestamp, channel_data.elevator.value);
  }
  if (channel_data.rudder.valid) {
    Publish(inst->yaw_channel_, channel_data.timestamp, channel_data.rudder.value);
  }
  if (channel_data.throttle.valid) {
    Publish(inst->throttle_channel_, channel_data.timestamp, channel_data.throttle.value);
  }
  if (channel_data.gear.valid) {
    Publish(inst->gear_channel_, channel_data.timestamp, channel_data.gear.value);
  }
  if (channel_data.aux1.valid) {
    Publish(inst->aux1_channel_, channel_data.timestamp, channel_data.aux1.value);
  }
  if (channel_data.aux2.valid) {
    Publish(inst->aux2_channel_, channel_data.timestamp, channel_data.aux2.value);
  }
  if (channel_data.aux3.valid) {
    Publish(inst->aux3_channel_, channel_data.timestamp, channel_data.aux3.value);
  }
}
