// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include <vector>
#include "hygrometer.hpp"
#include "core/scheduler.hpp"
#include "core/logger.hpp"

/**************************************************************************************************
 *     Public Methods                                                                             *
 **************************************************************************************************/
Hygrometer::Hygrometer(pulicast::Namespace puli_namespace):
sht25_(MeasurementCallback, this),
temperature_channel_(puli_namespace["Temperature"]),
humidity_channel_(puli_namespace["RelativeHumidity"]) {
  timeout_timer_.SetCallback(NoDataTimeoutCallback, this);
  timeout_timer_.SetTimeout(no_data_timeout_);
}

Hygrometer::~Hygrometer() = default;


/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void Hygrometer::MeasurementCallback(Sht25::MeasurementData measurement_data, void* ctx) {
  auto inst = reinterpret_cast<Hygrometer*>(ctx);

  // Publish temperature value
  Publish(inst->temperature_channel_,
          measurement_data.temperature_timestamp, measurement_data.temperature);
  // Publish temperature value
  Publish(inst->humidity_channel_,
          measurement_data.humidity_timestamp, measurement_data.humidity);
  // Measurement available, restart timeout timer
  inst->timeout_timer_.SetTimeout(no_data_timeout_);
}

void Hygrometer::NoDataTimeoutCallback(Hygrometer *inst) {
  Logger::Report("Sht25 timed out.", Logger::kWarning);
  inst->timeout_timer_.SetTimeout(no_data_timeout_);
}
