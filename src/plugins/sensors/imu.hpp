// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_PLUGINS_SENSORS_IMU_HPP_
#define SRC_PLUGINS_SENSORS_IMU_HPP_

#include "core/plugin_manager/plugin.hpp"
#include "core/software_timer.hpp"
#include "drivers/icm20948/device.hpp"
#include "drivers/ak09916.hpp"


/*!
 *  \brief Implements a plug-in containing and handling an IMU object.
 */
class Imu : public Plugin {
 public:
  /*!
   * \brief Constructor of the IMU component.
   *
   * \param puli_namespace  The pulicast namespace the IMU operates in.
   */
  explicit Imu(pulicast::Namespace puli_namespace);

  /*!
   * \brief Destructor of the IMU component.
   */
  ~Imu() override;

 private:
  //! Timeout to report that the IMU is not working after the last measurement in milliseconds.
  static constexpr const uint64_t no_data_timeout_ = 1000;

  //! Callback called when IMU measurement data is available.
  static void Icm20948MeasurementCallback(icm20948::Device::MeasurementData measurement_data,
                                          void *ctx);
  //! Publishes a warning. This method is called by the timeout_timer_ when a timeout occurred.
  static void Icm20948NoDataTimeoutCallback(Imu *inst);
  //! Callback called when IMU measurement data is available.
  static void Ak09916MeasurementCallback(Ak09916::MeasurementData measurement_data, void *ctx);
  //! Publishes a warning. This method is called by the timeout_timer_ when a timeout occurred.
  static void Ak09916NoDataTimeoutCallback(Imu *inst);

  icm20948::Device icm20948_;  //!< Icm20948 driver instance which handles the IMU communication.
  SoftwareTimer icm20948_timeout_timer_;  //!< Timer to detect that the icm20948 is not working.
  Ak09916 ak09916_;  //!< Ak09916 driver instance which handles the magnetometer.
  SoftwareTimer ak09916_timeout_timer_;  //!< Timer to detect that the Ak09916 is not working.
  //! Output channel for acceleration measurement values.
  pulicast::Channel& acceleration_channel_;
  //! Output channel for angular velocity measurement values.
  pulicast::Channel& gyro_channel_;
  //! Output channel for temperature measurement values.
  pulicast::Channel& temperature_channel_;
  //! Output channel for magnetic flux density measurement values.
  pulicast::Channel& magnetic_flux_density_channel_;
};

#endif  // SRC_PLUGINS_SENSORS_IMU_HPP_
