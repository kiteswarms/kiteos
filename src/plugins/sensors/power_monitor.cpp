// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "power_monitor.hpp"
#include <string>
#include "core/scheduler.hpp"
#include "core/board_signals.hpp"
#include "core/logger.hpp"

#define LOW_BATTERY_THRESHOLD 3.4

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/

PowerMonitor::PowerMonitor(pulicast::Namespace puli_namespace):
state_node_("OFF", static_cast<std::string>(puli_namespace / "PowerMonitor"),
            [this](std::string_view command) {
              CommandCallback(command);
            }),
battery_(SendBatteryVoltages, this),
ina_(SendInaVoltages, this),
mtron_state_(false),
button_state_(false),
battery_voltages_ochan_(puli_namespace["CellVoltage"]),
ina_voltages_ochan_(puli_namespace["Voltage"]) {
  gpio_update_timer_.SetTimeoutPeriodic(100, SendGpioSensing, this);
}

/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
void PowerMonitor::SendBatteryVoltages(double values[8], void *instance) {
    auto inst = reinterpret_cast<PowerMonitor*>(instance);
    kitecom::timestamped_vector_double msg;
    msg.ts = SystemTime::GetTimestamp();
    msg.values.assign(values, values + BATTERY_CELLS);
    msg.len = BATTERY_CELLS;
    inst->battery_voltages_ochan_ << msg;

    for (int i = 0; i < BATTERY_CELLS; i++) {
      if (values[i] < LOW_BATTERY_THRESHOLD) {
        inst->buzzer_.Enable();
        return;
      }
    }

    // If we're still here, all cells are above threshold -> disable sound
    inst->buzzer_.Disable();
}

void PowerMonitor::SendInaVoltages(double values[3], void *instance) {
    auto inst = reinterpret_cast<PowerMonitor*>(instance);
    kitecom::timestamped_vector_double msg;
    msg.ts = SystemTime::GetTimestamp();
    msg.values.assign(values, values + 3);
    msg.len = 3;
    inst->ina_voltages_ochan_ << msg;
}

void PowerMonitor::SendGpioSensing(PowerMonitor *inst) {
    bool new_mtron_state = inst->motor_power_sense_.GetMtronSenseSignal();
    bool new_button_state = inst->motor_power_sense_.GetButtonSenseSignal();

    if (new_mtron_state && !inst->mtron_state_) {
      inst->state_node_.SetState("POWERED");
      Logger::Report("MTRON signal enabled", Logger::kInfo);
    } else if (!new_mtron_state && inst->mtron_state_) {
      inst->state_node_.SetState("OFF");
      Logger::Report("MTRON signal disabled", Logger::kInfo);
    }

    if (new_button_state && !inst->button_state_) {
      Logger::Report("BUTTON signal enabled", Logger::kInfo);
    } else if (!new_button_state && inst->button_state_) {
      Logger::Report("BUTTON signal disabled", Logger::kInfo);
    }

    inst->mtron_state_ = new_mtron_state;
    inst->button_state_ = new_button_state;
}
