// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_PLUGINS_SENSORS_BAROMETER_HPP_
#define SRC_PLUGINS_SENSORS_BAROMETER_HPP_

#include "core/plugin_manager/plugin.hpp"
#include "core/software_timer.hpp"
#include "drivers/ms5611.hpp"

/*!
 *  \brief Implements a plug-in containing and handling an barometer object.
 */
class Barometer : public Plugin {
 public:
  /*!
   * \brief Constructor of the barometer component.
   *
   * \param puli_namespace  The pulicast namespace the barometer operates in.
   */
  explicit Barometer(pulicast::Namespace puli_namespace, bool secondary_sensor);

  /*!
   * \brief Destructor of the barometer component.
   */
  ~Barometer() override;

 private:
  //! Timeout to report that the barometer is not working after the last measurement in ms.
  static constexpr const uint64_t no_data_timeout_ = 1000;

  //! Callback called when barometer measurement data is available.
  static void MeasurementCallback(double pressure, double temperature, void *ctx);
  //! Publishes a warning. This method is called by the timeout_timer_ when a timeout occurred.
  static void NoDataTimeoutCallback(Barometer *inst);

  Ms5611 ms5611_;  //!< Mpl3115a2 driver instance which handles the barometer communication.
  SoftwareTimer timeout_timer_;  //!< Timer to detect that the Mpl3115a2 is not working.
  pulicast::Channel& pressure_channel_;  //!< Output channel for pressure measurement values.
  pulicast::Channel& temperature_channel_;  //!< Output channel for temperature measurement values.
};

#endif  // SRC_PLUGINS_SENSORS_BAROMETER_HPP_
