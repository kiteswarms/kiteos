// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_PLUGINS_SENSORS_GNSS_HPP_
#define SRC_PLUGINS_SENSORS_GNSS_HPP_

//*****************************************************************************/
//
//! \addtogroup plugins
//! @{
//!
//! \addtogroup sensors
//! @{
///
//*****************************************************************************/

#include "core/plugin_manager/plugin.hpp"
#include "drivers/neo_m8t/multi_access_handler.hpp"
#include "drivers/neo_m8t/ubx-utils/messages.hpp"

/*!
 * \brief  Implements the Gnss plug-in.
 *
 *  \author    Elias Rosch
 *  \date      2021
 *
 */
class Gnss : public Plugin {
 public:
  /*!
   * \brief Constructor of the GNSS component.
   *
   * \param puli_namespace  The pulicast namespace the GNSS plugin operates in.
   */
  explicit Gnss(pulicast::Namespace puli_namespace);

  /*!
   * \brief Destructor of the GNSS Component.
   */
  ~Gnss() override;

 private:
  //! Callback for TIM-TP messages.
  static void TimTpCallback(ubx::TimTp* tim_tp, Gnss* ctx);
  //! Callback for NAV_PVT messages.
  static void NavPvtCallback(ubx::NavPvt* nav_pvt, Gnss* ctx);
  //! Callback for RXM_RAWX messages.
  static void RxmRawxCallback(uint8_t* data, uint16_t size, Gnss* ctx);
  //! Callback for RXM_SFRBX messages.
  static void RxmSfrbxCallback(uint8_t* data, uint16_t size, Gnss* ctx);

  neo_m8t::MultiAccessHandler neo_m8t_;  //!< driver instance for Neo-M8T device.
  pulicast::Channel& fixtype_ochan_;     //!< Output channel for fix type.
  pulicast::Channel& gpstime_ochan_;     //!< Output channel for gpstime.
  pulicast::Channel& numsat_ochan_;      //!< Output channel for number of satellites.
  pulicast::Channel& pos_ochan_;         //!< Output channel for position.
  pulicast::Channel& altmsl_ochan_;      //!< Output channel for altitude (mean sea level).
  pulicast::Channel& altelip_ochan_;     //!< Output channel for altitude (elliptic WGS84).
  pulicast::Channel& vel_ochan_;         //!< Output channel for velocity.
  pulicast::Channel& pvacc_ochan_;       //!< Output channel for position/velocity accuracy.
  pulicast::Channel& rawx_ochan_;        //!< Output channel for raw measurement data.
  pulicast::Channel& sfrbx_ochan_;       //!< Output channel for raw navigation frame data.
  //! Setting which enables and disables raw measurements.
  pulicast::experimental::distributed_settings::DistributedSetting<bool> raw_enabled_;
};
/*! @} */
/*! @} */
#endif  // SRC_PLUGINS_SENSORS_GNSS_HPP_
