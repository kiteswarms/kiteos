// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "stepper_motor.hpp"
#include <cmath>
#include "core/system_time/system_time.hpp"

#define STEPPER_MOTOR_ENCODER_PORT hal::i2c::kPort1
#define STEPPER_MOTOR_ENCODER_ADDRESS 0x40

StepperMotor::StepperMotor(pulicast::Namespace puli_namespace):
set_speed_channel_(puli_namespace["SetSpeed"]),
set_acceleration_channel_(puli_namespace["SetAcceleration"]),
speed_channel_(puli_namespace["Speed"]),
acceleration_channel_(puli_namespace["Acceleration"]),
position_channel_(puli_namespace["Position"]),
tmc_2160_(200, StepperMotorMeasurementDataCallback, this),
encoder_(STEPPER_MOTOR_ENCODER_PORT, STEPPER_MOTOR_ENCODER_ADDRESS,
         EncoderMeasurementDataCallback, this) {
  set_speed_channel_.Subscribe<kitecom::timestamped_vector_double>(
      [this](const kitecom::timestamped_vector_double& msg) {
        tmc_2160_.SetSpeed(msg.values[0]);
      });
  set_acceleration_channel_.Subscribe<kitecom::timestamped_vector_double>(
      [this](const kitecom::timestamped_vector_double& msg) {
        tmc_2160_.SetAcceleration(msg.values[0]);
      });
}

void StepperMotor::StepperMotorMeasurementDataCallback(float current_speed,
                                                       float current_acceleration, void *instance) {
  auto inst = reinterpret_cast<StepperMotor*>(instance);
  uint64_t timestamp = SystemTime::GetTimestamp();
  Publish(inst->speed_channel_, timestamp, current_speed);
  Publish(inst->acceleration_channel_, timestamp, current_acceleration);
}

void StepperMotor::EncoderMeasurementDataCallback(float angle, uint64_t timestamp, void *instance) {
  auto inst = reinterpret_cast<StepperMotor*>(instance);

  if (inst->last_position_ > M_PI * 1.5 && angle < M_PI / 2.0) {
    inst->revolutions_++;
  } else if (inst->last_position_ < M_PI / 2.0 && angle > M_PI * 1.5) {
    inst->revolutions_--;
  }
  inst->last_position_ = angle;

  Publish(inst->position_channel_, timestamp, inst->revolutions_ * 2.0 * M_PI + angle);
}
