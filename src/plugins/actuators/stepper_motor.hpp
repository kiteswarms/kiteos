// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_PLUGINS_ACTUATORS_STEPPER_MOTOR_HPP_
#define SRC_PLUGINS_ACTUATORS_STEPPER_MOTOR_HPP_

#include "core/plugin_manager/plugin.hpp"
#include "drivers/tmc2160.hpp"
#include "drivers/as5048.hpp"

//! Implements a plug-in containing and handling a stepper motor object.
class StepperMotor : public Plugin {
 public:
  /*!
   * \brief Constructor of the stepper motor component.
   *
   * \param puli_namespace  The pulicast namespace the stepper motor operates in.
   */
  explicit StepperMotor(pulicast::Namespace puli_namespace);

  /*!
   * \brief  Destructor of the stepper motor component.
   */
  ~StepperMotor() override = default;

 private:
  //! Callback called by the TMC2160 driver when speed and acceleration data is available.
  static void StepperMotorMeasurementDataCallback(float current_speed, float current_acceleration,
                                                  void *instance);

  //! Callback called when encoder measurement data is available.
  static void EncoderMeasurementDataCallback(float angle, uint64_t timestamp, void *instance);

  pulicast::Channel& set_speed_channel_;         //!< Input channel for motor speed setpoint.
  pulicast::Channel& set_acceleration_channel_;  //!< Input channel for motor speed setpoint.
  pulicast::Channel& speed_channel_;             //!< Output channel for motor speed.
  pulicast::Channel& acceleration_channel_;      //!< Output channel for motor speed.
  pulicast::Channel& position_channel_;          //!< Output channel for motor position.

  //! TMC2160 driver instance, handling the communication with the stepper motor hardware.
  Tmc2160 tmc_2160_;

  As5048 encoder_;               //!< Instance of the angle sensor used as encoder.
  int64_t revolutions_ = 0;      //!< Number of full revolutions executed by the stepper motor.
  float last_position_ = 180.0;  //!< Last encoder position measured.
};

#endif  // SRC_PLUGINS_ACTUATORS_STEPPER_MOTOR_HPP_
