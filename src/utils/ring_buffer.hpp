// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_UTILS_RING_BUFFER_HPP_
#define SRC_UTILS_RING_BUFFER_HPP_

#include <cstdint>


/*!
 * \brief      Implements the RingBuffer class.
 *
 * \details    This class implements a ring buffer with variable size and a variable buffer element
 *             type. Buffer size and element type are implemented as template parameters.
 *
 * \author     Jan Lehmann
 *
 * \date       2020
 *
 */
template <class T, uint32_t BUFFER_SIZE>
class RingBuffer {
 public:
  /*!
   * \brief constructor of the RingBuffer class.
   */
  RingBuffer();

  /*!
   * \brief destructor of the RingBuffer class.
   */
  ~RingBuffer() = default;

  /*!
   * \brief  Checks if the ring buffer is full.
   *
   * \return True if the ring buffer is full, false otherwise.
   */
  bool IsFull();

  /*!
   * \brief  Checks if the ring buffer is empty.
   *
   * \return True if the ring buffer is empty, false otherwise.
   */
  bool IsEmpty();

  /*!
   * \brief  Gets the number of elements available in the ring buffer.
   *
   * \return Number of elements available in the ring buffer.
   */
  uint32_t ElementsAvailable();

  /*!
   * \brief  Writes data to the ring buffer.
   *
   * \param  data          Data array to write to the ring buffer.
   * \param  write_length  Number of elements in the data array.
   *
   * \return Number of elements written to the ring buffer.
   */
  uint32_t Write(T *data, uint32_t write_length);

  /*!
   * \brief  Writes a single element to the ring buffer.
   *
   * \param  data  Data to write to the ring buffer.
   *
   * \return true if element was written successfully, false otherwise.
   */
  bool WriteElement(T element);

  /*!
   * \brief  Reads data from the ring buffer.
   *
   * \param  data         Data array to store the elements read from the ring buffer
   * \param  read_length  Length of the data array.
   *
   * \return Number of elements read from the ring buffer.
   */
  uint32_t Read(T *data, uint32_t read_length);

  /*!
   * \brief  Reads a single element from the ring buffer.
   *
   * \return Element that was read from the ring buffer.
   */
  bool ReadElement(T* element);

  /*!
   * \brief  Resets write and read indices of the ring buffer.
   */
  void Clear();

 private:
  T buffer_[BUFFER_SIZE + 1];  //!< Array holding the data stored in the ring buffer.
  uint32_t write_index_;  //!< Index in the buffer where the next element is written.
  uint32_t read_index_;  //!< Index in the buffer where the next element is read.

  //! Increments the read index. If the read index reaches the end of the buffer, it is reset to
  //! zero.
  void IncrementReadIndex();

  //! Increments the write index. If the write index reaches the end of the buffer, it is reset to
  //! zero.
  void IncrementWriteIndex();
};


/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/

template<class T, uint32_t BUFFER_SIZE>
RingBuffer<T, BUFFER_SIZE>::RingBuffer() {
  Clear();
}

template<class T, uint32_t BUFFER_SIZE>
bool RingBuffer<T, BUFFER_SIZE>::IsFull() {
  return (write_index_ + 1 == read_index_) || ((write_index_ == BUFFER_SIZE) && (read_index_ == 0));
}

template<class T, uint32_t BUFFER_SIZE>
bool RingBuffer<T, BUFFER_SIZE>::IsEmpty() {
  return read_index_ == write_index_;
}

template<class T, uint32_t BUFFER_SIZE>
uint32_t RingBuffer<T, BUFFER_SIZE>::ElementsAvailable() {
  int element_cnt = write_index_ - read_index_;
  if (element_cnt < 0) element_cnt += BUFFER_SIZE + 1;
  return element_cnt;
}

template<class T, uint32_t BUFFER_SIZE>
uint32_t RingBuffer<T, BUFFER_SIZE>::Write(T *data, uint32_t write_length) {
  for (uint32_t i = 0; i < write_length; i++) {
    if (IsFull()) {
      return  i;
    } else {
      buffer_[write_index_] = data[i];
      IncrementWriteIndex();
    }
  }
  return write_length;
}

template<class T, uint32_t BUFFER_SIZE>
bool RingBuffer<T, BUFFER_SIZE>::WriteElement(T element) {
  return (Write(&element, 1) != 0);
}

template<class T, uint32_t BUFFER_SIZE>
uint32_t RingBuffer<T, BUFFER_SIZE>::Read(T *data, uint32_t read_length) {
  for (uint32_t i = 0; i < read_length; i++) {
    if (IsEmpty()) {
      return i;
    } else {
      data[i] = buffer_[read_index_];
      IncrementReadIndex();
    }
  }
  return read_length;
}

template<class T, uint32_t BUFFER_SIZE>
bool RingBuffer<T, BUFFER_SIZE>::ReadElement(T* element) {
  return Read(element, 1);
}

template<class T, uint32_t BUFFER_SIZE>
void RingBuffer<T, BUFFER_SIZE>::Clear() {
  write_index_ = 0;
  read_index_ = 0;
}
/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/

template<class T, uint32_t BUFFER_SIZE>
void RingBuffer<T, BUFFER_SIZE>::IncrementReadIndex() {
  if (read_index_ + 1 > BUFFER_SIZE)
    read_index_ = 0;
  else
    read_index_++;
}

template<class T, uint32_t BUFFER_SIZE>
void RingBuffer<T, BUFFER_SIZE>::IncrementWriteIndex() {
  if (write_index_ + 1 > BUFFER_SIZE)
    write_index_ = 0;
  else
    write_index_++;
}

#endif  // SRC_UTILS_RING_BUFFER_HPP_
