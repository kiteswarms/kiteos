// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_UTILS_THREAD_SAFE_BUFFER_HPP_
#define SRC_UTILS_THREAD_SAFE_BUFFER_HPP_

#include <cstdint>
#include "ring_buffer.hpp"
#include "software_semaphore.hpp"


// Defines the maximum number of parallel processes that can possibly occur. When choosing this
// value as parameter for PROCESS_CNT, the buffer is 100% thread safe. This value is calculated by
// incrementing the maximum number of nested interrupts by 1 for the main loop. The maximum number
// of nested interrupts is the number of different group priorities used in the program. Since
// priority grouping is set to 7 in hal::System::Init, there is only 1 priority group available, so
// the maximum number of parallel processes is 2.
#define THREAD_SAFE_BUFFER_MAX_PROCESS_CNT 2


/*!
 * \brief     Implements the thread safe buffer class.
 *
 * \details   This class implements a thread safe buffer with variable size and a variable buffer
 *            element type. Buffer size and element type are implemented as template parameters.
 *            This class is implemented thread safe, which means the buffer can be accessed from
 *            interrupts and the mainloop. The third template parameter PROCESS_CNT specifies the
 *            maximum number of processes that can access the buffer at once. The buffer implemented
 *            in this class is in general a FIFO buffer, however it is not guaranteed that the FIFO
 *            order is always followed.
 *
 * \attention This class should only be used if thread safety is really needed, since it adds
 *            overhead compared to the ring buffer class.
 *            BUFFER_SIZE should be a multiple of PROCESS_CNT.
 */
template <class T, uint32_t BUFFER_SIZE, uint32_t PROCESS_CNT>
class ThreadSafeBuffer {
 public:
  /*!
   * \brief constructor of the RingBuffer class.
   */
  ThreadSafeBuffer() = default;

  /*!
   * \brief destructor of the RingBuffer class.
   */
  ~ThreadSafeBuffer() = default;

  /*!
   * \brief  Writes a single element to the ring buffer.
   *
   * \param  element  Data element to write to the ring buffer.
   *
   * \return Returns true when write was successful, false otherwise.
   */
  bool WriteElement(T element);

  /*!
   * \brief  Reads a single element from the ring buffer.
   *
   * \param  element  Variable to write the read data element to.
   *
   * \return Returns true when read was successful, false otherwise.
   */
  bool ReadElement(T *element);

 private:
  //!< Represents a ring buffer that is protected by a semaphore and can therefore be accessed from
  //!< multiple threads.
  struct SharedBuffer {
    //! Ring buffer containing the actual data stored in the shared buffer.
    RingBuffer<T, (BUFFER_SIZE / PROCESS_CNT)> ring_buffer;
    //! Semaphore indicating if the ring buffer is currently accessed.
    SoftwareSemaphore semaphore;
  };

  //! The internal buffer is split in multiple shared buffers which are used sequentially. Each of
  //! the shared buffer can only be accessed by a process at once. If a shared buffer is already in
  //! use, the process tries to access the next shared buffer. The number of shared buffers defines
  //! the number of threads that can simultaneously access the thread safe buffer.
  SharedBuffer shared_buffer_[PROCESS_CNT];
  //! Cursor defining the index of the shared buffer used for the next write operation. The actual
  //! index can be calculated by (write_cursor_ % PROCESS_CNT). This behavior is chosen to ensure
  //! that the cursor is still valid even if two processes try to increment it at once.
  uint32_t write_cursor_ = 0;
  //! Cursor defining the index of the shared buffer used for the next read operation. The actual
  //! index can be calculated by (read_cursor_ % PROCESS_CNT). This behavior is chosen to ensure
  //! that the cursor is still valid even if two processes try to increment it at once.
  uint32_t read_cursor_ = 0;
};


/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/

template<class T, uint32_t BUFFER_SIZE, uint32_t PROCESS_CNT>
bool ThreadSafeBuffer<T, BUFFER_SIZE, PROCESS_CNT>::WriteElement(T element) {
  // Get copy of the write cursor and increment it afterwards. This is done to ensure the cursor is
  // not manipulated by another process while it is used in this process for iterating over the
  // shared buffer array
  uint32_t cursor_tmp = write_cursor_++;

  // Iterate over the shared buffer array to find a shared buffer that is currently not accessed.
  for (uint32_t i = 0; i < PROCESS_CNT; i++) {
    SharedBuffer *buffer = &shared_buffer_[(cursor_tmp + i)%PROCESS_CNT];

    // Try to lock the shared buffer semaphore to reserve the shared buffer for this process.
    if (buffer->semaphore.Lock()) {
      // Try to write element to the shared buffer
      bool write_succeded = buffer->ring_buffer.WriteElement(element);
      buffer->semaphore.Clear();
      if (write_succeded) {
        return true;
      }
    }
  }

  return false;
}

template<class T, uint32_t BUFFER_SIZE, uint32_t PROCESS_CNT>
bool ThreadSafeBuffer<T, BUFFER_SIZE, PROCESS_CNT>::ReadElement(T *element) {
  // Get copy of the read cursor and increment it afterwards. This is done to ensure the cursor is
  // not manipulated by another process while it is used in this process for iterating over the
  // shared buffer array
  uint32_t cursor_tmp = read_cursor_++;

  // Iterate over the shared buffer array to find a shared buffer that is currently not accessed.
  for (uint32_t i = 0; i < PROCESS_CNT; i++) {
    SharedBuffer *buffer = &shared_buffer_[(cursor_tmp + i)%PROCESS_CNT];

    // Try to lock the shared buffer semaphore to reserve the shared buffer for this process.
    if (buffer->semaphore.Lock()) {
      // Try to read element from the shared buffer
      bool read_succeded = buffer->ring_buffer.Read(element, 1);
      buffer->semaphore.Clear();
      if (read_succeded) {
        return true;
      }
    }
  }

  return false;
}


#endif  // SRC_UTILS_THREAD_SAFE_BUFFER_HPP_
