// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_UTILS_SOFTWARE_SEMAPHORE_HPP_
#define SRC_UTILS_SOFTWARE_SEMAPHORE_HPP_


/*!
 * \brief   Implements the software semaphore class.
 *
 * \details This class implements a semaphore. Atomic operations are implemented by disabling
 *          interrupts for the time of operation.
 */
class SoftwareSemaphore {
 public:
  /*!
   * \brief Constructor of the software semaphore class.
   */
  SoftwareSemaphore() = default;

  /*!
   * \brief Destructor of the software semaphore class.
   */
  ~SoftwareSemaphore() = default;

  /*!
   * \brief  Checks if the semaphore is currently locked.
   *
   * \return True if the semaphore is currently locked, false otherwise.
   */
  bool IsLocked();

  /*!
   * \brief  Tries to lock the semaphore.
   *
   * \return True if the semaphore was successfully locked,
   *         false if the semaphore was already locked by another process.
   */
  bool Lock();

  /*!
   * \brief  Clears the semaphore.
   */
  void Clear();

 private:
  bool locked_ = false;  //!< Internal semaphore state.
};

#endif  // SRC_UTILS_SOFTWARE_SEMAPHORE_HPP_
