#include "core/plugin_manager/plugin.hpp"
#include "mock/core/persistent_memory/fram_access_mock.hpp"
#include "core/name_provider.hpp"
#include "gtest/gtest.h"


class PluginImpl : public Plugin {
 public:
  PluginImpl() {}

  void ExposePublish(pulicast::Channel& ochan,
                     uint64_t timestamp, std::vector<int32_t> values) {
    Publish(ochan, timestamp, values);
  }
};

class CorePluginTest : public testing::Test {
 protected:
  void SetUp() override {
    std::string board_name = "BOARD";
    EXPECT_CALL(fram_access_mock_, GetValue(testing::_, testing::_))
        .Times(5)  // pcb_type, pcv_version, pcb_id, kite_name, board_name
        .WillRepeatedly(testing::Return(false));
    NameProvider::Init();
    plugin = new PluginImpl();
  }

  void TearDown() override {
    delete(plugin);
  }
  PluginImpl* plugin;
  FramAccessMock fram_access_mock_;
};
