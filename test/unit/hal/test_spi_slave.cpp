#include "hal/spi/slave.hpp"
#include "gtest/gtest.h"

class HalSpiSlaveTest: public testing::Test {
 protected:
  void TearDown() override {
    // clean stuff up here
    hal::spi::Device::FreeRegistry();
  }
};

void SpiSlaveCallback(void* cb_arg) {
}

TEST_F(HalSpiSlaveTest, Transceive) {
  uint8_t tx_buffer[3];
  uint8_t rx_buffer[3];
  void* ptr1 = reinterpret_cast<void*>(0x11111111);

  hal::spi::Slave test_slave(hal::spi::kPort1,
                             {hal::spi::kPhaseFirstEdge,
                              hal::spi::kPolarityHigh,
                              hal::spi::kBaud1MHz,
                              hal::Gpio::kPortA, hal::Gpio::kPin11},
                             tx_buffer, rx_buffer,
                             SpiSlaveCallback, ptr1);

  test_slave.Transceive(5);
}
