#include "hal/register.hpp"
#include "gtest/gtest.h"

#define EXPECT_BITS_EQ(expected, actual, mask) EXPECT_EQ(expected & mask, actual & mask)

TEST(HalRegisterTest, Config) {
  volatile uint32_t test_register;

  test_register = 0;
  hal::Register::Config register_config = {&test_register, 0xF << 4, 0xA << 4};

  EXPECT_FALSE(register_config.Verify());
  register_config.Write();
  EXPECT_BITS_EQ(0xA0, test_register, 0xFFFFFFFF) << "Config write failed";

  test_register = 0xA00;
  register_config = {&test_register, 0xF << 8, 0xA << 8};
  EXPECT_TRUE(register_config.Verify());
}

TEST(HalRegisterTest, BitfieldWrite) {
  volatile uint32_t test_register;

  test_register = 0xFFFFFFFF;
  hal::Register::BitfieldWrite(&test_register, 0x2, 3, 2);
  EXPECT_BITS_EQ(0xFFFFFFBF, test_register, 0xFFFFFFFF) << "width 2 failed";

  test_register = 0xFFFFFFFF;
  hal::Register::BitfieldWrite(&test_register, 0xA, 3, 4);
  EXPECT_BITS_EQ(0xFFFFAFFF, test_register, 0xFFFFFFFF) << "width 4 failed";

  test_register = 0xFFFFFFFF;
  hal::Register::BitfieldWrite(&test_register, 7, 1, 8);
  EXPECT_BITS_EQ(0xFFFF07FF, test_register, 0xFFFFFFFF) << "width 8 failed";

  test_register = 0xFFFFFFFF;
  hal::Register::BitfieldWrite(&test_register, 6, 0, 1);
  EXPECT_BITS_EQ(0xFFFFFFFE, test_register, 0xFFFFFFFF) << "width 1 failed";

  test_register = 0xFFFFFFFF;
  hal::Register::BitfieldWrite(&test_register, 0x12345678, 0, 32);
  EXPECT_BITS_EQ(0x12345678, test_register, 0xFFFFFFFF) << "width 32 failed";
}
