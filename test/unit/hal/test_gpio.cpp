#include "hal/gpio.hpp"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

#define EXPECT_BITS_EQ(expected, actual, mask) EXPECT_EQ(expected & mask, actual & mask)

testing::MockFunction<void(void*)>* mock_GpioCallback;

void GpioCallback(void* cb_arg) {
  mock_GpioCallback->AsStdFunction()(cb_arg);
}

TEST(HalGpioTest, ModeInput) {
  hal::Gpio test_gpio(hal::Gpio::kPortA, 0xFFFF, hal::Gpio::kModeInput);
  EXPECT_BITS_EQ(0x00000000, GPIOA->MODER, 0xFFFFFFFF) << "GPIOA mode input";
  EXPECT_BITS_EQ(0x00000000, GPIOA->PUPDR, 0xFFFFFFFF) << "no pull resistors";

  GPIOA->IDR = 0xABCD;
  EXPECT_BITS_EQ(0xABCD, test_gpio.Read(), 0xFFFFFFFF) << "read GPIO register";
}

TEST(HalGpioTest, ModeOutput) {
  hal::Gpio test_gpio(hal::Gpio::kPortA, 0xFFFF, hal::Gpio::kModeOutput);
  EXPECT_BITS_EQ(0x55555555, GPIOA->MODER, 0xFFFFFFFF) << "GPIOA mode output";
  EXPECT_BITS_EQ(0x00000000, GPIOA->OTYPER, 0xFFFFFFFF) << "GPIOA output type PP";
  EXPECT_BITS_EQ(0x00000000, GPIOA->PUPDR, 0xFFFFFFFF) << "no pull resistors";

  test_gpio.Write((uint16_t)0x0000);
  EXPECT_BITS_EQ(0xFFFF0000, GPIOA->BSRR, 0xFFFFFFFF) << "reset pin values";
  test_gpio.Write((uint16_t)0xAAAA);
  EXPECT_BITS_EQ(0x5555AAAA, GPIOA->BSRR, 0xFFFFFFFF) << "set pin values";
}

TEST(HalGpioTest, ModeInterrupt) {
  hal::Gpio test_gpio(hal::Gpio::kPortE, 0xFFFF, hal::Gpio::kModeInterruptRising, GpioCallback);

  EXPECT_BITS_EQ(0x00000000, GPIOE->PUPDR, 0xFFFFFFFF) << "no pull resistors";
  EXPECT_BITS_EQ(0x00004444, SYSCFG->EXTICR[0], 0xFFFFFFFF) << "configure EXTI";
  EXPECT_BITS_EQ(0x00004444, SYSCFG->EXTICR[1], 0xFFFFFFFF) << "configure EXTI";
  EXPECT_BITS_EQ(0x00004444, SYSCFG->EXTICR[2], 0xFFFFFFFF) << "configure EXTI";
  EXPECT_BITS_EQ(0x00004444, SYSCFG->EXTICR[3], 0xFFFFFFFF) << "configure EXTI";
}

TEST(HalGpioTest, ModeAnalog) {
  hal::Gpio test_gpio(hal::Gpio::kPortA, 0xFFFF, hal::Gpio::kModeAnalog);
  EXPECT_BITS_EQ(0x00000000, GPIOA->PUPDR, 0xFFFFFFFF) << "no pull resistors";
  EXPECT_BITS_EQ(0xFFFFFFFF, GPIOA->MODER, 0xFFFFFFFF) << "GPIOA mode output";
}

TEST(HalGpioTest, ModeAF4I2c) {
  hal::Gpio test_gpio(hal::Gpio::kPortA, 0xFFFF, hal::Gpio::kModeAF4I2c);
  EXPECT_BITS_EQ(0xAAAAAAAA, GPIOA->MODER, 0xFFFFFFFF) << "GPIOA mode AF";
  EXPECT_BITS_EQ(0x0000FFFF, GPIOA->OTYPER, 0xFFFFFFFF) << "GPIOA output type OD";
  EXPECT_BITS_EQ(0x55555555, GPIOA->PUPDR, 0xFFFFFFFF) << "pull up resistors";
  EXPECT_BITS_EQ(0x44444444, GPIOA->AFR[0], 0xFFFFFFFF) << "alternate function AF4";
  EXPECT_BITS_EQ(0x44444444, GPIOA->AFR[1], 0xFFFFFFFF) << "alternate function AF4";
}

TEST(HalGpioTest, ModeAF5Spi) {
  hal::Gpio test_gpio(hal::Gpio::kPortA, 0xFFFF, hal::Gpio::kModeAF5Spi);

  EXPECT_BITS_EQ(0xAAAAAAAA, GPIOA->MODER, 0xFFFFFFFF) << "GPIOA mode AF";
  EXPECT_BITS_EQ(0x00000000, GPIOA->OTYPER, 0xFFFFFFFF) << "GPIOA output type PP";
  EXPECT_BITS_EQ(0x55555555, GPIOA->PUPDR, 0xFFFFFFFF) << "configure pull up resistors";
  EXPECT_BITS_EQ(0x55555555, GPIOA->AFR[0], 0xFFFFFFFF) << "alternate function AF5";
  EXPECT_BITS_EQ(0x55555555, GPIOA->AFR[1], 0xFFFFFFFF) << "alternate function AF5";
}

TEST(HalGpioTest, test_hal_gpio_Callback) {
  mock_GpioCallback = new testing::MockFunction<void(void*)>;

  void* ptr1 = reinterpret_cast<void*>(0x11111111);
  hal::Gpio test_gpio1(hal::Gpio::kPortA, 0x00000001, hal::Gpio::kModeInterruptRising,
                       GpioCallback, ptr1);

  void* ptr2 = reinterpret_cast<void*>(0x22222222);
  hal::Gpio test_gpio2(hal::Gpio::kPortA, 0x00000002, hal::Gpio::kModeInterruptRising,
                       GpioCallback, ptr2);

  EXPECT_CALL(*mock_GpioCallback, Call(ptr1));
  EXTI0_IRQHandler();

  EXPECT_CALL(*mock_GpioCallback, Call(ptr2));
  EXTI1_IRQHandler();

  EXPECT_CALL(*mock_GpioCallback, Call(ptr1));
  EXTI0_IRQHandler();

  delete mock_GpioCallback;
}
