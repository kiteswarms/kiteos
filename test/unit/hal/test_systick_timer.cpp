#include <stm32h7xx.h>
#include "hal/systick_timer.hpp"
#include "gtest/gtest.h"

uint32_t SystemCoreClock = 400000000;

TEST(HalSystickTimerTest, Init) {
  hal::SystickTimer::Init();
  EXPECT_EQ(SystemCoreClock/1000 - 1, SysTick->LOAD) << "Number of ticks per millisecond";
}
