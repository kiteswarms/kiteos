#ifndef TEST_UNIT_MOCK_CORE_PERSISTENT_MEMORY_FRAM_ACCESS_MOCK_HPP_
#define TEST_UNIT_MOCK_CORE_PERSISTENT_MEMORY_FRAM_ACCESS_MOCK_HPP_

#include "core/persistent_memory/fram_access.hpp"
#include "gmock/gmock.h"

struct FramAccessMock {
  FramAccessMock();
  ~FramAccessMock();
  MOCK_METHOD(bool, GetValue, (uint32_t, uint32_t *));
};

#endif  // TEST_UNIT_MOCK_CORE_PERSISTENT_MEMORY_FRAM_ACCESS_MOCK_HPP_
