// Define macro for poisoned class.
#define FRAM_ACCESS FramAccess

#include "fram_access_mock.hpp"
#include <functional>
#include "gmock/gmock.h"
#include "utils/debug_utils.h"

static std::function<bool(uint32_t, uint32_t *)> _get_value;

FramAccessMock::FramAccessMock() {
  _get_value = [this](uint32_t key, uint32_t * value){
              return GetValue(key, value);
            };
}

FramAccessMock::~FramAccessMock() {
  _get_value = {};
}

bool FRAM_ACCESS::GetValue(uint32_t key, uint32_t * value) {
  ASSERT(_get_value);
  return _get_value(key, value);
}
