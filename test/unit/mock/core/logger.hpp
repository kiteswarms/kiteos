#ifndef TEST_UNIT_MOCK_CORE_LOGGER_HPP_
#define TEST_UNIT_MOCK_CORE_LOGGER_HPP_

#include <string>
#include "gmock/gmock.h"

class Logger {
 public:
  //! Use boost log severity values (boost/log/trivial.hpp)
  enum LogLevel{
    kTrace = 0,
    kDebug = 1,
    kInfo = 2,
    kWarning = 3,
    kError = 4,
    kFatal = 5,
  };
  /*!
  * \brief Function to report log messages. Currently the logs get published via pulicast.
  *
  * \param message    The message string reference.
  * \param level  The log level enum, see "LogLevel" above.
  */
  static void Report(const char *message, LogLevel level);
};

#endif  // TEST_UNIT_MOCK_CORE_LOGGER_HPP_
