#include <cstring>
#include "drivers/neo_m8t/ubx-utils/parser.hpp"
#include "drivers/neo_m8t/ubx-utils/dispatcher.hpp"
#include "drivers/neo_m8t/ubx-utils/composer.hpp"
#include "gtest/gtest.h"
#include "gmock/gmock.h"
/**************************************************************************************************
 *     GetChannelName                                                                             *
 **************************************************************************************************/
TEST(UbxTest, FrameCrc) {
  uint8_t buffer[] = { UBX_SYNC_CHAR_1,
                       UBX_SYNC_CHAR_2,
                       ubx::kMonVer >> 8 ,
                       ubx::kMonVer & 0xFF,
                       0x00,
                       0x00,
                       0x00,
                       0x00
                      };

  // Fill CRC fields
  uint16_t crc = ubx::Composer::FrameCrc(buffer, 0);
  uint16_t expected_crc = 0x0e34;

  EXPECT_EQ(expected_crc, crc);
}

TEST(UbxTest, ComposePollMonVerMessage) {
  uint8_t buffer[8];
  ubx::Composer::ComposePoll(buffer, ubx::ClassId::kMonVer);


  uint8_t expected[] = { UBX_SYNC_CHAR_1,
                         UBX_SYNC_CHAR_2,
                         ubx::kMonVer >> 8 ,
                         ubx::kMonVer & 0xFF,
                         0x00, 0x00,  // length
                         0x00, 0x00  // CRC
                       };

  // Fill CRC fields
  uint16_t crc = ubx::Composer::FrameCrc(expected, 0);
  expected[6] = crc >> 8;
  expected[7] = crc & 0xFF;

  EXPECT_TRUE(0 == std::memcmp(expected, buffer, 8));
}

TEST(UbxTest, ComposeCfgPrtMessage) {
  ubx::CfgPrt prt = {
    .port_id = 0x1,
    .reserved1 = 0x2,
    .tx_ready = 0x3,
    .mode = 0x4,
    .baud_rate = 0x5,
    .in_proto_mask = 0x6,
    .out_proto_mask = 0x7,
    .flags = 0x8,
    .reserved2 = 0x9,
  };
  uint8_t buffer[28];
  ubx::Composer::Compose(buffer, &prt);

  uint8_t expected_data[] = { UBX_SYNC_CHAR_1,
                         UBX_SYNC_CHAR_2,
                         ubx::kCfgPrt >> 8 ,
                         ubx::kCfgPrt & 0xFF,
                         0x14, 0x00,   // length
                         0x01,
                         0x02,
                         0x03, 0x00,
                         0x04, 0x00, 0x00, 0x00,
                         0x05, 0x00, 0x00, 0x00,
                         0x06, 0x00,
                         0x07, 0x00,
                         0x08, 0x00,
                         0x09, 0x00,
                         0x00, 0x00,  // CRC
                        };

  // Fill CRC fields
  uint16_t crc = ubx::Composer::FrameCrc(expected_data, 20);
  expected_data[26] = crc >> 8;
  expected_data[27] = crc & 0xFF;

  EXPECT_TRUE(0 == std::memcmp(expected_data, buffer, 28));
}

TEST(UbxTest, ParseMessage) {
  uint8_t payload_buffer[50];
  ubx::Parser parser = ubx::Parser(payload_buffer, 50);

  uint8_t test_data[] = { UBX_SYNC_CHAR_1,
                        UBX_SYNC_CHAR_2,
                        ubx::kAckAck >> 8 ,
                        ubx::kAckAck & 0xFF,
                        0x02,
                        0x00,
                        0xAB,
                        0xCD,
                        0x00,
                        0x00 };

  // Fill CRC fields
  uint16_t crc = ubx::Composer::FrameCrc(test_data, 2);
  test_data[8] = crc >> 8;
  test_data[9] = crc & 0xFF;

  // Check number of written bytes in ringbuffer
  uint8_t num_bytes = parser.CopyDataToRingbuf(test_data, sizeof(test_data));
  EXPECT_EQ(sizeof(test_data), num_bytes);

  uint16_t expected_len = 2;
  uint8_t expected_payload[expected_len] = {0xAB, 0xCD};
  ubx::ClassId expected_class_id = ubx::ClassId::kAckAck;

  EXPECT_EQ(expected_len, parser.ParseMessage());
  EXPECT_TRUE(0 == std::memcmp(expected_payload, payload_buffer, 2));
  EXPECT_EQ(expected_class_id, parser.GetClassId());
}


testing::MockFunction<void(uint8_t*, uint16_t, void*)>* mock_MonVerCallback;

void MonVerCallback(uint8_t* data, uint16_t size, void* ctx) {
  mock_MonVerCallback->AsStdFunction()(data, size, ctx);
}

TEST(UbxTest, DispatchMonVer) {
  mock_MonVerCallback = new testing::MockFunction<void(uint8_t*, uint16_t, void*)>;
  uint8_t buffer[] = { UBX_SYNC_CHAR_1, UBX_SYNC_CHAR_2,  // Header
                       ubx::kMonVer >> 8, ubx::kMonVer & 0xFF,  // class_id
                       0x46, 0x00,  // length

                       // swVersion
                       0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
                       0xBB, 0xBB, 0xBB, 0xBB, 0xBB, 0xBB, 0xBB, 0xBB, 0xBB, 0xBB,
                       0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC,

                       // hwVersion
                       0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD,

                      // extension
                       0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC,
                       0xBB, 0xBB, 0xBB, 0xBB, 0xBB, 0xBB, 0xBB, 0xBB, 0xBB, 0xBB,
                       0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,

                       0x00, 0x00  // CRC
                      };

  // Fill CRC fields
  uint16_t crc = ubx::Composer::FrameCrc(buffer, 70);
  buffer[76] = crc >> 8;
  buffer[77] = crc & 0xFF;

  ubx::Dispatcher dispatcher = ubx::Dispatcher();

  void* ctx = 0;
  dispatcher.Subscribe(ubx::ClassId::kMonVer, MonVerCallback, ctx);

  dispatcher.CopyDataToRingbuf(buffer, sizeof(buffer));

  EXPECT_CALL(*mock_MonVerCallback, Call(testing::_, 70, ctx));

  dispatcher.Dispatch();

  delete mock_MonVerCallback;
}

testing::MockFunction<void(ubx::RxmRawX*)>* mock_RxmRawXCallback;

void RxmRawXCallback(ubx::RxmRawX* rxm_rawx) {
  mock_RxmRawXCallback->AsStdFunction()(rxm_rawx);

  EXPECT_FLOAT_EQ(2.0, rxm_rawx->rcv_tow);
  EXPECT_EQ(0x0020, rxm_rawx->week);
  EXPECT_EQ(0x02, rxm_rawx->num_meas);

  EXPECT_FLOAT_EQ(2.0, rxm_rawx->meas[0].prMes);
  EXPECT_EQ(0x03, rxm_rawx->meas[0].gnssId);
  EXPECT_EQ(0x13, rxm_rawx->meas[1].gnssId);
}

TEST(UbxTest, DispatchRxmRawX) {
  mock_RxmRawXCallback = new testing::MockFunction<void(ubx::RxmRawX*)>;

  uint8_t buffer[] = { UBX_SYNC_CHAR_1, UBX_SYNC_CHAR_2,  // Header
                       ubx::kRxmRawX >> 8, ubx::kRxmRawX & 0xFF,  // class_id
                       0x50, 0x00,  // length

                       0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40,  // rcvTow = 2.0
                       0x20, 0x00,  // week
                       0x30,  // leapS
                       0x02,  // numMeas
                       0x40,  // recStat
                       0x50, 0x00, 0x00,  // reserved1
                       0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40,  // prMes
                       0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // cpMes
                       0x02, 0x00, 0x00, 0x00,  // doMes

                       0x03,  // gnssId
                       0x04,  // svId
                       0x05,  // reserved2
                       0x06,  // freqId
                       0x07, 0x00,  // locktime
                       0x08,  // cno
                       0x09,  // prStdev
                       0x0A,  // cpStdev
                       0x0B,  // doStdev
                       0x0C,  // trkStat
                       0x0D,  // reserved3
                       0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // prMes
                       0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // cpMes
                       0x00, 0x00, 0x00, 0x00,  // doMes

                       0x13,  // gnssId
                       0x14,  // svId
                       0x15,  // reserved2
                       0x16,  // freqId
                       0x17, 0x00,  // locktime
                       0x18,  // cno
                       0x19,  // prStdev
                       0x1A,  // cpStdev
                       0x1B,  // doStdev
                       0x1C,  // trkStat
                       0x1D,  // reserved3

                       0x00, 0x00  // CRC
                      };

  // Fill CRC fields
  uint16_t crc = ubx::Composer::FrameCrc(buffer, 80);
  buffer[86] = crc >> 8;
  buffer[87] = crc & 0xFF;

  ubx::Dispatcher dispatcher = ubx::Dispatcher();

  dispatcher.Subscribe(RxmRawXCallback);

  dispatcher.CopyDataToRingbuf(buffer, sizeof(buffer));

  EXPECT_CALL(*mock_RxmRawXCallback, Call(testing::_));

  dispatcher.Dispatch();

  delete mock_RxmRawXCallback;
}
