KiteOS plugins use pulicast channels to communicate with high level software. Sensors publish their sensor values on
specific channels while Actuators subscribe to certain channels and react on messages received on these channels.
Actuators may also publish values. The pulicast interface of the KiteOS plugins is documented in the following sections.
The data type shown in the table is only a simplification of the actual datatype which also contains a timestamp.
Messages publishing multiple values of the same type typically use timestamped vector messages. These are displayed as
simple arrays here.

Actuators
*********

Motor
-----

Subscribed Channels:

+--------------------------------+------------+------------+-----------------------------------------------------------+
| Name                           | Unit       | Data type  | Description                                               |
+================================+============+============+===========================================================+
| Setpoint                       | rad/s      | double[1]  | Speed setpoint of the motor.                              |
+--------------------------------+------------+------------+-----------------------------------------------------------+

Published Channels:

+--------------------------------+------------+------------+-----------------------------------------------------------+
| Name                           | Unit       | Data type  | Description                                               |
+================================+============+============+===========================================================+
| Speed                          | rad/s      | double     | Current speed of the motor.                               |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| Voltage                        | V          | double     | Current shunt voltage.                                    |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| Temperature                    | °C         | double     | Current A4964 chip junction temperature.                  |
+--------------------------------+------------+------------+-----------------------------------------------------------+

State Transitions:

+--------------------------------+-------------------------+-----------------------------------------------------------+
| Source State                   | Destination State       | Condition / Command                                       |
+================================+=========================+===========================================================+
| DISARMED                       | ARMED                   | ARM & current setpoint < arming limit                     |
+--------------------------------+-------------------------+-----------------------------------------------------------+
| DISARMED                       | THRUST_PROTECTION       | current setpoint >= arming limit                          |
+--------------------------------+-------------------------+-----------------------------------------------------------+
| THRUST_PROTECTION              | DISARMED                | current setpoint < arming limit                           |
+--------------------------------+-------------------------+-----------------------------------------------------------+
| ARMED                          | DISARMED                | DISARM                                                    |
+--------------------------------+-------------------------+-----------------------------------------------------------+

Stepper Motor
-------------

Subscribed Channels:

+--------------------------------+------------+------------+-----------------------------------------------------------+
| Name                           | Unit       | Data type  | Description                                               |
+================================+============+============+===========================================================+
| SetSpeed                       | m/s        | float      | Speed setpoint. If the plugin receives a speed setpoint,  |
|                                |            |            | it tries to reach the setpoint with maximum acceleration. |
|                                |            |            | The acceleration is automatically reduced, when the       |
|                                |            |            | target speed is reached and the motor keeps running at    |
|                                |            |            | the target speed until a new speed or acceleration        |
|                                |            |            | setpoint is received.                                     |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| SetAcceleration                | m/(s^2)    | float      | Acceleration setpoint. If the plugin receives an          |
|                                |            |            | acceleration setpoint, it accelerates stepper motor with  |
|                                |            |            | this acceleration. The acceleration is automatically      |
|                                |            |            | reduced to zero when the speed limits are reached and the |
|                                |            |            | motor keeps running at its maximum speed until a new      |
|                                |            |            | speed or acceleration setpoint is received.               |
+--------------------------------+------------+------------+-----------------------------------------------------------+

Published Channels:

+--------------------------------+------------+------------+-----------------------------------------------------------+
| Name                           | Unit       | Data type  | Description                                               |
+================================+============+============+===========================================================+
| Speed                          | m/s        | float      | Current speed of the stepper motor.                       |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| Acceleration                   | m/(s^2)    | float      | Current acceleration of the stepper motor.                |
+--------------------------------+------------+------------+-----------------------------------------------------------+

Sensors
*******

ADC
---

+--------------------------------+------------+------------+-----------------------------------------------------------+
| Name                           | Unit       | Data type  | Description                                               |
+================================+============+============+===========================================================+
| Voltage                        | V          | double[4]  | The first value is the motor current, measured over a     |
|                                |            |            | 2mΩ shunt. The second value is the differential voltage   |
|                                |            |            | measured on Connector K2. The third value is the battery  |
|                                |            |            | voltage. The fourth value is the differential voltage     |
|                                |            |            | measured on Connector K3.                                 |
+--------------------------------+------------+------------+-----------------------------------------------------------+

Barometer
---------

Published Channels:

+--------------------------------+------------+------------+-----------------------------------------------------------+
| Name                           | Unit       | Data type  | Description                                               |
+================================+============+============+===========================================================+
| Pressure                       | Pa         | double     | Measured pressure.                                        |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| Temperature                    | °C         | double     | Measured barometer chip temperature.                      |
+--------------------------------+------------+------------+-----------------------------------------------------------+

GNSS
____

Distributed Settings:

+--------------------------------+------------+------------+-----------------------------------------------------------+
| Name                           | Unit       | Data type  | Description                                               |
+================================+============+============+===========================================================+
| RawEnabled                     | \-         | bool       | Messages on the channels RawGnss and NavFrame are only    |
|                                |            |            | sent if true.                                             |
+--------------------------------+------------+------------+-----------------------------------------------------------+

Published Channels:

+--------------------------------+------------+------------+-----------------------------------------------------------+
| Name                           | Unit       | Data type  | Description                                               |
+================================+============+============+===========================================================+
| FixType                        | \-         | uint32     | Enum representing the GNSS fix type:                      |
|                                |            |            |                                                           |
|                                |            |            | 0: No fix                                                 |
|                                |            |            |                                                           |
|                                |            |            | 1: Dead reckoning only                                    |
|                                |            |            |                                                           |
|                                |            |            | 2: 2D-fix                                                 |
|                                |            |            |                                                           |
|                                |            |            | 3: 3D-fix                                                 |
|                                |            |            |                                                           |
|                                |            |            | 4: GNSS + dead reckoning combined                         |
|                                |            |            |                                                           |
|                                |            |            | 5: Time only fix                                          |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| GpsTime                        | s          | int32[2]   | GPS time in seconds. This is actually an uint64 which is  |
|                                |            |            | split in two int32 where the first element contains the   |
|                                |            |            | higher bytes.                                             |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| NumSatellites                  | \-         | uint32     | Number of satellites used in the nav solution.            |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| GeographicPosition             | °          | double[2]  | The first element is the latitude and the second element  |
|                                |            |            | is the longitude.                                         |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| Altitude                       | m          | float      | Height above mean sea level.                              |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| EllipsoidalAltitude            | m          | float      | Heigth above ellipsoid.                                   |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| Velocity                       | m/s        | double[3]  | Velocity vector in the NED coordinate frame.              |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| PVAccuracy                     | m, m, m/s  | double[3]  | The first element is the horizontal accuracy in meters.   |
|                                |            |            | The second element is the vertical accuracy in meters.    |
|                                |            |            | The third element is the speed accuracy in m/s.           |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| RawGnss                        | struct     | uint8[]    | Raw UBX-RXM-RAWX message of variable length. See UBX      |
|                                |            |            | protocol specification for details.                       |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| NavFrame                       | struct     | uint8[]    | Raw UBX-RXM-SFRBX message of variable length. See UBX     |
|                                |            |            | protocol specification for details.                       |
+--------------------------------+------------+------------+-----------------------------------------------------------+

Hygrometer
----------

Published Channels:

+--------------------------------+------------+------------+-----------------------------------------------------------+
| Name                           | Unit       | Data type  | Description                                               |
+================================+============+============+===========================================================+
| Temperature                    | °C         | double     | Temperature.                                              |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| RelativeHumidity               | %          | double     | Relative humidity.                                        |
+--------------------------------+------------+------------+-----------------------------------------------------------+

IMU
---

Published Channels:

+--------------------------------+------------+------------+-----------------------------------------------------------+
| Name                           | Unit       | Data type  | Description                                               |
+================================+============+============+===========================================================+
| Acceleration                   | m/(s^2)    | double[3]  | Acceleration in (x, y, z) direction.                      |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| AngularVelocity                | rad/s      | double[3]  | Angular velocity in (x, y, z) direction.                  |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| Temperature                    | °C         | double     | ICM-20948 die temperature.                                |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| MagneticFluxDensity            | T          | double[3]  | Magnetic flux density in (x, y, z) direction.             |
+--------------------------------+------------+------------+-----------------------------------------------------------+

Power Monitor
-------------

Published Channels:

+--------------------------------+------------+------------+-----------------------------------------------------------+
| Name                           | Unit       | Data type  | Description                                               |
+================================+============+============+===========================================================+
| CellVoltage                    | mV         | double[4]  | Voltages of the individual battery cells where the first  |
|                                |            |            | element is the voltage of the lowest cell next to ground. |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| Voltage                        | V          | double[3]  | The first value is the shunt voltage of a 0.5mΩ shunt     |
|                                |            |            | placed between the battery voltage and motor supply       |
|                                |            |            | voltage. It effectively measures the total motor current. |
|                                |            |            | The second value is the shunt voltage of a 10mΩ shunt,    |
|                                |            |            | which measures the current consumption of the right top   |
|                                |            |            | stack. the third value is the shunt voltage of a 10mΩ     |
|                                |            |            | shunt, which measures the current consumption of the      |
|                                |            |            | right top stack.                                          |
+--------------------------------+------------+------------+-----------------------------------------------------------+

State Transitions:

+--------------------------------+-------------------------+-----------------------------------------------------------+
| Source State                   | Destination State       | Condition / Command                                       |
+================================+=========================+===========================================================+
| OFF                            | POWERED                 | MTRON signal high                                         |
+--------------------------------+-------------------------+-----------------------------------------------------------+
| POWERED                        | OFF                     | MTRON signal low                                          |
+--------------------------------+-------------------------+-----------------------------------------------------------+

RC Receiver
-----------

Published Channels:

+--------------------------------+------------+------------+-----------------------------------------------------------+
| Name                           | Unit       | Data type  | Description                                               |
+================================+============+============+===========================================================+
| Roll                           | \-         | double     | Roll channel value as a value between -1 and 1, where 0   |
|                                |            |            | is the center position of the roll stick.                 |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| Pitch                          | \-         | double     | Pitch channel value as a value between -1 and 1, where 0  |
|                                |            |            | is the center position of the pitch stick.                |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| Yaw                            | \-         | double     | Yaw channel value as a value between -1 and 1, where 0 is |
|                                |            |            | the center position of the yaw stick.                     |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| Throttle                       | \-         | double     | Throttle channel value as a value between -1 and 1, where |
|                                |            |            | 0 is the center position of the throttle stick.           |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| Gear                           | \-         | double     | Gear channel value as a value between -1 and 1.           |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| Aux1                           | \-         | double     | Aux1 channel value as a value between -1 and 1.           |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| Aux2                           | \-         | double     | Aux2 channel value as a value between -1 and 1.           |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| Aux3                           | \-         | double     | Aux3 channel value as a value between -1 and 1.           |
+--------------------------------+------------+------------+-----------------------------------------------------------+

UWB
---

Distributed Settings:

+--------------------------------+------------+------------+-----------------------------------------------------------+
| Name                           | Unit       | Data type  | Description                                               |
+================================+============+============+===========================================================+
| NodeAddress                    | \-         | uint32     | Address of the UWB node directly connected to the device  |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| MeasurementInterval            | s          | double     | Interval between two range measurements with 1ms          |
|                                |            |            | resolution                                                |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| RemoteNodeAddresses            | \-         | uint32[4]  | Addresses of the anchor nodes                             |
+--------------------------------+------------+------------+-----------------------------------------------------------+

Published Channels:

+--------------------------------+------------+------------+-----------------------------------------------------------+
| Name                           | Unit       | Data type  | Description                                               |
+================================+============+============+===========================================================+
| Range1                         | m          | float      | Distance to anchor node 1                                 |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| Range2                         | m          | float      | Distance to anchor node 2                                 |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| Range3                         | m          | float      | Distance to anchor node 3                                 |
+--------------------------------+------------+------------+-----------------------------------------------------------+
| Range4                         | m          | float      | Distance to anchor node 4                                 |
+--------------------------------+------------+------------+-----------------------------------------------------------+
