***********
KiteOS Main
***********

KiteOS is the firmware running on all Ethernet generation boards. The main difference to the mfw generation is, that all
communication between boards and to the high level software is done through Ethernet. Additionally KiteOS is fully
written in C++.

The KiteOS firmware consists of only two targets, the "bootloader" and the "kiteOS" target. Both targets are flashed on
all Ethernet generation boards. All Ethernet generation boards are using the same firmware targets independent of the
actual Hardware available on the boards. The behavior of the firmware can be configured by setting configuration values
in the persistent memory. In this way software components can be enabled, disabled or configured to fit to the board
hardware.

The separation in Bootloader and firmware makes it possible to flash the boards via Ethernet. The in KiteOS core
contains an in application programmer service which implements this functionality on the firmware side. It can be
accessed from high level using the protocol documented under :ref:`iap_communication_protocol`. The KiteOS Ethernet
flash tool makes use of this protocol to implement a simple command line tool used to flash bootloader and kiteOS
binaries. The KS-Dashboard also uses this tool to implement an Ethernet flash procedure.

Memory Layout
#############

The KiteOS firmware consists of only two targets, the "bootloader" and the "kiteOS" target. Both targets are running on
the same Microcontroller. When powering up, the Microcontroller starts the bootloader firmware which then starts the
KiteOS firmware or executes the flash procedure of the kiteOS firmware depending on certain persistent memory values.
Bootloader and KiteOS firmware are stored in different memory regions of the flash memory. These regions are shown in
the figure below.

.. texfigure:: texfigures/flash_layout.tex
    :align: center

Beside heap and stack several hard an software components require dedicated memory regions in the RAM. One of them is
the Ethernet peripheral of the microcontroller which needs a dedicated space for Ethernet descriptors and buffers which
are used as an interface to the Ethernet DMA. Additionally the LwIP TCP/IP stack uses its own heap which also needs a
dedicated memory region. The memory mapping of the RAM is shown in the following figure.

.. texfigure:: texfigures/ram_layout.tex
    :align: center

Persistent Memory
#################

KiteOS uses a single target for all Ethernet generation boards. The hardware configuration is stored in the form of
configuration values in the persistent memory, an FRAM chip which is available on all Ethernet generation boards. The
persistent memory manager is a software component of the KiteOS core which can directly access this persistent memory.
High level software components can communicate with the persistent memory manager using the protocol documented under
:ref:`persistent_memory_communication_protocol`. The persistent memory values which are used to configure the Hardware
are defined and documented in the `persistent memory interface repository`_. This repository also offers tools to write
and read the persistent memory. The KS-Dashboard implements a GUI to access the persistent memory which internally uses
these tools.

.. _persistent memory interface repository: https://code.kiteswarms.com/kiteswarms/persistent-memory-interface

Firmware Design
###############

The hardware abstraction layer (HAL) implements classes to access the microcontroller peripheral hardware. Since the
software modules are dynamically instantiated depending of the persistent memory configuration it could potentially
occur that multiple modules are accessing the same peripheral. Therefore device registries are implemented in the HAL
that detect double usage of hardware ressources and ASSERT them. HAL modules are implemented interrupt based and
callbacks called by these modules are usually called in an interrupt context.

The Utils folder contains software modules that are rather generic and could potentially be useful for multiple other
modules, like ring buffers or CRC calculation libraries.

The KiteOS core contains all software modules that implement functionalities which are essential for the operating
system and needed on every board. The following core modules are contained:

:ethernet services:         Sets up and runs the TCP/IP stack and configures Ethernet switch hardware available on the
                            board.
:persistent memory:         Consists of FRAM access, FRAM emulator, persistent memory and persistent memory manager.
                            These classes are used to access the persistent memory from the firmware or via pulicast
                            directly from high level software like the KS dashboard persistent memory editor.
:plugin manager:            Contains the plugin base class, plugin definitions and plugin manager. The Plugin manager
                            instantiates Plugins based on the persistent memory content.
:system time:               Service providing and synchronizing the KiteOS system time. Supports synchronization via
                            PTP, PPS or direct synchronization with a GNSS device.
:board signals:             Service used for top/bottom board detection and Ethernet/PPS rerouting.
:core:                      KiteOS core main process containing the whole core initialization and mainloop.
:in application programmer: Service used for Ethernet flashing and jumping between bootloader and firmware.
:kiteos-version:            Provides the software version derived from git.
:logger:                    Class used to publish pulicast log messages over ethernet. Log messages may also be reported
                            in interrupts. They are internally buffered in a queue and send in the main loop context
                            later. This is why the logger can also be used within the HAL.
:name provider:             Service providing ASCII and binary board prefixes based on board information stored in the
                            persistent memory.
:pulicast node:             Holds the pulicast node used for pulicast communication.
:scheduler:                 The scheduler executes queued tasks in the main loop. Tasks can be enqueued from interrupts.
                            The scheduler can be used by drivers to trigger callbacks from an interrupt context that
                            need to be executed in the main loop.
:software timer:            The software timer class implements timers using the systick timer. Software timer callbacks
                            are executed in the main loop and can therefore easily be used in plugins or middleware
                            components.
:state node:                Class implementing state nodes accessible from high level via pulicast. These state nodes
                            are intended to be used by plugins.

Drivers for external chips can be found in the drivers folder. Drivers use the HAL modules to communicate with the chips
and provide a more abstract and functionality based interface for the higher software levels. Drivers are written in a
way that all callbacks they call are executed in the main loop. This can be achieved by using core components like
scheduler and software timers.

The middleware contains high level software that implements functionality on a high software level, uses drivers but
does not communicate over pulicast like plugins do. Software modules like failsafe that are shared by multiple plugins
fit here.

Plugins are the highest software level in KiteOS. All plugins are inherited from the Plugin base class. They implement
the main functionality of the boards (like reading out sensors and publishing the sensor data). Plugins are dynamically
instantiated by the Plugin scheduler core component based on the content of the persistent memory.

.. texfigure:: texfigures/firmware_layout.tex
    :align: center

Plugins
#######

.. include:: plugins.rst
