#-------------------------------------------------------------------------------
# Configure the compiler for the target machine
set (CMAKE_SYSTEM_NAME Generic)
set (CMAKE_SYSTEM_PROCESSOR ARM)
set (CMAKE_CROSSCOMPILING 1)
set (CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

# Define the compiler tools (arm eabi for embedded)
if (NOT DEFINED GNU_ARM_EMBEDDED_TOOLCHAIN_PATH)
  if(NOT DEFINED ENV{GNU_ARM_EMBEDDED_TOOLCHAIN_PATH})
    message(FATAL_ERROR "No GNU_ARM_EMBEDDED_TOOLCHAIN_PATH defined.\n"
                        "Please provide it either as:\n"
                        "1. Environment variable (recommended)\n"
                        "2. Command line argument:\n"
                        "cmake -DGNU_ARM_EMBEDDED_TOOLCHAIN_PATH=<path-to-toolchain> ..")
  endif()
  set(GNU_ARM_EMBEDDED_TOOLCHAIN_PATH "$ENV{GNU_ARM_EMBEDDED_TOOLCHAIN_PATH}")
endif()

set (CMAKE_C_COMPILER "${GNU_ARM_EMBEDDED_TOOLCHAIN_PATH}/bin/arm-none-eabi-gcc${EXECUTABLE_SUFFIX}")
set (CMAKE_CXX_COMPILER "${GNU_ARM_EMBEDDED_TOOLCHAIN_PATH}/bin/arm-none-eabi-g++${EXECUTABLE_SUFFIX}")
set (CMAKE_LINKER "${GNU_ARM_EMBEDDED_TOOLCHAIN_PATH}/bin/arm-none-eabi-ld${EXECUTABLE_SUFFIX}")
set (CMAKE_OBJCOPY "${GNU_ARM_EMBEDDED_TOOLCHAIN_PATH}/bin/arm-none-eabi-objcopy${EXECUTABLE_SUFFIX}"
     CACHE FILEPATH "The toolchain objcopy command" FORCE)
# Define general flags for compiler tools needed for the target device configuration
set (ARCH_FLAGS "-mcpu=cortex-m7 -mthumb -mfloat-abi=hard -mfpu=fpv5-d16")
