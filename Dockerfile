FROM $BASE_IMAGE

ARG GCC_ARM_RELEASE=gcc-arm-none-eabi-9-2020-q2-update
ARG GCC_ARM_RELEASE_FILE=${GCC_ARM_RELEASE}-x86_64-linux.tar.bz2
ADD https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2020q2/${GCC_ARM_RELEASE_FILE} ${GCC_ARM_RELEASE_FILE}


RUN tar -xjvf ${GCC_ARM_RELEASE_FILE}
RUN rm ${GCC_ARM_RELEASE_FILE}

ENV PATH="/${GCC_ARM_RELEASE}/bin/:${PATH}"
ENV GNU_ARM_EMBEDDED_TOOLCHAIN_PATH="/${GCC_ARM_RELEASE}"

# Print out installed version
RUN VERSION=$(arm-none-eabi-gcc -dumpversion); \
   echo "VERSION=$VERSION"

ADD http://ftp.br.debian.org/debian/pool/main/c/cpputest/libcpputest-dev_3.8-7_i386.deb libccputest-dev_i386.deb 
RUN dpkg --add-architecture i386 && dpkg -i libccputest-dev_i386.deb 

RUN apt-get -qq update \
    && apt-get install -y libusb-1.0-0-dev libtool \ 
    && rm -rf /var/lib/apt/lists/* 

RUN git clone --recurse-submodules --depth=1 git://git.code.sf.net/p/openocd/code openocd-code \
    && cd openocd-code \
    && ./bootstrap \
    && ./configure --enable-stlink --enable-ti-icdi  \
    && make -j$(expr $(nproc) - 1) \
    && make install \
    && cd .. && rm -rf openocd-code

