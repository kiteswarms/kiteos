cmake_minimum_required(VERSION 3.13)

# Define the compiler tools (arm eabi for embedded)
set (CMAKE_C_COMPILER gcc)
set (CMAKE_CXX_COMPILER g++)
set (OBJCOPY objcopy)

#-------------------------------------------------------------------------------
# Custom symbols that configure build properties
set (ARCH_FLAGS "")

set (X86_FLAGS "-fexceptions")
