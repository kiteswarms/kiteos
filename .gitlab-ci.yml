variables:
  GIT_SUBMODULE_STRATEGY: recursive

stages:
  - changelogtest
  - docker-build
  - configure
  - build
  - test
  - analysis
  - docs
  - upload


changelogtest_job:
  stage: changelogtest
  image: python:3.6
  tags:
    - docker
  before_script:
    - echo "Check that Changelog has been modified"
  script:
    - git fetch --all
    - if [ 0 -eq $(git diff --name-only origin/${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}...${CI_COMMIT_SHA} CHANGELOG.md | wc -l) ]; then exit 1; fi
  only:
    - merge_requests
  needs: []
  dependencies: []

Lint:
  image: >-
    registry.gitlab.com/kiteswarms/docker-embedded-build-env/ubuntu20.04/ks-embedded-build-env:latest
  stage: build
  tags:
    - docker
  before_script:
    - pip3 install cpplint
  script:
    - mkdir KiteOSMain/build
    - cd KiteOSMain/build
    - cmake ..
    - make lint
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_COMMIT_TAG
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_PIPELINE_SOURCE == "web"
  needs: []

build-test-gcc:
  image: >-
    registry.gitlab.com/kiteswarms/docker-embedded-build-env/ubuntu20.04/ks-embedded-build-env:latest
  stage: build
  tags:
    - docker
  before_script:
    - apt update -qq
    - apt install g++-multilib -y
  script:
    - mkdir KiteOSTest/build
    - cd KiteOSTest/build
    - cmake ..
    - make -j
  artifacts:
    paths:
      - docs/build/html/test_coverage
  coverage: '/lines[\.]+\: (\d+\.\d+)\%/'
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_COMMIT_TAG
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_PIPELINE_SOURCE == "web"
  needs: []

Build KiteOS:
  image: >-
    registry.gitlab.com/kiteswarms/docker-embedded-build-env/ubuntu20.04/ks-embedded-build-env:latest

  stage: build
  tags:
    - docker
  variables:
    BINARY_DIR: bin
  script:
    - mkdir KiteOSMain/build
    - cd KiteOSMain/build
    - cmake ..
    - make -j
    - cd ../..
    - mkdir $BINARY_DIR && mv KiteOSMain/build/*.out $BINARY_DIR && mv KiteOSMain/build/*.bin $BINARY_DIR
  artifacts:
    paths:
      - $BINARY_DIR
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_COMMIT_TAG
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_PIPELINE_SOURCE == "web"
  needs: []

Build KiteOS-x86:
  image: >-
    registry.gitlab.com/kiteswarms/docker-embedded-build-env/ubuntu20.04/ks-embedded-build-env:latest
  stage: build
  tags:
    - docker
  variables:
    PACKAGE_DIR: packing
  before_script:
    - apt update -qq
    - apt install rapidjson-dev libboost-program-options-dev -y
  script:
    - mkdir KiteOSX86/build
    - cd KiteOSX86/build
    - cmake ..
    - make -j
    - cpack
    - cd ../..
    - mkdir $PACKAGE_DIR && mv KiteOSX86/build/*.deb $PACKAGE_DIR
  artifacts:
    paths:
      - $PACKAGE_DIR
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_COMMIT_TAG
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_PIPELINE_SOURCE == "web"
  needs: []

Execute KiteOS-x86:
  image: >-
    registry.gitlab.com/kiteswarms/docker-embedded-build-env/ubuntu20.04/ks-embedded-build-env:latest
  stage: test
  tags:
    - docker
  variables:
    PACKAGE_DIR: packing

  script:
    - gdebi -n $PACKAGE_DIR/*.deb
    - kiteos& PID=$!; sleep 3; kill $PID
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
    - if: $CI_COMMIT_TAG
    - if: $CI_MERGE_REQUEST_ID
    - if: '$CI_PIPELINE_SOURCE == "web"'
  needs: ["Build KiteOS-x86"]

Documentation:
  image: >-
    registry.gitlab.com/kiteswarms/docker-ks-build-env/ubuntu20.04/ks-build-env:latest
  stage: docs
  tags:
    - docker
  before_script:
    - pip3 install -r docs-requirements.txt
  script:
    - mkdir KiteOSTest/build
    - cd KiteOSTest/build
    - cmake ..
    - make doc
  artifacts:
    paths:
      - docs/build/html
  coverage: '/lines[\.]+\: (\d+\.\d+)\%/'
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_COMMIT_TAG
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_PIPELINE_SOURCE == "web"
  needs: []

pages:
  image: >-
    registry.gitlab.com/kiteswarms/docker-ks-build-env/ubuntu20.04/ks-build-env:latest
  stage: upload
  tags:
    - docker
  script:
    - mkdir -p public
    - mv docs/build/html/* public
  artifacts:
    paths:
    - public
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
    - if: $CI_PIPELINE_SOURCE == "web"
  needs: [build-test-gcc, Documentation]

Upload to package registry:
  stage: upload
  tags:
    - local
  script:
    - export PACKAGE_VERSION=$(python3 -c 'from setuptools_scm import get_version; print(".".join(get_version().split(".")[:3]), end="")')
    - 'curl --http1.1 --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file KiteOSMain/build/KiteOS/kiteOS_release.bin ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/KiteOS/$PACKAGE_VERSION/kiteOS-$CI_COMMIT_TAG.bin'
    - 'curl --http1.1 --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file KiteOSMain/build/KiteOS/bootloader_release.bin ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/KiteOS/$PACKAGE_VERSION/bootloader-$CI_COMMIT_TAG.bin'
    - 'curl --http1.1 --request POST --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" --data name="kiteOS-$CI_COMMIT_TAG" --data url="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/KiteOS/$PACKAGE_VERSION/kiteOS-$CI_COMMIT_TAG.bin" --data link_type="image" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases/$CI_COMMIT_TAG/assets/links'
    - 'curl --http1.1 --request POST --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" --data name="bootloader-$CI_COMMIT_TAG" --data url="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/KiteOS/$PACKAGE_VERSION/bootloader-$CI_COMMIT_TAG.bin" --data link_type="image" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases/$CI_COMMIT_TAG/assets/links'

  rules:
    - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+$/ && $CI_COMMIT_REF_PROTECTED == "true"'
  needs: ["Build KiteOS"]
